/*
SQLyog Professional v12.5.1 (64 bit)
MySQL - 10.1.38-MariaDB : Database - daeyoung
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `master_jenis_asset` */

DROP TABLE IF EXISTS `master_jenis_asset`;

CREATE TABLE `master_jenis_asset` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT '',
  `jenis` varchar(60) DEFAULT '',
  `status` int(1) DEFAULT '1',
  `insert_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_insert` varchar(60) DEFAULT '',
  `update_at` datetime DEFAULT NULL,
  `user_update` varchar(60) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `master_jenis_asset` */

insert  into `master_jenis_asset`(`id`,`kode`,`jenis`,`status`,`insert_at`,`user_insert`,`update_at`,`user_update`) values 
(1,'001','Alat Tulis Kantor',1,'2020-07-18 07:30:07','bayu',NULL,''),
(2,'002','Kendaraan',1,'2020-07-18 07:30:50','bayu','2020-07-20 20:51:58','bayu'),
(3,'003','Elektronik',1,'2020-07-18 07:32:09','bayu','2020-07-20 20:51:41','bayu'),
(4,'TES','TES123890',0,'2020-07-20 20:08:50','bayu','2020-07-20 20:47:49','bayu');

/*Table structure for table `master_merk_asset` */

DROP TABLE IF EXISTS `master_merk_asset`;

CREATE TABLE `master_merk_asset` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `merk` varchar(60) DEFAULT '',
  `status` int(1) DEFAULT '1',
  `user_insert` varchar(60) DEFAULT '',
  `insert_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_update` varchar(60) DEFAULT '',
  `update_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

/*Data for the table `master_merk_asset` */

insert  into `master_merk_asset`(`id`,`merk`,`status`,`user_insert`,`insert_at`,`user_update`,`update_at`) values 
(1,'Broco',0,'admin','2020-04-05 11:49:02','admin','2020-04-05 06:49:02'),
(2,'Philip',0,'admin','2020-04-05 11:49:08','admin','2020-04-05 06:49:08'),
(3,'Epson',1,'admin','2020-04-02 09:58:21','',NULL),
(4,'Brother',1,'admin','2020-04-02 09:58:25','',NULL),
(5,'-',1,'admin','2020-04-04 14:08:05','',NULL),
(6,'Elite',1,'admin','2020-04-05 06:49:26','',NULL),
(7,'Acroe',1,'admin','2020-04-05 06:49:33','',NULL),
(8,'Chairman',1,'admin','2020-04-05 06:49:42','',NULL),
(9,'Chitose',1,'admin','2020-04-05 06:49:49','',NULL),
(10,'Orbitrend',1,'admin','2020-04-05 06:49:58','',NULL),
(11,'Polo Classic',1,'admin','2020-04-05 06:50:10','',NULL),
(12,'Alma',1,'admin','2020-04-05 06:50:17','',NULL),
(13,'Skype',1,'admin','2020-04-05 06:50:23','',NULL),
(14,'ODI',1,'admin','2020-04-05 06:50:30','',NULL),
(15,'VALSB',1,'admin','2020-04-05 06:50:37','',NULL),
(16,'MESTRO',1,'admin','2020-04-05 06:50:45','',NULL),
(17,'SHELF VEIN',1,'admin','2020-04-05 06:50:55','',NULL),
(18,'Krisbow',1,'admin','2020-04-05 06:51:03','',NULL),
(19,'Samsung',1,'admin','2020-04-05 06:51:23','',NULL),
(20,'HP',1,'admin','2020-04-05 06:51:30','',NULL),
(21,'Toshiba',1,'admin','2020-04-05 06:51:41','',NULL),
(22,'Vaio',1,'admin','2020-04-05 06:51:49','',NULL),
(23,'Fujitsu',1,'admin','2020-04-05 06:51:56','',NULL),
(24,'Toyota',1,'admin','2020-04-05 06:52:16','',NULL),
(25,'KIA',1,'admin','2020-04-05 06:52:22','',NULL),
(26,'Honda',1,'admin','2020-04-05 06:52:27','',NULL),
(27,'Asus',1,'admin','2020-04-05 07:16:14','',NULL);

/*Table structure for table `modul` */

DROP TABLE IF EXISTS `modul`;

CREATE TABLE `modul` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `up` varchar(50) NOT NULL DEFAULT '0',
  `categories` varchar(500) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `code` varchar(500) DEFAULT NULL,
  `url` text,
  `menu_order` int(11) DEFAULT '0',
  `note` varchar(500) DEFAULT NULL,
  `icon` text,
  `menu` varchar(1) DEFAULT NULL,
  `flag_menu` enum('yes','no') DEFAULT 'no',
  `su_only` varchar(1) DEFAULT NULL,
  `modul_view` varchar(1) DEFAULT NULL,
  `identifier` int(5) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

/*Data for the table `modul` */

insert  into `modul`(`id`,`up`,`categories`,`name`,`code`,`url`,`menu_order`,`note`,`icon`,`menu`,`flag_menu`,`su_only`,`modul_view`,`identifier`) values 
(1,'0','exim_cukai','Bea Cukai','exim_cukai','laporan_beacukai',8,NULL,'m_djbc.png',NULL,'yes','0',NULL,0),
(2,'','exim_bc23','Data BC 2.3','exim_bc23','exim_bc23',1,NULL,NULL,NULL,'yes','0',NULL,0),
(3,'','exim_bc27','Data BC 2.7','exim_bc27','exim_bc27',3,NULL,NULL,NULL,'yes','0',NULL,0),
(4,'','exim_upload_consumtion','Upload Consumtion','exim_upload_consumtion','exim_upload_consumtion',4,NULL,NULL,NULL,'yes','0',NULL,0),
(5,'','exim_bc40','Data BC 4.0','exim_bc40','exim_bc40',2,NULL,NULL,NULL,'yes','0',NULL,0),
(6,'0','customers','Pelanggan','customers','#',1,NULL,'m_pelanggan.png',NULL,'yes','0',NULL,0),
(7,'customers','customers_so','Sales Order','customers_so','customers_so',3,NULL,NULL,NULL,'yes','0',NULL,0),
(8,'customers','customers_do','Delivery Order','customers_do','customers_do',4,NULL,NULL,NULL,'yes','0',NULL,0),
(9,'0','inventory','Inventori','inventory','#',3,NULL,'m_inventori.png',NULL,'yes','0',NULL,0),
(10,'inventory','inventory_barang','Master Barang','inventory_barang','inventory_barang',1,NULL,NULL,NULL,'yes','0',NULL,0),
(11,'0','warehouse','Produksi','warehouse','#',4,NULL,'m_produksi.png',NULL,'yes','0',NULL,0),
(12,'warehouse','warehouse_rak_system','Rak System','warehouse_rak_system','warehouse_rak_system',2,NULL,NULL,NULL,'yes','0',NULL,0),
(13,'warehouse','warehouse_dyeing_system','Inventory Dyeing','warehouse_dyeing_system','warehouse_dyeing_system',1,NULL,NULL,NULL,'yes','0',NULL,0),
(14,'','laporan_beacukai','Laporan Bea Cukai','laporan_beacukai','laporan_beacukai',5,NULL,NULL,NULL,'yes','0',NULL,0),
(15,'0','suppliers','Pemasok','suppliers','#',2,NULL,'m_pemasok.png',NULL,'yes','0',NULL,0),
(16,'suppliers','suppliers_master','Master Suppliers','suppliers_master','suppliers_master',1,NULL,NULL,NULL,'yes','0',NULL,0),
(17,'suppliers','suppliers_po','Order Pembelian','suppliers_po','suppliers_po',2,NULL,NULL,NULL,'yes','0',NULL,0),
(18,'suppliers','suppliers_terima_barang','Penerimaan Barang','suppliers_terima_barang','suppliers_terima_barang',3,NULL,NULL,NULL,'yes','0',NULL,0),
(19,'suppliers','suppliers_tagihan','Tagihan Suppliers','suppliers_tagihan','suppliers_tagihan',4,NULL,NULL,NULL,'yes','0',NULL,0),
(20,'suppliers','suppliers_bayar','Pembayaran Tagihan','suppliers_bayar','suppliers_bayar',5,NULL,NULL,NULL,'yes','0',NULL,0),
(21,'suppliers','suppliers_po_langsung','Pembelian Langsung','suppliers_po_langsung','suppliers_po_langsung',6,NULL,NULL,NULL,'yes','0',NULL,0),
(22,'suppliers','suppliers_terima_langsung','Penerimaan Langsung','suppliers_terima_langsung','suppliers_terima_langsung',7,NULL,NULL,NULL,'yes','0',NULL,0),
(23,'suppliers','suppliers_retur','Retur Barang','suppliers_retur','suppliers_retur',8,NULL,NULL,NULL,'yes','0',NULL,0),
(24,'suppliers','suppliers_laporan','Laporan','suppliers_laporan','suppliers_laporan',9,NULL,NULL,NULL,'yes','0',NULL,0),
(25,'customers','customers_master','Master Customers','customers_master','customers_master',1,NULL,NULL,NULL,'yes','0',NULL,0),
(26,'inventory','inventory_so','Stok Opname','inventory_so','inventory_so',2,NULL,NULL,NULL,'yes','0',NULL,0),
(27,'0','dashboard',NULL,'dashboard','dashboard',0,NULL,'m_h.png',NULL,'yes','0',NULL,0),
(28,'0','equipment','Mesin Dan Peralatan','equipment','#',5,NULL,'aset.png',NULL,'yes','0',NULL,0),
(29,'0','accounting','Akunting','accounting','#',6,NULL,'m_akunting.png',NULL,'yes','0',NULL,0),
(30,'0','insw','INSW','insw','insw',7,NULL,'m_insw.png',NULL,'yes','0',NULL,0),
(31,'customers','customers_branch','Master Branch','customers_branch','customers_branch',2,NULL,NULL,NULL,'yes','0',NULL,0),
(32,'warehouse','warehouse_packing_list','Packing List','warehouse_packing_list','warehouse_packing_list',3,NULL,NULL,NULL,'yes','0',NULL,0),
(33,'equipment','jenis_asset','Jenis Asset','jenis_asset','jenis_asset',1,NULL,NULL,NULL,'yes','0',NULL,0),
(34,'equipment','control_asset','Control Asset','control_asset','control_asset',2,NULL,NULL,NULL,'yes','0',NULL,0),
(35,'equipment','penyusutan','Penyusutan','penyusutan','penyusutan',3,NULL,NULL,NULL,'yes','0',NULL,0),
(36,'equipment','report_penyusutan','Report Penyusutan','report_penyusutan','report_penyusutan',4,NULL,NULL,NULL,'yes','0',NULL,0);

/*Table structure for table `privileges` */

DROP TABLE IF EXISTS `privileges`;

CREATE TABLE `privileges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_group_id` varchar(500) NOT NULL,
  `modul` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

/*Data for the table `privileges` */

insert  into `privileges`(`id`,`user_group_id`,`modul`) values 
(1,'manager_exim','exim_cukai'),
(2,'manager_exim','exim_bc23'),
(3,'manager_exim','exim_bc27'),
(4,'manager_exim','exim_upload_consumtion'),
(5,'manager_exim','exim_bc40'),
(6,'superuser','exim_cukai'),
(7,'superuser','exim_bc23'),
(8,'superuser','exim_bc27'),
(9,'superuser','exim_upload_consumtion'),
(10,'superuser','exim_bc40'),
(11,'superuser','customers'),
(12,'superuser','customers_so'),
(13,'superuser','customers_do'),
(14,'superuser','inventory'),
(15,'superuser','inventory_barang'),
(16,'superuser','warehouse_rak_system'),
(17,'superuser','warehouse_dyeing_system'),
(18,'superuser','warehouse'),
(19,'superuser','laporan_beacukai'),
(20,'superuser','suppliers'),
(21,'superuser','suppliers_master'),
(22,'superuser','suppliers_po'),
(23,'superuser','suppliers_terima_barang'),
(24,'superuser','suppliers_tagihan'),
(25,'superuser','suppliers_bayar'),
(26,'superuser','suppliers_po_langsung'),
(27,'superuser','suppliers_terima_langsung'),
(28,'superuser','suppliers_retur'),
(29,'superuser','lap_mutasi_barang_pemasukan'),
(30,'superuser','dashboard'),
(31,'superuser','customers_master'),
(32,'superuser','inventory_so'),
(33,'so','inventory'),
(34,'so','inventory_so'),
(35,'so','dashboard'),
(36,'superuser','insw'),
(37,'superuser','equipment'),
(38,'superuser','accounting'),
(39,'superuser','customers_branch'),
(40,'superuser','warehouse_packing_list'),
(41,'superuser','jenis_asset'),
(42,'superuser','control_asset'),
(43,'superuser','penyusutan'),
(44,'superuser','report_penyusutan');

/*Table structure for table `tb_asset` */

DROP TABLE IF EXISTS `tb_asset`;

CREATE TABLE `tb_asset` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `kode_asset` varchar(20) DEFAULT '',
  `kode` varchar(20) DEFAULT '',
  `nama` varchar(250) DEFAULT '',
  `merk` int(20) DEFAULT '0',
  `jenis` int(20) DEFAULT '0',
  `keterangan` varchar(250) DEFAULT '',
  `unit` double DEFAULT '0',
  `tgl_beli` date DEFAULT NULL,
  `harga_beli` double DEFAULT '0',
  `mulai_penyusutan` date DEFAULT NULL,
  `selesai_penyusutan` date DEFAULT NULL,
  `durasi` int(11) DEFAULT '0',
  `tarif` double DEFAULT '0',
  `insert_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_insert` varchar(60) DEFAULT '',
  `update_at` datetime DEFAULT NULL,
  `user_update` varchar(60) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `tb_asset` */

insert  into `tb_asset`(`id`,`kode_asset`,`kode`,`nama`,`merk`,`jenis`,`keterangan`,`unit`,`tgl_beli`,`harga_beli`,`mulai_penyusutan`,`selesai_penyusutan`,`durasi`,`tarif`,`insert_at`,`user_insert`,`update_at`,`user_update`) values 
(11,'ATM001','','Avanza Veloz',24,1,'1.5 AT',1,'2017-11-01',202263637,'2017-11-01','2021-11-01',48,12.5,'2020-04-05 11:57:34','admin','2020-04-05 12:25:03','admin'),
(12,'ATM002','','Picanto',25,1,'MT',1,'2018-05-01',75000000,'2018-05-01','2022-05-01',48,12.5,'2020-04-05 11:58:36','admin','2020-04-05 12:25:17','admin'),
(13,'ATM003','','Vario',26,1,'125',1,'2019-05-01',19182182,'2019-05-01','2023-05-01',48,12.5,'2020-04-05 12:07:30','admin','2020-04-05 12:24:48','admin'),
(14,'ATF001','','Failling Kabinet Besi (Big Cabinet Sliding Door 104-200)',4,3,'4 Pintu',1,'2013-01-01',2800000,'2013-01-01','2017-01-01',48,25,'2020-04-05 12:11:47','admin','2020-04-05 12:25:37','admin'),
(15,'ATF002','','Meja Kantor',10,3,'',1,'2016-03-01',777000,'2020-04-05','2020-03-01',48,25,'2020-04-05 12:13:22','admin','2020-04-05 12:25:55','admin'),
(16,'ATF003','','Meja Komputer',10,3,'',1,'2016-03-01',777000,'2016-03-01','2020-03-01',48,25,'2020-04-05 12:14:14','admin','2020-04-05 12:26:29','admin'),
(17,'ATE001','','Laptop (Sekretaris - Retno)',27,2,'A Core i3',1,'2017-01-01',5200000,'2017-01-01','2021-01-01',48,25,'2020-04-05 12:17:35','admin','2020-04-05 12:26:43','admin'),
(20,'001004','','Pensil 2B',4,1,'Pensil 2B Mahal',12,'2020-07-01',120000,'2020-07-01','2021-05-01',10,15,'2020-07-21 19:44:27','bayu','2020-07-21 20:08:16','bayu');

/*Table structure for table `tb_control_asset` */

DROP TABLE IF EXISTS `tb_control_asset`;

CREATE TABLE `tb_control_asset` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `id_asset` int(20) DEFAULT '0',
  `tgl` date DEFAULT NULL,
  `kode` varchar(20) DEFAULT '',
  `nama` varchar(60) DEFAULT '',
  `jumlah` double DEFAULT '0',
  `status` varchar(60) DEFAULT '',
  `keterangan` varchar(250) DEFAULT '',
  `user_insert` varchar(60) DEFAULT '',
  `insert_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `tb_control_asset` */

insert  into `tb_control_asset`(`id`,`id_asset`,`tgl`,`kode`,`nama`,`jumlah`,`status`,`keterangan`,`user_insert`,`insert_at`) values 
(23,11,'2017-11-01','ATM001','Avanza Veloz',1,'Baru','','admin','2020-04-05 11:57:36'),
(24,12,'2018-05-01','ATM002','Picanto',1,'Baru','','admin','2020-04-05 11:58:38'),
(25,13,'2019-05-01','ATM003','Vario',1,'Baru','','admin','2020-04-05 12:07:32'),
(26,14,'2013-01-01','ATF001','Failling Kabinet Besi (Big Cabinet Sliding Door 104-200)',1,'Baru','','admin','2020-04-05 12:11:47'),
(27,15,'2016-03-01','ATF002','Meja Kantor',1,'Baru','','admin','2020-04-05 12:13:23'),
(28,16,'2016-03-01','ATF003','Meja Komputer',1,'Baru','','admin','2020-04-05 12:14:16'),
(29,17,'2017-01-01','ATE001','Laptop (Sekretaris - Retno)',1,'Baru','','admin','2020-04-05 12:17:37'),
(32,20,'2020-07-01','001004','Pensil 2B',10,'Baru','','bayu','2020-07-21 19:44:29'),
(33,20,'0000-00-00','001004','Pensil 2B',-1,'Dijual','Laku dijual','bayu','2020-07-21 19:44:54'),
(34,20,'2020-07-21','001004','Pensil 2B',-1,'Rusak','Rusak ','bayu','2020-07-21 19:45:49');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
