<?php


class Model_finance_ap_invoice extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function get_data_table()
	{
		$column_order = array(null, 'a.nomor', 'a.insert_at', 'a.tanggal', 'a.date_due', 'nama_sup', 'a.no_referensi', 'a.jumlah', null);
		$order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.nomor';
		$order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

		if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
		// $this->db->order_by($order_name, $order_dir);
		if (!empty($order_name) && !empty($order_dir)) {
			$order = "ORDER BY $order_name $order_dir";
		} else {
			$order = "ORDER BY a.nomor ASC";
		}
		$q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.*,
		DATE_FORMAT(a.tanggal, '%d-%m-%Y') AS tanggalnya,
		DATE_FORMAT(a.date_due, '%d-%m-%Y') AS date_duenya,
		IF(a.supplier = 0, 'LAIN2', b.nama) AS nama_sup
	  FROM
		erp_financev2.`gmd_finance_ap_invoice` a
		LEFT JOIN  inventory_v2.`ms_perusahaan` b
		  ON `a`.`supplier` = `b`.`id_perusahaan`
	  WHERE (
		`a`.`no_referensi` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`nomor` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
			OR `b`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		)
		AND (
			a.`insert_at` BETWEEN '" . $this->input->post('searchDateFirst') . "'
            AND '" . $this->input->post('searchDateFinish') . "'
		)
		AND `a`.`branch` = '8'
	  $order $limit");
		// $q = $this->db->get();
		$qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
		$n = $qn->row()->ttl;
		$data = array();
		$no = $_POST['start'];
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $r) {
				$no++;
				$opsi = '<a href="#" onClick="update_data(\'' . $r['id'] . '\')"><i class="icon-menu7 position-left text-slate-800"></i></a> <a href="#" onClick="delete_data(\'' . $r['id'] . '\')"><i class="icon-bin position-left text-grey"></i></a>';
				$row  = array(
					$no . '.',
					$r['nomor'],
					date("d-m-Y", strtotime($r['insert_at'])),
					$r['tanggalnya'],
					$r['date_duenya'],

					//$r['id_refnya'],
					//$r['transaksinya'],
					$r['nama_sup'],
					$r['no_referensi'],
					number_format($r['jumlah'], 0),
					$opsi,
				);


				$data[] = $row;
			}
		}
		$q->free_result();

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $n,
			"recordsFiltered" => $n,
			"data" => $data,
		);
		echo json_encode($output);
	}

	function create_queue_id($nomor = null)
	{
		$y = substr(date('Y'), 2);
		$q = $this->db->query('select * from erp_financev2.`gmd_finance_ap_invoice` where SUBSTRING(nomor, -2) =' . $y)->num_rows();
		if (!empty($nomor)) {
			$nomor = $nomor + 1;
		} else {
			$nomor = $q + 1;
		}
		$invoice = $nomor . '/INV/' . mdate("%m", time()) . '/' . mdate("%y", time());
		$query = $this->db->query("SELECT * FROM erp_financev2.`gmd_finance_ap_invoice` a WHERE a.`nomor`='$invoice'")->result();
		if (!empty($query)) {
			$invoice = $this->create_queue_id($nomor);
		}
		return $invoice;
	}

	function insert()
	{
		$this->db->trans_start();
		$create_queue_id = $this->create_queue_id(null);
		$no_referensi = str_replace(" ", "", $this->input->post('inv_supplier'));
		$potongan = str_replace(",", "", $this->input->post('potongan'));
		$materai = str_replace(",", "", $this->input->post('materai'));
		$ppn = str_replace(",", "", $this->input->post('ppn'));
		$pph = str_replace(",", "", $this->input->post('pph'));
		$jumlah = str_replace(",", "", $this->input->post('jumlah'));
		$kd_pajak = $this->input->post('kd_pajak');
		$now = date('Y-m-d H:i:s');
		$ongkir = $diskon = 0;
		$data = array(
			'nomor' => $create_queue_id,
			'tanggal' => $this->input->post('tanggal'),
			'date_due' => $this->input->post('date_due'),
			'supplier' => $this->input->post('supplier'),
			'potongan' => $potongan,
			'materai' => $materai,
			'ppn' => $ppn,
			'pph' => $pph,
			'pajak' => $kd_pajak,
			'jumlah' => $jumlah,
			'no_referensi' => $no_referensi,
			'branch' => $this->cek_id_regional($this->session->userdata('scope_area')),
			'regional' => $this->cek_id_regional($this->session->userdata('scope_regional')),
			'insert_at' => $now,
			'insert_by' => $this->session->userdata('user_id')
		);
		$result = $this->db->insert('finance_ap_invoice', $data);
		$id = $this->db->insert_id();
		$id_barang = $jumlah_barang = $satuan_barang = $jumlah_harga = null;
		if ($result == true) {
			if (isset($_POST['id_barang'])) {
				foreach ($_POST['id_barang'] as $k => $v) {
					if (!empty($_POST['id_barang'][$k])) {
						if ($_POST['id_barang'][$k] == 'x') {
							$ongkir = $ongkir + $_POST['jumlah_harga'][$k];
							$id_barang = $_POST['id_barang'][$k];
							$jumlah_barang = $_POST['jumlah_barang'][$k];
							$satuan_barang = $_POST['satuan_barang'][$k];
							$harga_barang = str_replace(",", "", $_POST['harga_barang'][$k]);
							$jumlah_harga = $_POST['jumlah_harga'][$k];
							$id_penerimaan = $_POST['id_penerimaan'][$k];
							$id_pembelian = $_POST['id_pembelian'][$k];
							$data = array(
								'id_ap' => $id,
								'id_barang' => $id_barang,
								'jumlah' => $jumlah_barang,
								'satuan' => $satuan_barang,
								'jumlah_harga' => $jumlah_harga,
								'id_pembelian' => $id_pembelian,
								'id_penerimaan' => $id_penerimaan
							);
							$this->db->insert('finance_ap_invoice_detail', $data);
						} else if ($_POST['id_barang'][$k] == 'xx') {
							$diskon = $diskon + $_POST['jumlah_harga'][$k];
							$id_barang = $_POST['id_barang'][$k];
							$jumlah_barang = $_POST['jumlah_barang'][$k];
							$satuan_barang = $_POST['satuan_barang'][$k];
							$harga_barang = str_replace(",", "", $_POST['harga_barang'][$k]);
							$jumlah_harga = $_POST['jumlah_harga'][$k];
							$id_penerimaan = $_POST['id_penerimaan'][$k];
							$id_pembelian = $_POST['id_pembelian'][$k];
							$data = array(
								'id_ap' => $id,
								'id_barang' => $id_barang,
								'jumlah' => $jumlah_barang,
								'satuan' => $satuan_barang,
								'jumlah_harga' => $jumlah_harga,
								'id_pembelian' => $id_pembelian,
								'id_penerimaan' => $id_penerimaan
							);
							$this->db->insert('finance_ap_invoice_detail', $data);
						} else {
							$id_barang = $_POST['id_barang'][$k];
							$jumlah_barang = $_POST['jumlah_barang'][$k];
							$satuan_barang = $_POST['satuan_barang'][$k];
							$harga_barang = str_replace(",", "", $_POST['harga_barang'][$k]);
							$jumlah_harga = $_POST['jumlah_harga'][$k];
							$id_penerimaan = $_POST['id_penerimaan'][$k];
							$id_pembelian = $_POST['id_pembelian'][$k];
							$data = array(
								'id_ap' => $id,
								'id_barang' => $id_barang,
								'jumlah' => $jumlah_barang,
								'satuan' => $satuan_barang,
								'jumlah_harga' => $jumlah_harga,
								'id_pembelian' => $id_pembelian,
								'id_penerimaan' => $id_penerimaan
							);
							$this->db->insert('finance_ap_invoice_detail', $data);
						}
					}
				}
				$data = array('ongkir' => $ongkir, 'diskon' => $diskon);
				$this->db->where('id', $id);
				$this->db->update('finance_ap_invoice', $data);
			}
			$msg = 1;
		} else {
			$msg = 0;
		}
		$this->db->trans_complete();
		return $msg;
	}

	function update()
	{
		$this->db->trans_start();
		$id = $this->input->post('id');
		$no_referensi = str_replace(" ", "", $this->input->post('inv_supplier'));
		$potongan = str_replace(",", "", $this->input->post('potongan'));
		$materai = str_replace(",", "", $this->input->post('materai'));
		$ppn = str_replace(",", "", $this->input->post('ppn'));
		$pph = str_replace(",", "", $this->input->post('pph'));
		$jumlah = str_replace(",", "", $this->input->post('jumlah'));
		$kd_pajak = $this->input->post('kd_pajak');
		$now = date('Y-m-d H:i:s');
		$ongkir = $diskon = 0;
		$data = array(
			'tanggal' => $this->input->post('tanggal'),
			'date_due' => $this->input->post('date_due'),
			'supplier' => $this->input->post('supplier'),
			'potongan' => $potongan,
			'materai' => $materai,
			'ppn' => $ppn,
			'pph' => $pph,
			'pajak' => $kd_pajak,
			'jumlah' => $jumlah,
			'no_referensi' => $no_referensi,
			'branch' => $this->cek_id_regional($this->session->userdata('scope_area')),
			'regional' => $this->cek_id_regional($this->session->userdata('scope_regional')),
			'update_at' => $now,
			'update_by' => $this->session->userdata('user_id')
		);
		$this->db->where('id', $id);
		$this->db->update('finance_ap_invoice', $data);
		$data = array('status' => 9);
		$this->db->where('id_ap', $id);
		$this->db->update('finance_ap_invoice_detail', $data);
		$id_barang = $jumlah_barang = $satuan_barang = $jumlah_harga = null;
		if (isset($_POST['id_barang'])) {
			foreach ($_POST['id_barang'] as $k => $v) {
				if (!empty($_POST['id_barang'][$k])) {
					if ($_POST['id_barang'][$k] == 'x') {
						$ongkir = $ongkir + $_POST['jumlah_harga'][$k];
						$id_barang = $_POST['id_barang'][$k];
						$jumlah_barang = $_POST['jumlah_barang'][$k];
						$satuan_barang = $_POST['satuan_barang'][$k];
						$harga_barang = str_replace(",", "", $_POST['harga_barang'][$k]);
						$jumlah_harga = $_POST['jumlah_harga'][$k];
						$id_penerimaan = $_POST['id_penerimaan'][$k];
						$id_pembelian = $_POST['id_pembelian'][$k];
						$data = array(
							'id_ap' => $id,
							'id_barang' => $id_barang,
							'jumlah' => $jumlah_barang,
							'satuan' => $satuan_barang,
							'jumlah_harga' => $jumlah_harga,
							'id_pembelian' => $id_pembelian,
							'id_penerimaan' => $id_penerimaan
						);
						$this->db->insert('finance_ap_invoice_detail', $data);
					} else if ($_POST['id_barang'][$k] == 'xx') {
						$diskon = $diskon + $_POST['jumlah_harga'][$k];
						$id_barang = $_POST['id_barang'][$k];
						$jumlah_barang = $_POST['jumlah_barang'][$k];
						$satuan_barang = $_POST['satuan_barang'][$k];
						$harga_barang = str_replace(",", "", $_POST['harga_barang'][$k]);
						$jumlah_harga = $_POST['jumlah_harga'][$k];
						$id_penerimaan = $_POST['id_penerimaan'][$k];
						$id_pembelian = $_POST['id_pembelian'][$k];
						$data = array(
							'id_ap' => $id,
							'id_barang' => $id_barang,
							'jumlah' => $jumlah_barang,
							'satuan' => $satuan_barang,
							'jumlah_harga' => $jumlah_harga,
							'id_pembelian' => $id_pembelian,
							'id_penerimaan' => $id_penerimaan
						);
						$this->db->insert('finance_ap_invoice_detail', $data);
					} else {
						$id_barang = $_POST['id_barang'][$k];
						$jumlah_barang = $_POST['jumlah_barang'][$k];
						$satuan_barang = $_POST['satuan_barang'][$k];
						$harga_barang = str_replace(",", "", $_POST['harga_barang'][$k]);
						$jumlah_harga = $_POST['jumlah_harga'][$k];
						$id_penerimaan = $_POST['id_penerimaan'][$k];
						$id_pembelian = $_POST['id_pembelian'][$k];
						$data = array(
							'id_ap' => $id,
							'id_barang' => $id_barang,
							'jumlah' => $jumlah_barang,
							'satuan' => $satuan_barang,
							'jumlah_harga' => $jumlah_harga,
							'id_pembelian' => $id_pembelian,
							'id_penerimaan' => $id_penerimaan
						);
						$this->db->insert('finance_ap_invoice_detail', $data);
					}
				}
			}
			$data = array('ongkir' => $ongkir, 'diskon' => $diskon);
			$this->db->where('id', $id);
			$this->db->update('finance_ap_invoice', $data);
		}
		$msg = 1;
		$this->db->trans_complete();
		return $msg;
	}

	function select()
	{
		$id = $this->input->post('id');
		$no = null;

		$q = $this->db->query("SELECT
		a.id AS id,a.`nomor`,a.`tanggal`,a.`date_due`,a.`supplier`,d.`nama`,a.`no_referensi`,
		a.`potongan`,a.`materai`,a.`ppn`,a.`pph`,a.`jumlah`,a.`lain2`,a.`pajak`,
		b.`id_penerimaan`,b.`id_pembelian`,b.`id_barang`,c.`nama_barang`,b.`jumlah` AS jml_barang,b.`satuan`,b.`jumlah_harga`
	  FROM
		erp_financev2.`gmd_finance_ap_invoice` a
		INNER JOIN erp_financev2.`gmd_finance_ap_invoice_detail` b
		  ON a.`id` = b.`id_ap`
		INNER JOIN inventory_v2.`ms_header_barang` c ON c.`id_header`=b.`id_barang`
		INNER JOIN inventory_v2.`ms_perusahaan` d ON a.`supplier`=d.`id_perusahaan`
		WHERE a.`status`=1 AND a.`lunas` =0 AND a.`id` = $id");

		$qr = $this->db->query("SELECT a.*,b.*,c.`nomor` AS nomor_po FROM erp_financev2.`gmd_finance_ap_invoice` a
		JOIN erp_financev2.`gmd_finance_ap_invoice_detail` b
		  ON a.`id` = b.`id_ap`
		  LEFT JOIN `inventory_v2`.`tr_h_pembelian` c
		  ON c.`id_header`=b.`id_pembelian`
		  LEFT JOIN  `inventory_v2`.`tr_h_penerimaan` d
		  ON d.`id_header`=b.`id_penerimaan` WHERE b.`status`=1 AND a.`status`=1 AND a.`id`=$id");

		if (!empty($qr)) {
			$id_pembelian1 = 0;
			$id_penerimaan1 = 0;
			foreach ($qr as $row) {
				if ($id_pembelian1 != $row->id_pembelian && $id_penerimaan1 != $row->id_penerimaan) {
					if ($no != null) {
						$qq = $this->db->query("SELECT *,SUM(hitung) as total FROM (SELECT b.`id_pembelian`,b.`id_penerimaan`,1 AS hitung FROM erp_financev2.`gmd_finance_ap_invoice` a
					JOIN erp_financev2.`gmd_finance_ap_invoice_detail` b
					ON a.`id` = b.`id_ap`
					LEFT JOIN `inventory_v2`.`tr_h_pembelian` c
					ON c.`id_header`=b.`id_pembelian`
					LEFT JOIN  `inventory_v2`.`tr_h_penerimaan` d
					ON d.`id_header`=b.`id_penerimaan` WHERE b.`status`=1 AND a.`status`=1 AND a.`id`=$id AND b.`id_penerimaan`=$row->id_penerimaan AND b.`id_pembelian`=$row->id_pembelian) z
					GROUP BY z.`id_pembelian`,z.`id_penerimaan`")->row();
						$data .= "<td style='vertical-align:middle;text-align:center;' rowspan='$qq->total'><button type='button' class='button' onclick='del_detail(" . $row->id_penerimaan . "," . $row->id_pembelian . ")'> X </button></td>";
						$data .= "</tbody></table>";
					}
					$data .= '
				<table class="' . $row->id_penerimaan . '_' . $row->id_pembelian . '" border="1" cellpadding="0" cellspacing="0" width="100%" style="margin-top:5px;">
					<thead>
						<tr>';
					$no = 1;
				}
				if ($no == 1) {
					$data .= "<th valign='top' colspan='3' style='text-align:center'><strong>$row->nomor_po</strong></th>
					<th style='text-align:center' valign='top'><strong>Jumlah</strong></th>
					<th style='text-align:center' valign='top'><strong>Action</strong></th>
					<th style='text-align:center' valign='top' onclick='del_detail(" . $$row->id_penerimaan . "," . $row->id_pembelian . ")'><strong>X</strong></th>
					<th style='text-align:center' valign='top'></th>
					</tr>
					</thead>
					<tbody>";
					$no = 2;
				}
				$data .= "<tr>
				<input type='hidden' name='id_barang[]' value='$row->id_barang'>
				<td style='width:470px;padding-left:2px'>$row->nama_barang</td>
				<td style='text-align:center;width:90px'>$row->jumlah $row->satuan</td>
				<input type='hidden' name='satuan_barang[]' value='$row->satuan'>
				<input type='hidden' id='jumlah_$row->id_barang' name='jumlah_barang[]' value='$row->jumlah'>
				<input type='hidden' name='id_penerimaan[]' value='$row->id_penerimaan'>
				<input type='hidden' name='id_pembelian[]' value='$row->id_pembelian'>
				<input type='hidden' id='total_$row->id_barang' name='jumlah_harga[]' value='" . ($row->jumlah * $row->harga) . "'>
				<td style='width:120px'><input class='form-control harga_barang text-right' id='$row->id_barang' type='text' name='harga_barang[]' value='" . number_format($row->harga, 0) . "' onclick='autonumber(this);' onkeyup='hitung(this);'></td>
				<td style='text-align:center;width:100px'><input class='form-control price total_harga' type='text' value='" . number_format(($row->jumlah * $row->harga), 0) . "' readonly></td>
				<td style='vertical-align:middle;text-align:center'><button type='button' class='button' onclick='del_per_detail(this)'> X </button></td>";
				if ($row->id_barang == 'x') {
					$data .= "
				<tr>
				<td style='width:470px;padding-left:2px'>Biaya Ongkir</td>
				<td style='text-align:center;width:90px'></td>
				<input type='hidden' name='id_barang[]' value='x'>
				<input type='hidden' id='jumlah_x' name='jumlah_barang[]' value='1'>
				<input type='hidden' name='satuan_barang[]' value=''>
				<input type='hidden' id='total_x' name='jumlah_harga[]' value='" . ($row->jumlah * $row->harga) . "'>
				<input type='hidden' name='id_penerimaan[]' value='$row->id_penerimaan'>
				<input type='hidden' name='id_pembelian[]' value='$row->id_pembelian'>
				<td style='width:120px'><input class='form-control harga_barang text-right' id='x' type='text' name='harga_barang[]' value='" . number_format($row->harga, 0) . "' onclick='autonumber(this);' onkeyup='hitung(this);'></td>
				<td style='text-align:center;width:100px'><input class='form-control price total_harga' type='text' value='" . number_format((1 * $row->harga), 0) . "' readonly></td>
				<td style='vertical-align:middle;text-align:center'><button type='button' class='button' onclick='del_per_detail(this)'> X </button></td>";
				}
				if ($row->id_barang == 'xx') {
					$data .= "<tr><td style='width:470px;padding-left:2px'>Diskon</td>
				<td style='text-align:center;width:90px'></td>
				<input type='hidden' name='id_barang[]' value='xx'>
				<input type='hidden' id='jumlah_xx' name='jumlah_barang[]' value='1'>
				<input type='hidden' name='satuan_barang[]' value=''>
				<input type='hidden' id='total_xx' name='jumlah_harga[]' value='" . ($row->jumlah * $row->harga) . "'>
				<input type='hidden' name='id_penerimaan[]' value='$row->id_penerimaan'>
					<input type='hidden' name='id_pembelian[]' value='$row->id_pembelian'>
				<td style='width:120px'><input class='form-control harga_barang text-right' id='xx' type='text' name='harga_barang[]' value='" . number_format($row->harga, 0) . "' onclick='autonumber(this);' onkeyup='hitung(this);'></td>
				<td style='text-align:center;width:100px'><input class='form-control price total_harga' type='text' value='" . number_format((-1 * $row->harga), 0) . "' readonly></td>
				<td style='vertical-align:middle;text-align:center'><button type='button' class='button' onclick='del_per_detail(this)'> X </button></td>";
				}
				$id_pembelian =  $row->id_pembelian;
				$id_penerimaan = $row->id_penerimaan;
			}

			$data2 = array('res' => $q->result(), 'det' => $data);
			return $data2;
		} else {
			$msg = 'Data tidak ditemukan';
			return $msg;
		}
	}



	function delete($id)
	{
		$this->db->trans_start();
		$data = array('status' => 9);
		$this->db->where('id', $id);
		$result = $this->db->update('finance_ap_invoice', $data);
		if ($result == true) {
			$this->db->where('id_ap', $id);
			$this->db->delete('finance_ap_invoice_detail', $data);
			/*
			//OLD TRANSAKSI
			if($old_transaksi == '1'){
				$jml = 0;
				
				$q = $this->db->query("select sum(jumlah) as jml from gmd_finance_transaksi_kasir 
				where id_ref = '".$old_id_ref."' 
				and transaksi = '".$old_transaksi."'");
				if($q->num_rows() > 0){
					foreach($q->result_array() as $r){
						$jml += $r['jml'];
					}
				}
				
				$data = array( 
							'bayar' => $jml,
						);
				$this->db->where('id', $old_id_ref);
				$this->db->update('finance_invoice_customer', $data);	
			}
			*/
			$msg = 1;
		} else {
			$msg = 0;
		}
		$this->db->trans_complete();
		return $msg;
	}

	function select_data_detail_invoice()
	{
		$data = '';

		$this->db->select("a.*", false);
		$this->db->from('finance_ap_invoice_detail AS a');
		$this->db->where('a.invoice_id', $this->input->post('id'));
		$this->db->order_by('a.deskripsi', 'asc');
		$q = $this->db->get();
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $k => $r) {
				$data .= '<tr class="remove">';
				$data .= '<td style="vertical-align:middle;"><input class="form-control" type="text" style="width:300px;" readonly="readonly" name="tambah_description[]" value="' . $r['deskripsi'] . '" /></td>';
				$data .= '<td style="vertical-align:middle;"><input class="form-control price" type="text" style="width:200px;" readonly="readonly" name="tambah_jumlah[]" value="' . number_format($r['harga'], 0) . '" /></td>';
				$data .= '<td style="vertical-align:middle;"><button type="button" class="button">X</button></td>';
				$data .= '</tr>';
			}
		}
		$q->free_result();
		return $data;
	}

	function finance_bank()
	{
		$this->db->order_by('name', 'asc');
		$q = $this->db->get('finance_bank');
		return $q;
	}

	function cek_id_regional($id)
	{
		$data = 0;

		$q = $this->db->query("select id from gmd_regional where code = '" . $id . "'");
		if ($q->num_rows() > 0) {
			foreach ($q->result_array() as $r) {
				$data = $r['id'];
			}
		}
		$q->free_result();

		return $data;
	}

	function select_autocomplite()
	{
		if ($this->input->post('transaksi') == '1') {
			$this->db->select("a.id, a.no_invoice, concat('<div>No Invoice: <b>',a.no_invoice,'</b>
			, Inv Date: <b>',date_format(a.date_invoice, '%d-%m-%Y'),'</b>
			, Due Date: <b>',date_format(a.date_due, '%d-%m-%Y'),'</b>
			<br> Service ID: <b>',a.service_id,'</b>
			, Cust. ID: <b>',b.customer_id,'</b>
			, Customer: <b>',b.nama,'</b>
			<br> Jumlah Tagihan: <b>',CAST(format(a.jumlah,0) AS CHAR CHARACTER SET utf8),'</b></div>') as konten", false);
			$this->db->from("finance_invoice_customer a");
			$this->db->join('marketing_customer_service as b', 'a.service_id = b.service_id', 'left');
			$this->db->where("(a.no_invoice like '%" . $this->input->post('term') . "%'
		or b.nama like '%" . $this->input->post('term') . "%'
		OR a.no_invoice like '%" . $this->input->post('term') . "%'
		OR a.service_id like '%" . $this->input->post('term') . "%'
		OR b.customer_id like '%" . $this->input->post('term') . "%')", NULL, FALSE);
			$q = $this->db->get();
		}
		return $q->result();
	}

	function select_detail_ref($trx, $id_ref)
	{
		$data = '';
		if ($trx == '1') {
			$this->db->select("concat('(NO INVOICE): ',a.no_invoice,', (INV DATE): ',date_format(a.date_invoice, '%d-%m-%Y'),', (DUE DATE): ',date_format(a.date_due, '%d-%m-%Y'),'
(SERVICE ID): ',a.service_id,', (CUST. ID): ',b.customer_id,', (CUSTOMER): ',b.nama,'
(JUMLAH TAGIHAN): ',CAST(format(a.jumlah,0) AS CHAR CHARACTER SET utf8),'') as konten", false);
			$this->db->from("finance_invoice_customer a");
			$this->db->join('marketing_customer_service as b', 'a.service_id = b.service_id', 'left');
			$this->db->where("a.id", $id_ref);
			$q = $this->db->get();
			if ($q->num_rows() > 0) {
				foreach ($q->result_array() as $r) {
					$data = $r['konten'];
				}
			}
			$q->free_result();
		}
		return $data;
	}

	function get_supp($param = '')
	{
		$q = $this->db->query("SELECT * FROM inventory_v2.`ms_perusahaan` a WHERE a.`status`=1 AND a.`nama` LIKE '%$param%' ORDER BY a.`nama` ASC LIMIT 10")->result();
		$data = array();
		foreach ($q as $row) {
			$data[] = array(
				"id" => $row->id_perusahaan,
				"text" => $row->nama
			);
		}
		return json_encode($data);
	}
	function select_po()
	{
		$id = $this->input->post('id');
		$total = 0;
		// 	$q = $this->db->query("SELECT h.`tanggal` AS tanggal,h.id_perusahaan,NULL AS id_penerimaan, d.id_header AS id_pembelian, NULL AS nomor,h.`nomor` AS nomor_po,d.id_barang, b.nama_barang, 
		// 	d.qty AS jumlah, IFNULL(t.jumlah_terima,0) AS jumlah_terima, u.nama_ukuran AS satuan, d.keterangan
		// 	FROM inventory_v2.tr_d_pembelian d 
		// 	JOIN inventory_v2.tr_h_pembelian h ON d.id_header=h.id_header
		// 	JOIN inventory_v2.ms_header_barang b ON d.id_barang=b.id_header
		// 	JOIN inventory_v2.ms_ukuran u ON d.id_ukuran=u.id_ukuran
		// 	LEFT JOIN (
		// 		SELECT d.id_pembelian, d.id_barang, SUM(d.jumlah) AS jumlah_terima 
		// 			FROM erp_financev2.gmd_finance_ap_invoice_detail d 
		// 			JOIN erp_financev2.gmd_finance_ap_invoice h ON d.id_ap=h.id
		// 			WHERE d.id_penerimaan IS NULL
		// 			GROUP BY d.id_barang, d.id_pembelian
		// 	) t ON d.id_header=t.id_pembelian AND d.id_barang=t.id_barang
		// 	WHERE b.flag='J' AND d.`status`=1 AND h.id_perusahaan=$id
		// 	AND (d.qty - IFNULL(t.jumlah_terima,0)) > 0
		// 	GROUP BY d.id_header,h.`id_perusahaan`
		// UNION ALL
		// SELECT * FROM (
		// 	SELECT h.`tanggal` AS tanggal,hp.id_perusahaan, d.id_header AS id_penerimaan,hp.`id_header` AS id_pembelian, h.`nomor`,hp.`nomor` AS nomor_po,d.id_barang, b.nama_barang, SUM(d.qty) AS jumlah, IFNULL(t.jumlah_terima,0) AS jumlah_terima, 
		// 		u.nama_ukuran AS satuan, d.keterangan
		// 		FROM inventory_v2.tr_d_penerimaan d 
		// 		JOIN inventory_v2.tr_h_penerimaan h ON d.id_header=h.id_header
		// 		JOIN inventory_v2.ms_header_barang b ON d.id_barang=b.id_header
		// 		JOIN inventory_v2.ms_ukuran u ON d.id_ukuran=u.id_ukuran
		// 		JOIN inventory_v2.tr_h_pembelian hp ON h.id_pembelian=hp.id_header
		// 		LEFT JOIN (
		// 			SELECT d.id_penerimaan, d.id_barang, SUM(d.jumlah) AS jumlah_terima 
		// 				FROM erp_financev2.gmd_finance_ap_invoice_detail d 
		// 				JOIN erp_financev2.gmd_finance_ap_invoice h ON d.id_ap=h.id
		// 				GROUP BY d.id_barang, d.id_penerimaan
		// 		) t ON d.id_header=t.id_penerimaan AND d.id_barang=t.id_barang
		// 		GROUP BY d.id_header,hp.`id_perusahaan`
		// ) terima_barang
		// WHERE (jumlah - jumlah_terima) > 0 AND terima_barang.id_perusahaan=$id
		// ORDER BY tanggal DESC")->result();
		$q = $this->db->query("SELECT *,(SUM(total)-diskon+ongkir) AS total_harga FROM (SELECT h.`tanggal` AS tanggal,h.id_perusahaan,0 AS id_penerimaan, d.id_header AS id_pembelian, NULL AS nomor,h.`nomor` AS nomor_po,d.id_barang, b.nama_barang, 
		SUM(d.qty) AS jumlah, IFNULL(t.jumlah_terima,0) AS jumlah_terima, u.nama_ukuran AS satuan, d.keterangan,d.`harga`,(SUM(d.`qty`)-COALESCE(t.jumlah_terima,0))*d.`harga` AS total,COALESCE(h.diskon-COALESCE(t.diskon,0),0) AS diskon,COALESCE(h.ongkir-COALESCE(t.ongkir,0),0) AS ongkir,cc.nama AS supplier
		FROM inventory_v2.tr_d_pembelian d 
		JOIN inventory_v2.tr_h_pembelian h ON d.id_header=h.id_header
		JOIN inventory_v2.ms_header_barang b ON d.id_barang=b.id_header
		JOIN inventory_v2.ms_ukuran u ON d.id_ukuran=u.id_ukuran
		JOIN inventory_v2.ms_perusahaan cc ON h.id_perusahaan=cc.id_perusahaan
		LEFT JOIN (
			SELECT d.id_pembelian, d.id_barang, SUM(d.jumlah) AS jumlah_terima,satuan,jumlah_harga,h.diskon,h.ongkir,d.status
				FROM erp_financev2.gmd_finance_ap_invoice_detail d 
				JOIN erp_financev2.gmd_finance_ap_invoice h ON d.id_ap=h.id
				WHERE d.id_penerimaan=0 AND d.status=1
				GROUP BY d.id_barang, d.id_pembelian
		) t ON d.id_header=t.id_pembelian AND d.id_barang=t.id_barang
		WHERE b.flag='J' AND d.`status`=1
		AND (d.qty - IFNULL(t.jumlah_terima,0)) > 0 AND h.id_perusahaan=$id
		GROUP BY d.id_barang,d.id_header) z
		GROUP BY z.id_penerimaan,z.id_pembelian
	UNION ALL
	SELECT *,(SUM(z.total)-z.diskon+z.ongkir) AS total_harga FROM (
	SELECT * FROM (
		SELECT h.`tanggal` AS tanggal,hp.id_perusahaan, d.id_header AS id_penerimaan,hp.`id_header` AS id_pembelian, h.`nomor`,hp.`nomor` AS nomor_po,d.id_barang, b.nama_barang, SUM(d.qty) AS jumlah, IFNULL(t.jumlah_terima,0) AS jumlah_terima, 
			u.nama_ukuran AS satuan, d.keterangan,d.`harga`,(SUM(d.`qty`)-COALESCE(t.jumlah_terima,0))*d.`harga` AS total,COALESCE(hp.diskon-COALESCE(t.diskon,0),0) AS diskon,COALESCE(hp.ongkir-COALESCE(t.ongkir,0),0) AS ongkir,cc.nama AS supplier
			FROM inventory_v2.tr_d_penerimaan d 
			JOIN inventory_v2.tr_h_penerimaan h ON d.id_header=h.id_header
			JOIN inventory_v2.ms_header_barang b ON d.id_barang=b.id_header
			JOIN inventory_v2.ms_ukuran u ON d.id_ukuran=u.id_ukuran
			JOIN inventory_v2.tr_h_pembelian hp ON h.id_pembelian=hp.id_header
			JOIN inventory_v2.ms_perusahaan cc ON hp.id_perusahaan=cc.id_perusahaan
			LEFT JOIN (
				SELECT d.id_penerimaan, d.id_barang, SUM(d.jumlah) AS jumlah_terima,satuan,jumlah_harga,h.diskon,h.ongkir,d.status
					FROM erp_financev2.gmd_finance_ap_invoice_detail d 
					JOIN erp_financev2.gmd_finance_ap_invoice h ON d.id_ap=h.id
					WHERE d.status=1
					GROUP BY d.id_barang, d.id_penerimaan
			) t ON d.id_header=t.id_penerimaan AND d.id_barang=t.id_barang
			GROUP BY d.id_barang,d.id_header
	) terima_barang
	WHERE (jumlah - jumlah_terima) > 0 AND id_perusahaan=$id )  z 
	GROUP BY z.id_penerimaan,z.id_pembelian
	
	ORDER BY tanggal DESC")->result();
		$data = null;
		$data .= '
<tbody>';
		foreach ($q as $row) {
			if (empty($row->id_penerimaan)) {
				$row->id_penerimaan = 0;
			}
			$data .= "<tr>
		<td style='text-align: center;width:60px;padding:0 20px'><input type='checkbox' class='check_po_" . $row->id_penerimaan . "_" . $row->id_pembelian . "' onclick='set_up(this,$id,$row->id_penerimaan,$row->id_pembelian);' name='po[]' value=" . $row->id_penerimaan . "," . $row->id_pembelian . "></td>
		<td style='text-align: center;width:230px;padding:0 20px'>$row->nomor</td>
		<td style='text-align: center;width:230px;padding:0 20px'>$row->nomor_po</td>
		<td style='text-align: center;width:340px;padding:0 20px'>$row->supplier</td>
		<td style='text-align: center;width:160px;padding:0 20px'>$row->tanggal</td>
		<td style='text-align: right;width:120px;padding:0 20px'>" . number_format($row->total_harga, 0) . "</td></tr>";
		}
		$data .= "</tbody>";
		$res = array();
		$res = array(
			'html' => $data
		);
		return $res;
	}

	// //get_detail_barang_old
	// SELECT
	// 	z.`id_header`,
	// 	z.`nomor`,
	// 	z.`tanggal`,
	// 	z.`id_pembelian`,
	// 	z.`id_perusahaan`,
	// 	z.`supplier`,
	// 	z.`nomor_po`,
	// 	z.`tgl_pembelian`,
	// 	z.`id_barang`,
	// 	z.`nama_barang`,
	// 	z.`qty` AS jumlah,
	// 	(SELECT
	// 	  nama_ukuran
	// 	FROM
	// 	inventory_v2.`ms_ukuran` p
	// 	WHERE p.`id_ukuran` = z.`id_ukuran`) ukuran,
	// 	z.`harga`,
	// 	z.`diskon`,
	// 	z.`ongkir`,
	// 	z.`keterangan`,
	// 	z.`flag`
	//   FROM
	// 	(SELECT
	// 	  NULL AS id_header,
	// 	  NULL AS nomor,
	// 	  h.`tanggal` AS tanggal,
	// 	  d.`id_header` AS id_pembelian,
	// 	  h.`id_perusahaan`,
	// 	  p.`nama` AS supplier,
	// 	  h.`nomor` AS nomor_po,
	// 	  h.`tanggal` AS tgl_pembelian,
	// 	  d.`id_barang`,
	// 	  br.`nama_barang`,
	// 	  d.`harga`,
	// 	  h.`diskon`,
	// 	  h.`ongkir`,
	// 	  IF(
	// 		k.`qty_konversi` IS NULL,
	// 		d.`qty`,
	// 		ROUND((d.`qty` / k.`qty_konversi`))
	// 	  ) AS qty,
	// 	  d.`id_ukuran`,
	// 	  IF(
	// 		h.`keterangan` IS NOT NULL,
	// 		h.`keterangan`,
	// 		''
	// 	  ) AS keterangan,
	// 	  br.`flag`
	// 	FROM
	// 	  inventory_v2.`tr_d_pembelian` d
	// 	  JOIN inventory_v2.`tr_h_pembelian` h
	// 		ON d.`id_header` = h.`id_header`
	// 	  JOIN inventory_v2.`ms_perusahaan` p
	// 		ON h.`id_perusahaan` = p.`id_perusahaan`
	// 	  JOIN inventory_v2.`ms_header_barang` br
	// 		ON d.`id_barang` = br.`id_header`
	// 	  LEFT JOIN inventory_v2.`ms_konversi` k
	// 	  ON br.`id_header` = k.`id_barang`
	// 	WHERE h.`id_perusahaan` = $id_supp AND h.`id_header` = $id_header) z")->result();
	// 	} else {
	// 		$id_header = $this->input->post('id_header');
	// 		$q = $this->db->query("SELECT
	// 		z.`id_header`,
	// 		z.`nomor`,
	// 		z.`tanggal`,
	// 		z.`id_pembelian`,
	// 		z.`id_perusahaan`,
	// 		z.`supplier`,
	// 		z.`nomor_po`,
	// 		z.`tgl_pembelian`,
	// 		z.`id_barang`,
	// 		z.`nama_barang`,
	// 		  IF(
	// 			z.`qty_konversi` IS NULL,
	// 			z.`qty`,
	// 			ROUND((z.`qty` / z.`qty_konversi`))
	// 		  ) AS jumlah,
	// 		z.`ukuran`,
	// 		z.`harga`,
	// 		z.`diskon`,
	// 		z.`ongkir`,
	// 		z.`keterangan`
	// 	  FROM
	// 		(SELECT
	// 		  d.`id_header`,
	// 		  h.`nomor`,
	// 		  h.`tanggal`,
	// 		  h.`id_pembelian`,
	// 		  b.`id_perusahaan`,
	// 		  b.`supplier`,
	// 		  b.`nomor_po`,
	// 		  b.`tgl_pembelian`,
	// 		  d.`id_barang`,
	// 		  br.`nama_barang`,
	// 		  k.`qty_konversi`,
	// 		  d.`qty`,
	// 		  (SELECT
	// 			nama_ukuran
	// 		  FROM
	// 			inventory_v2.`ms_ukuran` p
	// 		  WHERE p.`id_ukuran` = d.`id_ukuran`) ukuran,
	// 		  b.`harga`,
	// 		  b.`diskon`,
	// 		  b.`ongkir`,
	// 		  b.`keterangan`
	// 		FROM
	// 		  inventory_v2.`tr_d_penerimaan` d
	// 		  JOIN inventory_v2.`ms_header_barang` br
	// 			ON d.`id_barang` = br.`id_header`
	// 		  JOIN inventory_v2.`tr_h_penerimaan` h
	// 			ON d.`id_header` = h.`id_header`
	// 		  LEFT JOIN inventory_v2.`ms_konversi` k
	// 			ON d.`id_barang` = k.`id_barang`
	// 		  JOIN
	// 			(SELECT
	// 			  d.`id_header` AS id_pembelian,
	// 			  h.`id_perusahaan`,
	// 			  p.`nama` AS supplier,
	// 			  h.`nomor` AS nomor_po,
	// 			  h.`tanggal` AS tgl_pembelian,
	// 			  d.`id_barang`,
	// 			  d.`harga`,
	// 			  h.`diskon`,
	// 			  h.`ongkir`,
	// 			  d.`qty`,
	// 			  d.`total`,
	// 			  IF(
	// 				h.`keterangan` IS NOT NULL,
	// 				h.`keterangan`,
	// 				''
	// 			  ) AS keterangan
	// 			FROM
	// 			  inventory_v2.`tr_d_pembelian` d
	// 			  JOIN inventory_v2.`tr_h_pembelian` h
	// 				ON d.`id_header` = h.`id_header`
	// 			  JOIN inventory_v2.`ms_perusahaan` p
	// 				ON h.`id_perusahaan` = p.`id_perusahaan`
	// 				WHERE p.`id_perusahaan`= $id_supp) b
	// 			ON h.`id_pembelian` = b.`id_pembelian`
	// 			AND d.`id_barang` = b.`id_barang`
	// 			WHERE d.`id_header`=$id_header
	// 		) z
	function get_detail_barang()
	{
		$id_supp = $kondisi1 = $kondisi2 = $data = null;
		$id_supp = $this->input->post('id_supp');
		$kondisi1 = " AND d.id_header =" . $this->input->post('id_header');
		$kondisi2 = " AND d.id_header =" . $this->input->post('id_header');
		if (empty($this->input->post('id_header'))) {
			$kondisi1 = " AND d.id_header =" . $this->input->post('id_pembelian');
			$kondisi2 = " AND hp.id_header =" . $this->input->post('id_pembelian');
		}
		$q = $this->db->query("SELECT h.`tanggal` AS tanggal,h.id_perusahaan,0 AS id_penerimaan, d.id_header AS id_pembelian, NULL AS nomor,h.`nomor` AS nomor_po,d.id_barang, b.nama_barang, 
			SUM(d.qty) AS jumlah, IFNULL(t.jumlah_terima,0) AS jumlah_terima, u.nama_ukuran AS satuan, d.keterangan,d.`harga`,(SUM(d.`qty`)-COALESCE(t.jumlah_terima,0))*d.`harga` AS total,COALESCE(h.diskon-COALESCE(t.diskon,0),0) AS diskon,COALESCE(h.ongkir-COALESCE(t.ongkir,0),0) AS ongkir
			FROM inventory_v2.tr_d_pembelian d 
			JOIN inventory_v2.tr_h_pembelian h ON d.id_header=h.id_header
			JOIN inventory_v2.ms_header_barang b ON d.id_barang=b.id_header
			JOIN inventory_v2.ms_ukuran u ON d.id_ukuran=u.id_ukuran
			LEFT JOIN (
				SELECT d.id_pembelian, d.id_barang, SUM(d.jumlah) AS jumlah_terima,satuan,jumlah_harga,h.diskon,h.ongkir
					FROM erp_financev2.gmd_finance_ap_invoice_detail d 
					JOIN erp_financev2.gmd_finance_ap_invoice h ON d.id_ap=h.id
					WHERE d.id_penerimaan=0 AND d.status=1
					GROUP BY d.id_barang, d.id_pembelian
			) t ON d.id_header=t.id_pembelian AND d.id_barang=t.id_barang
			WHERE b.flag='J' AND d.`status`=1 AND h.id_perusahaan = $id_supp $kondisi1
			AND (d.qty - IFNULL(t.jumlah_terima,0)) > 0
			GROUP BY d.id_barang,d.id_header
		UNION ALL
		SELECT * FROM (
			SELECT h.`tanggal` AS tanggal,hp.id_perusahaan, d.id_header AS id_penerimaan,hp.`id_header` AS id_pembelian, h.`nomor`,hp.`nomor` AS nomor_po,d.id_barang, b.nama_barang, SUM(d.qty) AS jumlah, IFNULL(t.jumlah_terima,0) AS jumlah_terima, 
				u.nama_ukuran AS satuan, d.keterangan,d.`harga`,(SUM(d.`qty`)-COALESCE(t.jumlah_terima,0))*d.`harga` AS total,COALESCE(hp.diskon-COALESCE(t.diskon,0),0) AS diskon,COALESCE(hp.ongkir-COALESCE(t.ongkir,0),0) AS ongkir
				FROM inventory_v2.tr_d_penerimaan d 
				JOIN inventory_v2.tr_h_penerimaan h ON d.id_header=h.id_header
				JOIN inventory_v2.ms_header_barang b ON d.id_barang=b.id_header
				JOIN inventory_v2.ms_ukuran u ON d.id_ukuran=u.id_ukuran
				JOIN inventory_v2.tr_h_pembelian hp ON h.id_pembelian=hp.id_header
				LEFT JOIN (
					SELECT d.id_penerimaan, d.id_barang, SUM(d.jumlah) AS jumlah_terima,satuan,jumlah_harga,h.diskon,h.ongkir
						FROM erp_financev2.gmd_finance_ap_invoice_detail d 
						JOIN erp_financev2.gmd_finance_ap_invoice h ON d.id_ap=h.id
						WHERE d.status=1
						GROUP BY d.id_barang, d.id_penerimaan
				) t ON d.id_header=t.id_penerimaan AND d.id_barang=t.id_barang
				WHERE hp.id_perusahaan = $id_supp $kondisi2
				GROUP BY d.id_barang,d.id_header
		) terima_barang
		WHERE (jumlah - jumlah_terima) > 0
		
		ORDER BY tanggal DESC")->result();

		$no = $ongkir =  $diskon = 0;
		$qn = $this->db->query("SELECT FOUND_ROWS() as total");
		$tot = $qn->row()->total;
		if (!empty($q)) {
			$data .= '
			<table class="' . $this->input->post('id_header') . '_' . $this->input->post('id_pembelian') . '" border="1" cellpadding="0" cellspacing="0" width="100%" style="margin-top:5px;">
				<thead>
					<tr>';
			foreach ($q as $row) {
				$no++;
				if ($no == 1) {
					$data .= "<th valign='top' colspan='3' style='text-align:center'><strong>$row->nomor_po</strong></th>
					<th style='text-align:center' valign='top'><strong>Jumlah</strong></th>
					<th style='text-align:center' valign='top'><strong>Action</strong></th>
					<th style='text-align:center' valign='top' onclick='del_detail(" . $this->input->post('id_header') . "," . $this->input->post('id_pembelian') . ")'><strong>X</strong></th>
					</tr>
					</thead>
					<tbody>";
				}
				$data .= "<tr>
				<input type='hidden' name='id_barang[]' value='$row->id_barang'>
				<td style='width:470px;padding-left:2px'>$row->nama_barang</td>
				<td style='text-align:center;width:90px'>$row->jumlah $row->satuan</td>
				<input type='hidden' name='satuan_barang[]' value='$row->satuan'>
				<input type='hidden' id='jumlah_$row->id_barang' name='jumlah_barang[]' value='$row->jumlah'>
				<input type='hidden' name='id_penerimaan[]' value='$row->id_penerimaan'>
				<input type='hidden' name='id_pembelian[]' value='$row->id_pembelian'>
				<input type='hidden' id='total_$row->id_barang' name='jumlah_harga[]' value='" . ($row->jumlah * $row->harga) . "'>
				<td style='width:120px'><input class='form-control harga_barang text-right' id='$row->id_barang' type='text' name='harga_barang[]' value='" . number_format($row->harga, 0) . "' onclick='autonumber(this);' onkeyup='hitung(this);'></td>
				<td style='text-align:center;width:100px'><input class='form-control price total_harga' type='text' value='" . number_format(($row->jumlah * $row->harga), 0) . "' readonly></td>
				<td style='vertical-align:middle;text-align:center'><button type='button' class='button' onclick='del_per_detail(this)'> X </button></td>";
				if ($no == 1) {
					if (!empty($row->diskon)) {
						$tot = $tot + 1;
						$diskon = $row->diskon;
					}
					if (!empty($row->ongkir)) {
						$tot = $tot + 1;
						$ongkir = $row->ongkir;
					}
					$data .= "<td style='vertical-align:middle;text-align:center;' rowspan='$tot'><button type='button' class='button' onclick='del_detail(" . $this->input->post('id_header') . "," . $this->input->post('id_pembelian') . ")'> X </button></td>";
				}
			}
			if (!empty($ongkir)) {
				$data .= "
				<tr>
				<td style='width:470px;padding-left:2px'>Biaya Ongkir</td>
				<td style='text-align:center;width:90px'></td>
				<input type='hidden' name='id_barang[]' value='x'>
				<input type='hidden' id='jumlah_x' name='jumlah_barang[]' value='1'>
				<input type='hidden' name='satuan_barang[]' value=''>
				<input type='hidden' id='total_x' name='jumlah_harga[]' value='" . ($row->jumlah * $ongkir) . "'>
				<input type='hidden' name='id_penerimaan[]' value='$row->id_penerimaan'>
				<input type='hidden' name='id_pembelian[]' value='$row->id_pembelian'>
				<td style='width:120px'><input class='form-control harga_barang text-right' id='x' type='text' name='harga_barang[]' value='" . number_format($ongkir, 0) . "' onclick='autonumber(this);' onkeyup='hitung(this);'></td>
				<td style='text-align:center;width:100px'><input class='form-control price total_harga' type='text' value='" . number_format((1 * $ongkir), 0) . "' readonly></td>
				<td style='vertical-align:middle;text-align:center'><button type='button' class='button' onclick='del_per_detail(this)'> X </button></td>";
			}
			if (!empty($diskon)) {
				$data .= "<tr><td style='width:470px;padding-left:2px'>Diskon</td>
			<td style='text-align:center;width:90px'></td>
			<input type='hidden' name='id_barang[]' value='xx'>
			<input type='hidden' id='jumlah_xx' name='jumlah_barang[]' value='1'>
			<input type='hidden' name='satuan_barang[]' value=''>
			<input type='hidden' id='total_xx' name='jumlah_harga[]' value='" . ($row->jumlah * $diskon) . "'>
			<input type='hidden' name='id_penerimaan[]' value='$row->id_penerimaan'>
				<input type='hidden' name='id_pembelian[]' value='$row->id_pembelian'>
			<td style='width:120px'><input class='form-control harga_barang text-right' id='xx' type='text' name='harga_barang[]' value='" . number_format($diskon, 0) . "' onclick='autonumber(this);' onkeyup='hitung(this);'></td>
			<td style='text-align:center;width:100px'><input class='form-control price total_harga' type='text' value='" . number_format((-1 * $diskon), 0) . "' readonly></td>
			<td style='vertical-align:middle;text-align:center'><button type='button' class='button' onclick='del_per_detail(this)'> X </button></td>";
			}

			$data .= "</tbody></table>";
			$res = array('html' => $data);
			return json_encode($res);
		} else {
			$msg = 'Data tidak ditemukan';
			return $msg;
		}
	}

	function search_po()
	{
		$id = $this->input->post('id');
		$kode = $this->input->post('kode');
		$check = $this->input->post('check');
		$total = 0;
		$q = $this->db->query("SELECT * FROM (SELECT
		z.`id_header`,
		z.`nomor`,
		z.`tanggal`,
		z.`id_pembelian`,
		z.`id_perusahaan`,
		z.`supplier`,
		z.`nomor_po`,
		z.`tgl_pembelian`,
		z.`id_barang`,
		z.`nama_barang`,SUM(
			IF(
			  z.`qty_konversi` IS NULL,
			  z.`qty`,
			  ROUND((z.`qty` / z.`qty_konversi`))
			)
		  ) AS jumlah,
		  z.`ukuran`,
		  z.`harga`,
		  z.`diskon`,
		  z.`ongkir`,
		SUM(
			  IF(
				z.`qty_konversi` IS NULL,
				z.`qty`,
				ROUND((z.`qty` / z.`qty_konversi`))
		)* z.`harga`) AS total,
		  z.`keterangan`
	  FROM
		(SELECT
		  d.`id_header`,
		  h.`nomor`,
		  h.`tanggal`,
		  h.`id_pembelian`,
		  b.`id_perusahaan`,
		  b.`supplier`,
		  b.`nomor_po`,
		  b.`tgl_pembelian`,
		  d.`id_barang`,
		  br.`nama_barang`,
		  k.`qty_konversi`,
		  d.`qty`,
		  (SELECT
			nama_ukuran
		  FROM
		  inventory_v2.`ms_ukuran` p
		  WHERE p.`id_ukuran` = d.`id_ukuran`) ukuran,
		  b.`harga`,
		  b.`diskon`,
		  b.`ongkir`,
		  b.`keterangan`
		FROM
		  inventory_v2.`tr_d_penerimaan` d
		  JOIN inventory_v2.`ms_header_barang` br
			ON d.`id_barang` = br.`id_header`
		  JOIN inventory_v2.`tr_h_penerimaan` h
			ON d.`id_header` = h.`id_header`
		  LEFT JOIN inventory_v2.`ms_konversi` k
			ON d.`id_barang` = k.`id_barang`
		  JOIN
			(SELECT
			  d.`id_header` AS id_pembelian,
			  h.`id_perusahaan`,
			  p.`nama` AS supplier,
			  h.`nomor` AS nomor_po,
			  h.`tanggal` AS tgl_pembelian,
			  d.`id_barang`,
			  d.`harga`,
			  h.`diskon`,
			  h.`ongkir`,
			  d.`total`,
			  IF(
				h.`keterangan` IS NOT NULL,
				h.`keterangan`,
				''
			  ) AS keterangan
			FROM
			  inventory_v2.`tr_d_pembelian` d
			  JOIN inventory_v2.`tr_h_pembelian` h
				ON d.`id_header` = h.`id_header`
			  JOIN inventory_v2.`ms_perusahaan` p
				ON h.`id_perusahaan` = p.`id_perusahaan`
			WHERE p.`id_perusahaan`=$id) b
			ON h.`id_pembelian` = b.`id_pembelian`
			AND d.`id_barang` = b.`id_barang`
		WHERE br.`flag`='B') z
	  GROUP BY id_header
	  UNION ALL 
	  SELECT
		z.`id_header`,
		z.`nomor`,
		z.`tanggal`,
		z.`id_pembelian`,
		z.`id_perusahaan`,
		z.`supplier`,
		z.`nomor_po`,
		z.`tgl_pembelian`,
		z.`id_barang`,
		z.`nama_barang`,
		SUM(
		  IF(
			k.`qty_konversi` IS NULL,
			z.`qty`,
			ROUND((z.`qty` / k.`qty_konversi`))
		  )
		) AS jumlah,
		(SELECT
		  nama_ukuran
		FROM
		inventory_v2.`ms_ukuran` p
		WHERE p.`id_ukuran` = z.`id_ukuran`) ukuran,
		z.`harga`,
		z.`diskon`,
		z.`ongkir`,
		SUM(
			IF(
			  k.`qty_konversi` IS NULL,
			  z.`qty`,
			  ROUND((z.`qty` / k.`qty_konversi`))
		) * z.`harga`)AS total,
		z.`keterangan`
	  FROM
		(SELECT
		  NULL AS id_header,
		  NULL AS nomor,
		  h.`tanggal` AS tanggal,
		  d.`id_header` AS id_pembelian,
		  h.`id_perusahaan`,
		  p.`nama` AS supplier,
		  h.`nomor` AS nomor_po,
		  h.`tanggal` AS tgl_pembelian,
		  d.`id_barang`,
		  br.`nama_barang`,
		  d.`harga`,
		  h.`diskon`,
		  h.`ongkir`,
		  d.`id_ukuran`,
		  d.`qty`,
		  d.`total`,
		  IF(
			h.`keterangan` IS NOT NULL,
			h.`keterangan`,
			''
		  ) AS keterangan
		FROM
		  inventory_v2.`tr_d_pembelian` d
		  JOIN inventory_v2.`tr_h_pembelian` h
			ON d.`id_header` = h.`id_header`
		  JOIN inventory_v2.`ms_perusahaan` p
			ON h.`id_perusahaan` = p.`id_perusahaan`
		  JOIN inventory_v2.`ms_header_barang` br
			ON d.`id_barang` = br.`id_header`
		WHERE p.`id_perusahaan`=$id AND NOT EXISTS (SELECT a.`id_header` FROM inventory_v2.`tr_h_penerimaan` a
        JOIN inventory_v2.`tr_d_penerimaan` c
        ON a.`id_header`=c.`id_header`
        JOIN inventory_v2.`ms_header_barang` b
          ON c.`id_barang` = b.`id_header`
      WHERE a.`id_pembelian` = d.`id_header`)) z
    LEFT JOIN inventory_v2.`ms_konversi` k
		  ON z.`id_barang` = k.`id_barang`
		  GROUP BY z.`id_pembelian`) X WHERE (x.`nomor` LIKE '%$kode%' OR x.`nomor_po` LIKE '%$kode%')ORDER BY x.`tanggal` DESC")->result();
		$data = null;
		$idid = null;
		foreach ($q as $row) {
			$checked = null;
			if (empty($row->id_header)) {
				$row->id_header = 0;
			}
			$idid = $row->id_header . ',' . $row->id_pembelian;
			for ($a = 0; $a < count($check); $a++) {
				if ($check[$a][0] == $idid) {
					$checked = 'checked="checked"';
				}
			}
			$total = $row->total - $row->diskon + $row->ongkir;
			$data .= "<div class='row'>
			<div class='col-md-1 text-center'><input type='checkbox' class='check_po_" . $row->id_header . "_" . $row->id_pembelian . "' onclick='set_up(this,$id,$row->id_header,$row->id_pembelian);' name='po' value=" . $row->id_header . "," . $row->id_pembelian . " " . $checked . "></div>
			<div class='col-md-2'>$row->nomor</div>
			<div class='col-md-3'>$row->nomor_po</div>
			<div class='col-md-2'>$row->supplier</div>
			<div class='col-md-2 text-center'>$row->tanggal</div>
			<div class='col-md-2 text-right' style='padding-right:15px'>" . number_format($total, 0) . "</div></div>";
		}
		$res = array();
		$res = array(
			'html' => $data
		);
		return $res;
	}

	function select_po2()
	{
		$id = $this->input->post('id');
		$total = 0;
		$q = $this->db->query("SELECT * FROM (SELECT
		z.`id_header`,
		z.`nomor`,
		z.`tanggal`,
		z.`id_pembelian`,
		z.`id_perusahaan`,
		z.`supplier`,
		z.`nomor_po`,
		z.`tgl_pembelian`,
		z.`id_barang`,
		z.`nama_barang`,SUM(
			IF(
			  z.`qty_konversi` IS NULL,
			  z.`qty`,
			  ROUND((z.`qty` / z.`qty_konversi`))
			)
		  ) AS jumlah,
		  z.`ukuran`,
		  z.`harga`,
		  z.`diskon`,
		  z.`ongkir`,
		SUM(
			  IF(
				z.`qty_konversi` IS NULL,
				z.`qty`,
				ROUND((z.`qty` / z.`qty_konversi`))
		)* z.`harga`) AS total,
		  z.`keterangan`
	  FROM
		(SELECT
		  d.`id_header`,
		  h.`nomor`,
		  h.`tanggal`,
		  h.`id_pembelian`,
		  b.`id_perusahaan`,
		  b.`supplier`,
		  b.`nomor_po`,
		  b.`tgl_pembelian`,
		  d.`id_barang`,
		  br.`nama_barang`,
		  k.`qty_konversi`,
		  d.`qty`,
		  (SELECT
			nama_ukuran
		  FROM
		  inventory_v2.`ms_ukuran` p
		  WHERE p.`id_ukuran` = d.`id_ukuran`) ukuran,
		  b.`harga`,
		  b.`diskon`,
		  b.`ongkir`,
		  b.`keterangan`
		FROM
		  inventory_v2.`tr_d_penerimaan` d
		  JOIN inventory_v2.`ms_header_barang` br
			ON d.`id_barang` = br.`id_header`
		  JOIN inventory_v2.`tr_h_penerimaan` h
			ON d.`id_header` = h.`id_header`
		  LEFT JOIN inventory_v2.`ms_konversi` k
			ON d.`id_barang` = k.`id_barang`
		  JOIN
			(SELECT
			  d.`id_header` AS id_pembelian,
			  h.`id_perusahaan`,
			  p.`nama` AS supplier,
			  h.`nomor` AS nomor_po,
			  h.`tanggal` AS tgl_pembelian,
			  d.`id_barang`,
			  d.`harga`,
			  h.`diskon`,
			  h.`ongkir`,
			  d.`total`,
			  IF(
				h.`keterangan` IS NOT NULL,
				h.`keterangan`,
				''
			  ) AS keterangan
			FROM
			  inventory_v2.`tr_d_pembelian` d
			  JOIN inventory_v2.`tr_h_pembelian` h
				ON d.`id_header` = h.`id_header`
			  JOIN inventory_v2.`ms_perusahaan` p
				ON h.`id_perusahaan` = p.`id_perusahaan`
			WHERE p.`id_perusahaan`=$id) b
			ON h.`id_pembelian` = b.`id_pembelian`
			AND d.`id_barang` = b.`id_barang`
		WHERE br.`flag`='B') z
	  GROUP BY id_header
	  UNION ALL 
	  SELECT
		z.`id_header`,
		z.`nomor`,
		z.`tanggal`,
		z.`id_pembelian`,
		z.`id_perusahaan`,
		z.`supplier`,
		z.`nomor_po`,
		z.`tgl_pembelian`,
		z.`id_barang`,
		z.`nama_barang`,
		SUM(
		  IF(
			k.`qty_konversi` IS NULL,
			z.`qty`,
			ROUND((z.`qty` / k.`qty_konversi`))
		  )
		) AS jumlah,
		(SELECT
		  nama_ukuran
		FROM
		inventory_v2.`ms_ukuran` p
		WHERE p.`id_ukuran` = z.`id_ukuran`) ukuran,
		z.`harga`,
		z.`diskon`,
		z.`ongkir`,
		SUM(
			IF(
			  k.`qty_konversi` IS NULL,
			  z.`qty`,
			  ROUND((z.`qty` / k.`qty_konversi`))
		) * z.`harga`)AS total,
		z.`keterangan`
	  FROM
		(SELECT
		  NULL AS id_header,
		  NULL AS nomor,
		  h.`tanggal` AS tanggal,
		  d.`id_header` AS id_pembelian,
		  h.`id_perusahaan`,
		  p.`nama` AS supplier,
		  h.`nomor` AS nomor_po,
		  h.`tanggal` AS tgl_pembelian,
		  d.`id_barang`,
		  br.`nama_barang`,
		  d.`harga`,
		  h.`diskon`,
		  h.`ongkir`,
		  d.`id_ukuran`,
		  d.`qty`,
		  d.`total`,
		  IF(
			h.`keterangan` IS NOT NULL,
			h.`keterangan`,
			''
		  ) AS keterangan
		FROM
		  inventory_v2.`tr_d_pembelian` d
		  JOIN inventory_v2.`tr_h_pembelian` h
			ON d.`id_header` = h.`id_header`
		  JOIN inventory_v2.`ms_perusahaan` p
			ON h.`id_perusahaan` = p.`id_perusahaan`
		  JOIN inventory_v2.`ms_header_barang` br
			ON d.`id_barang` = br.`id_header`
		WHERE p.`id_perusahaan`=$id AND NOT EXISTS (SELECT a.`id_header` FROM inventory_v2.`tr_h_penerimaan` a
        JOIN inventory_v2.`tr_d_penerimaan` c
        ON a.`id_header`=c.`id_header`
        JOIN inventory_v2.`ms_header_barang` b
          ON c.`id_barang` = b.`id_header`
      WHERE a.`id_pembelian` = d.`id_header`)) z
    LEFT JOIN inventory_v2.`ms_konversi` k
		  ON z.`id_barang` = k.`id_barang`
		  GROUP BY z.`id_pembelian`) X ORDER BY x.`tanggal` DESC")->result();
		$data = null;
		$data .= '
	<tbody>';
		foreach ($q as $row) {
			if (empty($row->id_header)) {
				$row->id_header = 0;
			}
			$total = $row->total - $row->diskon + $row->ongkir;
			$data .= "<tr>
			<td style='text-align: center;width:60px;padding:0 20px'><input type='checkbox' class='check_po_" . $row->id_header . "_" . $row->id_pembelian . "' onclick='set_up(this,$id,$row->id_header,$row->id_pembelian);' name='po' value=" . $row->id_header . "," . $row->id_pembelian . "></td>
			<td style='text-align: center;width:230px;padding:0 20px'>$row->nomor</td>
			<td style='text-align: center;width:230px;padding:0 20px'>$row->nomor_po</td>
			<td style='text-align: center;width:340px;padding:0 20px'>$row->supplier</td>
			<td style='text-align: center;width:160px;padding:0 20px'>$row->tanggal</td>
			<td style='text-align: right;width:120px;padding:0 20px'>" . number_format($total, 0) . "</td></tr>";
		}
		$data .= "</tbody>";
		$res = array();
		$res = array(
			'html' => $data
		);
		return $res;
	}
}
