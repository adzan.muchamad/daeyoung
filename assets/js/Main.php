<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Main extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_model', '', TRUE);
		$this->load->model('Kamus_model', '', TRUE);
		$this->load->helper('cookie');
	}

	function cek_login()
	{
		$get_cookie = $this->input->cookie('pengajuan');
		// if ($this->session->userdata('login') != '1' || $this->session->userdata('username') == '' || $this->session->userdata('session_id_pengajuan') == '') {
		// 	$this->session->set_flashdata('message', 'Anda harus login terlebih dahulu.');
		// 	$this->session->set_flashdata('notifikasi', 'danger');
		// 	redirect('auth');
		// }
		// $db = $this->load->database('absensi', TRUE);
		if (!empty($get_cookie)) {
			$query = $this->db->get_where('absensi.ms_user', array('session_id_pengajuan' => $get_cookie, 'status' => 1));
			if ($query->num_rows() > 0) {
				$query = $query->row();
				$session = array(
					'session_id_pengajuan' => $get_cookie,
					'id_employee' => $query->id_employee,
					'nama' => $query->keterangan,
					'username' => $query->username,
					'login' => TRUE,
					'flag' => 1
				);
				// print_r($session);exit;
				$this->session->set_userdata($session);
				return TRUE;
			} else {
				$this->session->set_flashdata('message', 'Anda harus login terlebih dahulu.');
				$this->session->set_flashdata('notifikasi', 'danger');
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata('message', 'Anda harus login terlebih dahulu.');
			$this->session->set_flashdata('notifikasi', 'danger');
			redirect('auth');
		}
	}

	function index()
	{
		$this->cek_login();
		$data['stat'] = $this->Main_model->get_from_query('select count(header.id) as jml, sum(detail.nominal) as total from header inner join detail on detail.id_header=header.id where header.timestamp >= "' . date('Y-m-d 00:00:00') . '" AND header.timestamp <= "' . date('Y-m-d 23:59:59') . '" AND header.status!="9"')->row();
		// $query = $this->Main_model->get_from_query('select header.*, sum(detail.nominal) as nominal from header inner join detail on detail.id_header=header.id where header.status="0" GROUP BY header.id');
		// $tab = '';$j=0;
		// if($query->num_rows()){
		// 	foreach($query->result() as $row){
		// 		$c='';
		// 		$user = $this->Main_model->get_from_absensi('ms_user',array('id_employee'=>$row->uid))->row();
		// 		if($row->urgent==1){
		// 			$c = '<span class="m-badge m-badge--success m-badge--wide">Non Urgent</span>';
		// 		}else if($row->urgent==2){
		// 			$c = '<span class="m-badge m-badge--warning m-badge--wide">Medium</span>';
		// 		}else if($row->urgent==3){
		// 			$c = '<span class="m-badge m-badge--danger m-badge--wide">Urgent</span>';
		// 		}
		// 		$tab .= '<tr>
		// 					<td style="vertical-align:middle">'.++$j.'</td>
		// 					<td style="vertical-align:middle">'.$row->nomor.'<br><small>'.$user->keterangan.'</small></td>
		// 					<td style="vertical-align:middle">'.$this->Kamus_model->tanggal_indo($row->timestamp).'</td>
		// 					<td style="vertical-align:middle">'.$this->Kamus_model->uang($row->nominal).'</td>
		// 					<td style="vertical-align:middle">'.$c.'</td>
		// 					<td style="vertical-align:middle">
		// 						<span style="overflow: visible; width: 110px;">
		// 							<a data-toggle="modal" data-target="#m_modal_4" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill trig_modal" id="pengajuan_'.$row->id.'" title="View details">
		// 								<i class="la la-search"></i>
		// 							</a>
		// 							<a href="'.base_url().'main/form_pengajuan/'.$row->id.'" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">
		// 								<i class="la la-edit"></i>
		// 							</a>
		// 							<a href="'.base_url().'main/del_pengajuan/'.$row->id.'/on_progress" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill trig_confirm" title="Delete">
		// 								<i class="la la-trash"></i>
		// 							</a>
		// 						</span>
		// 					</td>
		// 				</tr>';
		// 	}
		// }
		$data['opt_akun'] = preg_replace('/\s+/', ' ', $this->Main_model->get_opt_akun());
		$data['opt_rek2'] = $this->Main_model->get_opt_rekening(1);
		$data['opt_bank'] = $this->Main_model->get_opt_bank();
		// $data['table'] = $tab;
		$this->load->view('head2');
		$this->load->view('dashboard', $data);
		$this->load->view('foot');
	}
	function form_pengajuan($id = '', $ulang = '')
	{
		$this->cek_login();
		$data['edit_h'] = $this->Main_model->get_from_arr('header', array('id' => $id))->row();
		$data['edit_d2'] = $this->Main_model->get_from_arr('detail', array('id_header' => $id, 'flag' => 2, 'status !=' => 9))->result();
		$data['edit_d'] = $this->Main_model->get_from_arr('detail', array('id_header' => $id, 'flag' => 1, 'status !=' => 9))->result();
		$data['ulang'] = $ulang;
		if (!empty($id)) {
			$exp = explode(' ', $data['edit_h']->timestamp);
			$data['tgl'] = $exp[0];
			$data['no'] = $data['edit_h']->nomor;
		} else {
			$last = $this->Main_model->get_from_arr('nomor', array())->row();
			$no = $last->count + 1;
			$data['no'] = $no . '/SF03/' . date('m') . '/' . date('Y');
		}
		$data['opt_atasan'] = $this->Main_model->get_opt_atasan();
		$data['opt_bank'] = $this->Main_model->get_opt_bank();
		$data['opt_jurnal'] = $this->Main_model->get_opt_jurnal();
		$data['opt_rek'] = $this->Main_model->get_opt_rekening();
		$data['opt_rek2'] = $this->Main_model->get_opt_rekening(1);
		$data['opt_akun'] = preg_replace('/\s+/', ' ', $this->Main_model->get_opt_akun());
		$data['opt_akun2'] = $this->Main_model->get_opt_akun2();
		$data['opt_card'] = $this->Main_model->get_opt_card();
		$data['opt_akun3'] = $this->Main_model->get_opt_akun3(null);
		$data['opt_akun4'] = preg_replace('/\s+/', ' ', $this->Main_model->get_opt_akun4(null));
		$data['opt_po'] = preg_replace('/\s+/', ' ', $this->Main_model->get_opt_po());
		$data['opt_po2'] = $this->Main_model->get_opt_po2();
		$this->load->view('head');
		$this->load->view('form_pengajuan', $data);
		$this->load->view('foot');
	}
	function get_card()
	{
		$id = $this->input->post('id');
		$data = $this->Main_model->get_opt_akun4($id);
		echo $data;
	}

	function select_autocomplite()
	{
		echo json_encode($this->Main_model->select_autocomplite());
	}
	function proses_pengajuan()
	{
		// echo $this->session->userdata('username');exit;
		// print_r($_POST);exit;
		// echo 'sedang maintenance, tunggu beberapa saat lagi..';exit;
		$this->cek_login();
		$id = $this->input->post('id');
		$ulang = $this->input->post('ulang');
		$user       = $this->Main_model->get_user($this->session->userdata('session_id_pengajuan'), $this->session->userdata('username'))->row();
		$tanggal = $this->input->post('tanggal');
		$tanggal = date("Y-m-d", strtotime($tanggal));
		// print_r($this->session->userdata('session_id_pengajuan'));exit;
		$data = array(
			'urgent' 	=> $this->input->post('urgent'),
			'rek' 		=> $this->input->post('sumber'),
			'kd_gl' 		=> $this->input->post('kode_jurnal'),
			'timestamp' => $tanggal . ' ' . date('H:i:s'),
			'keterangan' => $this->input->post('note'),
			'uid' 		=> $user->id_employee,
			'atasan' 	=> $this->input->post('atasan')
		);
		if (empty($id)) {
			$last = $this->Main_model->get_from_arr('nomor', array())->row();
			$no = $last->count + 1;
			$this->Main_model->update('nomor', array('count' => $no), array('id' => 1));
			$no = $no . '/SF03/' . date('m') . '/' . date('Y');
			$data['nomor'] = $no;
		} else {
			if (!empty($ulang)) {
				$data['nomor'] = $no = $this->input->post('nomor');
				$data['flag'] = 2;
			}
		}

		if ((!empty($id)) and (empty($ulang))) {
			$this->Main_model->update('header', $data, array('id' => $id));
		} else {
			$id = $this->Main_model->insert('header', $data, 1);
			// $get_last = $this->Main_model->get_from_arr('header', $data, '1')->row();
			// $id = $get_last->id;
		}

		$id_an = $this->input->post('id_an');
		$pemohon = $this->input->post('pemohon');
		$nominal = $this->input->post('nominal');
		$idnominal = $this->input->post('id_nominal');
		$idakun = $this->input->post('id_akun');
		$idcard = $this->input->post('id_card');
		foreach ($pemohon as $key => $val) {
			if ($id_an[$key] == 'new') {
				$dat = array(
					'nama' => $this->input->post('an')[$key],
					'bank' => $this->input->post('bank')[$key],
					'rek' => $this->input->post('rek')[$key]
				);
				$this->Main_model->insert('rekening', $dat);
			}
			if (empty($val)) {
				continue;
			}
			$id2 = $this->input->post('id2')[$key];
			$data = array(
				'pemohon' 	=> $val,
				'id_header' => $id,
				// 'nominal' 	=> str_replace(',','',$nominal[$key]),
				'id_rek' => $id_an[$key],
				'keterangan' => $this->input->post('keterangan')[$key],
				'bank' 		=> $this->input->post('bank')[$key],
				'rek'		=> $this->input->post('rek')[$key],
				'an' 		=> $this->input->post('an')[$key],
				'flag' 		=> '1'
			);
			$id_det = $id2;
			if (!empty($id2)) {
				if (!empty($ulang)) {
					// if(empty($nominal[$key])){continue;}
					$id_det = $this->Main_model->insert('detail', $data, 1);
				} else {
					// if(empty($nominal[$key])){
					// $this->Main_model->update('detail', array('status'=>9), array('id'=>$id2));
					// }else{
					$this->Main_model->update('detail', $data, array('id' => $id2));
					// }
				}
			} else {
				$id_det = $this->Main_model->insert('detail', $data, 1);
			}

			$keynom = $key + 1;
			$tot = 0;
			if (!empty($nominal[$keynom])) {
				for ($w = 0; $w < sizeof($nominal[$keynom]); $w++) {
					$tot += str_replace(',', '', $nominal[$keynom][$w]);
					$data = array(
						'id_detail' 	=> $id_det,
						'id_akun' 		=> $idakun[$keynom][$w],
						'id_card'		=> $idcard[$keynom][$w],
						'nominal' 		=> str_replace(',', '', $nominal[$keynom][$w])
					);
					if (empty($idnominal[$keynom][$w])) {
						if (empty($nominal[$keynom][$w])) {
							continue;
						} else {
							$this->Main_model->insert('nominal', $data);
						}
					} else {
						if (empty($nominal[$keynom][$w])) {
							// print_r($idnominal[$keynom][$w]);exit;
							$this->Main_model->update('nominal', array('status' => '9'), array('id' => $idnominal[$keynom][$w]));
						} else {
							// print_r('a');exit;
							$this->Main_model->update('nominal', $data, array('id' => $idnominal[$keynom][$w]));
						}
					}
				}
			}
			$this->Main_model->update('detail', array('nominal' => $tot), array('id' => $id_det));
		}

		$po = $this->input->post('no');
		$nominal = $this->input->post('nominal2');
		$idnominal = $this->input->post('id_nominal2');
		$idakun = $this->input->post('id_akun2');
		$idcard = $this->input->post('id_card_1');
		foreach ($po as $key => $val) {
			if (empty($this->input->post('po')[$key])) {
				continue;
			}
			$id3 = $this->input->post('id3')[$key];
			$data = array(
				'pemohon' 	=> $this->input->post('po')[$key],
				'id_header' => $id,
				'id_po' 	=> $val,
				// 'nominal' 	=> str_replace(',','',$nominal[$key]),
				'id_rek' => 0,
				'keterangan' => (!empty($this->input->post('keterangan2')[$key]) ? $this->input->post('keterangan2')[$key] : ''),
				'bank' 		=> (!empty($this->input->post('bank2')[$key]) ? $this->input->post('bank2')[$key] : ''),
				'rek'		=> (!empty($this->input->post('rek2')[$key]) ? $this->input->post('rek2')[$key] : ''),
				'an' 		=> (!empty($this->input->post('an2')[$key]) ? $this->input->post('an2')[$key] : ''),
				'invoice' 	=> (!empty($this->input->post('invoice')[$key]) ? $this->input->post('invoice')[$key] : ''),
				'flag' 		=> '2'
			);

			// $invc = '';
			// if(!empty($this->input->post('invoice')[$key])){
			// $invc = $this->input->post('invoice')[$key];
			// }
			// $data['invoice'] = $invc;

			$id_det = $id3;
			if (!empty($id3)) {
				if (!empty($ulang)) {
					// if(empty($nominal[$key])){continue;}
					$id_det = $this->Main_model->insert('detail', $data, 1);
				} else {
					// if(empty($nominal[$key])){
					// $this->Main_model->update('detail', array('status'=>9), array('id'=>$id3));
					// }else{
					$this->Main_model->update('detail', $data, array('id' => $id3));
					// }
				}
			} else {
				$id_det = $this->Main_model->insert('detail', $data, 1);
			}

			$keynom = $key + 1;
			$tot = 0;
			if (!empty($nominal[$keynom])) {
				for ($w = 0; $w < sizeof($nominal[$keynom]); $w++) {
					$tot += str_replace(',', '', $nominal[$keynom][$w]);
					$data = array(
						'id_detail' 	=> $id_det,
						'id_akun' 		=> $idakun[$keynom][$w],
						'id_card' 		=> $idcard[$keynom][$w],
						'nominal' 		=> str_replace(',', '', $nominal[$keynom][$w])
					);
					if (empty($idnominal[$keynom][$w])) {
						if (empty($nominal[$keynom][$w])) {
							continue;
						} else {
							$this->Main_model->insert('nominal', $data);
						}
					} else {
						if (empty($nominal[$keynom][$w])) {
							$this->Main_model->update('nominal', array('status' => '9'), array('id' => $idnominal[$keynom][$w]));
						} else {
							$this->Main_model->update('nominal', $data, array('id' => $idnominal[$keynom][$w]));
						}
					}
				}
			}
			$this->Main_model->update('detail', array('nominal' => $tot), array('id' => $id_det));
		}

		$user       = $this->Main_model->get_user($this->session->userdata('session_id_pengajuan'), $this->session->userdata('username'))->row();
		$employee   = $this->Main_model->get_employee($user->id_employee)->row();
		// $email_a    = $this->Marketing_model->get_email('Z')->row();
		$from       = $employee->email;
		// $to         = 'priyo@gmedia.co.id, adhi@gmedia.co.id';
		$to         = 'adhi.darminto@gmedia.co.id';
		// $to         = 'dwi.aji@gmedia.co.id, maulana.husni@gmedia.co.id';
		$cc         = '';
		$bcc        = '';
		$title2		= '';
		if (!empty($ulang)) {
			$title2 	= 'Pengajuan Dana Ulang ' . $no;
			$subject    = '[APV] Pengajuan Dana Ulang ' . $no;
		} else {
			$title2 	= 'Pengajuan Dana Baru ' . $no;
			$subject    = '[APV] Pengajuan Dana ' . $no;
		}
		$email      = $this->email_notif($id);
		$title      = $employee->nama;
		$this->Kamus_model->kirim_email($title, $from, $to,  $subject, $email, $cc, '', '', $bcc);

		$this->notif_mobile($title2, $this->input->post('note'));

		$this->session->set_flashdata('message', 'Data berhasil tersimpan');
		$this->session->set_flashdata('notifikasi', 'success');
		redirect('main/data_pengajuan/on_progress');
	}
	function email_notif($id)
	{
		$h = $this->Main_model->get_from_arr('header', array('id' => $id))->row();
		$d = $this->Main_model->get_from_arr('detail', array('id_header' => $id, 'status !=' => 9))->result();
		$b = $this->Main_model->get_from_arr('erp_financev2.gmd_finance_coa_card_name', array('id' => $h->rek))->row();

		$res = $c = '';
		if ($h->urgent == 1) {
			$c = '<span style="background-color:#3787FE;padding-top:3px;padding-bottom:3px;padding-left:15px;padding-right:15px;color:white;">Non Urgent<span>';
		} else if ($h->urgent == 2) {
			$c = '<span style="background-color:#FEAE37;padding-top:3px;padding-bottom:3px;padding-left:15px;padding-right:15px;color:black;">Medium<span>';
		} else if ($h->urgent == 3) {
			$c = '<span style="background-color:#FE3737;padding-top:3px;padding-bottom:3px;padding-left:15px;padding-right:15px;color:white;">Urgent<span>';
		}
		$res .= '<h4>PENGAJUAN TRANSFER DANA</h4>
				<table>
					<tr>
						<td colspan="3">' . $this->Kamus_model->tanggal($h->timestamp) . '</td>
					</tr>
					<tr>
						<td>Nomor Pengajuan</td>
						<td style="text-align:center;width:30px;"> : </td>
						<td>' . $h->nomor . '</td>
					</tr>
					<tr>
						<td>Urgency level</td>
						<td style="text-align:center;width:30px;"> : </td>
						<td>' . $c . '</td>
					</tr>
					<tr>
						<td>Rekening Sumber</td>
						<td style="text-align:center;width:30px;"> : </td>
						<td>' . $b->nama . '</td>
					</tr>
					<tr>
						<td>Keterangan</td>
						<td style="text-align:center;width:30px;"> : </td>
						<td>' . $h->keterangan . '</td>
					</tr>
				</table><br>
				<table style="float:left;border-collapse: collapse;border: 1px solid #e7ecf1;padding: 12px;">
					<tr>
						<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:center;background-color:#C9C9FF"><b>No</b></td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:center;background-color:#C9C9FF"><b>Diajukan Oleh</b></td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:center;background-color:#C9C9FF"><b>Keterangan</b></td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:center;background-color:#C9C9FF"><b>Pembayaran Ke</b></td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:center;background-color:#C9C9FF"><b>Nominal</b></td>
					</tr>';
		$tot = $k = 0;
		foreach ($d as $row) {
			$rup = 'Rp';
			// if(!empty($row->id_po)){
			// $curr = $this->Main_model->get_from_query('select a.* from inventory_v2.tr_h_pembelian a where a.id_header="'.$row->id_po.'"')->row();
			// if($curr->id_currency==2){
			// $rup = '$';				
			// }
			// }

			if ($row->id_po == 0) {
				$pmohon = $row->pemohon;
				$ktrangan = $h->keterangan;
			} else {
				$user = $this->Main_model->get_from_absensi('hrd.ms_employee', array('id' => $h->uid))->row();
				$pmohon = $user->nama;
				$ktrangan = $row->pemohon;
			}
			$res .= '<tr>
						<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:center">' . ++$k . '</td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;">' . $pmohon . '</td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;">' . $ktrangan . '</td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;">' . $row->bank . ' : ' . $row->rek . ' (' . $row->an . ')</td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:right">' . $rup . ' ' . $this->Kamus_model->uang($row->nominal) . '</td>
					</tr>';
			$tot += $row->nominal;
		}
		$res .= '<tr>
						<td> </td>
						<td> </td>
						<td></td>
						<td></td>
						<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:right"><b>' . $rup . ' ' . $this->Kamus_model->uang($tot) . '</b></td>
					</tr>';
		$res .= '</table>';

		// echo $res;
		return $res;
	}
	function notif_mobile($title, $desc)
	{
		$url = 'https://fcm.googleapis.com/fcm/send';
		$method = 'POST';
		$parameter = '{ 
		  "notification": 
				 { 
					"title": "' . $title . '",
					"text": "' . $desc . '", 
					"click_action": "ACT_MAIN", 
					"sound" : "content://settings/system/notification_sound" 
				  },
		 "data": 
				 {
				   "Jenis":"main" 
				 },
		 "to" : "/topics/gmedia_finance", "priority": "high" 
		}';
		$list_header[] = "Content-Type:application/json";
		$list_header[] = "Authorization:key=AAAA1dvskuE:APA91bH-BGjNfjpAYwUymullCGRaHb_rSix70ApswuSRIZfvmDKqQwqGgjOu96GThG4T00C3gKlAp6yRKEwPHxZ40yPTTQ9knHev0GrFHS3p-VSozRtkkQOTp1WMEnOxiCuYrTHkaxJK";

		$curl = curl_init();
		curl_setopt_array(
			$curl,
			array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 10000,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => $method,
				CURLOPT_POSTFIELDS => str_replace("\r\n", "", $parameter),
				CURLOPT_HTTPHEADER => $list_header,
				CURLOPT_FRESH_CONNECT => true
			)
		);
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
	}

	function data_pengajuan($flag = 'on_progress')
	{
		$this->cek_login();
		if ($flag == 'on_progress') {
			$data['flagt'] = 'On Progress';
			$n = 0;
		} else if ($flag == 'approved') {
			$data['flagt'] = 'Approved';
			$n = 1;
		} else if ($flag == 'rejected') {
			$data['flagt'] = 'Rejected';
			$n = 2;
		} else if ($flag == 'complete') {
			$data['flagt'] = 'Complete';
			$n = 3;
		} else if ($flag == 'cancel') {
			$data['flagt'] = 'Canceled';
			$n = 9;
		}
		$query = $this->Main_model->get_from_query('select b.nama AS cabang, header.*, sum(detail.nominal) as nominal, detail.id_po, detail.pemohon from header inner join erp_financev2.gmd_finance_coa_card_name b on b.id=header.rek left join detail on detail.id_header=header.id where header.status="' . $n . '" GROUP BY header.id');
		$tab = '';
		$j = 0;
		if ($query->num_rows()) {

			foreach ($query->result() as $row) {
				$rek_tujuan = '';
				$totale = '';
				$detail = $this->Main_model->get_from_query('select d.*, IFNULL(r.nama,d.an) AS nama_rekening from detail d
				LEFT JOIN rekening r ON d.id_rek=r.id
				where d.id_header="' . $row->id . '" group by d.rek')->result();
				foreach ($detail as $row2) {
					if (empty($row->nama_rekening)) {
						$detail = $this->Main_model->get_from_query('select d.*, IFNULL(r.nama,d.an) AS nama_rekening from detail d
																LEFT JOIN rekening r ON d.rek=r.rek AND d.rek!=""
																where d.id_header="' . $row->id . '" group by d.rek')->result();
						continue;
					}
				}
				$l = 0;
				$total = 0;
				foreach ($detail as $row2) {
					$l++;
					$rek_tujuan .= '<small>' . $l . '. ' . $row2->bank . ' ' . $row2->rek . ' ' . $row2->nama_rekening . ' </small><br style="mso-data-placement:same-cell;" />';
					if (sizeof($detail) > 1) {
						$totale .= '<small>' . $l . '. Rp ' . $this->Kamus_model->uang($row2->nominal) . ' </small><br style="mso-data-placement:same-cell;" />';
					}
					$total = $total + $row2->nominal;
				}
				if (sizeof($detail) > 1) {
					$totale .= '<small>Total Rp ' . $this->Kamus_model->uang($total) . ' </small>';
				} else {
					$totale = $this->Kamus_model->uang2($row->nominal);
				}

				$act = '';
				if ($row->urgent == 1) {
					$c = '<span class="m-badge m-badge--success m-badge--wide">Non Urgent</span>';
				} else if ($row->urgent == 2) {
					$c = '<span class="m-badge m-badge--warning m-badge--wide">Medium</span>';
				} else if ($row->urgent == 3) {
					$c = '<span class="m-badge m-badge--danger m-badge--wide">Urgent</span>';
				}

				if ($flag == 'on_progress') {
					$act = '<span style="overflow: visible; width: 110px;">
								<a data-toggle="modal" data-target="#m_modal_4" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill trig_modal" id="pengajuan_' . $row->id . '" title="View details">
									<i class="la la-search"></i>
								</a>
								<a href="' . base_url() . 'main/form_pengajuan/' . $row->id . '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">
									<i class="la la-edit"></i>
								</a>
								<a href="' . base_url() . 'main/del_pengajuan/' . $row->id . '/' . $flag . '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill trig_confirm" title="Delete">
									<i class="la la-trash"></i>
								</a>
							</span>';
				} else if ($flag == 'approved') {
					$act = '<span style="overflow: visible; width: 110px;">
								<a data-toggle="modal" data-target="#m_modal_4" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill trig_modal" id="pengajuan_' . $row->id . '" title="View details">
									<i class="la la-search"></i>
								</a>
								<a data-toggle="modal" data-target="#m_modal_1" id="idne_' . $row->id . '_' . $row->rek . '" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill trig_complete" title="Complete">
									<i class="la la-check-circle"></i>
								</a>
								<a href="' . base_url() . 'main/del_pengajuan/' . $row->id . '/' . $flag . '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill trig_confirm" title="Cancel">
									<i class="la la-trash"></i>
								</a>
							</span>';
				} else if ($flag == 'rejected') {
					$act = '<span style="overflow: visible; width: 110px;">
								<a data-toggle="modal" data-target="#m_modal_4" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill trig_modal" id="pengajuan_' . $row->id . '" title="View details">
									<i class="la la-search"></i>
								</a>
								<a href="' . base_url() . 'main/form_pengajuan/' . $row->id . '/ulang" class="m-portlet__nav-link btn m-btn m-btn--hover-warning m-btn--icon m-btn--icon-only m-btn--pill" title="Pengajuan Ulang">
									<i class="la la-reply"></i>
								</a>
							</span>';
				} else if ($flag == 'complete') {
					$act = '<span style="overflow: visible; width: 110px;">
								<a data-toggle="modal" data-target="#m_modal_4" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill trig_modal" id="pengajuan_' . $row->id . '" title="View details">
									<i class="la la-search"></i>
								</a>
								
							</span>';
				} else if ($flag == 'cancel') {
					$act = '<span style="overflow: visible; width: 110px;">
								<a data-toggle="modal" data-target="#m_modal_4" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill trig_modal" id="pengajuan_' . $row->id . '" title="View details">
									<i class="la la-search"></i>
								</a>
								
							</span>';
				}
				// $user = $this->Main_model->get_from_absensi('ms_employee',array('id'=>$row->uid))->row();
				$user = $this->Main_model->get_from_absensi('absensi.ms_user', array('id_employee' => $row->uid))->row();
				$ket = $row->keterangan;
				if (!empty($row->id_po)) {
					$ket = $row->pemohon;
				}
				$tab .= '<tr>
							<td style="vertical-align:middle">' . ++$j . '</td>
							<td style="vertical-align:middle">' . $row->nomor . '<br><small> ' . $user->keterangan . '</small></td>
							<td style="vertical-align:middle">' . $row->cabang . '</td>
							<td style="vertical-align:middle">' . $rek_tujuan . '</td>
							<td style="vertical-align:middle">' . $this->Kamus_model->tanggal_indo($row->timestamp) . '</td>
							<td style="vertical-align:middle">' . $totale . '</td>
							<td style="vertical-align:middle">' . $ket . '</td>
							<td style="vertical-align:middle">' . $c . '</td>
							<td style="vertical-align:middle">' . $act . '</td>
						</tr>';
			}
		}
		$data['table'] = $tab;
		$data['opt_bank'] = $this->Main_model->get_opt_bank();
		// $data['opt_rek2'] = $this->Main_model->get_opt_rekening(1);
		$this->load->view('head');
		$this->load->view('table', $data);
		$this->load->view('foot');
	}
	function del_pengajuan($id, $flag)
	{
		$this->cek_login();
		$this->Main_model->update('header', array('status' => 9), array('id' => $id));
		$this->session->set_flashdata('message', 'Data berhasil dihapus');
		$this->session->set_flashdata('notifikasi', 'success');
		redirect('main/data_pengajuan/' . $flag);
	}
	function complete()
	{
		// echo 'sedang maintenance 1 menit';exit;
		$this->cek_login();
		$all_bukti = $this->input->post('all_bukti');
		$id = $this->input->post('id');
		$tanggal = $this->input->post('tanggal');
		$files = $_FILES['bukti']['name'];
		$bukti = $_FILES['bukti'];
		foreach ($files as $key => $val) {
			// for($m=0;$m<sizeof($files['bukti']['name']);$m++){
			$name   				 = str_replace(' ', '', $val);
			$acak                    = rand(000000, 999999);
			$nama_file               = $acak . '-' . $name;
			if (!empty($name)) {
				$_FILES['file']['name'] = $nama_file;
				$_FILES['file']['type'] = $bukti['type'][$key];
				$_FILES['file']['tmp_name'] = $bukti['tmp_name'][$key];
				$_FILES['file']['error'] = $bukti['error'][$key];
				$_FILES['file']['size'] = $bukti['size'][$key];
				$config['upload_path'] = './assets/bukti/';
				$config['allowed_types']    = 'jpg|jpeg|png|pdf';
				$config['overwrite']	= true;
				$this->load->library('upload', $config);
				$upload  = $this->upload->do_upload('file', $nama_file);
				//---------------------------------------------------------
				if ($upload == FALSE) {
					$error = array('error' => $this->upload->display_errors());
					// echo $error['error'];exit;
					$this->session->set_flashdata('message', $error['error']);
					$this->session->set_flashdata('notifikasi', 'error');
					redirect('main/data_pengajuan/approved');
				} else {

					$data = array(
						"bukti" => $nama_file
					);
					if ($all_bukti == 'on') {
						$this->Main_model->update('detail', $data, array('id_header' => $id));
						break;
					} else {
						$this->Main_model->update('detail', $data, array('id' => $key));
					}
				}
			}
		}
		$this->Main_model->update('header', array('pembayaran' => $this->input->post('bank'), 'tgl_pembayaran' => $tanggal, 'status' => 3), array('id' => $id));
		$this->session->set_flashdata('message', 'Data berhasil diproses');
		$this->session->set_flashdata('notifikasi', 'success');
		redirect('main/data_pengajuan/approved');
	}
	function ajax_get_detail()
	{
		$detail = $this->Main_model->get_from_arr('detail', array('id_header' => $_POST['id'], 'status !=' => 9))->result();
		$tmpl = array('table_open' => '<table class="table table-striped table-bordered table-hover table-header-fixed" id="sample_1">');
		$this->table->set_template($tmpl);
		$this->table->set_empty("&nbsp");
		$this->table->set_heading('No', 'Pengajuan Dari', 'Nominal', 'Upload Bukti');
		$i = 0;
		foreach ($detail as $row => $val) {
			$class_attr = '';
			if ($row > 0) {
				$class_attr = 'disabled-form';
			}
			$btn = '<input name="bukti[' . $val->id . ']" required type="file" class="form-control m-input ' . $class_attr . '">';

			$this->table->add_row(++$i, $val->pemohon, $this->Kamus_model->uang($val->nominal), $btn);
		}
		echo $this->table->generate();
	}
	function ajax_get_pengajuan()
	{
		// $id=1341;
		$h = $this->Main_model->get_from_arr('header', array('id' => $_POST['id']))->row();
		// $d = $this->Main_model->get_from_arr('detail', array('id_header'=>$_POST['id'],'status !='=>9))->result();
		$d = $this->Main_model->get_from_arr('detail', array('id_header' => $_POST['id']))->result();
		$b = $this->Main_model->get_from_arr('erp_financev2.gmd_finance_coa_card_name', array('id' => $h->rek))->row();

		$res = $c = '';
		if ($h->urgent == 1) {
			$c = '<span style="background-color:#3787FE ;padding-top:3px;padding-bottom:3px;padding-left:15px;padding-right:15px;color:white;">Non Urgent<span>';
		} else if ($h->urgent == 2) {
			$c = '<span style="background-color:#FEAE37;padding-top:3px;padding-bottom:3px;padding-left:15px;padding-right:15px;color:black;">Medium<span>';
		} else if ($h->urgent == 3) {
			$c = '<span style="background-color:#FE3737;padding-top:3px;padding-bottom:3px;padding-left:15px;padding-right:15px;color:white;">Urgent<span>';
		}
		$res .= '<table>
					<tr>
						<td colspan="3">' . $this->Kamus_model->tanggal($h->timestamp) . '</td>
					</tr>
					<tr>
						<td>Nomor Pengajuan</td>
						<td style="text-align:center;width:30px;"> : </td>
						<td>' . $h->nomor . '</td>
					</tr>
					<tr>
						<td>Urgency level</td>
						<td style="text-align:center;width:30px;"> : </td>
						<td>' . $c . '</td>
					</tr>
					<tr>
						<td>Rekening Sumber</td>
						<td style="text-align:center;width:30px;"> : </td>
						<td>' . $b->nama . '</td>
					</tr>
					<tr>
						<td>Keterangan</td>
						<td style="text-align:center;width:30px;"> : </td>
						<td>' . $h->keterangan . '</td>
                    </tr>';
		if ($h->tgl_pembayaran) {
			$res .= '<tr>
                        <td>Tanggal Pembayaran</td>
                        <td style="text-align:center;width:30px;"> : </td>
                        <td>' . $this->Kamus_model->tanggal_indo($h->tgl_pembayaran) . '</td>
                    </tr>';
		}
		if ($h->pembayaran) {
			$b2 = $this->Main_model->get_from_arr('erp_financev2.gmd_finance_coa_card_name', array('id' => $h->pembayaran))->row();
			$res .= '<tr>
                        <td>Bank Pembayaran</td>
                        <td style="text-align:center;width:30px;"> : </td>
                        <td>' . $b2->nama . '</td>
                    </tr>';
		}
		$get_atasan = $this->Main_model->get_from_query("select a.* from absensi.ms_user a where a.id_employee='" . $h->atasan . "'")->row();
		$res .= '<tr>
                        <td>Pengajuan Ke</td>
                        <td style="text-align:center;width:30px;"> : </td>
                        <td>' . $get_atasan->keterangan . '</td>
                    </tr>';
		$res .= '</table><br>
				<table class="table m-table m-table--head-bg-brand">
					<thead>
						<tr>
							<th style="text-align:center;"><b>No</b></th>
							<th style="text-align:center;"><b>Diajukan Oleh</b></th>
							<th style="text-align:center;"><b>Rekening Tujuan</b></th>
							<th style="text-align:center;"><b>Keterangan</b></th>
							<th style="text-align:center;"><b>Pembayaran Ke</b></th>
							<th style="text-align:center;"><b>Bukti</b></th>
							<th style="text-align:center;"><b>Nominal</b></th>
						</tr>
					</thead><tbody>';
		$tot = $k = 0;
		foreach ($d as $row) {
			$rup = 'Rp';
			// if(!empty($row->id_po)){
			// $curr = $this->Main_model->get_from_query('select a.* from inventory_v2.tr_h_pembelian a where a.id_header="'.$row->id_po.'"')->row();
			// if($curr->id_currency==2){
			// $rup = '$';				
			// }
			// }
			$bkti = '';
			if ($row->bukti) {
				$bkti = '<a href="' . base_url() . 'assets/bukti/' . $row->bukti . '" target="_blank"><i class="la la-search"></i></a>';
			}
			$res .= '<tr>
						<th style="vertical-align:middle" scope="row">' . ++$k . '</th>
						<td style="vertical-align:middle">' . $row->pemohon . '</td>
						<td style="vertical-align:middle">' . $row->rek . '</td>
						<td style="vertical-align:middle">' . $row->keterangan . '</td>
						<td style="vertical-align:middle">' . $row->bank . ': ' . $row->rek . '<br>an. ' . $row->an . '</td>
						<td style="vertical-align:middle">' . $bkti . '</td>
						<td style="vertical-align:middle;text-align:right">' . $rup . ' ' . $this->Kamus_model->uang($row->nominal) . '</td>
					</tr>';
			$tot += $row->nominal;
		}
		$res .= '<tr>
					<td> </td>
					<td> </td>
					<td> </td>
					<td></td>
					<td></td>
					<td></td>
					<td style="border: 1px solid #e7ecf1;padding: 12px;text-align:right"><b>' . $rup . ' ' . $this->Kamus_model->uang($tot) . '</b></td>
				</tr>';
		$res .= '</tbody></table>';
		echo 'Detail Pengajuan___' . $res;
	}
	function ajax_po()
	{
		$get = $this->Main_model->get_info_po($_POST['id'])->row();
		$invoice = $this->Main_model->get_info_invoice($_POST['id'])->row();
		$nom = $this->Main_model->get_from_query("select sum(b.nominal) as nominale from detail a
		inner join nominal b on a.id=b.id_detail
		where a.id_po='" . $_POST['id'] . "' and b.`status`='1' and a.`status`='1'
		group by a.id_po")->row();
		$nominal = 0;
		if ($nom) {
			$nominal = $get->total - $nom->nominale;
		} else {
			$nominal = $get->total;
		}

		// get sisa nominal //
		$get_data = $this->Main_model->select_autocomplite_id($_POST['id']);
		$nominal  = isset($get_data->total) ? $get_data->total : 0;

		$no = '';
		if (!empty($invoice)) {
			$no = $invoice->invoice;
		}
		echo $nominal . '__' . strip_tags($get->keterangan) . '__' . $get->bank . '__' . $get->norek . '__' . $get->pic . '__' . $get->nomor . '__' . $no;
	}
	function ajax_rek()
	{
		if (!empty($_POST['id'])) {
			$get = $this->Main_model->get_from_arr('rekening', array('id' => $_POST['id']))->row();
			echo $get->nama . '__' . $get->bank . '__' . $get->rek;
		}
	}

	function search()
	{
		$this->cek_login();
		$data['key'] = $key = $this->input->post('key');
		$query = $this->Main_model->get_from_query('select header.*, sum(detail.nominal) as nominal from header left join detail on detail.id_header=header.id left join inventory_v2.tr_h_pembelian c on c.id_header=detail.id_po where (header.nomor like "%' . $key . '%") or (c.nomor like "%' . $key . '%") GROUP BY header.id');
		$tab = '';
		$j = 0;
		if ($key) {
			if ($query->num_rows()) {
				foreach ($query->result() as $row) {
					if ($row->urgent == 1) {
						$c = '<span class="m-badge m-badge--success m-badge--wide">Non Urgent</span>';
					} else if ($row->urgent == 2) {
						$c = '<span class="m-badge m-badge--warning m-badge--wide">Medium</span>';
					} else if ($row->urgent == 3) {
						$c = '<span class="m-badge m-badge--danger m-badge--wide">Urgent</span>';
					}
					$act = '<span style="overflow: visible; width: 110px;">
								<a data-toggle="modal" data-target="#m_modal_4" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill trig_modal" id="pengajuan_' . $row->id . '" title="View details">
									<i class="la la-search"></i>
								</a>
								
							</span>';
					$user = $this->Main_model->get_from_absensi('absensi.ms_user', array('id_employee' => $row->uid))->row();
					if ($row->status == 0) {
						$stat = 'On Progress';
					} else if ($row->status == 1) {
						$stat = 'Approved';
					} else if ($row->status == 2) {
						$stat = 'Rejected';
					} else if ($row->status == 3) {
						$stat = 'Complete';
					} else if ($row->status == 9) {
						$stat = 'Canceled';
					}
					$tab .= '<tr>
								<td style="vertical-align:middle">' . ++$j . '</td>
								<td style="vertical-align:middle">' . $row->nomor . '<br><small>' . $user->keterangan . '</td>
								<td style="vertical-align:middle">' . $this->Kamus_model->tanggal_indo($row->timestamp) . '</td>
								<td style="vertical-align:middle">' . $this->Kamus_model->uang($row->nominal) . '</td>
								<td style="vertical-align:middle">' . $row->keterangan . '</td>
								<td style="vertical-align:middle">' . $c . '</td>
								<td style="vertical-align:middle"><b>' . $stat . '</b></td>
								<td style="vertical-align:middle">' . $act . '</td>
							</tr>';
				}
			}
		}
		$data['table'] = $tab;
		$this->load->view('head');
		$this->load->view('table_search', $data);
		$this->load->view('foot');
	}

	function get_data_dashboard()
	{
		$this->Main_model->get_data_dashboard();
	}
	function get_sumber()
	{
		$id = $this->input->post('sumber');
		$q = $this->db->query("SELECT a.`id_jurnal` FROM kd_jurnal a WHERE a.`id_rek`=$id AND a.`status`=1")->row();
		if (!empty($q)) {
			echo $q->id_jurnal;
		}
		return false;
	}
}
