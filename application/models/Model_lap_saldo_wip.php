<?php
class Model_lap_saldo_wip extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $stok_kgm = $stok_yrd = 0;
        $kondisi = null;
        if (!empty($this->input->post('searchDateFirst')) || !empty($this->input->post('searchDateFinish'))) {
            $kondisi = " AND b.`insert_at` BETWEEN '" . $this->input->post('searchDateFirst') . "' AND '" . $this->input->post('searchDateFinish') . "'";
        }
        if (!empty($this->input->post('idbarang'))) {
            $kondisi .= " AND b.`id_header` = '" . $this->input->post('idbarang') . "'";
        }
        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS b.*
FROM
warehouse_inventory_dyeing a
JOIN warehouse_inventory_dyeing_detail b
ON a.`id` = b.`id_header`
WHERE a.`status` = 1 AND b.`status` = 1 AND (
b.`lot` LIKE '%" . $this->input->post('searchLot') . "%' ESCAPE '!' OR b.`ket` LIKE '%" . $this->input->post('searchLot') . "%' ESCAPE '!')
$kondisi GROUP BY b.`id` ORDER BY b.`timestamp` ASC");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        $row = array(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        );
        $data[] = $row;
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $qty_mtr = $qty_yrd = $qty_kgm = 0;
                $no++;
                if ($r['flag'] == 1) {
                    $opsi = '<a class="text-success" title="IN"><input class="stat" type="hidden" value="in" /><i class="icon-arrow-up-right3"></input> IN</a>';
                } else if ($r['flag'] == 2) {
                    $opsi = '<a class="text-warning" title="OUT"><input class="stat" type="hidden" value="out" /><i class="icon-arrow-down-left3"></i> OUT</a>';
                }
                if (!empty($r['m_yrd'])) {
                    $qty_yrd = $r['m_yrd'];
                    $stok_yrd = $stok_yrd + $r['m_yrd'];
                }
                if (!empty($r['k_yrd'])) {
                    $qty_yrd = $r['k_yrd'];
                    $stok_yrd = $stok_yrd - $r['k_yrd'];
                }
                if (!empty($r['m_kgm'])) {
                    $qty_kgm = $r['m_kgm'];
                    $stok_kgm = $stok_kgm + $r['m_kgm'];
                }
                if (!empty($r['k_kgm'])) {
                    $qty_kgm = $r['k_kgm'];
                    $stok_kgm = $stok_kgm - $r['k_kgm'];
                }

                $row = array(
                    $no . '.',
                    $r['lot'],
                    $qty_kgm,
                    $qty_yrd,
                    $qty_mtr,
                    $stok_kgm,
                    $stok_yrd,
                    $r['timestamp'],
                    $r['ket'],
                    $opsi
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }
}
