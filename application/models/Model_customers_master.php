<?php
class Model_customers_master extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $kondisi  = null;
        $column_order = array(null, 'a.kode', 'a.nama', 'a.alamat', 'a.taxno', 'a.kaber', 'a.limit_kredit', null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.kode';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY a.kode ASC";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.*
      FROM
      `master_customers` a
      WHERE a.`status` = 1 AND (
		`a`.`kode` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`alamat` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`keterangan` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		) $kondisi GROUP BY a.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $opsi = '<a class="text-primary" href="#" style="margin-right:20px" onClick="view_data(\'' . $r['id'] . '\')" title="Detail Customer"><i class="icon-file-eye"></i></a><a class="text-success" href="#" style="margin-right:20px" onClick="update_data(\'' . $r['id'] . '\')" title="Edit Customer"><i class="icon-pencil"></i></a> <a class="text-warning" href="#" onClick="delete_data(\'' . $r['id'] . '\')" title="Hapus"><i class="icon-bin"></i></a>';
                $no++;
                $row  = array(
                    $no . '.',
                    $r['kode'],
                    $r['nama'],
                    $r['alamat'],
                    $r['keterangan'],
                    $opsi
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function insert_data()
    {
        $nama = $this->input->post('nama_cust');
        $kode =  $this->input->post('kode');
        // $mata_uang =  $this->input->post('mata_uang');
        $alamat =  $this->input->post('alamat');
        $keterangan =  $this->input->post('keterangan');
        $data = array(
            'kode' => $kode,
            'nama' => $nama,
            'alamat' => $alamat,
            'keterangan' => $keterangan,
            'status' => 1,
            'insert_at' => date('Y-m-d H:i:s'),
            'insert_by' => $this->session->userdata('userid')
        );
        $res = $this->db->insert('master_customers', $data);
        if ($res == TRUE) {
            return 1;
        } else {
            return 0;
        }
    }
    function update_data()
    {
        $id = $this->input->post('id');
        $nama = $this->input->post('nama_cust');
        $kode =  $this->input->post('kode');
        // $mata_uang =  $this->input->post('mata_uang');
        $alamat =  $this->input->post('alamat');
        $keterangan =  $this->input->post('keterangan');
        $data = array(
            'kode' => $kode,
            'nama' => $nama,
            'alamat' => $alamat,
            'keterangan' => $keterangan,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $res = $this->db->update('master_customers', $data);
        if ($res == TRUE) {
            return 1;
        } else {
            return 0;
        }
    }
    function select_data()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT * FROM master_customers WHERE id=$id")->result();

        $data = array('data' => $query);
        return $data;
    }
    function delete_data($id = '')
    {
        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $return = $this->db->update('master_customers', $data);
        if ($return == true) {
            return 1;
        } else {
            return 0;
        }
    }

    function cek_kode()
    {
        $id = $this->input->post('id');
        $kode = $this->input->post('kode');
        if (empty($id)) {
            $q = $this->db->query("SELECT * FROM master_customers WHERE kode='" . $kode . "' AND status=1")->result();
            if (empty($q)) {
                return false;
            } else {
                return 1;
            }
        } else {
            $q = $this->db->query("SELECT * FROM master_customers WHERE kode='" . $kode . "' AND status=1 AND id!='" . $id . "'")->result();
            if (empty($q)) {
                return false;
            } else {
                return 1;
            }
        }
    }

    function view_data($id = '')
    {
        $detail = null;
        $query = $this->db->query("SELECT a.* FROM master_customers a WHERE a.`id`=$id")->result();
        if (!empty($query)) {
            foreach ($query as $row) {
                $detail .= '<div class="col-lg-12">
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Kode
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control" value="' . $row->kode . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        Nama Customer
                    </div>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="' . $row->nama . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Alamat
                    </div>
                    <div class="col-lg-10">
                        <textarea class="form-control" style="resize:vertical;" readonly>' . $row->alamat . '</textarea>
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Keterangan
                    </div>
                    <div class="col-lg-10">
                        <textarea class="form-control" id="ket" name="keterangan" style="resize:vertical;" readonly>' . $row->keterangan . '</textarea>
                    </div>
                </div>
            </div>';
            };
        }
        $detail .= '<br><hr><label><b>Cabang</b></label>';
        $query = $this->db->query("SELECT b.* FROM master_customers a JOIN master_customers_branch b ON a.`id`=b.`id_cust` AND a.`id`=$id WHERE b.`status`=1")->result();
        if (!empty($query)) {
            $no = 1;
            foreach ($query as $row) {
                if ($no == 1) {
                    $detail .= '
                <div class="contact">
                    <div class="row">
                        <div class="col-lg-1 text-center">
                            <label>No</label>
                            <input type="text" class="form-control text-center" value="' . $no . '" readonly />
                        </div>
                        <div class="col-lg-4 text-center">
                            <label>Nama</label>
                            <input type="text" class="form-control" value="' . $row->nama . '" readonly />
                        </div>
                        <div class="col-lg-7 text-center">
                            <label>Alamat</label>
                            <input type="text" class="form-control" value="' . $row->alamat . '" readonly />
                        </div>
                    </div>
                </div>';
                    $no++;
                } else {
                    $detail .= '
                <div class="contact">
                    <div class="row">
                        <div class="col-lg-1 text-center">
                            <input type="text" class="form-control text-center" value="' . $no . '" readonly />
                        </div>
                        <div class="col-lg-4 text-center">
                            <input type="text" class="form-control" value="' . $row->nama . '" readonly />
                        </div>
                        <div class="col-lg-7 text-center">
                            <input type="text" class="form-control" value="' . $row->alamat . '" readonly />
                        </div>
                    </div>
                </div>';
                    $no++;
                }
            }
        }
        $detail .= '<br><br>';
        return $detail;
    }
}
