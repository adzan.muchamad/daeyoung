<?php
class Model_lap_mutasi_barang_pemasukan extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $db = $this->load->database('inventory', TRUE);
        $kondisi = $tanggal1 = $tanggal2 = null;
        if (!empty($this->input->post('idbarang'))) {
            $kondisi .= " AND z.`kode_barang` = '" . $this->input->post('idbarang') . "'";
        }
        if (!empty($this->input->post('idloc'))) {
            $kondisi .= " AND z.`loc_code` = '" . $this->input->post('idloc') . "'";
        }
        if (!empty($this->input->post('tipe_bc'))) {
            $kondisi .= " AND z.`jenis_dok` = '" . $this->input->post('tipe_bc') . "'";
        }
        if (!empty($this->input->post('no_dok'))) {
            $kondisi .= " AND z.`no_dok` = '" . $this->input->post('no_dok') . "'";
        }
        if (!empty($this->input->post('searchDateFirst')) || !empty($this->input->post('searchDateFinish'))) {
            $tanggal1 .= " AND a.`delivery_date` BETWEEN '" . $this->input->post('searchDateFirst') . "' AND '" . $this->input->post('searchDateFinish') . "'";
            $tanggal2 .= " AND a.`tran_date` BETWEEN '" . $this->input->post('searchDateFirst') . "' AND '" . $this->input->post('searchDateFinish') . "'";
        }
        $q = $db->query("SELECT * FROM (SELECT
        a.`id` AS trans_no,
        NULL AS TYPE,
        b.`id`,
        a.`loc_code`,
        a.`pjenis` AS jenis_dok,
        a.`pno` AS no_dok,
        a.`ptgl` AS tgl_dok,
        a.`reference` AS no_bukti,
        a.`delivery_date` AS tgl_penerimaan,
        c.`supp_name` AS pengiriman_barang,
        b.`item_code` AS kode_barang,
        b.`description` AS nama_barang,
        b.`qty_recd` AS pemasukan,
        e.`units` AS satuan,
        ROUND(a.`rate`*d.`act_price`,2) AS nilai_satuan
      FROM
        0_grn_batch a
        JOIN 0_grn_items b
          ON a.`id` = b.`grn_batch_id`
        JOIN 0_suppliers c
          ON a.`supplier_id` = c.`supplier_id`
         JOIN 0_purch_order_details d
        ON b.`po_detail_item`=d.`po_detail_item`
         JOIN 0_stock_master e
        ON d.`item_code`=e.`stock_id`
      WHERE (e.`purchase_cost` > 0 OR e.`material_cost` > 0) AND e.`nofasilitas`=0 $tanggal1
        UNION ALL
        SELECT 
        a.`trans_no`,
        a.`type`,
        b.`id`,
        'GFG' AS loc_code,
        a.`pjenis` AS jenis_dok,
        a.`pno` AS no_dok,
        a.`ptgl` AS tgl_dok,
        a.`reference` AS no_bukti,
        a.`tran_date` AS tgl_penerimaan,
        c.`name` AS pengiriman_barang,
        b.`stock_id` AS kode_barang,
        b.`description` AS nama_barang,
        b.`quantity` AS pemasukan,
        e.`units` AS satuan,
        ROUND(a.`rate`*b.`unit_price`,2) AS nilai_satuan
        FROM `0_debtor_trans` a
        JOIN 0_debtor_trans_details b
        ON a.`trans_no`=b.`debtor_trans_no` AND a.`type`=b.`debtor_trans_type`
        JOIN 0_debtors_master c
          ON a.`debtor_no` = c.`debtor_no`
          JOIN 0_stock_master e
        ON b.`stock_id`=e.`stock_id`
        WHERE a.`type`=11 $tanggal2 ) z
        WHERE (
		z.`no_bukti` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR z.`nama_barang` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR z.`no_dok` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR z.`pengiriman_barang` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		)
        $kondisi
        ORDER BY z.`tgl_penerimaan`,z.`trans_no`,z.`id` ASC");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $no++;
                $row = array(
                    $no . '.',
                    $r['jenis_dok'],
                    $r['no_dok'],
                    $r['tgl_dok'],
                    $r['no_bukti'],
                    $r['tgl_penerimaan'],
                    $r['pengiriman_barang'],
                    $r['kode_barang'],
                    $r['nama_barang'],
                    number_format($r['pemasukan'], 2, '.', ','),
                    $r['satuan'],
                    number_format($r['nilai_satuan'], 2, '.', ',')
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_barang($param = '')
    {
        $db = $this->load->database('inventory', TRUE);
        $q = $db->query("SELECT a.`idbrg` AS id,a.`description`,a.`stock_id`,b.`description` AS kategori FROM 0_stock_master a JOIN 0_stock_category b 
        ON a.`category_id` = b.`category_id` WHERE (a.`description` LIKE '%$param%' OR a.`stock_id` LIKE '%$param%' OR b.`description` LIKE '%$param%') 
        ORDER BY a.`description` ASC LIMIT 50")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->stock_id,
                "text" =>  $row->stock_id . ' - ' . $row->description . ' ( ' . $row->kategori . ' )'
            );
        }
        return json_encode($data);
    }
}
