<?php
class Model_customers_branch extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $kondisi = $stat_kaber = null;
        $column_order = array(null, 'b.kode', 'a.nama', 'a.alamat', 'a.taxno', 'a.kaber', 'a.limit_kredit', null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.kode';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY a.nama ASC";
        }
        if (!empty($this->input->post('searchkaber'))) {
            if ($this->input->post('searchkaber') == 2) {
                $kondisi = " AND a.`kaber` = '0'";
            } else {
                $kondisi = " AND a.`kaber` = '" . $this->input->post('searchkaber') . "'";
            }
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.*,b.`kode`,b.`nama` AS head
      FROM
      `master_customers_branch` a
      JOIN `master_customers` b
      ON a.`id_cust`=b.`id`
      WHERE a.`status` = 1 AND (
		`b`.`kode` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`alamat` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `b`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		) $kondisi GROUP BY a.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                if ($r['kaber'] == 1) {
                    $stat_kaber = 'Ya';
                } else {
                    $stat_kaber = 'Tidak';
                }
                $opsi = '<a class="text-primary" href="#" style="margin-right:20px" onClick="view_data(\'' . $r['id'] . '\')" title="Detail Customer"><i class="icon-file-eye"></i></a><a class="text-success" href="#" style="margin-right:20px" onClick="update_data(\'' . $r['id'] . '\')" title="Edit Customer"><i class="icon-pencil"></i></a> <a class="text-warning" href="#" onClick="delete_data(\'' . $r['id'] . '\')" title="Hapus"><i class="icon-bin"></i></a>';
                $no++;
                $row  = array(
                    $no . '.',
                    $r['kode'],
                    $r['nama'],
                    $r['head'],
                    $r['alamat'],
                    number_format($r['limit_kredit'], 0, ',', '.'),
                    $stat_kaber,

                    $opsi
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function insert_data()
    {
        $limit = 250000000;
        $id_cust = $this->input->post('nama_cust');
        $nama_branch = $this->input->post('nama_branch');
        $npwp =  $this->input->post('npwp');
        $mata_uang =  $this->input->post('mata_uang');
        $alamat =  $this->input->post('alamat');
        $alamat_invoice =  $this->input->post('alamat_invoice');
        $keterangan =  $this->input->post('keterangan');
        if (!empty($this->input->post('limit'))) {
            $limit =  str_replace(',', '.', str_replace('.', '', $this->input->post('limit')));
        }
        $termin =  $this->input->post('termin');
        $ppn =  $this->input->post('harga_pajak');
        $kaber =  $this->input->post('kaber');
        $fax =  $this->input->post('fax');
        $data = array(
            'id_cust' => $id_cust,
            'nama' => $nama_branch,
            'alamat' => $alamat,
            'alamat_invoice' => $alamat_invoice,
            'taxno' => $npwp,
            'mata_uang' => $mata_uang,
            'keterangan' => $keterangan,
            'limit_kredit' => $limit,
            'kaber' => $kaber,
            'ppn' => $ppn,
            'fax' => $fax,
            'tempo_bayar' => $termin,
            'status' => 1,
            'insert_at' => date('Y-m-d H:i:s'),
            'insert_by' => $this->session->userdata('userid')
        );
        $res = $this->db->insert('master_customers_branch', $data);
        if ($res == TRUE) {
            $id = $this->db->insert_id();
            if (isset($_POST['nama_contact'])) {
                foreach ($_POST['nama_contact'] as $k => $v) {
                    $data = array(
                        'id_branch' => $id,
                        'nama' => $_POST['nama_contact'][$k],
                        'jabatan' => $_POST['jabatan'][$k],
                        'phone' => $_POST['phone'][$k],
                        'email' => $_POST['email'][$k]
                    );
                    $this->db->insert('master_contact_cust', $data);
                }
            }
            return 1;
        } else {
            return 0;
        }
    }
    function update_data()
    {
        $limit = 250000000;
        $id = $this->input->post('id');
        $id_cust = $this->input->post('nama_cust');
        $nama_branch = $this->input->post('nama_branch');
        $npwp =  $this->input->post('npwp');
        $mata_uang =  $this->input->post('mata_uang');
        $alamat =  $this->input->post('alamat');
        $alamat_invoice =  $this->input->post('alamat_invoice');
        $keterangan =  $this->input->post('keterangan');
        if (!empty($this->input->post('limit'))) {
            $limit =  str_replace(',', '.', str_replace('.', '', $this->input->post('limit')));
        }
        $termin =  $this->input->post('termin');
        $ppn =  $this->input->post('harga_pajak');
        $kaber =  $this->input->post('kaber');
        $fax =  $this->input->post('fax');
        $data = array(
            'id_cust' => $id_cust,
            'nama' => $nama_branch,
            'alamat' => $alamat,
            'alamat_invoice' => $alamat_invoice,
            'taxno' => $npwp,
            'mata_uang' => $mata_uang,
            'keterangan' => $keterangan,
            'limit_kredit' => $limit,
            'kaber' => $kaber,
            'ppn' => $ppn,
            'fax' => $fax,
            'tempo_bayar' => $termin,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $this->db->update('master_customers_branch', $data);
        //del contact
        $data = array('status' => 9);
        $this->db->where('id_branch', $id);
        $this->db->update('master_contact_cust', $data);
        if (isset($_POST['nama_contact'])) {
            foreach ($_POST['nama_contact'] as $k => $v) {
                $data = array(
                    'id_branch' => $id,
                    'nama' => $_POST['nama_contact'][$k],
                    'jabatan' => $_POST['jabatan'][$k],
                    'phone' => $_POST['phone'][$k],
                    'email' => $_POST['email'][$k]
                );
                $this->db->insert('master_contact_cust', $data);
            }
        }
        return 1;
    }
    function select_data()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT a.*,b.`nama` AS nama_cust FROM master_customers_branch a JOIN master_customers b ON a.`id_cust`=b.`id` WHERE a.`id`=$id")->result();
        $contact = null;
        $a = 1;
        $q = $this->db->query("SELECT * FROM `master_customers_branch` a JOIN `master_contact_cust` b ON a.`id`=b.`id_branch` WHERE a.`id`=$id AND a.`status`=1 AND b.`status` =1")->result();
        if (!empty($q)) {
            foreach ($q as $row) {
                if ($a == 1) {
                    $contact .= '<div class="row">
                    <div class="col-lg-1 text-center">
                        <label>No</label>
                        <input type="text" class="form-control text-center no_urutan2" value="1" disabled />
                    </div>
                    <div class="col-lg-3 text-center">
                        <label>Nama</label>
                        <input type="text" class="form-control nama_contact" name="nama_contact[]" value="' . $row->nama . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Jabatan</label>
                        <input type="text" class="form-control jabatan" name="jabatan[]" value="' . $row->jabatan . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Phone</label>
                        <input type="text" class="form-control phone" name="phone[]" value="' . $row->phone . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Email</label>
                        <input type="text" class="form-control email" name="email[]" value="' . $row->email . '" />
                    </div>
                    <div class="col-lg-2">
                        <label>&nbsp;</label>
                        <a class="btn btn-primary form-control" onclick="add_contact(this)">Tambah</a>
                    </div>
                </div>';
                    $a++;
                } else {
                    $contact .= '<div class="row">
                    <div class="col-lg-1 text-center">
                        <input type="text" class="form-control text-center no_urutan2" value="' . $a . '" disabled />
                    </div>
                    <div class="col-lg-3 text-center">
                        <input type="text" class="form-control nama_contact" name="nama_contact[]" value="' . $row->nama . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control jabatan" name="jabatan[]" value="' . $row->jabatan . '"/>
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control phone" name="phone[]" value="' . $row->phone . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control email" name="email[]" value="' . $row->email . '" />
                    </div>
                    <div class="col-lg-2">
                        <a class="btn btn-warning form-control" onclick="delete_contact(this)">Hapus</a>
                    </div>
                </div>';
                    $a++;
                }
            }
        } else {
            $contact .= '<div class="row">
            <div class="col-lg-1 text-center">
                <label>No</label>
                <input type="text" class="form-control text-center no_urutan2" value="1" disabled />
            </div>
            <div class="col-lg-3 text-center">
                <label>Nama</label>
                <input type="text" class="form-control nama_contact" name="nama_contact[]"/>
            </div>
            <div class="col-lg-2 text-center">
                <label>Jabatan</label>
                <input type="text" class="form-control jabatan" name="jabatan[]" />
            </div>
            <div class="col-lg-2 text-center">
                <label>Phone</label>
                <input type="text" class="form-control phone" name="phone[]" />
            </div>
            <div class="col-lg-2 text-center">
                <label>Email</label>
                <input type="text" class="form-control email" name="email[]" />
            </div>
            <div class="col-lg-2">
                <label>&nbsp;</label>
                <a class="btn btn-primary form-control" onclick="add_contact(this)">Tambah</a>
            </div>
        </div>';
        }

        $data = array('data' => $query, 'contact' => $contact);
        return $data;
    }
    function delete_data($id = '')
    {
        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $return = $this->db->update('master_customers_branch', $data);
        if ($return == true) {
            $data = array('status' => 9);
            $this->db->where('id_branch', $id);
            $this->db->update('master_contact_cust', $data);
            //
            $this->db->where('id', $id);
            $this->db->update('master_customers_branch', $data);
            return 1;
        } else {
            return 0;
        }
    }

    function view_data($id = '')
    {
        $detail = null;
        $termin = null;
        $query = $this->db->query("SELECT a.*,b.`nama` AS head,b.`kode` FROM master_customers_branch a JOIN master_customers b ON a.`id_cust`=b.`id` WHERE a.`id`=$id")->result();
        if (!empty($query)) {
            foreach ($query as $row) {
                if ($row->tempo_bayar == 1) {
                    $termin = 'Tunai';
                } else if ($row->tempo_bayar == 2) {
                    $termin = '7 Hari';
                } else if ($row->tempo_bayar == 3) {
                    $termin = '30 Hari';
                } else if ($row->tempo_bayar == 4) {
                    $termin = 'Tanggal 10 BB';
                } else if ($row->tempo_bayar == 5) {
                    $termin = 'Bayar Dimuka';
                }
                $detail .= '<div class="col-lg-12">
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Kode
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control" value="' . $row->kode . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        Nama Customer
                    </div>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="' . $row->head . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Nama Cabang
                    </div>
                    <div class="col-lg-10 text-center">
                        <input type="text" class="form-control" value="' . $row->nama . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Alamat
                    </div>
                    <div class="col-lg-10">
                        <textarea class="form-control" style="resize:vertical;" readonly>' . $row->alamat . '</textarea>
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Alamat Invoice
                    </div>
                    <div class="col-lg-10">
                        <textarea class="form-control" style="resize:vertical;" readonly>' . $row->alamat_invoice . '</textarea>
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        NPWP
                    </div>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" value="' . $row->taxno . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        FAX
                    </div>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" value="' . $row->fax . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Limit Kredit
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . ($row->limit_kredit > 0 ? number_format($row->limit_kredit, 0, ',', '.') : $row->limit_kredit) . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        Termin Pembayaran
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . $termin . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Kawasan Berikat
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . (1 == $row->kaber ? "Ya" : "Tidak") . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        PPN
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . (1 == $row->ppn ? "Ya" : "Tidak") . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Keterangan
                    </div>
                    <div class="col-lg-10">
                        <textarea class="form-control" id="ket" name="keterangan" style="resize:vertical;" readonly>' . $row->keterangan . '</textarea>
                    </div>
                </div>
            </div>';
            };
        }
        $detail .= '<hr><label><b>Contact</b></label>';
        $query = $this->db->query("SELECT b.* FROM master_customers_branch a JOIN master_contact_cust b ON a.`id`=b.`id_branch` AND a.`id`=$id WHERE b.`status`=1")->result();
        if (!empty($query)) {
            $no = 1;
            foreach ($query as $row) {
                if ($no == 1) {
                    $detail .= '
                <div class="contact">
                    <div class="row">
                        <div class="col-lg-1 text-center">
                            <label>No</label>
                            <input type="text" class="form-control text-center" value="' . $no . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <label>Nama</label>
                            <input type="text" class="form-control" value="' . $row->nama . '" readonly />
                        </div>
                        <div class="col-lg-3 text-center">
                            <label>Jabatan</label>
                            <input type="text" class="form-control" value="' . $row->jabatan . '" readonly />
                        </div>
                        <div class="col-lg-2 text-center">
                            <label>Phone</label>
                            <input type="text" class="form-control" value="' . $row->phone . '" readonly />
                        </div>
                        <div class="col-lg-3 text-center">
                            <label>Email</label>
                            <input type="text" class="form-control" value="' . $row->email . '" readonly />
                        </div>
                    </div>
                </div>';
                    $no++;
                } else {
                    $detail .= '
                <div class="contact">
                    <div class="row">
                        <div class="col-lg-1 text-center">
                            <input type="text" class="form-control text-center" value="' . $no . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <input type="text" class="form-control" value="' . $row->nama . '" readonly />
                        </div>
                        <div class="col-lg-3 text-center">
                            <input type="text" class="form-control" value="' . $row->jabatan . '" readonly />
                        </div>
                        <div class="col-lg-2 text-center">
                            <input type="text" class="form-control" value="' . $row->phone . '" readonly />
                        </div>
                        <div class="col-lg-3 text-center">
                            <input type="text" class="form-control" value="' . $row->email . '" readonly />
                        </div>
                    </div>
                </div>';
                    $no++;
                }
            }
        }
        $detail .= '<br><br>';
        return $detail;
    }

    function get_customers($param = null)
    {
        $q = $this->db->query("SELECT * FROM master_customers_branch WHERE status=1 AND (nama LIKE '%$param%' OR kode LIKE '%$param%') ORDER BY kode ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->kode . ' - ' . $row->nama
            );
        }
        return json_encode($data);
    }
}
