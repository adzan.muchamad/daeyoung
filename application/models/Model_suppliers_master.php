<?php
class Model_suppliers_master extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $kondisi = null;
        $column_order = array(null, 'a.kode_nama', 'a.nama', 'a.alamat', 'a.taxno', 'a.mata_uang', 'b.tax_nama', 'a.limit_kredit', null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.nama';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY a.nama ASC";
        }
        if (!empty($this->input->post('searchtaxgroup'))) {
            $kondisi = " AND b.`id` = '" . $this->input->post('searchtaxgroup') . "'";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.*,IF(a.`kode_supp` IS NULL,a.`kode_nama`,a.`kode_supp`) AS kode,b.`nama` AS tax_nama,c.`nama` AS uang
      FROM
      `master_suppliers` a
      JOIN master_tax_group b
      ON a.`tax_id`=b.`id`
      JOIN master_currency c
      ON a.`mata_uang`=c.`id`
      WHERE a.`status` = 1 AND (
		`a`.`kode_nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`alamat` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`kode_supp` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		) $kondisi GROUP BY a.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $opsi = '<a class="text-primary" href="#" style="margin-right:20px" onClick="view_data(\'' . $r['id'] . '\')" title="Detail Supplier"><i class="icon-file-eye"></i></a><a class="text-success" href="#" style="margin-right:20px" onClick="update_data(\'' . $r['id'] . '\')" title="Edit Supplier"><i class="icon-pencil"></i></a> <a class="text-warning" href="#" onClick="delete_data(\'' . $r['id'] . '\')" title="Hapus"><i class="icon-bin"></i></a>';
                $no++;
                $row  = array(
                    $no . '.',
                    $r['kode'],
                    $r['nama'],
                    $r['alamat'],
                    $r['taxno'],
                    $r['uang'],
                    $r['tax_nama'],
                    number_format($r['limit_kredit'], 0, ',', '.'),
                    $opsi
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function insert_data()
    {
        $nama_supp = $this->input->post('nama_supp');
        $kode =  $this->input->post('kode');
        $grup_tax =  $this->input->post('grup_tax');
        $npwp =  $this->input->post('npwp');
        $mata_uang =  $this->input->post('mata_uang');
        $website =  $this->input->post('website');
        $alamat =  $this->input->post('alamat');
        $alamat_invoice =  $this->input->post('alamat_invoice');
        $keterangan =  $this->input->post('keterangan');
        $limit =  $this->input->post('limit');
        $termin =  $this->input->post('termin');
        $harga_pajak =  $this->input->post('harga_pajak');
        $kaber =  $this->input->post('kaber');
        $fax =  $this->input->post('fax');
        $data = array(
            'kode_supp' => $kode,
            'nama' => $nama_supp,
            'alamat' => $alamat,
            'alamat_invoice' => $alamat_invoice,
            'taxno' => $npwp,
            'tax_id' => $grup_tax,
            'mata_uang' => $mata_uang,
            'website' => $website,
            'keterangan' => $keterangan,
            'limit_kredit' => $limit,
            'kaber' => $kaber,
            'tax_included' => $harga_pajak,
            'fax' => $fax,
            'tempo_bayar' => $termin,
            'status' => 1,
            'insert_at' => date('Y-m-d H:i:s'),
            'insert_by' => $this->session->userdata('userid')
        );
        $res = $this->db->insert('master_suppliers', $data);
        if ($res == TRUE) {
            $id = $this->db->insert_id();
            if (isset($_POST['norek'])) {
                foreach ($_POST['norek'] as $k => $v) {
                    $data = array(
                        'id_supp' => $id,
                        'no_rek' => $_POST['norek'][$k],
                        'nama' => $_POST['an'][$k],
                        'bank' => $_POST['bank'][$k],
                        'cabang' => $_POST['cabang'][$k],
                    );
                    $this->db->insert('master_rekening', $data);
                }
            }
            if (isset($_POST['nama_contact'])) {
                foreach ($_POST['nama_contact'] as $k => $v) {
                    $data = array(
                        'id_supp' => $id,
                        'nama' => $_POST['nama_contact'][$k],
                        'jabatan' => $_POST['jabatan'][$k],
                        'no_hp' => $_POST['phone'][$k],
                        'email' => $_POST['email'][$k]
                    );
                    $this->db->insert('master_contact_supp', $data);
                }
            }
            return 1;
        } else {
            return 0;
        }
    }
    function update_data()
    {
        $id = $this->input->post('id');
        $nama_supp = $this->input->post('nama_supp');
        $kode =  $this->input->post('kode');
        $grup_tax =  $this->input->post('grup_tax');
        $npwp =  $this->input->post('npwp');
        $mata_uang =  $this->input->post('mata_uang');
        $website =  $this->input->post('website');
        $alamat =  $this->input->post('alamat');
        $alamat_invoice =  $this->input->post('alamat_invoice');
        $keterangan =  $this->input->post('keterangan');
        $limit =  $this->input->post('limit');
        $termin =  $this->input->post('termin');
        $harga_pajak =  $this->input->post('harga_pajak');
        $kaber =  $this->input->post('kaber');
        $fax =  $this->input->post('fax');
        $data = array(
            'kode_supp' => $kode,
            'nama' => $nama_supp,
            'alamat' => $alamat,
            'alamat_invoice' => $alamat_invoice,
            'taxno' => $npwp,
            'tax_id' => $grup_tax,
            'mata_uang' => $mata_uang,
            'website' => $website,
            'keterangan' => $keterangan,
            'limit_kredit' => $limit,
            'kaber' => $kaber,
            'tax_included' => $harga_pajak,
            'fax' => $fax,
            'tempo_bayar' => $termin,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $this->db->update('master_suppliers', $data);
        //del rek
        $this->db->where('id_supp', $id);
        $this->db->delete('master_rekening');
        //del contact
        $this->db->where('id_supp', $id);
        $this->db->delete('master_contact_supp');
        if (isset($_POST['norek'])) {
            foreach ($_POST['norek'] as $k => $v) {
                $data = array(
                    'id_supp' => $id,
                    'no_rek' => $_POST['norek'][$k],
                    'nama' => $_POST['an'][$k],
                    'bank' => $_POST['bank'][$k],
                    'cabang' => $_POST['cabang'][$k],
                );
                $this->db->insert('master_rekening', $data);
            }
        }
        if (isset($_POST['nama_contact'])) {
            foreach ($_POST['nama_contact'] as $k => $v) {
                $data = array(
                    'id_supp' => $id,
                    'nama' => $_POST['nama_contact'][$k],
                    'jabatan' => $_POST['jabatan'][$k],
                    'no_hp' => $_POST['phone'][$k],
                    'email' => $_POST['email'][$k]
                );
                $this->db->insert('master_contact_supp', $data);
            }
        }
        return 1;
    }
    function select_data()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT * FROM master_suppliers WHERE id=$id")->result();
        $contact = $bank = null;
        $a = 1;
        $q = $this->db->query("SELECT * FROM `master_suppliers` a JOIN `master_contact_supp` b ON a.`id`=b.`id_supp` WHERE a.`id`=$id AND a.`status`=1")->result();
        if (!empty($q)) {
            foreach ($q as $row) {
                if ($a == 1) {
                    $contact .= '<div class="row">
                    <div class="col-lg-1 text-center">
                        <label>No</label>
                        <input type="text" class="form-control text-center no_urutan2" value="1" disabled />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Nama</label>
                        <input type="text" class="form-control nama_contact" name="nama_contact[]" value="' . $row->nama . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Jabatan</label>
                        <input type="text" class="form-control jabatan" name="jabatan[]" value="' . $row->jabatan . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Phone</label>
                        <input type="text" class="form-control phone" name="phone[]" value="' . $row->no_hp . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Email</label>
                        <input type="text" class="form-control email" name="email[]" value="' . $row->email . '" />
                    </div>
                    <div class="col-lg-1">
                        <label>&nbsp;</label>
                        <a class="btn btn-primary form-control" onclick="add_contact(this)">Tambah</a>
                    </div>
                </div>';
                    $a++;
                } else {
                    $contact .= '<div class="row">
                    <div class="col-lg-1 text-center">
                        <input type="text" class="form-control text-center no_urutan2" value="' . $a . '" disabled />
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control nama_contact" name="nama_contact[]" value="' . $row->nama . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control jabatan" name="jabatan[]" value="' . $row->jabatan . '"/>
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control phone" name="phone[]" value="' . $row->no_hp . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control email" name="email[]" value="' . $row->email . '" />
                    </div>
                    <div class="col-lg-1">
                        <a class="btn btn-warning form-control" onclick="delete_contact(this)">Hapus</a>
                    </div>
                </div>';
                    $a++;
                }
            }
        } else {
            $contact .= '<div class="row">
            <div class="col-lg-1 text-center">
                <label>No</label>
                <input type="text" class="form-control text-center no_urutan2" value="1" disabled />
            </div>
            <div class="col-lg-2 text-center">
                <label>Nama</label>
                <input type="text" class="form-control nama_contact" name="nama_contact[]"/>
            </div>
            <div class="col-lg-2 text-center">
                <label>Jabatan</label>
                <input type="text" class="form-control jabatan" name="jabatan[]" />
            </div>
            <div class="col-lg-2 text-center">
                <label>Phone</label>
                <input type="text" class="form-control phone" name="phone[]" />
            </div>
            <div class="col-lg-2 text-center">
                <label>Email</label>
                <input type="text" class="form-control email" name="email[]" />
            </div>
            <div class="col-lg-1">
                <label>&nbsp;</label>
                <a class="btn btn-primary form-control" onclick="add_contact(this)">Tambah</a>
            </div>
        </div>';
        }
        $a = 1;
        $q = $this->db->query("SELECT * FROM `master_suppliers` a JOIN `master_rekening` b ON a.`id`=b.`id_supp` WHERE a.`id`=$id AND a.`status`=1")->result();
        if (!empty($q)) {

            foreach ($q as $row) {
                if ($a == 1) {
                    $bank .= '<div class="row">
                    <div class="col-lg-1 text-center">
                        <label>No</label>
                        <input type="text" class="form-control text-center no_urutan" value="' . $a . '" disabled />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>No Rekening</label>
                        <input type="text" class="form-control norek" name="norek[]" value="' . $row->no_rek . '" />
                    </div>
                    <div class="col-lg-3 text-center">
                        <label>Atas Nama</label>
                        <input type="text" class="form-control an" name="an[]" value="' . $row->nama . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Bank</label>
                        <input type="text" class="form-control bank" name="bank[]" value="' . $row->bank . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <label>Cabang</label>
                        <input type="text" class="form-control cabang" name="cabang[]" value="' . $row->cabang . '" />
                    </div>
                    <div class="col-lg-1">
                        <label>&nbsp;</label>
                        <a class="btn btn-primary form-control" onclick="add_bank(this)">Tambah</a>
                    </div>
                </div>';
                    $a++;
                } else {
                    $bank .= '<div class="row">
                    <div class="col-lg-1 text-center">
                        <input type="text" class="form-control text-center no_urutan" value="' . $a . '" disabled />
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control norek" name="norek[]" value="' . $row->no_rek . '" />
                    </div>
                    <div class="col-lg-3 text-center">
                        <input type="text" class="form-control an" name="an[]" value="' . $row->nama . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control bank" name="bank[]" value="' . $row->bank . '" />
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control cabang" name="cabang[]" value="' . $row->cabang . '" />
                    </div>
                    <div class="col-lg-1">
                        <a class="btn btn-warning form-control" onclick="delete_bank(this)">Hapus</a>
                    </div>
                </div>';
                    $a++;
                }
            }
        } else {
            $bank .= '<div class="row">
            <div class="col-lg-1 text-center">
                <label>No</label>
                <input type="text" class="form-control text-center no_urutan" value="1" disabled />
            </div>
            <div class="col-lg-2 text-center">
                <label>No Rekening</label>
                <input type="text" class="form-control norek" name="norek[]" />
            </div>
            <div class="col-lg-3 text-center">
                <label>Atas Nama</label>
                <input type="text" class="form-control an" name="an[]" />
            </div>
            <div class="col-lg-2 text-center">
                <label>Bank</label>
                <input type="text" class="form-control bank" name="bank[]" />
            </div>
            <div class="col-lg-2 text-center">
                <label>Cabang</label>
                <input type="text" class="form-control cabang" name="cabang[]" />
            </div>
            <div class="col-lg-1">
                <label>&nbsp;</label>
                <a class="btn btn-primary form-control" onclick="add_bank(this)">Tambah</a>
            </div>
        </div>';
        }

        $data = array('data' => $query, 'contact' => $contact, 'bank' => $bank);
        return $data;
    }
    function delete_data($id = '')
    {
        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $return = $this->db->update('master_suppliers', $data);
        if ($return == true) {
            $data = array('status' => 9);
            $this->db->where('id_supp', $id);
            $this->db->update('master_rekening', $data);
            $this->db->where('id_supp', $id);
            $this->db->update('master_contact_supp', $data);
            //
            $this->db->where('id', $id);
            $this->db->update('master_suppliers', $data);
            return 1;
        } else {
            return 0;
        }
    }

    function cek_kode($id = '')
    {
        $q = $this->db->query("SELECT * FROM master_suppliers WHERE kode_supp=$id AND status=1")->result();
        if (empty($q)) {
            return false;
        } else {
            return 1;
        }
    }

    function cek_kode_edit()
    {
        $id = $this->input->post('id');
        $kode = $this->input->post('kode');
        $q = $this->db->query("SELECT * FROM master_suppliers WHERE kode_supp=$kode AND status=1 AND id!=$id")->result();
        if (empty($q)) {
            return false;
        } else {
            return 1;
        }
    }

    function view_data($id = '')
    {
        $detail = null;
        $termin = null;
        $query = $this->db->query("SELECT a.*,b.`nama` AS tax_group,c.`nama` AS nama_curr FROM master_suppliers a LEFT JOIN master_tax_group b ON a.`tax_id`=b.`id` LEFT JOIN master_currency c ON a.`mata_uang`=c.`id` WHERE a.`id`=$id")->result();
        if (!empty($query)) {
            foreach ($query as $row) {
                if ($row->tempo_bayar == 1) {
                    $termin = 'Tunai';
                } else if ($row->tempo_bayar == 2) {
                    $termin = '7 Hari';
                } else if ($row->tempo_bayar == 3) {
                    $termin = '30 Hari';
                } else if ($row->tempo_bayar == 4) {
                    $termin = 'Tanggal 10 BB';
                } else if ($row->tempo_bayar == 5) {
                    $termin = 'Bayar Dimuka';
                }
                $detail .= '<div class="col-lg-12">
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Kode
                    </div>
                    <div class="col-lg-2 text-center">
                        <input type="text" class="form-control" value="' . $row->kode_supp . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        Nama Supplier
                    </div>
                    <div class="col-lg-6">
                        <input type="text" class="form-control" value="' . $row->nama . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Alamat
                    </div>
                    <div class="col-lg-10">
                        <textarea class="form-control" style="resize:vertical;" readonly>' . $row->alamat . '</textarea>
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Alamat Invoice
                    </div>
                    <div class="col-lg-10">
                        <textarea class="form-control" style="resize:vertical;" readonly>' . $row->alamat_invoice . '</textarea>
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Grup Pajak
                    </div>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" value="' . $row->tax_group . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        NPWP
                    </div>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" value="' . $row->taxno . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Website
                    </div>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" value="' . $row->website . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        FAX
                    </div>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" value="' . $row->fax . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Limit Kredit
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . ($row->limit_kredit > 0 ? number_format($row->limit_kredit, 0, ',', '.') : $row->limit_kredit) . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        Mata Uang
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . $row->nama_curr . '" readonly />
                    </div>
                    <div class="col-lg-2">
                        Termin Pembayaran
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . $termin . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Kawasan Berikat
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . (1 == $row->kaber ? "Ya" : "Tidak") . '" readonly />
                    </div>
                    <div class="col-lg-3">
                        Harga sudah termasuk pajak
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" value="' . (1 == $row->tax_included ? "Ya" : "Tidak") . '" readonly />
                    </div>
                </div>
                <div class="row display-flex-center">
                    <div class="col-lg-2">
                        Keterangan
                    </div>
                    <div class="col-lg-10">
                        <textarea class="form-control" id="ket" name="keterangan" style="resize:vertical;" readonly>' . $row->keterangan . '</textarea>
                    </div>
                </div>
            </div>';
            };
        }
        $detail .= '<hr><label><b>Bank</b></label>';
        $query = $this->db->query("SELECT b.* FROM master_suppliers a JOIN master_rekening b ON a.`id`=b.`id_supp` AND a.`id`=$id WHERE b.`status`=1")->result();
        if (!empty($query)) {
            $no = 1;
            foreach ($query as $row) {
                if ($no == 1) {
                    $detail .= '
                <div class="bank">
                    <div class="row">
                        <div class="col-lg-1 text-center">
                            <label>No</label>
                            <input type="text" class="form-control text-center" value="' . $no . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <label>No Rekening</label>
                            <input type="text" class="form-control" value="' . $row->no_rek . '" disabled />
                        </div>
                        <div class="col-lg-4 text-center">
                            <label>Atas Nama</label>
                            <input type="text" class="form-control" value="' . $row->nama . '" disabled/>
                        </div>
                        <div class="col-lg-2 text-center">
                            <label>Bank</label>
                            <input type="text" class="form-control" value="' . $row->bank . '" disabled />
                        </div>
                        <div class="col-lg-2 text-center">
                            <label>Cabang</label>
                            <input type="text" class="form-control" value="' . $row->cabang . '" disabled />
                        </div>
                    </div>
                </div>';
                    $no++;
                } else {
                    $detail .= '
                <div class="bank">
                    <div class="row">
                        <div class="col-lg-1 text-center">
                            <input type="text" class="form-control text-center" value="' . $no . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <input type="text" class="form-control" value="' . $row->no_rek . '" disabled />
                        </div>
                        <div class="col-lg-4 text-center">
                            <input type="text" class="form-control" value="' . $row->nama . '" disabled/>
                        </div>
                        <div class="col-lg-2 text-center">
                            <input type="text" class="form-control" value="' . $row->bank . '" disabled/>
                        </div>
                        <div class="col-lg-2 text-center">
                            <input type="text" class="form-control" value="' . $row->cabang . '" disabled/>
                        </div>
                    </div>
                </div>';
                    $no++;
                }
            }
        }
        $detail .= '<hr><label><b>Contact</b></label>';
        $query = $this->db->query("SELECT b.* FROM master_suppliers a JOIN master_contact_supp b ON a.`id`=b.`id_supp` AND a.`id`=$id WHERE b.`status`=1")->result();
        if (!empty($query)) {
            $no = 1;
            foreach ($query as $row) {
                if ($no == 1) {
                    $detail .= '
                <div class="contact">
                    <div class="row">
                        <div class="col-lg-1 text-center">
                            <label>No</label>
                            <input type="text" class="form-control text-center" value="' . $no . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <label>Nama</label>
                            <input type="text" class="form-control" value="' . $row->nama . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <label>Jabatan</label>
                            <input type="text" class="form-control" value="' . $row->jabatan . '" disabled />
                        </div>
                        <div class="col-lg-2 text-center">
                            <label>Phone</label>
                            <input type="text" class="form-control" value="' . $row->no_hp . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <label>Email</label>
                            <input type="text" class="form-control" value="' . $row->email . '" disabled />
                        </div>
                    </div>
                </div>';
                    $no++;
                } else {
                    $detail .= '
                <div class="contact">
                    <div class="row">
                        <div class="col-lg-1 text-center">
                            <input type="text" class="form-control text-center" value="' . $no . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <input type="text" class="form-control" value="' . $row->nama . '" disabled/>
                        </div>
                        <div class="col-lg-3 text-center">
                            <input type="text" class="form-control" value="' . $row->jabatan . '" disabled />
                        </div>
                        <div class="col-lg-2 text-center">
                            <input type="text" class="form-control" value="' . $row->no_hp . '" disabled />
                        </div>
                        <div class="col-lg-3 text-center">
                            <input type="text" class="form-control" value="' . $row->email . '" disabled />
                        </div>
                    </div>
                </div>';
                    $no++;
                }
            }
        }
        $detail .= '<br><br>';
        return $detail;
    }
}
