<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_asset extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		
	}

	public function view_jenis_asset($params)
	{
		$draw = $params['draw'];
		$order_column = $params['order']['column'];
		$order_mode = $params['order']['mode'];
		$start = $params['start'];
		$length = $params['length'];
		$keyword = $params['keyword'];

		$keyword = $this->db->escape_str($keyword);

		$condition = '';
		$order = '';
		$limit = '';
		$data = [];

		$kolom_search = ['kode', 'jenis'];
		$kolom_order = ['kode', 'kode', 'jenis', 'jenis'];

		$condition .= dt_searching($kolom_search, $keyword);
		$order .= dt_order($kolom_order, $order_column, $order_mode);

		if ($length > 0) {
			$limit = " LIMIT $start, $length ";
		}

		$db = daeyoung();
		$sql = " SELECT * FROM $db.master_jenis_asset a WHERE a.status = 1 ";

		$view = $this->db->query(" 
			SELECT * 
			FROM ( $sql ) AS tb 
			WHERE 1 = 1 
			$condition 
			$order 
			$limit ")->result();

		$count = $this->db->query(" 
			SELECT COUNT(*) AS jumlah 
			FROM ( $sql ) AS tb 
			WHERE 1 = 1 
			$condition ")->row();
		$jumlah = isset($count->jumlah) ? $count->jumlah : 0;

		if ($view) {
			$i = 1;
			foreach ($view as $row) {

				$btn_edit = '<a href="javascript:;" class="text-success btn-edit" data-id="'.$row->id.'"><i class="icon-pencil"></i> </a>';
				$btn_delete = '<a href="javascript:;" class="text-warning btn-hapus" data-id="'.$row->id.'"><i class="icon-bin"></i> </a>';

				$tools = $btn_edit.'&nbsp;'.$btn_delete;

				$data[] = [
					'no' => $i++,
					'kode' => $row->kode,
					'jenis' => $row->jenis,
					'tools' => $tools
				];
			}
		}

		$response = [
			'draw' => $draw,
			'recordsTotal' => $jumlah,
			'recordsFiltered' => $jumlah,
			'data' => $data
		];

		return $response;
	}

	public function view_control_asset($params)
	{
		$draw = $params['draw'];
		$order_column = $params['order']['column'];
		$order_mode = $params['order']['mode'];
		$start = $params['start'];
		$length = $params['length'];
		$keyword = $params['keyword'];

		$keyword = $this->db->escape_str($keyword);

		$condition = '';
		$order = '';
		$limit = '';
		$data = [];

		$kolom_search = ['kode_asset', 'nama', 'merk', 'jenis', 'keterangan', 'tgl_beli_format', 'unit', 'harga_beli', 'total'];
		$kolom_order = ['kode_asset', 'kode_asset', 'nama', 'merk', 'jenis', 'keterangan', 'tgl_beli', 'unit', 'harga_beli', 'total', 'kode_asset'];

		$condition .= dt_searching($kolom_search, $keyword);
		$order .= dt_order($kolom_order, $order_column, $order_mode);

		if ($length > 0) {
			$limit = " LIMIT $start, $length ";
		}

		$db = daeyoung();
		$sql = " SELECT a.id, a.`kode_asset`, a.`nama`, 
				a.`unit`, b.merk, c.jenis, a.keterangan,
				a.`harga_beli`, a.`tgl_beli`, 
				DATE_FORMAT(a.tgl_beli, '%d/%m/%Y') AS tgl_beli_format,
				(a.harga_beli * a.unit) AS total
				FROM tb_asset a
				LEFT JOIN master_merk_asset b ON a.merk = b.id 
				LEFT JOIN master_jenis_asset c ON a.jenis = c.id 
				ORDER BY a.tgl_beli DESC ";

		$view = $this->db->query(" 
			SELECT * 
			FROM ( $sql ) AS tb 
			WHERE 1 = 1 
			$condition 
			$order 
			$limit ")->result();

		$count = $this->db->query(" 
			SELECT COUNT(*) AS jumlah 
			FROM ( $sql ) AS tb 
			WHERE 1 = 1 
			$condition ")->row();
		$jumlah = isset($count->jumlah) ? $count->jumlah : 0;

		if ($view) {
			$i = 1;
			foreach ($view as $row) {

				$kurang = '<a href="javascript:;" class="btn-kurang text-danger" data-kode="'.$row->kode_asset.'" data-nama="'.$row->nama.'" data-id="'.$row->id.'" style="margin-bottom: 5px;" title="Form Pengurangan"><i class="icon-minus-circle2"></i> </a>';
				$history = '<a href="javascript:;" class="btn-history text-success" data-id="'.$row->id.'" style="margin-bottom: 5px;" title="History Asset"><i class="icon-history"></i> </a>';
				$delete = '<a href="javascript:;" class="btn-delete text-danger" data-id="'.$row->id.'" style="margin-bottom: 5px;" title="Hapus Asset"><i class="icon-trash-alt"></i> </a>';
				$edit = '<a href="javascript:;" class="btn-edit text-warning" data-id="'.$row->id.'" style="margin-bottom: 5px;" title="Edit Data"><i class="icon-pencil"></i> </a>';

				$tools = $kurang . '&nbsp;' . $history . '&nbsp;' . $edit . '&nbsp;' . $delete;

				$data[] = [
					'no' => $i++,
					'kode_asset' => $row->kode_asset,
					'nama' => $row->nama,
					'merk' => $row->merk,
					'jenis' => $row->jenis,
					'keterangan' => $row->keterangan,
					'tgl_beli' => $row->tgl_beli_format,
					'unit' => $row->unit,
					'harga_beli' => number_format($row->harga_beli, 0, ',', '.'),
					'total' => number_format($row->total, 0, ',', '.'),
					'tools' => $tools
				];
			}
		}

		$response = [
			'draw' => $draw,
			'recordsTotal' => $jumlah,
			'recordsFiltered' => $jumlah,
			'data' => $data
		];

		return $response;
	}

	public function view_penyusutan($params)
	{
		$draw = $params['draw'];
		$order_column = $params['order']['column'];
		$order_mode = $params['order']['mode'];
		$start = $params['start'];
		$length = $params['length'];
		$keyword = $params['keyword'];

		$keyword = $this->db->escape_str($keyword);

		$condition = '';
		$order = '';
		$limit = '';
		$data = [];

		$kolom_search = ['kode_asset', 'nama', 'tgl_beli_format', 'unit', 'tarif', 'durasi_format'];
		$kolom_order = ['kode_asset', 'kode_asset', 'nama', 'tgl_beli', 'unit', 'tarif', 'durasi', 'kode_asset'];

		$condition .= dt_searching($kolom_search, $keyword);
		$order .= dt_order($kolom_order, $order_column, $order_mode);

		if ($length > 0) {
			$limit = " LIMIT $start, $length ";
		}

		$db = daeyoung();
		$sql = " SELECT id, kode_asset, nama, tgl_beli, 
			DATE_FORMAT(tgl_beli, '%d/%m/%Y') AS tgl_beli_format,
			unit, tarif, mulai_penyusutan, selesai_penyusutan, durasi,
			DATE_FORMAT(mulai_penyusutan, '%d/%m/%Y') AS mulai_penyusutan_format,
			DATE_FORMAT(selesai_penyusutan, '%d/%m/%Y') AS selesai_penyusutan_format,
			IF (mulai_penyusutan <> '' AND selesai_penyusutan <> '', CONCAT(DATE_FORMAT(mulai_penyusutan, '%b %Y'), ' - ', DATE_FORMAT(selesai_penyusutan, '%b %Y'), ' (', durasi, ')'), '') AS durasi_format
				FROM tb_asset ";

		$view = $this->db->query(" 
			SELECT * 
			FROM ( $sql ) AS tb 
			WHERE 1 = 1 
			$condition 
			$order 
			$limit ")->result();

		$count = $this->db->query(" 
			SELECT COUNT(*) AS jumlah 
			FROM ( $sql ) AS tb 
			WHERE 1 = 1 
			$condition ")->row();
		$jumlah = isset($count->jumlah) ? $count->jumlah : 0;

		if ($view) {
			$i = 1;
			foreach ($view as $row) {

				$btn_penyusutan = '<a href="javascript:;" class="btn-penyusutan text-info" data-kode="'.$row->kode_asset.'" data-nama="'.$row->nama.'" data-id="'.$row->id.'" data-tglbeli="'.$row->tgl_beli.'" title="Setting Penyusutan"><i class="icon-chart"></i> </a>';

				$tools = $btn_penyusutan;

				$data[] = [
					'no' => $i++,
					'kode_asset' => $row->kode_asset,
					'nama' => $row->nama,
					'tgl_beli' => $row->tgl_beli_format,
					'unit' => $row->unit,
					'tarif' => $row->tarif,
					'durasi' => $row->durasi_format,
					'tools' => $tools
				];
			}
		}

		$response = [
			'draw' => $draw,
			'recordsTotal' => $jumlah,
			'recordsFiltered' => $jumlah,
			'data' => $data
		];

		return $response;
	}

	public function create_counter($jenis)
	{
		$db = daeyoung();
		$ms_jenis = $this->db->where(['id' => $jenis])->get($db.'.master_jenis_asset')->row();
		$kode = isset($ms_jenis->kode) ? $ms_jenis->kode : '';

		$tb_asset = $this->db->query("
			SELECT RIGHT(a.kode_asset, 3) AS maks 
			FROM $db.tb_asset a 
			WHERE a.jenis = '$jenis' 
			ORDER BY RIGHT(a.kode_asset, 3) DESC 
			LIMIT 1 ")->row();
		$maks = isset($tb_asset->maks) ? (int)$tb_asset->maks : 0;

		$maks = $maks + 1;
		switch (strlen($maks)) {
			case '1':
				$maks = '00'.$maks;
				break;
			case '2':
				$maks = '0'.$maks;
				break;
			default:
				$maks = $maks;
				break;
		}

		return $kode.$maks;
	}

	public function add_asset($data, $id)
	{
		$this->db->trans_begin();
		$db = daeyoung();

		if ($id == '') {
			$this->db->insert($db.'.tb_asset', $data);

			$id = $this->db->insert_id();

			$this->db->insert($db.'.tb_control_asset', [
				'id_asset' => $id,
				'kode' => $data['kode_asset'],
				'nama' => $data['nama'],
				'jumlah' => $data['unit'],
				'user_insert' => $data['user_insert'],
				'tgl' => $data['tgl_beli'],
				'status' => 'Baru'
			]);
		} else {
			$this->db->update($db.'.tb_asset', $data, ['id' => $id]);
		}

		if ($this->db->trans_status()) {
			$this->db->trans_commit();

			return true;
		} else {
			$this->db->trans_rollback();

			return false;
		}
	}

	public function pengurangan($data)
	{
		$db = daeyoung();
		$this->db->trans_begin();

		$this->db->query("
			UPDATE $db.tb_asset a 
			SET a.unit = (a.unit + ".$data['jumlah'].")
			WHERE a.id = '".$data['id_asset']."' ");

		$this->db->insert($db.'.tb_control_asset', $data);

		if ($this->db->trans_status()) {
			$this->db->trans_commit();

			return true;
		} else {
			$this->db->trans_rollback();

			return false;
		}
	}

	public function dt_history($id)
	{
		$db = daeyoung();
		return 
			$this->db->query("
				SELECT a.kode, a.nama, 
				a.tgl, a.jumlah, a.status, a.keterangan
				FROM $db.tb_control_asset a
				WHERE a.id_asset = '$id' 
				ORDER BY a.tgl DESC ")->result();
	}

	public function add_penyusutan($data, $id)
	{
		$db = daeyoung();
		$this->db->where(['id' => $id])
			->update($db.'.tb_asset', $data);

		if ($this->db->affected_rows() > 0) {
			return true;
		}

		return false;
	}

	public function ms_jenis($ids = [])
	{
		$db = daeyoung();
		$condition = '';
		if ($ids) {
			$condition .= " AND ( ";
			foreach ($ids as $id) {
				$condition .= " a.id = '$id' ";

				if (end($ids) != $id) {
					$condition .= ' OR ';
				}
			}
			$condition .= " ) ";
		}

		return 
			$this->db->query("
				SELECT * 
				FROM $db.master_jenis_asset a 
				WHERE a.status = 1
				$condition
				ORDER BY a.jenis ASC ")->result();
	}

	public function asset_jenis($jenis, $tgl)
	{
		$db = daeyoung();
		return
			$this->db->query("
				SELECT a.id, a.kode_asset, a.nama, a.keterangan, 
				a.unit, a.tarif, a.tgl_beli, a.harga_beli, a.durasi, b.merk,
				a.mulai_penyusutan, a.selesai_penyusutan
				FROM $db.tb_asset a 
				INNER JOIN $db.master_merk_asset b ON a.merk = b.id
				WHERE a.jenis = '$jenis' 
				AND a.tgl_beli <= '$tgl' ")->result();
	}

	public function dt_pengurangan($id, $thn)
	{
		$db = daeyoung();
		return 
			$this->db->query("
				SELECT a.id_asset, SUM(a.jumlah * b.harga_beli) AS jumlah 
				FROM $db.tb_control_asset a 
				INNER JOIN $db.tb_asset b ON a.id_asset = b.id
				WHERE a.id = '$id' 
				AND YEAR(a.tgl) = '$thn'
				AND a.jumlah < 0 
				GROUP BY a.id_asset ")->row();
	}
}

/* End of file Model_asset.php */
/* Location: ./application/models/Model_asset.php */ ?>