<?php

use Dompdf\FrameDecorator\Page;

class Model_warehouse_rak_system extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    //bahan
    function get_detail_baku()
    {
        $id = $this->input->post('id');
        $query = $this->db->query('SELECT * FROM warehouse_rak WHERE rak="' . $id . '" AND status =1 AND tipe=1 ORDER BY tgl_prd ASC')->result();
        if (empty($query)) {
            return 0;
        } else {
            return 1;
        }
    }

    function get_bahan($param = '')
    {
        $q = $this->db->query("SELECT * FROM master_barang WHERE status=1 AND kategori=16 AND (nama LIKE '%$param%' OR kode LIKE '%$param%') ORDER BY nama ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->kode . ' - ' . $row->nama . ' ' . $row->color
            );
        }
        return json_encode($data);
    }

    function get_bahan_head($param = '')
    {
        $q = $this->db->query("SELECT * FROM warehouse_rak a JOIN master_barang b ON a.`id_brg`=b.`id` WHERE a.`status`=1 AND a.`tipe`=1 AND (b.`nama` LIKE '%$param%' OR b.`kode` LIKE '%$param%') ORDER BY nama ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->kode . ' - ' . $row->nama . ' ' . $row->color
            );
        }
        return json_encode($data);
    }

    function get_detail_gudang_bahan()
    {
        $column_order = array(null, 'z.kode', 'z.nama', 'z.m_mtr', 'z.m_mtr', 'z.m_kg', 'z.tgl_prd', 'z.cust', 'z.ingrey');
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.tgl_prd';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY z.tgl_prd ASC";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT a.*,b.`kode`,b.`nama`
      FROM
      warehouse_rak a
      JOIN master_barang b
      ON a.`id_brg` = b.`id`
      WHERE a.`status` = 1 AND a.`rak`='" . $this->input->post('rak') . "' AND a.`tipe`=1) z
      WHERE (
		`z`.`kode` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `z`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `z`.`supp` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `z`.`ref_supp` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		) GROUP BY z.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $out = null;
                $kg = 0;
                $opsi = null;
                $out = $this->barang_keluar($r['id']);

                if (!empty($out)) {
                    $opsi = '<a class="text-primary" href="#" style="margin-right:20px" onClick="view_history_bahan(\'' . $r['id'] . '\')" title="View History"><span class="label label-primary">View History</span></a>';
                    foreach ($out as $brs) {
                        if ($brs->satuan == 2) {
                            $kg = $kg + $brs->k_kg;
                        } else {
                            $kg = $kg + $brs->k_kg;
                        }
                    }
                }

                $kg = ($r['m_kg'] != 0) ? ($r['m_kg'] - $kg) : 0;
                $pack = ceil($kg / $r['isi']);
                $no++;
                $row  = array(
                    $no . '.',
                    $r['kode'],
                    $r['nama'],
                    $r['isi'] . ' KG',
                    $kg . ' KG',
                    $pack . ' Dus',
                    $r['tgl_prd'],
                    $r['supp'],
                    $r['ref_supp'],
                    $opsi,
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function cek_data_bahan()
    {
        $query = $data = null;
        $kode = $this->input->post('key');
        $tipe = $this->input->post('tipe');
        if (!empty($kode)) {
            $query = $this->db->query("SELECT a.`rak` FROM warehouse_rak a JOIN master_barang b ON a.`id_brg`=b.`id` WHERE a.`status`=1 AND a.`tipe`=$tipe AND (
                `b`.`kode` LIKE '%" . $kode . "%' ESCAPE '!'
                    OR `b`.`nama` LIKE '%" . $kode . "%' ESCAPE '!'
                )  ")->result();
            $data = array('data' => $query);
            return $data;
        } else {
            return 0;
        }
    }

    function get_history_bahan()
    {
        $id = $this->input->post('lot');
        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS c.`kode`,c.`nama`,a.*
      FROM
      warehouse_rak_out a
      LEFT JOIN warehouse_rak b
      ON a.`id_header`=b.`id`
      JOIN master_barang c
      ON b.`id_brg`=c.`id`
      WHERE b.`status` = 1 AND a.`id_header`='" . $id . "' AND a.`status`=1");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $satuan = null;
                if ($r['satuan'] ==  1) {
                    $satuan = 'MTR';
                } else {
                    $satuan = 'YRD';
                }
                $no++;
                $row  = array(
                    $no . '.',
                    $r['lot'] . '-' . sprintf('%02d', $r['rol']) . 'R',
                    $r['kode'],
                    $r['nama'],
                    $r['k_mtr'],
                    $satuan,
                    $r['k_kg'] . ' KG',
                    $r['note'],
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }


    function insert_data_bahan()
    {
        $q = $this->db->query("SELECT id_header FROM warehouse_rak GROUP BY id_header ORDER BY id_header DESC LIMIT 1")->row();
        if (empty($q)) {
            $id_header = 1;
        } else {
            $id_header = $q->id_header + 1;
        }

        if (isset($_POST['idbrg'])) {
            foreach ($_POST['idbrg'] as $k => $v) {
                $isi = $qty = 0;
                $isi = str_replace(',', '.', str_replace('.', '', $_POST['isi'][$k]));
                $qty = str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k]));
                $data = array(
                    'id_header' => $id_header,
                    'id_brg' => $_POST['idbrg'][$k],
                    'tgl_prd' => $_POST['tglprd'][$k],
                    'isi' => $isi,
                    'satuan' => $_POST['satuan'][$k],
                    'm_kg' => $qty,
                    'supp' => $_POST['supp'][$k],
                    'ref_supp' => $_POST['refbahan'][$k],
                    'rak' => $_POST['rak'][$k],
                    'tipe' => 1,
                    'insert_at' => date('Y-m-d H:i:s'),
                    'insert_by' => $this->session->userdata('userid')
                );
                $this->db->insert('warehouse_rak', $data);
            }
            return 1;
        } else {
            return 0;
        }
    }

    //barang_jadi
    function get_detail_jadi()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("
        SELECT * FROM (
        SELECT z.`id`,z.`m_mtr`-SUM(z.mtr_keluar) AS s_mtr,z.`m_kg`-SUM(z.k_kg) AS s_kg FROM (
          SELECT
          a.`id`,
          a.`m_mtr`,
          a.`m_kg`,
            CASE WHEN b.satuan=2
            THEN b.k_mtr * 0.9144
            ELSE b.k_mtr
            END AS mtr_keluar,
            b.`k_kg`
          FROM
            warehouse_rak a
            LEFT JOIN warehouse_rak_out b
            ON a.`id`=b.`id_header` WHERE a.`status` = 1 AND b.`status`=1 AND a.`tipe`=3 AND a.`rak`='" . $id . "') z GROUP BY z.`id` ) c WHERE c.s_mtr > 0 AND c.s_kg > 0")->result();
        if (empty($query)) {
            return 0;
        } else {
            return 1;
        }
    }

    function get_detail_gudang_jadi()
    {
        $nz = 0;
        $column_order = array(null, 'z.lotno', 'z.kode', 'z.nama', 'z.m_mtr', 'z.m_mtr', 'z.m_kg', 'z.tgl_prd', 'z.cust', 'z.ingrey');
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.tgl_prd';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY z.tgl_prd ASC";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT a.*,CONCAT(a.`lot`,'-',a.`rol`) AS lotno,b.`kode`,b.`nama`
      FROM
      warehouse_rak a
      JOIN master_barang b
      ON a.`id_brg` = b.`id`
      WHERE a.`status` = 1 AND a.`rak`='" . $this->input->post('rak') . "' AND a.`tipe`=3) z
      WHERE (
		`z`.`kode` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `z`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `z`.`lotno` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		) GROUP BY z.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $out = null;
                $mtr = 0;
                $kg = 0;
                $yrd = 0;
                $opsi = null;
                $out = $this->barang_keluar($r['id']);

                if (!empty($out)) {
                    $opsi = '<a class="text-primary" href="#" style="margin-right:20px" onClick="view_history(\'' . $r['id'] . '\')" title="View History"><span class="label label-primary">View History</span></a>';
                    foreach ($out as $brs) {
                        if ($brs->satuan == 2) {
                            $mtr = $mtr + ($brs->k_mtr * 0.9144);
                            $yrd = $yrd + $brs->k_mtr;
                            $kg = $kg + $brs->k_kg;
                        } else {
                            $mtr = $mtr + $brs->k_mtr;
                            $yrd = $yrd + ($brs->k_mtr * 1.0936133);
                            $kg = $kg + $brs->k_kg;
                        }
                    }
                }

                $mtr = $r['m_mtr'] - $mtr;
                $yrd = ($r['m_mtr'] * 1.0936133) - $yrd;
                $kg = ($r['m_kg'] != 0) ? ($r['m_kg'] - $kg) : 0;
                if ($kg > 0 || $mtr > 0) {
                    $no++;
                    $row  = array(
                        $no . '.',
                        $r['lot'] . '-' . sprintf('%02d', $r['rol']) . 'R',
                        $r['kode'],
                        $r['nama'],
                        round($mtr, 6) . ' MTR',
                        round($yrd, 0) . ' YRD',
                        $kg . ' KG',
                        $r['tgl_prd'],
                        $r['cust'],
                        $r['ingrey'],
                        $opsi,
                    );

                    $data[] = $row;
                } else {
                    $nz = $nz + 1;
                }
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n - $nz,
            "recordsFiltered" => $n - $nz,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function cek_data_jadi()
    {
        $query = $data = null;
        $kode = $this->input->post('key');
        $tipe = $this->input->post('tipe');
        if (!empty($kode)) {
            $query = $this->db->query("SELECT * FROM (
                SELECT z.`rak`,z.`id_brg`,z.`id`,z.`lot`,z.`m_mtr`-SUM(z.`mtr_keluar`) AS s_mtr,z.`m_kg`-SUM(z.`k_kg`) AS s_kg FROM (
                  SELECT
                  a.`id`,
                  a.`id_brg`,
                  a.`lot`,
                  a.`rak`,
                  a.`m_mtr`,
                  a.`m_kg`,
                    CASE WHEN b.`satuan`=2
                    THEN b.`k_mtr` * 0.9144
                    ELSE b.`k_mtr`
                    END AS mtr_keluar,
                    b.`k_kg`
                  FROM
                    warehouse_rak a
                    LEFT JOIN warehouse_rak_out b
                    ON a.`id`=b.`id_header` WHERE a.`status` = 1 AND b.`status`=1 AND a.`tipe`='" . $tipe . "') z GROUP BY z.`id` ) c 
                    JOIN master_barang d ON c.`id_brg`=d.`id` WHERE c.`s_mtr` > 0 AND c.`s_kg` > 0 AND (
                `d`.`kode` LIKE '%" . $kode . "%' ESCAPE '!'
                    OR `d`.`nama` LIKE '%" . $kode . "%' ESCAPE '!'
                    OR `c`.`lot` LIKE '%" . $kode . "%' ESCAPE '!'
                )")->result();
            $data = array('data' => $query);
            return $data;
        } else {
            return 0;
        }
    }

    function get_history_jadi()
    {
        $id = $this->input->post('lot');
        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS b.`lot`,c.`kode`,c.`nama`,b.`rol`,a.*
      FROM
      warehouse_rak_out a
      LEFT JOIN warehouse_rak b
      ON a.`id_header`=b.`id`
      JOIN master_barang c
      ON b.`id_brg`=c.`id`
      WHERE b.`status` = 1 AND a.`id_header`='" . $id . "' AND a.`status`=1");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $satuan = null;
                if ($r['satuan'] ==  1) {
                    $satuan = 'MTR';
                } else {
                    $satuan = 'YRD';
                }
                $no++;
                $row  = array(
                    $no . '.',
                    $r['lot'] . '-' . sprintf('%02d', $r['rol']) . 'R',
                    $r['kode'],
                    $r['nama'],
                    $r['k_mtr'],
                    $satuan,
                    $r['k_kg'] . ' KG',
                    $r['note'],
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_barang_jadi($param = '')
    {
        $q = $this->db->query("SELECT * FROM master_barang WHERE status=1 AND kategori=3 AND (nama LIKE '%$param%' OR kode LIKE '%$param%') ORDER BY nama ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->kode . ' - ' . $row->nama . ' ' . $row->color
            );
        }
        return json_encode($data);
    }




    function insert_data_jadi()
    {
        $q = $this->db->query("SELECT id_header FROM warehouse_rak GROUP BY id_header ORDER BY id_header DESC LIMIT 1")->row();
        if (empty($q)) {
            $id_header = 1;
        } else {
            $id_header = $q->id_header + 1;
        }

        if (isset($_POST['lot'])) {
            foreach ($_POST['lot'] as $k => $v) {
                $qty = 0;
                if ($_POST['satuan'][$k] == 2) {
                    $qty = 0.9144 * str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k]));
                } else {
                    $qty = str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k]));
                }
                $data = array(
                    'id_header' => $id_header,
                    'id_brg' => $_POST['idbrg'][$k],
                    'lot' => $_POST['lot'][$k],
                    'rol' => $_POST['rol'][$k],
                    'tgl_prd' => $_POST['tglprd'][$k],
                    'm_mtr' => $qty,
                    'satuan' => $_POST['satuan'][$k],
                    'm_kg' => str_replace(',', '.', str_replace('.', '', $_POST['berat'][$k])),
                    'cust' => $_POST['cust'][$k],
                    'ingrey' => $_POST['ingrey'][$k],
                    'rak' => $_POST['rak'][$k],
                    'tipe' => 3,
                    'insert_at' => date('Y-m-d H:i:s'),
                    'insert_by' => $this->session->userdata('userid')
                );
                $this->db->insert('warehouse_rak', $data);
            }
            return 1;
        } else {
            return 0;
        }
    }

    function insert_data_out_jadi()
    {
        $rol = 0;
        $berat = 0;
        $qty = 0;
        $satuan = 0;
        if (!empty($_POST['id'])) {
            foreach ($_POST['id'] as $k => $v) {
                $satuan = $_POST['satuan'][$k];
                $data = array(
                    'id_header' => $_POST['id'][$k],
                    'k_mtr' => str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k])),
                    'satuan' => $_POST['satuan'][$k],
                    'k_kg' => str_replace(',', '.', str_replace('.', '', $_POST['berat'][$k])),
                    'status' => 1,
                    'note' => $this->input->post('note_out'),
                    'insert_at' => date('Y-m-d H:i:s'),
                    'insert_by' => $this->session->userdata('userid')
                );
                $res = $this->db->insert('warehouse_rak_out', $data);
                $rol = $rol + 1;
                $berat = $berat + str_replace(',', '.', str_replace('.', '', $_POST['berat'][$k]));
                $qty = $qty + str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k]));
            }
            if ($res == TRUE) {
                $data = array(
                    'id_branch' => $this->input->post('customer'),
                    'no_surat' => $this->input->post('no_surat'),
                    'tanggal' => $this->input->post('tanggal'),
                    'rol' => $rol,
                    'total_kg' => $berat,
                    'total_qty' => $qty,
                    'satuan' => $satuan,
                    'insert_at' => date('Y-m-d H:i:s'),
                    'insert_by' => $this->session->userdata('userid')
                );
                $this->db->insert('warehouse_surat_jalan', $data);
                $id = $this->db->insert_id();
                foreach ($_POST['id'] as $k => $v) {
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    function get_barang_out_jadi()
    {
        $data = null;
        $q = null;
        $nama = null;
        $id_brg = 0;
        $meter = 0;
        $yard = 0;
        $berat = 0;
        $satuan = 0;
        $rak = $id_header  = $tgl = null;
        $lot = $this->input->post('lot');
        $rol = $this->input->post('rol');
        $q = $this->db->query("SELECT *,a.`id` AS id_rak,a.`satuan` AS satuan1 FROM warehouse_rak a LEFT JOIN warehouse_rak_out b ON a.`id`=b.`id_header` AND b.`status`=1 JOIN master_barang c ON a.`id_brg`=c.`id` WHERE a.`tipe`=3 AND a.`lot`='" . $lot . "' AND a.`rol` = '" . $rol . "' GROUP BY a.`id`")->result();
        if (!empty($q)) {
            foreach ($q as $row) {
                $id_header = $row->id_rak;
                $out = null;
                $mtr = 0;
                $kg = 0;
                $yrd = 0;
                $jumlah = 0;
                $id_brg = $row->id_brg;
                $out = $this->barang_keluar($row->id_rak);
                $nama = $row->kode . ' - ' . $row->nama . ' ' . $row->color;
                $rak = $row->rak;
                $tgl = $row->tgl_prd;
                if (!empty($out)) {
                    foreach ($out as $brs) {
                        if ($brs->satuan == 2) {
                            $mtr = $mtr + ($brs->k_mtr * 0.9144);
                            $yrd = $yrd + $brs->k_mtr;
                            $kg = $kg + $brs->k_kg;
                        } else {
                            $mtr = $mtr + $brs->k_mtr;
                            $yrd = $yrd + ($brs->k_mtr * 1.0936133);
                            $kg = $kg + $brs->k_kg;
                        }
                    }
                }
                $meter = $row->m_mtr;
                $yard = $row->m_mtr * 1.0936133;
                $berat = $row->m_kg - $kg;
                $satuan = $row->satuan1;
            }
            if ($satuan == 1) {
                $jumlah = $meter - $mtr;
                if (ceil($jumlah) < 1 || ceil($berat) < 1) {
                    return $data;
                } else {
                    $data = array(
                        'id_header' => $id_header,
                        'id_brg' => $id_brg,
                        'tgl' => $tgl,
                        'rak' => $rak,
                        'nama' => $nama,
                        'jumlah' => number_format($jumlah, 4, ',', '.'),
                        'satuan' => $satuan,
                        'berat' => number_format($berat, 2, ',', '.'),
                    );
                }
            } else {
                $jumlah = $yard - $yrd;
                echo $berat;
                if (ceil($jumlah) < 1 || ceil($berat) < 1) {
                    return $data;
                } else {
                    $data = array(
                        'id_header' => $id_header,
                        'id_brg' => $id_brg,
                        'tgl' => $tgl,
                        'rak' => $rak,
                        'nama' => $nama,
                        'jumlah' => number_format($jumlah, 4, ',', '.'),
                        'satuan' => $satuan,
                        'berat' => number_format($berat, 2, ',', '.'),
                    );
                }
            }
        }
        return $data;
    }

    //global
    function barang_keluar($id)
    {
        return $this->db->query("SELECT * FROM warehouse_rak_out a WHERE a.`id_header`=$id AND a.`status`=1")->result();
    }

    function get_customer($param = null)
    {
        $q = $this->db->query("SELECT * FROM master_customers WHERE status=1 AND (nama LIKE '%$param%' OR kode LIKE '%$param%') ORDER BY kode ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->kode . ' - ' . $row->nama
            );
        }
        return json_encode($data);
    }
}
