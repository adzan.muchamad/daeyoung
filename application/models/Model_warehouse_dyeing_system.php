<?php
class Model_warehouse_dyeing_system extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $kondisi = null;
        $column_order = array(null, 'b.stock_id', 'b.description', 'a.flag', null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'b.description';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY b.description ASC";
        }
        if (!empty($this->input->post('searchlokasi'))) {
            $kondisi = " AND a.`flag` = '" . $this->input->post('searchlokasi') . "'";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS *,a.`id` AS id_header
      FROM
      warehouse_inventory_dyeing a
    JOIN 0_stock_master b
          ON a.`id_barang` = b.`idbrg`
      WHERE a.`status` = 1 AND (
		`b`.`description` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `b`.`stock_id` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		) $kondisi GROUP BY a.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                if ($r['flag'] == 1) {
                    $lokasi = 'Dyeing';
                } else if ($r['flag'] == 2) {
                    $lokasi = 'Finishing';
                }
                $res = $this->get_stok($r['id_header']);
                $opsi = '<a class="text-success" href="#" style="margin-right:20px" onClick="view_data(\'' . $r['id_header'] . '\')" title="Lihat Riwayat"><i class="icon-file-eye"></i></a> <a class=text-warning" href="#" onClick="delete_data(\'' . $r['id_header'] . '\')" title="Hapus"><i class="icon-bin"></i></a>';
                $no++;
                $row  = array(
                    $no . '.',
                    $r['stock_id'],
                    $r['description'],
                    $lokasi,
                    $res['stok_kgm'],
                    $res['stok_yrd'],
                    $res['stok_mtr'],
                    $opsi
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_stok($id)
    {
        $stok_kgm = $stok_yrd = $stok_mtr =  0;
        $query = $this->db->query("SELECT SUM(COALESCE(m_kgm,0)) AS m_kgm,SUM(COALESCE(k_kgm,0)) AS k_kgm,SUM(COALESCE(m_yrd,0)) AS m_yrd,SUM(COALESCE(k_yrd,0)) AS k_yrd,
        SUM(COALESCE(m_mtr,0)) AS m_mtr,SUM(COALESCE(k_mtr,0)) AS k_mtr FROM warehouse_inventory_dyeing_detail WHERE id_header = $id AND status =1 GROUP BY id_header")->result();
        if (!empty($query)) {
            foreach ($query as $row) {
                if (!empty($row->m_mtr)) {
                    $stok_mtr = $stok_mtr + $row->m_mtr;
                }
                if (!empty($row->k_mtr)) {
                    $stok_mtr = $stok_mtr - $row->k_mtr;
                }
                if (!empty($row->m_kgm)) {
                    $stok_kgm = $stok_kgm + $row->m_kgm;
                }
                if (!empty($row->k_kgm)) {
                    $stok_kgm = $stok_kgm - $row->k_kgm;
                }
                if (!empty($row->m_yrd)) {
                    $stok_yrd = $stok_yrd + $row->m_yrd;
                }
                if (!empty($row->k_yrd)) {
                    $stok_yrd = $stok_yrd - $row->k_yrd;
                }
            }
        }
        $data = array('stok_kgm' => $stok_kgm, 'stok_yrd' => $stok_yrd, 'stok_mtr' => $stok_mtr);
        return $data;
    }


    function get_barang($param = '')
    {
        $db = $this->load->database('inventory', TRUE);
        $q = $db->query("SELECT a.`idbrg` AS id,a.`description`,a.`stock_id`,b.`description` AS kategori FROM 0_stock_master a JOIN 0_stock_category b ON a.`category_id` = b.`category_id` AND (b.`category_id` = 3 OR b.`category_id` = 21)
        WHERE (a.`description` LIKE '%$param%' OR a.`stock_id` LIKE '%$param%' OR b.`description` LIKE '%$param%') ORDER BY a.`description` ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" =>  $row->stock_id . ' - ' . $row->description . ' ( ' . $row->kategori . ' )'
            );
        }
        return json_encode($data);
    }

    function get_barang_daftar($param = '')
    {
        $lokasi = null;
        $q = $this->db->query("SELECT b.`id`,a.`description`,a.`stock_id`,b.`flag` AS lokasi FROM 0_stock_master a JOIN warehouse_inventory_dyeing b ON a.`id` = b.`id_barang`
        WHERE (a.`description` LIKE '%$param%' OR a.`stock_id` LIKE '%$param%') AND b.`status`=1 ORDER BY a.`description` ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            if ($row->lokasi == 1) {
                $lokasi = ' ( DYEING )';
            } else if ($row->lokasi == 2) {
                $lokasi = ' ( FINISHING )';
            }

            $data[] = array(
                "id" => $row->id,
                "text" =>  $row->stock_id . ' - ' . $row->description . $lokasi
            );
        }
        return json_encode($data);
    }

    function insert_data()
    {
        $db = $this->load->database('inventory', TRUE);
        $id = $this->input->post('barang');
        $lokasi = $this->input->post('lokasi');
        $msg = $res = $cek = null;
        $q = $this->db->query("SELECT * FROM warehouse_inventory_dyeing WHERE id_barang = $id AND flag = $lokasi")->result();
        if (empty($q)) {
            $data = array(
                'id_barang' => $id,
                'flag' => $lokasi,
                'insert_at' => date('Y-m-d'),
                'insert_by' => $this->session->userdata('userid')
            );
            $res = $this->db->insert('warehouse_inventory_dyeing', $data);
            if ($res == true) {
                $cek = $this->db->query("SELECT * FROM 0_stock_master WHERE idbrg= $id")->row();
                if (empty($cek)) {
                    $r = $db->query("SELECT * FROM 0_stock_master WHERE idbrg = $id")->row();
                    $data = array(
                        'id' => $r->idbrg,
                        'stock_id' => $r->stock_id,
                        'category_id' => $r->category_id,
                        'tax_type_id' => $r->tax_type_id,
                        'description' => $r->description,
                        'long_description' => $r->long_description,
                        'units' => $r->units,
                        'mb_flag' => $r->mb_flag,
                        'sales_account' => $r->sales_account,
                        'cogs_account' => $r->cogs_account,
                        'inventory_account' => $r->inventory_account,
                        'adjustment_account' => $r->adjustment_account,
                        'wip_account' => $r->wip_account,
                        'dimension_id' => $r->dimension_id,
                        'dimension2_id' => $r->dimension2_id,
                        'purchase_cost' => $r->purchase_cost,
                        'material_cost' => $r->material_cost,
                        'labour_cost' => $r->labour_cost,
                        'overhead_cost' => $r->overhead_cost,
                        'inactive' => $r->inactive,
                        'no_sale' => $r->no_sale,
                        'no_purchase' => $r->no_purchase,
                        'editable' => $r->editable,
                        'depreciation_method' => $r->depreciation_method,
                        'depreciation_rate' => $r->depreciation_rate,
                        'depreciation_factor' => $r->depreciation_factor,
                        'depreciation_start' => $r->depreciation_start,
                        'depreciation_date' => $r->depreciation_date,
                        'fa_class_id' => $r->fa_class_id,
                        'scrap' => $r->scrap,
                        'produksi_flag' => $r->produksi_flag,
                        'composisi' => $r->composisi,
                        'width' => $r->width,
                        'gramasi' => $r->gramasi,
                        'color' => $r->color,
                        'subcont' => $r->subcont,
                        'expense_account' => $r->expense_account,
                        'nofasilitas' => $r->nofasilitas,
                        'wip' => $r->wip
                    );
                    $this->db->insert('0_stock_master', $data);
                    return 1;
                } else {
                    return 1;
                }
            } else {
                return 0;
            }
        } else {
            $msg = 'Barang Sudah Pernah di Input';
            return $msg;
        }
    }

    function insert_data_in()
    {
        $qty3 = 0;
        $barang = $this->input->post('barang');
        $qty1 = $this->input->post('qty1');
        if (!empty($qty1)) {
            $qty1 = str_replace('.', '', $qty1);
            $qty1 = str_replace(',', '.', $qty1);
        } else {
            $qty1 = 0;
        }
        $qty2 = $this->input->post('qty2');
        if (!empty($qty2)) {
            $qty2 = str_replace('.', '', $qty2);
            $qty2 = str_replace(',', '.', $qty2);
        } else {
            $qty2 = 0;
        }
        $satuan = $this->input->post('satuan2');
        if ($satuan == 1) {
            $qty3 = $qty2;
            $qty2 = 0;
        }
        $lot = $this->input->post('lot_barang');
        $ket = $this->input->post('ket');
        $data = array(
            'id_header' => $barang,
            'm_kgm' => $qty1,
            'm_yrd' => $qty2,
            'm_mtr' => $qty3,
            'lot' => $lot,
            'flag' => 1,
            'ket' => $ket,
            'insert_at' => date('Y-m-d'),
            'insert_by' => $this->session->userdata('userid')
        );
        $result = $this->db->insert('warehouse_inventory_dyeing_detail', $data);
        if ($result ==  true) {
            return 1;
        } else {
            return 0;
        }
    }

    function insert_data_out()
    {
        $qty3 = 0;
        $barang = $this->input->post('barang');
        $qty = $this->input->post('qty');
        if (!empty($qty)) {
            $qty = str_replace('.', '', $qty);
            $qty = str_replace(',', '.', $qty);
        } else {
            $qty = 0;
        }
        $qty2 = $this->input->post('qty2');
        if (!empty($qty2)) {
            $qty2 = str_replace('.', '', $qty2);
            $qty2 = str_replace(',', '.', $qty2);
        } else {
            $qty2 = 0;
        }
        $satuan = $this->input->post('satuan2');
        if ($satuan == 1) {
            $qty3 = $qty2;
            $qty2 = 0;
        }
        $lot = $this->input->post('lot_barang');
        $ket = $this->input->post('ket');
        $data = array(
            'id_header' => $barang,
            'k_kgm' => $qty,
            'k_yrd' => $qty2,
            'k_mtr' => $qty3,
            'lot' => $lot,
            'flag' => 2,
            'ket' => $ket,
            'insert_at' => date('Y-m-d'),
            'insert_by' => $this->session->userdata('userid')
        );
        $result = $this->db->insert('warehouse_inventory_dyeing_detail', $data);
        if ($result ==  true) {
            return 1;
        } else {
            return 0;
        }
    }

    function delete_data($id = '')
    {
        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $return = $this->db->update('warehouse_inventory_dyeing', $data);
        if ($return == true) {
            return 1;
        } else {
            return 0;
        }
    }

    function get_data_barang()
    {
        $stok_kgm =  $stok_yrd = 0;
        $kondisi = null;
        if (!empty($this->input->post('searchDateFirst')) || !empty($this->input->post('searchDateFinish'))) {
            $kondisi = " AND b.`insert_at` BETWEEN '" . $this->input->post('searchDateFirst') . "' AND '" . $this->input->post('searchDateFinish') . "'";
        }
        if (!empty($this->input->post('idbarang'))) {
            $kondisi .= " AND b.`id_header` = '" . $this->input->post('idbarang') . "'";
        }
        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS b.*
      FROM
      warehouse_inventory_dyeing a
    JOIN warehouse_inventory_dyeing_detail b
    ON a.`id` = b.`id_header`
      WHERE a.`status` = 1 AND b.`status` = 1 AND (
	b.`lot` LIKE '%" . $this->input->post('searchLot') . "%' ESCAPE '!' OR b.`ket` LIKE '%" . $this->input->post('searchLot') . "%' ESCAPE '!')
        $kondisi GROUP BY b.`id` ORDER BY b.`timestamp` ASC");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        $res = $this->get_stok_riwayat($this->input->post('idbarang'), $this->input->post('searchDateFirst'));
        $row  = array(
            null,
            null,
            null,
            null,
            null,
            $res['stok_kgm_before'],
            $res['stok_yrd_before'],
            $res['stok_mtr_before'],
            null,
            null,
            null
        );
        $stok_kgm = $res['stok_kgm_before'];
        $stok_yrd = $res['stok_yrd_before'];
        $stok_mtr = $res['stok_mtr_before'];
        $data[] = $row;
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $qty_mtr = $qty_yrd = $qty_kgm = 0;
                $no++;
                if ($r['flag'] == 1) {
                    $opsi = '<a class="text-success" title="IN"><input class="stat" type="hidden" value="in"/><i class="icon-arrow-up-right3"></input> IN</a>';
                } else if ($r['flag'] == 2) {
                    $opsi = '<a class="text-warning" title="OUT"><input class="stat" type="hidden" value="out"/><i class="icon-arrow-down-left3"></i> OUT</a>';
                }
                if (!empty($r['m_mtr'])) {
                    $qty_mtr = $r['m_mtr'];
                    $stok_mtr = $stok_mtr + $r['m_mtr'];
                }
                if (!empty($r['k_mtr'])) {
                    $qty_mtr = $r['k_mtr'];
                    $stok_mtr = $stok_mtr - $r['m_mtr'];
                }
                if (!empty($r['m_yrd'])) {
                    $qty_yrd = $r['m_yrd'];
                    $stok_yrd = $stok_yrd + $r['m_yrd'];
                }
                if (!empty($r['k_yrd'])) {
                    $qty_yrd = $r['k_yrd'];
                    $stok_yrd = $stok_yrd - $r['k_yrd'];
                }
                if (!empty($r['m_kgm'])) {
                    $qty_kgm = $r['m_kgm'];
                    $stok_kgm = $stok_kgm + $r['m_kgm'];
                }
                if (!empty($r['k_kgm'])) {
                    $qty_kgm = $r['k_kgm'];
                    $stok_kgm = $stok_kgm - $r['k_kgm'];
                }

                $row  = array(
                    $no . '.',
                    $r['lot'],
                    $qty_kgm,
                    $qty_yrd,
                    $qty_mtr,
                    $stok_kgm,
                    $stok_yrd,
                    $stok_mtr,
                    $r['timestamp'],
                    $r['ket'],
                    $opsi
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_stok_riwayat($id, $tgl_mulai)
    {
        $stok_kgm = $stok_yrd = $stok_mtr  = 0;
        if (!empty($id)) {
            $query = $this->db->query("SELECT SUM(COALESCE(m_kgm,0)) AS m_kgm,SUM(COALESCE(k_kgm,0)) AS k_kgm,SUM(COALESCE(m_yrd,0)) AS m_yrd,SUM(COALESCE(k_yrd,0)) AS k_yrd,
            SUM(COALESCE(m_mtr,0)) AS m_mtr,SUM(COALESCE(k_mtr,0)) AS k_mtr FROM warehouse_inventory_dyeing_detail WHERE id_header = $id AND status =1 AND insert_at < '" . $tgl_mulai . "'GROUP BY id_header")->result();
            if (!empty($query)) {
                foreach ($query as $row) {
                    if (!empty($row->m_mtr)) {
                        $stok_mtr = $stok_mtr + $row->m_mtr;
                    }
                    if (!empty($row->k_mtr)) {
                        $stok_mtr = $stok_mtr - $row->k_mtr;
                    }
                    if (!empty($row->m_kgm)) {
                        $stok_kgm = $stok_kgm + $row->m_kgm;
                    }
                    if (!empty($row->k_kgm)) {
                        $stok_kgm = $stok_kgm - $row->k_kgm;
                    }
                    if (!empty($row->m_yrd)) {
                        $stok_yrd = $stok_yrd + $row->m_yrd;
                    }
                    if (!empty($row->k_yrd)) {
                        $stok_yrd = $stok_yrd - $row->k_yrd;
                    }
                }
            }
            $data = array('stok_kgm_before' => $stok_kgm, 'stok_yrd_before' => $stok_yrd, 'stok_mtr_before' => $stok_mtr);
            return $data;
        } else {
            $data = array('stok_kgm_before' => 0, 'stok_yrd_before' => 0, 'stok_mtr_before' => 0);
            return $data;
        }
    }

    function view_data($id = null)
    {
        $lokasi = null;
        if (!empty($id)) {
            $query = $this->db->query("SELECT b.`stock_id`,b.`description`,a.`flag` FROM warehouse_inventory_dyeing a JOIN 0_stock_master b ON a.`id_barang`=b.`idbrg` WHERE a.`id` = $id")->row();
            if (!empty($query)) {
                if ($query->flag == 1) {
                    $lokasi = 'Dyeing';
                } else if ($query->flag == 2) {
                    $lokasi = 'Finishing';
                }
                $data = array('kode_barang' => $query->stock_id, 'lokasi' => $lokasi, 'nama_barang' => $query->description);
                return $data;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
}
