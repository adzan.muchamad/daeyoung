<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_main extends CI_Model {

    function time_server()
    {
        $query = $this->db->query("SELECT NOW() AS time")->row();
        return isset($query->time) ? $query->time : '';
    }

    function view_by_id($table='',$condition='',$row='row')
    {
        if($row == 'row') {
            if($condition) {
                return $this->db->where($condition)->get($table)->row();
            } else {
                return $this->db->get($table)->row();
            }
        } else if($row == 'result') {
            if($condition) {
                return $this->db->where($condition)->get($table)->result();
            } else {
                return $this->db->get($table)->result();
            }
        } else if($row == 'num_rows') {
            if($condition) {
                return $this->db->where($condition)->get($table)->num_rows();
            } else {
                return $this->db->get($table)->num_rows();
            }
        }
    }

    function process_data($table='', $data='', $condition='') 
    {
        if($condition) {
            $this->db->where($condition)->update($table, $data);
            return $this->db->affected_rows();
        } else {
            $this->db->insert($table, $data);
            return $this->db->insert_id();
        }
    }

    function delete_data($table='', $condition='')
    {
        $this->db->where($condition)->delete($table);
        return $this->db->affected_rows();
    }

    function ajax_function()
    {
        $is_ajax = $this->input->is_ajax_request();
        if($is_ajax == TRUE) {
            return TRUE;
        } else {
            show_404();
        }
    }

    function replace_money($val=0)
    {
        $result = '';

        if($val != '' || $val != 0) {
            $result = str_replace(".","",$val);
            $result = str_replace(",",".",$result);
        }

        return $result;
    }

    function gt_counter($kode = '')
    {
        $respon = [];

        $tahun_def = date('Y');
        $bulan_def = date('m');

        $check_exists = $this->view_by_id('acc_counter', ['kode' => $kode]);
        if(empty($check_exists)) {
            $respon['nomor'] = $kode.'/1/'.$bulan_def.'/'.$tahun_def;

            $this->db->insert('acc_counter', [
                'kode' => $kode,
                'counter' => 1,
                'bulan' => $bulan_def,
                'tahun' => $tahun_def
            ]);
        }

        if(!empty($check_exists)) {
            $bulan_exists = isset($check_exists->bulan) ? $check_exists->bulan:'';
            $tahun_exists = isset($check_exists->tahun) ? $check_exists->tahun:'';

            if($bulan_exists != $bulan_def || $tahun_exists != $tahun_def) {
                $bulan_def = ($bulan_exists == $bulan_def) ? $bulan_exists:$bulan_def;
                $tahun_def = ($tahun_exists == $tahun_def) ? $tahun_exists:$tahun_def;

                $respon['nomor'] = $kode.'/1/'.$bulan_def.'/'.$tahun_def;

                $this->db->where('kode', $kode)
                ->update('acc_counter', [
                    'counter' => 1,
                    'bulan' => $bulan_def,
                    'tahun' => $tahun_def
                ]);
                
            } else {
                $counter_exists = isset($check_exists->counter) ? $check_exists->counter:0;
                $counter_exists = $counter_exists + 1;

                $respon['nomor'] = $kode.'/'.$counter_exists.'/'.$bulan_exists.'/'.$tahun_exists;

                $this->db->where('kode', $kode)
                ->update('acc_counter', [
                    'counter' => $counter_exists
                ]);
                
            }
        }

        return $respon;
    }

    function opt_bulan()
    {
        $result[] = '- Pilih Bulan -';
        $result['01'] = 'Januari';
        $result['02'] = 'Februari';
        $result['03'] = 'Maret';
        $result['04'] = 'April';
        $result['05'] = 'Mei';
        $result['06'] = 'Juni';
        $result['07'] = 'Juli';
        $result['08'] = 'Agustus';
        $result['09'] = 'September';
        $result['10'] = 'Oktober';
        $result['11'] = 'November';
        $result['12'] = 'Desember';
        return $result;
    }

    function get_bulan($bln)
    {
        switch ($bln){
            case '01': 
                    return "Januari";
                    break;
            case '02':
                    return "Februari";
                    break;
            case '03':
                    return "Maret";
                    break;
            case '04':
                    return "April";
                    break;
            case '05':
                    return "Mei";
                    break;
            case '06':
                    return "Juni";
                    break;
            case '07':
                    return "Juli";
                    break;
            case '08':
                    return "Agustus";
                    break;
            case '09':
                    return "September";
                    break;
            case '10':
                    return "Oktober";
                    break;
            case '11':
                    return "November";
                    break;
            case '12':
                    return "Desember";
                    break;
        }
    }
}

/* End of file Model_main.php */

?>