<?php

class Model_inventory_so extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $kondisi = null;
        $column_order = array(null, 'a.reference', 'a.loc_nama', 'a.memo', 'a.tran_date', 'a.insert_by', null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.trans_no';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY a.trans_no ASC";
        }
        if (!empty($this->input->post('searchkat'))) {
            $kondisi = " AND a.`loc_code` = '" . $this->input->post('searchkat') . "'";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.*,b.`loc_nama`,c.`keterangan` FROM draf_so a
        JOIN master_loc b ON a.`loc_code`=b.`kode` JOIN ms_user c ON a.`insert_by`=c.`id`
      WHERE a.`status` = 1 AND (
		`a`.`reference` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `b`.`loc_nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		) $kondisi GROUP BY a.`trans_no` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $opsi = '<a class="text-primary" href="#" style="margin-right:20px" onClick="view_data(\'' . $r['trans_no'] . '\')" title="Detail Supplier"><i class="icon-file-eye"></i></a><a class="text-success" href="#" style="margin-right:20px" onClick="update_data(\'' . $r['trans_no'] . '\')" title="Edit"><i class="icon-pencil"></i></a> <a class="text-warning" href="#" onClick="delete_data(\'' . $r['trans_no'] . '\')" title="Hapus"><i class="icon-bin"></i></a>';
                $no++;
                $row  = array(
                    $no . '.',
                    $r['reference'],
                    $r['loc_nama'],
                    $r['memo'],
                    $r['tran_date'],
                    $r['keterangan'],
                    $opsi
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_lokasibarang()
    {
        $query = $this->db->query("SELECT * FROM master_loc WHERE status=0 AND flag=0");
        return $query;
    }

    function get_barang($param = '')
    {
        $db = $this->load->database('inventory', TRUE);
        $q = $db->query("SELECT * FROM (SELECT *,replace(a.`description`,'_',' ') AS nama_barang FROM 0_stock_master a WHERE inactive=0) aa 
        WHERE (aa.`stock_id` LIKE '%$param%' OR aa.`nama_barang` LIKE '%$param%' OR aa.`long_description` LIKE '%$param%') ORDER BY aa.`nama_barang` ASC LIMIT 30")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->stock_id,
                "text" => $row->stock_id . ' - ' . $row->description
            );
        }
        return json_encode($data);
    }

    function get_satuanbarang()
    {
        $id = $this->input->post('id');
        $db = $this->load->database('inventory', TRUE);
        $q = $db->query("SELECT units FROM 0_stock_master a WHERE a.`stock_id` = '" . $id . "'")->row();
        return $q->units;
    }

    function insert_data()
    {
        $db = $this->load->database('inventory', TRUE);
        $tanggal = $this->input->post('tanggal');
        $lokasi = $this->input->post('lokasi');
        $ket = $this->input->post('ket');
        $qty = 0;
        $counter = $this->db->query("SELECT RIGHT(a.`reference`,5) AS coun FROM draf_so a WHERE LEFT(RIGHT(a.`reference`,7),2)='" . $this->session->userdata("userid") . "' ORDER BY a.`reference` DESC LIMIT 1")->row();
        $trans_no = $this->db->query("SELECT trans_no FROM draf_so a ORDER BY a.`trans_no` DESC LIMIT 1")->row();
        if (empty($trans_no)) {
            $trans_no = $db->query("SELECT trans_no FROM 0_stock_opname a ORDER BY a.`trans_no` DESC LIMIT 1")->row();
            $counter = $db->query("SELECT RIGHT(a.`reference`,5) AS coun FROM 0_refs a WHERE LEFT(RIGHT(a.`reference`,7),2)='" . $this->session->userdata("userid") . "' ORDER BY a.`reference` DESC LIMIT 1")->row();
        }
        $date = date('Y-m-d');
        $tahun = substr($date, 2, 2);
        $ref = "OB/PTDYT/" . $tahun . "/" . sprintf('%02d', $this->session->userdata("userid")) . sprintf('%05d', $counter->coun + 1);
        if (isset($_POST['barang'])) {
            foreach ($_POST['barang'] as $k => $v) {
                $qty = str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k]));
                $data = array(
                    'trans_no' => $trans_no->trans_no + 1,
                    'stock_id' => $_POST['barang'][$k],
                    'loc_code' => $lokasi,
                    'tran_date' => $tanggal,
                    'reference' => $ref,
                    'qty' => $qty,
                    'status' => 1,
                    'insert_at' => date('Y-m-d H:i:s'),
                    'insert_by' => $this->session->userdata('userid'),
                    'memo' => $ket
                );
                $this->db->insert('draf_so', $data);
            }
            return 1;
        } else {
            return 0;
        }
    }

    function update_data()
    {
        $trans_no = $this->input->post('id');
        $tanggal = $this->input->post('tanggal');
        $lokasi = $this->input->post('lokasi');
        $ket = $this->input->post('ket');
        $qty = 0;
        $date = date('Y-m-d');
        $ref = $this->input->post('ref');
        $data = array('status' => 9, 'update_at' => date('Y-m-d H:i:s'), 'update_by' => $this->session->userdata('userid'));
        $this->db->where('trans_no', $trans_no);
        $this->db->update('draf_so', $data);
        if (isset($_POST['barang'])) {
            foreach ($_POST['barang'] as $k => $v) {
                $qty = str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k]));
                $data = array(
                    'trans_no' => $trans_no,
                    'stock_id' => $_POST['barang'][$k],
                    'loc_code' => $lokasi,
                    'tran_date' => $tanggal,
                    'reference' => $ref,
                    'qty' => $qty,
                    'status' => 1,
                    'insert_at' => date('Y-m-d H:i:s'),
                    'insert_by' => $this->session->userdata('userid'),
                    'memo' => $ket
                );
                $this->db->insert('draf_so', $data);
            }
            return 1;
        } else {
            return 0;
        }
    }

    function delete_data($id = '')
    {
        $data = array(
            'status' => 8,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('trans_no', $id);
        $return = $this->db->update('draf_so', $data);
        if ($return == true) {
            return 1;
        } else {
            return 0;
        }
    }

    function select_data()
    {
        $db = $this->load->database('inventory', TRUE);
        $det = $satuan = null;
        $barang = array();
        $no = 1;
        $i = $j = 0;
        $trans_no = $this->input->post('id');
        $query = $this->db->query("SELECT * FROM draf_so WHERE trans_no = $trans_no AND status=1")->result();
        foreach ($query as $row) {
            $qty = number_format($row->qty, 6, ',', '.');
            $satuan = $db->query("SELECT * FROM 0_stock_master WHERE stock_id='" . $row->stock_id . "'")->row();
            $barang[$i]['id'] = $satuan->stock_id;
            $barang[$i]['namabarang'] = $satuan->description;
            $i++;
            if ($no == 1) {
                $det .= '<div class="row">
                <div class="col-lg-1 text-center">
                    <label>No</label>
                    <input class="form-control no_urut text-center" type="text" readonly />
                </div>
                <div class="col-lg-7 text-center">
                    <label>Barang</label>
                    <select class="form-control barang" name="barang[]" id="get_barang_' . $j . '"  onchange="get_satuan(this);">
                    </select>
                </div>
                <div class="col-lg-2 text-center">
                    <label>Qty</label>
                    <input class="form-control qty" type="text" name="qty[]" value="' . $qty . '" />
                </div>
                <div class="col-lg-1 text-center">
                    <label>Satuan</label>
                    <input class="form-control satuan text-center" type="text" value="' . $satuan->units . '" readonly />
                </div>
                <div class="col-lg-1">
                    <label>Action</label>
                    <a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a>
                </div>
            </div>';
                $no++;
                $j++;
            } else {
                $det .= '<div class="row">
                <div class="col-lg-1 text-center">
                    <input class="form-control no_urut text-center" type="text" readonly />
                </div>
                <div class="col-lg-7 text-center">
                    <select class="form-control barang" name="barang[]" id="get_barang_' . $j . '"  onchange="get_satuan(this);">
                    </select>
                </div>
                <div class="col-lg-2 text-center">
                    <input class="form-control qty" type="text" name="qty[]" value="' . $qty . '" />
                </div>
                <div class="col-lg-1 text-center">
                    <input class="form-control satuan text-center" type="text" value="' . $satuan->units . '" readonly />
                </div>
                <div class="col-lg-1">
                    <a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a>
                </div>
            </div>';
                $no++;
                $j++;
            }
        }
        $data = array('data' => $query, 'det' => $det, 'barang' => $barang);
        return $data;
    }

    function view_data($id)
    {
        $db = $this->load->database('inventory', TRUE);
        $det = $status = null;
        $no = 1;
        $query = $this->db->query("SELECT *,b.`loc_nama` FROM draf_so a JOIN master_loc b ON a.`loc_code`=b.`kode` WHERE a.`status`=1 AND a.`trans_no`='" . $id . "'")->result();
        foreach ($query as $row) {
            $qty = number_format($row->qty, 6, ',', '.');
            $barang = $db->query(" SELECT a.`description`,a.`units`,b.`description` AS nama_kategori,SUM(COALESCE(c.`qty`,0)) AS stok FROM `0_stock_master` a 
            JOIN `0_stock_category` b ON a.`category_id`=b.`category_id` 
            JOIN `0_stock_moves` c ON a.`stock_id`=c.`stock_id` AND c.`loc_code`='" . $row->loc_code . "'
            WHERE a.`stock_id`='" . $row->stock_id . "'")->row();
            $stok = number_format($barang->stok, 6, ',', '.');
            if ($qty == $stok) {
                $status = '<a class="text-success text-center form-control " title="Sama" style="border: 0px"><i class="icon-checkmark"></i></a>';
            } else {
                $status = '<a class="text-warning text-center form-control " title="Tidak Sama" style="border: 0px"><i class="icon-cross2"></i></a>';
            }
            if ($no == 1) {
                $det .= '<div class="row display-flex-center">
                <div class="col-lg-1">
                    Lokasi
                </div>
                <div class="col-lg-2">
                    <input class="form-control" type="text" value="' . $row->loc_nama . '" readonly />
                </div>
                <div class="col-lg-1">
                    Tanggal
                </div>
                <div class="col-lg-1">
                    <input class="form-control" type="text" value="' . $row->tran_date . '" readonly />
                </div>
                <div class="col-lg-1">
                    Memo
                </div>
                <div class="col-lg-6">
                    <input class="form-control" type="text" value="' . $row->memo . '" readonly />
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-1 text-center">
                    <label><b>No</b></label>
                    <input class="form-control no_urut2 text-center" type="text" disabled />
                </div>
                <div class="col-lg-2 text-center">
                    <label><b>ID</b></label>
                    <input class="form-control" type="text" value="' . $row->stock_id . '" readonly />
                </div>
                <div class="col-lg-3 text-center">
                    <label><b>Nama</b></label>
                    <input class="form-control" type="text" value="' . ($barang->description ? $barang->description : "Barang Tidak Tercatat Di Lokasi,Cek Kembali") . '" readonly />
                </div>
                <div class="col-lg-2 text-center">
                    <label><b>Qty</b></label>
                    <input class="form-control" type="text" value="' . $qty . '" readonly />
                </div>
                <div class="col-lg-1 text-center">
                    <label><b>Satuan</b></label>
                    <input class="form-control text-center" type="text" value="' . $barang->units . '" readonly />
                </div>
                <div class="col-lg-2 text-center">
                    <label><b>Stok IT Inventory</b></label>
                    <input class="form-control" type="text" value="' . $stok . '" readonly />
                </div>
                <div class="col-lg-1 text-center">
                    <label><b>Status</b></label>
                    ' . $status . '
                </div>
            </div>';
                $no++;
            } else {
                $det .= '
            <div class="row">
                <div class="col-lg-1 text-center">
                    <input class="form-control no_urut2 text-center" type="text" disabled />
                </div>
                <div class="col-lg-2 text-center">
                    <input class="form-control" type="text" value="' . $row->stock_id . '" readonly />
                </div>
                <div class="col-lg-3 text-center">
                    <input class="form-control" type="text" value="' . ($barang->description ? $barang->description : "Barang Tidak Tercatat Di Lokasi,Cek Kembali") . '" readonly />
                </div>
                <div class="col-lg-2 text-center">
                    <input class="form-control" type="text" value="' . $qty . '" readonly />
                </div>
                <div class="col-lg-1 text-center">
                    <input class="form-control text-center" type="text" value="' . $barang->units . '" readonly />
                </div>
                <div class="col-lg-2 text-center">
                    <input class="form-control" type="text" value="' . $stok . '" readonly />
                </div>
                <div class="col-lg-1 text-center">
                    ' . $status . '
                </div>
            </div>';
            }
        }
        return $det;
    }

    function preview_data()
    {
        $db = $this->load->database('inventory', TRUE);
        $column_order = array(null, 'a.stock_id', 'b.description', null, 'b.units', null, null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.id';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';
        $limit = null;
        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY a.id ASC";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.*,b.`description`,b.`units`,c.`loc_nama` FROM draf_so a JOIN 0_stock_master b ON a.`stock_id` = b.`stock_id` JOIN master_loc c ON a.`loc_code`=c.`kode`
      WHERE a.`status` = 1 AND (
		`b`.`description` LIKE '%" . $this->input->post('searchKeyword') . "%' ESCAPE '!'
            OR `a`.`stock_id` LIKE '%" . $this->input->post('searchKeyword') . "%' ESCAPE '!'
		) $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $barang = $db->query("SELECT a.`description`,a.`units`,b.`description` AS nama_kategori,SUM(COALESCE(c.`qty`,0)) AS stok FROM `0_stock_master` a 
            JOIN `0_stock_category` b ON a.`category_id`=b.`category_id` 
            JOIN `0_stock_moves` c ON a.`stock_id`=c.`stock_id` AND c.`loc_code`='" . $r['loc_code'] . "'
            WHERE a.`stock_id`='" . $r['stock_id'] . "'")->row();
                $stok = number_format($barang->stok, 6, ',', '.');
                if ($r['qty'] == $barang->stok) {
                    $status = '<a class="text-success text-center form-control " title="Sama" style="border: 0px"><i class="icon-checkmark"></i></a>';
                } else {
                    $status = '<a class="text-warning text-center form-control " title="Tidak Sama" style="border: 0px"><i class="icon-cross2"></i></a>';
                }
                $no++;
                $row  = array(
                    $no . '.',
                    $r['stock_id'],
                    $barang->description,
                    number_format($r['qty'], 6, ',', '.'),
                    $barang->units,
                    $stok,
                    $status
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function proses_data()
    {
        $db = $this->load->database('inventory', TRUE);
        $query = $this->db->query("SELECT * FROM draf_so WHERE status=1")->result();
        $ref = $res = $tran_date = null;
        foreach ($query as $row) {
            if ($ref != $row->reference) {
                $data = array(
                    'id' => $row->trans_no,
                    'type' => 15,
                    'reference' => $row->reference
                );
                $res = $db->insert('0_refs', $data);
                if ($res == true) {
                    $ref = $row->reference;
                }
            }
            $data = array(
                'trans_no' => $row->trans_no,
                'stock_id' => $row->stock_id,
                'type' => 15,
                'loc_code' => $row->loc_code,
                'tran_date' => $row->tran_date,
                'reference' => $row->reference,
                'qty' => $row->qty
            );
            $db->insert('0_stock_opname', $data);
            $tran_date = $row->tran_date;
        }
        $counter = $db->query("SELECT RIGHT(a.`reference`,5) AS coun FROM 0_refs a WHERE LEFT(RIGHT(a.`reference`,7),2)='45' AND a.`type`='15' ORDER BY a.`reference` DESC LIMIT 1")->row();
        $trans_no = $db->query("SELECT trans_no FROM 0_stock_opname a ORDER BY a.`trans_no` DESC LIMIT 1")->row();
        $date = date('Y-m-d');
        $tahun = substr($date, 2, 2);
        $ref = "OB/PTDYT/" . $tahun . "/" . sprintf('%02d', 45) . sprintf('%05d', $counter->coun + 1);
        $query = $this->db->query("SELECT stock_id,SUM(qty) AS qty FROM draf_so WHERE STATUS=1 GROUP BY stock_id")->result();
        foreach ($query as $row) {
            $data = array(
                'trans_no' => $trans_no->trans_no + 1,
                'stock_id' => $row->stock_id,
                'type' => 15,
                'loc_code' => 'ALL',
                'tran_date' => $tran_date,
                'reference' => $ref,
                'qty' => $row->qty
            );
            $db->insert('0_stock_opname', $data);
        }

        $data = array(
            'id' => $trans_no->trans_no + 1,
            'type' => 15,
            'reference' => $ref
        );
        $res = $db->insert('0_refs', $data);

        $data = array('status' => 2);
        $this->db->where('status', 1);
        $this->db->update('draf_so', $data);
        return 1;
    }
}
