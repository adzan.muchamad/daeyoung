<?php

class Model_customers_so extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $kondisi = null;
        $column_order = array(null, 'a.kode', 'd.nama', 'b.nama', 'a.buyer', 'a.ref', 'a.tanggal', 'a.tempo_bayar', 'a.total', null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.nama';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY a.nomor ASC";
        }
        if (!empty($this->input->post('searchkat'))) {
            $kondisi = " AND a.`kategori` = '" . $this->input->post('searchkat') . "'";
        }
        if (!empty($this->input->post('searchtipe'))) {
            $kondisi .= " AND a.`tipe` = '" . $this->input->post('searchtipe') . "'";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.`id`, a.`kode`, d.`nama` AS nama_customer,b.`nama` AS nama_branch,a.`ref`,a.`tanggal` AS tanggal_order,
        e.`symbol` AS mata_uang, a.`total` AS total,a.`tempo_bayar`,a.`buyer` FROM tr_sales_order a
        JOIN master_customers d ON a.`id_cust`=d.`id` JOIN master_customers_branch b ON d.`id`=b.`id_cust` JOIN master_currency e ON a.`mata_uang`=e.`id` WHERE a.`status`=1 AND (
		`d`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
        OR `a`.`kode` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
        OR `a`.`ref` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
        OR `a`.`note` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
        OR `b`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!')
        $kondisi GROUP BY a.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $tempo = null;
                if ($r['tempo_bayar'] == 1) {
                    $tempo = 'Tunai';
                } else if ($r['tempo_bayar'] == 2) {
                    $tempo = '7 Hari';
                } else if ($r['tempo_bayar'] == 3) {
                    $tempo = '30 Hari';
                } else if ($r['tempo_bayar'] == 4) {
                    $tempo = 'Tgl 10 BB';
                } else if ($r['tempo_bayar'] == 5) {
                    $tempo = 'Bayar dimuka';
                }
                $opsi = '<a class="text-success" href="#" style="margin-right:20px" onClick="update_data(\'' . $r['id'] . '\')" title="Edit"><i class="icon-pencil"></i></a> <a class="text-warning" href="#" onClick="delete_data(\'' . $r['id'] . '\')" title="Hapus"><i class="icon-bin"></i></a>';
                $no++;
                $row  = array(
                    $no . '.',
                    $r['kode'],
                    $r['nama_customer'],
                    $r['nama_branch'],
                    $r['buyer'],
                    $r['ref'],
                    $r['tanggal_order'],
                    $tempo,
                    $r['mata_uang'] . ' ' . number_format($r['total'], 2, ',', '.'),
                    $opsi
                );
                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_customers($param = '')
    {
        $q = $this->db->query("SELECT * FROM (SELECT id,CONCAT(kode,' - ',nama) AS nama_cust FROM master_customers WHERE status=1) aa WHERE (aa.`nama_cust` LIKE '%$param%') ORDER BY aa.`nama_cust`,id ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->nama_cust
            );
        }
        return json_encode($data);
    }

    function get_branch($param = '', $id)
    {
        $q = $this->db->query("SELECT * FROM (SELECT id,nama AS nama_branch FROM master_customers_branch WHERE status=1 AND id_cust=$id) aa WHERE (aa.`nama_branch` LIKE '%$param%') ORDER BY aa.`nama_branch`,id ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->nama_branch
            );
        }
        return json_encode($data);
    }

    function get_alamat()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT alamat FROM master_loc WHERE id='" . $id . "'")->row();
        return $query->alamat;
    }

    function get_mata_uang()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT mata_uang FROM master_customers_branch WHERE id='" . $id . "'")->row();
        return $query->mata_uang;
    }

    function get_kurs()
    {
        $rate = 0;
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT a.`mata_uang`,c.`rate` FROM master_customers_branch a JOIN master_currency b ON a.`mata_uang`=b.`id` LEFT JOIN master_kurs c ON b.`id`=c.`id_curr` WHERE a.`id`='" . $id . "' AND tipe=1 ORDER BY c.`timestamp` DESC LIMIT 1")->row();
        if (empty($query)) {
            return 0;
        } else {
            if (!empty($query->rate)) {
                $rate = number_format($query->rate, 2, ',', '.');
            }
            $data = array('rate' => $rate, 'mata_uang' => $query->mata_uang);
            return json_encode($data);
        }
    }

    function get_satuanbarang()
    {
        $data = null;
        $id = $this->input->post('id');
        $q = $this->db->query("SELECT * FROM ( SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan1` AS id_satuan,b.`nama` AS satuan FROM master_barang a JOIN master_satuan b ON a.`satuan1`=b.`id` WHERE a.`status`=1
        UNION ALL
        SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan2` AS id_satuan,b.`nama` AS satuan FROM master_barang a JOIN master_satuan b ON a.`satuan2`=b.`id` WHERE a.`status`=1) z WHERE z.`id`= '" . $id . "'")->result();
        foreach ($q as $row) {
            $data .= '<option value="' . $row->id_satuan . '">' . $row->satuan . '</option>';
        }
        return $data;
    }

    function my_number_format($number, $dec_point, $thousands_sep)
    {
        $tmp = explode('.', $number);
        $out = number_format($tmp[0], 0, $dec_point, $thousands_sep);
        if (isset($tmp[1])) $out .= $dec_point . $tmp[1];

        return $out;
    }

    function get_satuanbarang_detail($id, $id_satuan)
    {
        $data = null;
        $q = $this->db->query("SELECT * FROM ( SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan1` AS id_satuan,b.`nama` AS satuan FROM master_barang a JOIN master_satuan b ON a.`satuan1`=b.`id` WHERE a.`status`=1
        UNION ALL
        SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan2` AS id_satuan,b.`nama` AS satuan FROM master_barang a JOIN master_satuan b ON a.`satuan2`=b.`id` WHERE a.`status`=1) z WHERE z.`id`= '" . $id . "'")->result();
        foreach ($q as $row) {
            if ($row->id_satuan == $id_satuan) {
                $data .= '<option value="' . $row->id_satuan . '" selected>' . $row->satuan . '</option>';
            } else {
                $data .= '<option value="' . $row->id_satuan . '">' . $row->satuan . '</option>';
            }
        }
        return $data;
    }

    function get_barang($param = '')
    {
        $query = $this->db->query("SELECT * FROM (
            SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan1` AS id_satuan,b.`nama` AS satuan,c.`nama` AS kategori FROM master_barang a 
            JOIN master_satuan b ON a.`satuan1`=b.`id` 
            JOIN master_kategori_barang c ON a.`tipe_barang`=c.`id`
            WHERE a.`status`=1
            UNION ALL
            SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan2` AS id_satuan,b.`nama` AS satuan,c.`nama` AS kategori  FROM master_barang a 
            JOIN master_satuan b ON a.`satuan2`=b.`id`
            JOIN master_kategori_barang c ON a.`tipe_barang`=c.`id`
            WHERE a.`status`=1) z WHERE z.`id`=1 AND (z.`nama` LIKE '%" . $param . "%' OR z.`kode` LIKE '%" . $param . "%') GROUP BY z.`id` ORDER BY z.`nama`")->result();
        $data = array();
        foreach ($query as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->kode . ' - ' . $row->nama . ' (' . $row->kategori . ')'
            );
        }
        return json_encode($data);
    }

    function insert_data()
    {
        $id_cust = $this->input->post('nama_cust');
        $id_branch = $this->input->post('branch');
        $referensi =  $this->input->post('ref_cust');
        $note =  $this->input->post('note_order');
        $tgl_order =  $this->input->post('tgl_order');
        $tgl_kirim =  $this->input->post('tgl_kirim');
        $buyer =  $this->input->post('buyer');
        $tempo =  $this->input->post('tempo');
        $jumlah_harga = str_replace(',', '.', str_replace('.', '', $this->input->post('jumlah_harga')));
        $diskon =  str_replace(',', '.', str_replace('.', '', $this->input->post('diskon')));
        $total =  str_replace(',', '.', str_replace('.', '', $this->input->post('total')));
        $ppn =  0;
        if (!empty($this->input->post('ppn'))) {
            $ppn =  $this->input->post('ppn');
        }
        $date = date('Y-m-d');
        $tahun = substr($date, 2, 2);
        $ref = $this->db->query("SELECT * FROM master_refs WHERE kode='SO' AND tahun ='" . $tahun . "' AND user_id = '" . $this->session->userdata("userid") . "'")->row();
        if (empty($ref)) {
            $data = array(
                'kode' => 'SO',
                'user_id' => $this->session->userdata("userid"),
                'counter' => 0,
                'tahun' => $tahun
            );
            $this->db->insert('master_refs', $data);
            $ref = $this->db->query("SELECT * FROM master_refs WHERE kode='SO' AND tahun ='" . $tahun . "' AND user_id = '" . $this->session->userdata("userid") . "'")->row();
        }
        $no_ref = $ref->kode . "/PTDYT/" . $tahun . "/" . sprintf('%02d', $this->session->userdata("userid")) . sprintf('%05d', $ref->counter + 1);
        $mata_uang = $this->db->query("SELECT * FROM master_customers_branch WHERE id='" . $id_branch . "'")->row();
        $data = array(
            'kode' => $no_ref,
            'id_cust' => $id_cust,
            'id_branch' => $id_branch,
            'tempo_bayar' => $tempo,
            'tanggal' => $tgl_order,
            'tanggal_batas' => $tgl_kirim,
            'jumlah' => $jumlah_harga,
            'ppn' => $ppn,
            'diskon' => $diskon,
            'total' => $total,
            'mata_uang' => $mata_uang->mata_uang,
            'buyer' => $buyer,
            'ref' => $referensi,
            'note' => $note,
            'status' => 1,
            'insert_at' => date('Y-m-d H:i:s'),
            'insert_by' => $this->session->userdata('userid')
        );
        $res = $this->db->insert('tr_sales_order', $data);
        if ($res == TRUE) {
            $id = $this->db->insert_id();
            //update counter refs
            $this->db->where('kode', 'SO');
            $this->db->where('user_id', $this->session->userdata('userid'));
            $this->db->where('tahun', $tahun);
            $data = array('counter' => $ref->counter + 1);
            $this->db->update('master_refs', $data);
            //add details
            if (isset($_POST['barang'])) {
                foreach ($_POST['barang'] as $k => $v) {
                    $data = array(
                        'id_header' => $id,
                        'id_barang' => $_POST['barang'][$k],
                        'satuan' => $_POST['satuan'][$k],
                        'qty' => str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k])),
                        'harga' => str_replace(',', '.', str_replace('.', '', $_POST['harga'][$k])),
                        'jumlah' => str_replace(',', '.', str_replace('.', '', $_POST['jumlah'][$k])),
                        'insert_at' => date('Y-m-d H:i:s'),
                        'insert_by' => $this->session->userdata('userid')
                    );
                    $this->db->insert('tr_sales_order_detail', $data);
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    function update_data()
    {
        $id = $this->input->post('id');
        $id_cust = $this->input->post('nama_cust');
        $id_branch = $this->input->post('branch');
        $ref =  $this->input->post('ref_cust');
        $note =  $this->input->post('note_order');
        $tgl_order =  $this->input->post('tgl_order');
        $tgl_kirim =  $this->input->post('tgl_kirim');
        $buyer =  $this->input->post('buyer');
        $tempo =  $this->input->post('tempo');
        $jumlah_harga = str_replace(',', '.', str_replace('.', '', $this->input->post('jumlah_harga')));
        $diskon =  str_replace(',', '.', str_replace('.', '', $this->input->post('diskon')));
        $total =  str_replace(',', '.', str_replace('.', '', $this->input->post('total')));
        $ppn =  0;
        if (!empty($this->input->post('ppn'))) {
            $ppn =  $this->input->post('ppn');
        }
        $mata_uang = $this->db->query("SELECT * FROM master_customers_branch WHERE id='" . $id_branch . "'")->row();
        $data = array(
            'id_cust' => $id_cust,
            'id_branch' => $id_branch,
            'tempo_bayar' => $tempo,
            'tanggal' => $tgl_order,
            'tanggal_batas' => $tgl_kirim,
            'jumlah' => $jumlah_harga,
            'ppn' => $ppn,
            'diskon' => $diskon,
            'total' => $total,
            'mata_uang' => $mata_uang->mata_uang,
            'buyer' => $buyer,
            'ref' => $ref,
            'note' => $note,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $res = $this->db->update('tr_sales_order', $data);
        if ($res == TRUE) {
            $data = array('status' => 9);
            $this->db->where('id_header', $id);
            $this->db->update('tr_sales_order_detail', $data);
            //add details
            if (isset($_POST['barang'])) {
                foreach ($_POST['barang'] as $k => $v) {
                    $data = array(
                        'id_header' => $id,
                        'id_barang' => $_POST['barang'][$k],
                        'satuan' => $_POST['satuan'][$k],
                        'qty' => str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k])),
                        'harga' => str_replace(',', '.', str_replace('.', '', $_POST['harga'][$k])),
                        'jumlah' => str_replace(',', '.', str_replace('.', '', $_POST['jumlah'][$k])),
                        'insert_at' => date('Y-m-d H:i:s'),
                        'insert_by' => $this->session->userdata('userid')
                    );
                    $this->db->insert('tr_sales_order_detail', $data);
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    function delete_data($id = '')
    {
        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $return = $this->db->update('tr_sales_order', $data);
        if ($return == true) {
            $data = array('status' => 9);
            $this->db->where('id_header', $id);
            $this->db->update('tr_sales_order_detail', $data);
            return 1;
        } else {
            return 0;
        }
    }

    function select_data()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT a.*,b.*,c.`nama` AS nama_cust,d.`nama` AS nama_branch,REPLACE(a.`diskon`,'.',',') AS diskon FROM tr_sales_order a JOIN `tr_sales_order_detail` b ON a.`id`=b.`id_header`
        JOIN `master_customers` c ON a.`id_cust`=c.`id` JOIN `master_customers_branch` d ON c.`id`=d.`id_cust` WHERE a.`id`=$id AND a.`status`=1 AND b.`status`=1")->result();
        $det = null;
        $satuan = null;
        $a = 0;
        $barang = $this->db->query("SELECT a.`id_barang`,b.`kode` AS kode_barang,b.`nama` AS nama_barang FROM tr_sales_order_detail a JOIN master_barang b ON a.`id_barang` = b.`id` WHERE a.`id_header`=$id AND b.`status`=1")->result();
        foreach ($query as $row) {
            if ($a == 0) {
                $satuan = $this->get_satuanbarang_detail($row->id_barang, $row->satuan);
                $det .= '<div class="row">
            <div class="col-lg-1 text-center" style="width:5%">
                <label>No</label>
                <input class="form-control no_urut text-center" type="text" readonly />
            </div>
            <div class="col-lg-4 text-center">
                <label>Barang</label>
                <select class="form-control barang" id="get_barang_' . $a . '" name="barang[]" onchange="get_satuan(this);">
                </select>
            </div>
            <div class="col-lg-1 text-center">
                <label>Qty</label>
                <input class="form-control qty" type="text" name="qty[]" onkeyup="count_jmlh(this);" value="' . number_format($row->qty, 6, ',', '.') . '" />
            </div>
            <div class="col-lg-1 text-center">
                <label>Satuan</label>
                <select name="satuan[]" class="form-control satuan">
                    ' . $satuan . '
                </select>
            </div>
            <div class="col-lg-2 text-center">
                <label>Harga</label>
                <input class="form-control harga text-right" type="text" name="harga[]" onkeyup="count_jmlh2(this);" value="' . $this->my_number_format($row->harga, ',', '.') . '"/>
            </div>
            <div class="col-lg-2 text-center">
                <label>Jumlah Harga</label>
                <input class="form-control text-right jumlah" type="text" name="jumlah[]" value="' . $this->my_number_format($row->jumlah, ',', '.') . '" readonly />
            </div>
            <div class="col-lg-1 text-center">
                <label>Action</label>
            </div>
        </div>';
            } else {
                $satuan = $this->get_satuanbarang_detail($row->id_barang, $row->satuan);
                $det .= '<div class="row">
            <div class="col-lg-1 text-center" style="width:5%">
                <input class="form-control no_urut text-center" type="text" readonly />
            </div>
            <div class="col-lg-4 text-center">
                <select class="form-control barang" id="get_barang_' . $a . '" name="barang[]" onchange="get_satuan(this);">
                </select>
            </div>
            <div class="col-lg-1 text-center">
                <input class="form-control qty" type="text" name="qty[]" onkeyup="count_jmlh(this);" value="' . number_format($row->qty, 6, ',', '.') . '" />
            </div>
            <div class="col-lg-1 text-center">
                <select name="satuan[]" class="form-control satuan">
                    ' . $satuan . '
                </select>
            </div>
            <div class="col-lg-2 text-center">
                <input class="form-control harga text-right" type="text" name="harga[]" onkeyup="count_jmlh2(this);" value="' . $this->my_number_format($row->harga, ',', '.') . '"/>
            </div>
            <div class="col-lg-2 text-center">
                <input class="form-control text-right jumlah" type="text" name="jumlah[]" value="' . $this->my_number_format($row->jumlah, ',', '.') . '" readonly />
            </div>
            <div class="col-lg-1 text-center">
            <a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a>
            </div>
        </div>';
            }
            $a++;
        }
        $data = array('data' => $query, 'det' => $det, 'barang' => $barang);
        return $data;
    }

    function buat_master()
    {

        $query = $this->db->query("SELECT*,RIGHT(z.reference,4) AS no
        FROM
          (SELECT
            a.type,
            LEFT(a.reference, 3) AS kode,
            LEFT(RIGHT(a.reference, 7), 2) AS user_id,
            LEFT(RIGHT(a.reference, 10), 2) AS tahun,
            a.reference
          FROM
            0_refs a
          WHERE a.type = 11
            OR a.type = 21
            OR a.type = 25
            OR a.type = 28
            OR a.type = 29) z")->result();
        foreach ($query as $row) {
            $cek = null;
            $cek = $this->db->query("SELECT * FROM master_refs WHERE kode='" . $row->kode . "' AND user_id = '" . $row->user_id . "' AND tahun='" . $row->tahun . "'")->row();
            if (empty($cek)) {
                $data = array(
                    'kode' => $row->kode,
                    'user_id' => $row->user_id,
                    'tahun' => $row->tahun
                );
                $this->db->insert('master_refs', $data);
            } else {
                $data = array(
                    'counter' => $row->no * 1
                );
                $this->db->where('kode', $row->kode);
                $this->db->where('user_id', $row->user_id);
                $this->db->where('tahun', $row->tahun);
                $this->db->update('master_refs', $data);
            }
        }
        return 1;
    }
}
