<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_jurnal extends CI_Model {

    public function opt_tahun_jurnal()
    {
        $result[''] = ' - Pilih Tahun - ';
        $data = $this->db->query("SELECT YEAR(tanggal) AS tahun 
                                    FROM acc_jurnalumum
                                    GROUP BY tanggal")->result();
        foreach ($data as $row) {
            $result[$row->tahun] = $row->tahun;
        }
        return $result;
    }

    public function data_jurnal_umum_id($id='')
    {
        return $this->db->query("SELECT a.*, k.`namaakun`, 
	    CASE
		WHEN LENGTH(SUBSTRING_INDEX(a.`nobukti`,'/', 1)) = 2
		THEN 
		    LEFT(a.`nobukti`,2) 
		ELSE 
		    LEFT(a.`nobukti`,3)
	    END AS kodejurnal
	    FROM acc_jurnalumum a 
	    JOIN acc_master_akun k ON a.`kodeakun`=k.`kodeakun`
	    WHERE a.`id`='$id'")->row();
    }

    public function data_jurnal_umum($date_start='', $date_end='', $keterangan='')
    {
        return $this->db->query("SELECT a.*, DATE_FORMAT(a.tanggal,'%d/%m/%Y') AS tanggal_format, k.`namaakun`
                                    FROM acc_jurnalumum a 
                                    LEFT JOIN acc_master_akun k ON a.`kodeakun`=k.`kodeakun`
                                    WHERE a.`tanggal` BETWEEN '$date_start' AND '$date_end'
                                    AND (
                                            LEFT(a.nobukti,2) = 'JU'
                                        )
                                    AND (
                                            a.keterangan LIKE '%$keterangan%'
                                        )
                                    ORDER BY a.tanggal ASC, a.nobukti, a.debit DESC, a.kredit ASC")->result();
    }

    public function data_jurnal_kas_bank($date_start='', $date_end='', $keterangan='')
    {
        return $this->db->query("SELECT a.*, DATE_FORMAT(a.tanggal,'%d/%m/%Y') AS tanggal_format, k.`namaakun`
                                    FROM acc_jurnalumum a 
                                    LEFT JOIN acc_master_akun k ON a.`kodeakun`=k.`kodeakun`
                                    WHERE a.`tanggal` BETWEEN '$date_start' AND '$date_end'
                                    AND (
                                            LEFT(a.nobukti,3) = 'JBM' OR 
                                            LEFT(a.nobukti,3) = 'JBK' OR 
                                            LEFT(a.nobukti,3) = 'JKM' OR 
                                            LEFT(a.nobukti,3) = 'JKK' 
                                        )
                                    AND (
                                            a.keterangan LIKE '%$keterangan%'
                                        )
                                    ORDER BY a.tanggal ASC, a.nobukti, a.debit DESC, a.kredit ASC")->result();
    }

    public function data_jurnal_otomatis($date_start='', $date_end='', $keterangan='')
    {
        return $this->db->query("SELECT a.*, DATE_FORMAT(a.tanggal,'%d/%m/%Y') AS tanggal_format, k.`namaakun`
                                    FROM acc_jurnalumum a 
                                    LEFT JOIN acc_master_akun k ON a.`kodeakun`=k.`kodeakun`
                                    WHERE a.`tanggal` BETWEEN '$date_start' AND '$date_end'
                                    AND (
                                            LEFT(a.nobukti,3) != 'JBM' AND 
                                            LEFT(a.nobukti,3) != 'JBK' AND 
                                            LEFT(a.nobukti,3) != 'JKM' AND 
                                            LEFT(a.nobukti,3) != 'JKK' AND 
                                            LEFT(a.nobukti,2) != 'JU'
                                        )
                                    AND (
                                            a.keterangan LIKE '%$keterangan%'
                                        )
                                    ORDER BY a.tanggal ASC, a.nobukti, a.debit DESC, a.kredit ASC")->result();
    }

    public function detail_jurnal($nobukti='')
    {
        return $this->db->query("SELECT * FROM (
                                    SELECT j.*, a.`namaakun` 
                                        FROM acc_jurnalumum j
                                        JOIN acc_master_akun a ON j.`kodeakun`=a.`kodeakun`
                                        WHERE j.`nobukti`='$nobukti'
                                        AND j.debit > 0
                                        ORDER BY j.id ASC
                                ) debit
                                UNION ALL
                                SELECT * FROM (
                                    SELECT j.*, a.`namaakun` 
                                        FROM acc_jurnalumum j
                                        JOIN acc_master_akun a ON j.`kodeakun`=a.`kodeakun`
                                        WHERE j.`nobukti`='$nobukti'
                                        AND j.kredit > 0
                                        ORDER BY j.id ASC
                                ) kredit")->result();
    }

    function detail_jurnal_temp($jobs_id='')
    {
        return $this->db->query("SELECT * FROM (
                                    SELECT j.*, a.`namaakun` 
                                        FROM acc_jurnalumum_temp j
                                        JOIN acc_master_akun a ON j.`kodeakun`=a.`kodeakun`
                                        WHERE j.`jobs_id`='$jobs_id'
                                        AND j.debit > 0
                                        ORDER BY j.id ASC
                                ) debit
                                UNION ALL
                                SELECT * FROM (
                                    SELECT j.*, a.`namaakun` 
                                        FROM acc_jurnalumum_temp j
                                        JOIN acc_master_akun a ON j.`kodeakun`=a.`kodeakun`
                                        WHERE j.`jobs_id`='$jobs_id'
                                        AND j.kredit > 0
                                        ORDER BY j.id ASC
                                ) kredit")->result();
    }

    // ===== start jurnal umum ===== //
    public function simpan_detail($params=[])
    {
        $respon = [];

        $jobs_id = $params['jobs_id'];
        $nobukti = $params['nobukti'];
        $data    = $params['data'];
        $rule    = $params['get_rule'];
        $data_join = $params['data_join'];
        $id_auto_data = $params['id_auto_data'];

        $this->db->trans_begin();

        if(!empty($rule)) {
            $oto_debit  = isset($rule->debit) ? $rule->debit:'';
            $oto_kredit = isset($rule->kredit) ? $rule->kredit:'';
            
            $check_eksis = $this->db->query("SELECT * FROM acc_jurnalumum WHERE nobukti='$nobukti'")->row();
            if(!empty($check_eksis)) {
                if($oto_debit == '1' || $oto_kredit == '1') {
                    // push value //
                    $get_data_auto = $this->db->query("SELECT * FROM acc_jurnalumum 
                                                            WHERE nobukti='$nobukti' 
                                                            AND `auto`=1")->row();
                    if(empty($get_data_auto)) {
                        $data_join[0]['nobukti'] = $nobukti;
                    }
                    $data_join[1]['nobukti'] = $nobukti;
                    // insert batch
                    $this->db->insert_batch('acc_jurnalumum', $data_join);
                } else {
                    // push value
                    $data['nobukti'] = $nobukti;
                    // save jurnalumum
                    $this->db->insert('acc_jurnalumum', $data);
                }

                // get sum jurnalumum //
                if($oto_debit == '1') {
                    $get_sum = $this->db->query("SELECT SUM(kredit) AS total 
                                                FROM acc_jurnalumum 
                                                WHERE nobukti='$nobukti'
                                                AND `auto`=0")->row();
                    $total = isset($get_sum->total) ? $get_sum->total:0;
                    // update by id //
                    $this->db->query("UPDATE acc_jurnalumum SET debit='$total' WHERE id='$id_auto_data'");
                }

                if($oto_kredit == '1') {
                    $get_sum = $this->db->query("SELECT SUM(debit) AS total 
                                                FROM acc_jurnalumum 
                                                WHERE nobukti='$nobukti'
                                                AND `auto`=0")->row();
                    $total = isset($get_sum->total) ? $get_sum->total:0;
                    // update by id //
                    $this->db->query("UPDATE acc_jurnalumum SET kredit='$total' WHERE id='$id_auto_data'");
                }                    
            } else {
                if($oto_debit == '1' || $oto_kredit == '1') {
                    // push value //
                    $get_data_auto = $this->db->query("SELECT * FROM acc_jurnalumum_temp 
                                                            WHERE jobs_id='$jobs_id' 
                                                            AND `auto`=1")->row();
                    if(empty($get_data_auto)) {
                        $data_join[0]['jobs_id'] = $jobs_id;
                    }
                    $data_join[1]['jobs_id'] = $jobs_id;
                    // insert batch
                    $this->db->insert_batch('acc_jurnalumum_temp', $data_join);
                } else {
                    // push value
                    $data['jobs_id'] = $jobs_id;
                    // save to jurnalumum temporary
                    $this->db->insert('acc_jurnalumum_temp', $data);
                }

                // get sum jurnalumum temp //
                if($oto_debit == '1') {
                    $get_sum = $this->db->query("SELECT SUM(kredit) AS total 
                                                FROM acc_jurnalumum_temp 
                                                WHERE jobs_id='$jobs_id'
                                                AND `auto`=0")->row();
                    $total = isset($get_sum->total) ? $get_sum->total:0;
                    // update by id //
                    $this->db->query("UPDATE acc_jurnalumum_temp SET debit='$total' WHERE id='$id_auto_data'");
                }
                
                if($oto_kredit == '1') {
                    $get_sum = $this->db->query("SELECT SUM(debit) AS total 
                                                FROM acc_jurnalumum_temp 
                                                WHERE jobs_id='$jobs_id'
                                                AND `auto`=0")->row();
                    $total = isset($get_sum->total) ? $get_sum->total:0;
                    // update by id //
                    $this->db->query("UPDATE acc_jurnalumum_temp SET kredit='$total' WHERE id='$id_auto_data'");
                }
            }
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $respon = [
                'status'  => FALSE, 
                'message' => 'Gagal menambahkan data',
                'nobukti' => ''
            ];
        } else {
            $this->db->trans_commit();

            $respon = [
                'status'  => TRUE, 
                'message' => 'Data berhasil di tambahkan',
                'nobukti' => $nobukti
            ];
        }

        return $respon;
    }

    public function hapus_detail($params=[])
    {
        $respon = [];

        $kodejurnal = $params['kodejurnal'];
        $nobukti = $params['nobukti'];
        $id = $params['id'];

        $this->db->trans_begin();

        $get_rule   = $this->db->query("SELECT * FROM `acc_rule_jurnal` WHERE kode='$kodejurnal'")->row();
        $oto_debit  = isset($get_rule->debit) ? $get_rule->debit:'';
        $oto_kredit = isset($get_rule->kredit) ? $get_rule->kredit:'';

        $check_eksis = $this->db->query("SELECT * FROM acc_jurnalumum WHERE nobukti='$nobukti'")->row();
        if(!empty($check_eksis)) {
            $total  = 0;
            if($oto_debit == '1') {
                $get_sum = $this->db->query("SELECT debit AS total_debit 
                                                    FROM acc_jurnalumum 
                                                    WHERE nobukti='$nobukti'
                                                    AND `auto`=1")->row();
                $total_debit = isset($get_sum->total_debit) ? $get_sum->total_debit:'0';

                // detail kredit //
                $get_id = $this->db->query("SELECT kredit FROM acc_jurnalumum WHERE id='$id'")->row();
                $total  = isset($get_id->kredit) ? $get_id->kredit:'0';

                $grand_total  = $total_debit - $total;
                //  update debit //
                $this->db->query("UPDATE acc_jurnalumum SET debit='$grand_total' WHERE nobukti='$nobukti' AND `auto`=1");
            }

            if($oto_kredit == '1') {
                $get_sum = $this->db->query("SELECT kredit AS total_kredit 
                                                    FROM acc_jurnalumum 
                                                    WHERE nobukti='$nobukti'
                                                    AND `auto`=1")->row();
                $total_kredit = isset($get_sum->total_kredit) ? $get_sum->total_kredit:'0';

                $get_id = $this->db->query("SELECT debit FROM acc_jurnalumum WHERE id='$id'")->row();
                $total  = isset($get_id->debit) ? $get_id->debit:'0';

                $grand_total  = $total_kredit - $total;
                //  update debit //
                $this->db->query("UPDATE acc_jurnalumum SET kredit='$grand_total' WHERE nobukti='$nobukti' AND `auto`=1");
            }

            // delete data //
            $this->db->query("DELETE FROM acc_jurnalumum WHERE id='$id'");
        } else {
            $get_jobs_id = $this->db->query("SELECT * FROM acc_jurnalumum_temp WHERE id='$id'")->row();
            $jobs_id = isset($get_jobs_id->jobs_id) ? $get_jobs_id->jobs_id:'';

            $total  = 0;
            if($oto_debit == '1') {
                $get_sum = $this->db->query("SELECT debit AS total_debit 
                                                    FROM acc_jurnalumum_temp 
                                                    WHERE jobs_id='$jobs_id'
                                                    AND `auto`=1")->row();
                $total_debit = isset($get_sum->total_debit) ? $get_sum->total_debit:'0';

                // detail kredit //
                $get_id = $this->db->query("SELECT kredit FROM acc_jurnalumum_temp WHERE id='$id'")->row();
                $total  = isset($get_id->kredit) ? $get_id->kredit:'0';

                $grand_total  = $total_debit - $total;
                //  update debit //
                $this->db->query("UPDATE acc_jurnalumum_temp SET debit='$grand_total' WHERE jobs_id='$jobs_id' AND `auto`=1");
            }

            if($oto_kredit == '1') {
                $get_sum = $this->db->query("SELECT kredit AS total_kredit 
                                                    FROM acc_jurnalumum_temp 
                                                    WHERE jobs_id='$jobs_id'
                                                    AND `auto`=1")->row();
                $total_kredit = isset($get_sum->total_kredit) ? $get_sum->total_kredit:'0';

                $get_id = $this->db->query("SELECT debit FROM acc_jurnalumum_temp WHERE id='$id'")->row();
                $total  = isset($get_id->debit) ? $get_id->debit:'0';

                $grand_total  = $total_kredit - $total;
                //  update debit //
                $this->db->query("UPDATE acc_jurnalumum_temp SET kredit='$grand_total' WHERE jobs_id='$jobs_id' AND `auto`=1");
            }

            // delete data //
            $this->db->query("DELETE FROM acc_jurnalumum_temp WHERE id='$id'");
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $respon = [
                'status' =>FALSE,
                'message'=>'Hapus data gagal'
            ];
        } else {
            $this->db->trans_commit();

            $respon = [
                'status' =>TRUE, 
                'message'=>'Hapus data berhasil'
            ];
        }

        return $respon;
    }

    public function simpan_jurnal_umum($params=[])
    {
        $respon = array();
        $id_auto_data = '';

        $jobs_id    = $params['jobs_id'];
        $kodejurnal = $params['kodejurnal'];
        $nobukti    = $params['nobukti'];
        $keterangan = $params['keterangan'];

        $this->db->trans_begin();

        $get_data = $this->db->where('jobs_id', $jobs_id)->get('acc_jurnalumum_temp')->result();
        if(!empty($get_data)) {
            $counter = $this->Model_main->gt_counter($kodejurnal);

            $nobukti = $counter['nomor'];

            $data = [];
            foreach ($get_data as $row => $val) {
                $data[] = [
                    'nobukti' => $nobukti,
                    'tanggal' => $val->tanggal,
                    'nip' => $val->nip,
                    'kodeakun' => $val->kodeakun,
                    'keterangan' => $keterangan,
                    'debit' => $val->debit,
                    'kredit' => $val->kredit,
                    'userid' => $val->userid,
                    'usertgl' => $val->usertgl,
                    'auto' => $val->auto
                ];
            }

            $this->db->insert_batch('acc_jurnalumum', $data);

            $this->db->where([
                'jobs_id' => $jobs_id
            ])
            ->delete('acc_jurnalumum_temp');

            $get = $this->db->query("
            SELECT * FROM acc_jurnalumum 
            WHERE nobukti='$nobukti' 
            AND `auto`=1")->row();
            if(empty($get)) {
                $get = $this->db->query("
                SELECT * FROM acc_jurnalumum 
                WHERE nobukti='$nobukti' 
                LIMIT 1")->row();
            }

            $id_auto_data = isset($get->id) ? $get->id:'';
            
        } else {
            $data = ['keterangan' => $keterangan];
            $this->db->where('nobukti', $nobukti)
            ->update('acc_jurnalumum', $data);   

            $get = $this->db->query("
            SELECT * FROM acc_jurnalumum 
            WHERE nobukti='$nobukti' 
            AND `auto`=1")->row();
            if(empty($get)) {
                $get = $this->db->query("
                SELECT * FROM acc_jurnalumum 
                WHERE nobukti='$nobukti' 
                LIMIT 1")->row();
            }

            $id_auto_data = isset($get->id) ? $get->id:'';
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $respon = [
                'status'  => FALSE, 
                'message' => 'Simpan data gagal',
                'nobukti' => '', 
                'id_auto' => ''
            ];
        } else {
            $this->db->trans_commit();

            $respon = [
                'status'  => TRUE, 
                'message' => 'Simpan data berhasil',
                'nobukti' => $nobukti, 
                'id_auto' => $id_auto_data
            ];
        }

        return $respon;
    }

    public function hapus_jurnal_umum($params=[])
    {
        $respon = array();

        $nobukti= $params['nobukti'];

        // transaction begin
        $this->db->trans_begin();

        $this->db->query("INSERT INTO acc_jurnalumum_delete SELECT * 
        FROM acc_jurnalumum 
        WHERE nobukti='$nobukti'");

        $this->db->where('nobukti', $nobukti)
        ->delete('acc_jurnalumum');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            $respon = [
                'status'  => FALSE, 
                'message' => 'Data gagal di hapus'
            ];
        } else {
            $this->db->trans_commit();

            $respon = [
                'status'  => TRUE,
                'message' => 'Data berhasil di hapus'
            ];
        }
        return $respon;
    }
    // ===== end jurnal umum ===== //

}

/* End of file Model_jurnal.php */

?>