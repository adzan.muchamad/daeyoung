<?php

class Model_suppliers_po extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $kondisi = $tipe = null;
        $column_order = array(null, 'a.kode', 'd.nama', 'a.nama', 'satuana', 'satuanb', 'a.tipe_pajak', 'a.tipe_barang', null, null, null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.nama';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY a.nomor ASC";
        }
        if (!empty($this->input->post('searchkat'))) {
            $kondisi = " AND a.`kategori` = '" . $this->input->post('searchkat') . "'";
        }
        if (!empty($this->input->post('searchtipe'))) {
            $kondisi .= " AND a.`tipe` = '" . $this->input->post('searchtipe') . "'";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.`id`, a.`kode`, d.`nama` AS nama_supplier, f.`loc_nama` AS nama_lokasi,a.`req`,a.`tanggal` AS tanggal_order,
        e.`symbol` AS mata_uang, a.`total` AS total,a.`tipe` FROM tr_purchase_order a
        JOIN master_suppliers d ON a.`id_supp`=d.`id` JOIN master_currency e ON a.`mata_uang`=e.`id`
        JOIN master_loc f ON a.`id_loc`=f.`id` WHERE a.`status`=1 AND (
		`d`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
        OR `a`.`kode` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
        OR `a`.`req` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!')
        $kondisi GROUP BY a.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                if ($r['tipe'] == 1) {
                    $tipe = 'Produksi';
                } else {
                    $tipe = 'Inventaris';
                }
                $opsi = '<a class="text-success" href="#" style="margin-right:20px" onClick="update_data(\'' . $r['id'] . '\')" title="Edit"><i class="icon-pencil"></i></a> <a class="text-warning" href="#" onClick="delete_data(\'' . $r['id'] . '\')" title="Hapus"><i class="icon-bin"></i></a>';
                $no++;
                $row  = array(
                    $no . '.',
                    $tipe,
                    $r['kode'],
                    $r['nama_supplier'],
                    $r['nama_lokasi'],
                    $r['req'],
                    $r['tanggal_order'],
                    $r['mata_uang'] . ' ' . number_format($r['total'], 2, ',', '.'),
                    $opsi
                );
                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function get_suppliers($param = '')
    {
        $q = $this->db->query("SELECT * FROM (SELECT id,IF(kode_supp IS NULL,nama,CONCAT(kode_supp,' - ',nama)) AS nama_supp FROM master_suppliers WHERE status=1) aa WHERE (aa.`nama_supp` LIKE '%$param%') ORDER BY aa.`nama_supp`,id ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->nama_supp
            );
        }
        return json_encode($data);
    }

    function get_alamat()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT alamat FROM master_loc WHERE id='" . $id . "'")->row();
        return $query->alamat;
    }

    function get_mata_uang()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT mata_uang FROM master_suppliers WHERE id='" . $id . "'")->row();
        return $query->mata_uang;
    }

    function get_kurs()
    {
        $rate = 0;
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT a.`mata_uang`,c.`rate` FROM master_suppliers a JOIN master_currency b ON a.`mata_uang`=b.`id` LEFT JOIN master_kurs c ON b.`id`=c.`id_curr` WHERE a.`id`='" . $id . "' AND tipe=1 ORDER BY c.`timestamp` DESC LIMIT 1")->row();
        if (empty($query)) {
            return 0;
        } else {
            if (!empty($query->rate)) {
                $rate = number_format($query->rate, 2, ',', '.');
            }
            $data = array('rate' => $rate, 'mata_uang' => $query->mata_uang);
            return json_encode($data);
        }
    }

    function get_satuanbarang()
    {
        $data = null;
        $id = $this->input->post('id');
        $q = $this->db->query("SELECT * FROM ( SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan1` AS id_satuan,b.`nama` AS satuan FROM master_barang a JOIN master_satuan b ON a.`satuan1`=b.`id` WHERE a.`status`=1
        UNION ALL
        SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan2` AS id_satuan,b.`nama` AS satuan FROM master_barang a JOIN master_satuan b ON a.`satuan2`=b.`id` WHERE a.`status`=1) z WHERE z.`id`= '" . $id . "'")->result();
        foreach ($q as $row) {
            $data .= '<option value="' . $row->id_satuan . '">' . $row->satuan . '</option>';
        }
        return $data;
    }

    function my_number_format($number, $dec_point, $thousands_sep)
    {
        $tmp = explode('.', $number);
        $out = number_format($tmp[0], 0, $dec_point, $thousands_sep);
        if (isset($tmp[1])) $out .= $dec_point . $tmp[1];

        return $out;
    }

    function get_satuanbarang_detail($id, $id_satuan)
    {
        $data = null;
        $q = $this->db->query("SELECT * FROM ( SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan1` AS id_satuan,b.`nama` AS satuan FROM master_barang a JOIN master_satuan b ON a.`satuan1`=b.`id` WHERE a.`status`=1
        UNION ALL
        SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan2` AS id_satuan,b.`nama` AS satuan FROM master_barang a JOIN master_satuan b ON a.`satuan2`=b.`id` WHERE a.`status`=1) z WHERE z.`id`= '" . $id . "'")->result();
        foreach ($q as $row) {
            if ($row->id_satuan == $id_satuan) {
                $data .= '<option value="' . $row->id_satuan . '" selected>' . $row->satuan . '</option>';
            } else {
                $data .= '<option value="' . $row->id_satuan . '">' . $row->satuan . '</option>';
            }
        }
        return $data;
    }

    function get_barang($param = '')
    {
        $query = $this->db->query("SELECT * FROM (
            SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan1` AS id_satuan,b.`nama` AS satuan,c.`nama` AS kategori FROM master_barang a 
            JOIN master_satuan b ON a.`satuan1`=b.`id` 
            JOIN master_kategori_barang c ON a.`tipe_barang`=c.`id`
            WHERE a.`status`=1
            UNION ALL
            SELECT a.`id`,a.`kode`,a.`nama`,a.`satuan2` AS id_satuan,b.`nama` AS satuan,c.`nama` AS kategori  FROM master_barang a 
            JOIN master_satuan b ON a.`satuan2`=b.`id`
            JOIN master_kategori_barang c ON a.`tipe_barang`=c.`id`
            WHERE a.`status`=1) z WHERE z.`id`=1 AND (z.`nama` LIKE '%" . $param . "%' OR z.`kode` LIKE '%" . $param . "%') GROUP BY z.`id` ORDER BY z.`nama`")->result();
        $data = array();
        foreach ($query as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->kode . ' - ' . $row->nama . ' (' . $row->kategori . ')'
            );
        }
        return json_encode($data);
    }

    function insert_data()
    {
        $nama_supp = $this->input->post('nama_supp');
        $tipe =  $this->input->post('tipe');
        $tgl_order =  $this->input->post('tgl_order');
        $req =  $this->input->post('req');
        $lokasi =  $this->input->post('lokasi');
        $nilai =  str_replace(',', '.', str_replace('.', '', $this->input->post('nilai')));
        $jumlah_harga = str_replace(',', '.', str_replace('.', '', $this->input->post('jumlah_harga')));
        $diskon =  str_replace(',', '.', str_replace('.', '', $this->input->post('diskon')));
        $total =  str_replace(',', '.', str_replace('.', '', $this->input->post('total')));
        $ppn =  0;
        if (!empty($this->input->post('ppn'))) {
            $ppn =  $this->input->post('ppn');
        }
        $pph = 0;
        if (!empty($this->input->post('kd_pajak'))) {
            $pph =  $this->input->post('kd_pajak');
        }
        $date = date('Y-m-d');
        $tahun = substr($date, 2, 2);
        $ref = $this->db->query("SELECT * FROM master_refs WHERE kode='PO' AND tahun ='" . $tahun . "' AND user_id = '" . $this->session->userdata("userid") . "'")->row();
        if (empty($ref)) {
            $data = array(
                'kode' => 'PO',
                'user_id' => $this->session->userdata("userid"),
                'counter' => 0,
                'tahun' => $tahun
            );
            $this->db->insert('master_refs', $data);
            $ref = $this->db->query("SELECT * FROM master_refs WHERE kode='PO' AND tahun ='" . $tahun . "' AND user_id = '" . $this->session->userdata("userid") . "'")->row();
        }
        $no_ref = $ref->kode . "/PTDYT/" . $tahun . "/" . sprintf('%02d', $this->session->userdata("userid")) . sprintf('%05d', $ref->counter + 1);
        $mata_uang = $this->db->query("SELECT * FROM master_suppliers WHERE id='" . $nama_supp . "'")->row();
        $data = array(
            'kode' => $no_ref,
            'id_supp' => $nama_supp,
            'id_loc' => $lokasi,
            'tipe' => $tipe,
            'tanggal' => $tgl_order,
            'jumlah' => $jumlah_harga,
            'ppn' => $ppn,
            'pph' => $pph,
            'diskon' => $diskon,
            'total' => $total,
            'mata_uang' => $mata_uang->mata_uang,
            'rate' => $nilai,
            'req' => $req,
            'status' => 1,
            'insert_at' => date('Y-m-d H:i:s'),
            'insert_by' => $this->session->userdata('userid')
        );
        $res = $this->db->insert('tr_purchase_order', $data);
        if ($res == TRUE) {
            $id = $this->db->insert_id();
            //update counter refs
            $this->db->where('kode', 'PO');
            $this->db->where('user_id', $this->session->userdata('userid'));
            $this->db->where('tahun', $tahun);
            $data = array('counter' => $ref->counter + 1);
            $this->db->update('master_refs', $data);
            //add details
            if (isset($_POST['barang'])) {
                foreach ($_POST['barang'] as $k => $v) {
                    $data = array(
                        'id_header' => $id,
                        'id_barang' => $_POST['barang'][$k],
                        'satuan' => $_POST['satuan'][$k],
                        'qty' => str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k])),
                        'harga' => str_replace(',', '.', str_replace('.', '', $_POST['harga'][$k])),
                        'jumlah' => str_replace(',', '.', str_replace('.', '', $_POST['jumlah'][$k])),
                        'tanggal' => $_POST['tgl_kirim'][$k],
                        'insert_at' => date('Y-m-d H:i:s'),
                        'insert_by' => $this->session->userdata('userid')
                    );
                    $this->db->insert('tr_purchase_order_detail', $data);
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    function update_data()
    {
        $id = $this->input->post('id');
        $nama_supp = $this->input->post('nama_supp');
        $tipe =  $this->input->post('tipe');
        $tgl_order =  $this->input->post('tgl_order');
        $req =  $this->input->post('req');
        $lokasi =  $this->input->post('lokasi');
        $nilai =  str_replace(',', '.', str_replace('.', '', $this->input->post('nilai')));
        $jumlah_harga = str_replace(',', '.', str_replace('.', '', $this->input->post('jumlah_harga')));
        $diskon =  str_replace(',', '.', str_replace('.', '', $this->input->post('diskon')));
        $total =  str_replace(',', '.', str_replace('.', '', $this->input->post('total')));
        $ppn =  0;
        if (!empty($this->input->post('ppn'))) {
            $ppn =  $this->input->post('ppn');
        }
        $pph = 0;
        if (!empty($this->input->post('kd_pajak'))) {
            $pph =  $this->input->post('kd_pajak');
        }
        $date = date('Y-m-d');
        $tahun = substr($date, 2, 2);
        $mata_uang = $this->db->query("SELECT * FROM master_suppliers WHERE id='" . $nama_supp . "'")->row();
        $data = array(
            'id_supp' => $nama_supp,
            'id_loc' => $lokasi,
            'tipe' => $tipe,
            'tanggal' => $tgl_order,
            'jumlah' => $jumlah_harga,
            'ppn' => $ppn,
            'pph' => $pph,
            'diskon' => $diskon,
            'total' => $total,
            'mata_uang' => $mata_uang->mata_uang,
            'rate' => $nilai,
            'req' => $req,
            'update_at' => date('Y-m-d H:i:s'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $res = $this->db->update('tr_purchase_order', $data);
        if ($res == TRUE) {
            $data = array('status' => 9);
            $this->db->where('id_header', $id);
            $this->db->update('tr_purchase_order_detail', $data);
            //add details
            if (isset($_POST['barang'])) {
                foreach ($_POST['barang'] as $k => $v) {
                    $data = array(
                        'id_header' => $id,
                        'id_barang' => $_POST['barang'][$k],
                        'satuan' => $_POST['satuan'][$k],
                        'qty' => str_replace(',', '.', str_replace('.', '', $_POST['qty'][$k])),
                        'harga' => str_replace(',', '.', str_replace('.', '', $_POST['harga'][$k])),
                        'jumlah' => str_replace(',', '.', str_replace('.', '', $_POST['jumlah'][$k])),
                        'tanggal' => $_POST['tgl_kirim'][$k],
                        'insert_at' => date('Y-m-d H:i:s'),
                        'insert_by' => $this->session->userdata('userid')
                    );
                    $this->db->insert('tr_purchase_order_detail', $data);
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    function delete_data($id = '')
    {
        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $return = $this->db->update('tr_purchase_order', $data);
        if ($return == true) {
            $data = array('status' => 9);
            $this->db->where('id_header', $id);
            $this->db->update('tr_purchase_order_detail', $data);
            return 1;
        } else {
            return 0;
        }
    }

    function select_data()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT a.*,b.*,c.`nama` AS nama_supp,REPLACE(a.`diskon`,'.',',') AS diskon FROM tr_purchase_order a JOIN `tr_purchase_order_detail` b ON a.`id`=b.`id_header`
        JOIN `master_suppliers` c ON a.`id_supp`=c.`id` WHERE a.`id`=$id AND a.`status`=1 AND b.`status`=1")->result();
        $det = null;
        $satuan = null;
        $a = 0;
        $barang = $this->db->query("SELECT a.`id_barang`,b.`kode` AS kode_barang,b.`nama` AS nama_barang FROM tr_purchase_order_detail a JOIN master_barang b ON a.`id_barang` = b.`id` WHERE a.`id_header`=$id AND b.`status`=1")->result();
        foreach ($query as $row) {
            if ($a == 0) {
                $satuan = $this->get_satuanbarang_detail($row->id_barang, $row->satuan);
                $det .= '<div class="row">
                <div class="col-lg-1 text-center" style="width:5%">
                    <label>No</label>
                    <input class="form-control no_urut text-center" type="text" readonly />
                </div>
                <div class="col-lg-4 text-center">
                    <label>Barang</label>
                    <select class="form-control barang" id="get_barang_' . $a . '" name="barang[]" onchange="get_satuan(this);">
                    </select>
                </div>
                <div class="col-lg-1 text-center">
                    <label>Qty</label>
                    <input class="form-control qty" type="text" name="qty[]" onkeyup="count_jmlh(this);" value="' . number_format($row->qty, 6, ',', '.') . '" />
                </div>
                <div class="col-lg-1 text-center">
                    <label>Satuan</label>
                    <select name="satuan[]" class="form-control satuan">
                        ' . $satuan . '
                    </select>
                </div>
                <div class="col-lg-1 text-center">
                    <label>Tanggal Pengiriman</label>
                    <input type="text" class="form-control date_picker tgl_kirim" name="tgl_kirim[]" value="' . $row->tanggal . '" />
                </div>
                <div class="col-lg-1 text-center" style="width: 11.5%;">
                    <label>Harga</label>
                    <input class="form-control harga text-right" type="text" name="harga[]" onkeyup="count_jmlh2(this);" value="' . $this->my_number_format($row->harga, ',', '.') . '"/>
                </div>
                <div class="col-lg-2 text-center">
                    <label>Jumlah Harga</label>
                    <input class="form-control text-right jumlah" type="text" name="jumlah[]" value="' . $this->my_number_format($row->jumlah, ',', '.') . '" readonly />
                </div>
                <div class="col-lg-1 text-center">
                    <label>Action</label>
                </div>
            </div>';
            } else {
                $satuan = $this->get_satuanbarang_detail($row->id_barang, $row->satuan);
                $det .= '<div class="row">
                <div class="col-lg-1 text-center" style="width:5%">
                    <input class="form-control no_urut text-center" type="text" readonly />
                </div>
                <div class="col-lg-4 text-center">
                    <select class="form-control barang" id="get_barang_' . $a . '" name="barang[]" onchange="get_satuan(this);">
                    </select>
                </div>
                <div class="col-lg-1 text-center">
                    <input class="form-control qty" type="text" name="qty[]" onkeyup="count_jmlh(this);" value="' . number_format($row->qty, 6, ',', '.') . '" />
                </div>
                <div class="col-lg-1 text-center">
                    <select name="satuan[]" class="form-control satuan">
                        ' . $satuan . '
                    </select>
                </div>
                <div class="col-lg-1 text-center">
                    <input type="text" class="form-control date_picker tgl_kirim" name="tgl_kirim[]" value="' . $row->tanggal . '" />
                </div>
                <div class="col-lg-1 text-center" style="width: 11.5%;">
                    <input class="form-control harga text-right" type="text" name="harga[]" onkeyup="count_jmlh2(this);" value="' . $this->my_number_format($row->harga, ',', '.') . '"/>
                </div>
                <div class="col-lg-2 text-center">
                    <input class="form-control text-right jumlah" type="text" name="jumlah[]" value="' . $this->my_number_format($row->jumlah, ',', '.') . '" readonly />
                </div>
                <div class="col-lg-1 text-center">
                <a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a>
                </div>
            </div>';
            }

            $a++;
        }
        $data = array('data' => $query, 'det' => $det, 'barang' => $barang);
        return $data;
    }

    function buat_master()
    {

        $query = $this->db->query("SELECT*,RIGHT(z.reference,4) AS no
        FROM
          (SELECT
            a.type,
            LEFT(a.reference, 3) AS kode,
            LEFT(RIGHT(a.reference, 7), 2) AS user_id,
            LEFT(RIGHT(a.reference, 10), 2) AS tahun,
            a.reference
          FROM
            0_refs a
          WHERE a.type = 11
            OR a.type = 21
            OR a.type = 25
            OR a.type = 28
            OR a.type = 29) z")->result();
        foreach ($query as $row) {
            $cek = null;
            $cek = $this->db->query("SELECT * FROM master_refs WHERE kode='" . $row->kode . "' AND user_id = '" . $row->user_id . "' AND tahun='" . $row->tahun . "'")->row();
            if (empty($cek)) {
                $data = array(
                    'kode' => $row->kode,
                    'user_id' => $row->user_id,
                    'tahun' => $row->tahun
                );
                $this->db->insert('master_refs', $data);
            } else {
                $data = array(
                    'counter' => $row->no * 1
                );
                $this->db->where('kode', $row->kode);
                $this->db->where('user_id', $row->user_id);
                $this->db->where('tahun', $row->tahun);
                $this->db->update('master_refs', $data);
            }
        }
        return 1;
    }
}
