<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_accounting extends CI_Model {

    // ===== START COA ===== //
    function data_coa_category()
    {
        return $this->db->query("SELECT *, 
                                    IF(mekanisme = 0, 'Debet (+), Kredit (-)', 'Debet (-), Kredit (+)') AS mekanisme_name
                                    FROM acc_master_akun 
                                    WHERE type_data='header' 
                                    AND `status`=1")->result();
    }

    function data_coa_category_id($id='')
    {
        return $this->db->query("SELECT * FROM acc_master_akun 
                                    WHERE type_data='header' 
                                    AND `status`=1
                                    AND id='$id'")->row();
    }

    function data_coa_kas_bank()
    {
        return $this->db->query("SELECT *
                                    FROM acc_master_akun 
                                    WHERE type_data='detail' 
                                    AND `status`=1
                                    AND (flag='kas' OR flag='bank')")->result();
    }

    function data_coa_lain_lain($parent='')
    {
        return $this->db->query("SELECT a.* , IFNULL(d.`detail`,0) AS detail
                                    FROM acc_master_akun a 
                                    LEFT JOIN (
                                        SELECT parent, COUNT(*) AS detail 
                                            FROM acc_master_akun
                                            WHERE parent <> 0
                                            AND status=1
                                            GROUP BY parent
                                    ) d ON a.`kodeakun`=d.`parent`
                                    WHERE a.`status`=1
                                    AND a.`flag`='lain-lain'
                                    AND a.`parent`='$parent'
                                    GROUP BY a.`id`")->result();
    }

    public function json_coa_lain_lain($parent=0)
    {
        $data = $this->data_coa_lain_lain($parent);
            $arr = array();
            if(!empty($data)) {
                foreach ($data as $row => $val) {
                    
                    $btn_edit  = '<a title="Edit" onclick="edit('.$val->id.')" type="button"><i class="icon-pencil"></i></a>';
                    $btn_hapus = '<a title="Hapus" onclick="hapus('.$val->id.')" type="button"><i class="icon-bin" style="color:red;"></i></a>';

                    $btn_group = $btn_edit.'&nbsp;&nbsp;'.$btn_hapus.'&nbsp;&nbsp;';

                    $text = $val->kodeakun.' | '.$val->namaakun.'&nbsp;';

                    $arr[$row] = array(
                                    'id' => $val->id,
                                    'text' => ($val->detail == 0) ? $btn_group.$text:'<span class="bold">'.$text.'</span>',
                                    'icon' => 'icon-folder icon-state-success '
                                );

                    $arr[$row]['state'] = array(
                            'opened' => true
                        );
                    
                    if ($val->detail > 0) {
                        $arr[$row]['children'] = $this->json_coa_lain_lain($val->kodeakun);
                    }
                }
            }
        return $arr;
    }

    function ajax_data_all_coa($search='', $start=0, $limit=0)
    {
        $json = [];
        $data = $this->db->query("SELECT * FROM acc_master_akun
                                    WHERE `status`=1
                                    AND type_data='detail'
                                    AND (
                                        kodeakun LIKE '%$search%'
                                        OR namaakun LIKE '%$search%'
                                    )
                                    LIMIT $start, $limit")->result();
        foreach ($data as $val) {
            $json[] = array(
                        'id' => $val->kodeakun,
                        'text' => $val->kodeakun.' | '.$val->namaakun
                    );
        }
        return $json;
    }

    function ajax_data_all_coa_count($search='')
    {
        $data = $this->db->query("SELECT count(*) AS total 
                                    FROM acc_master_akun
                                    WHERE `status`=1
                                    AND type_data='detail'
                                    AND (
                                        kodeakun LIKE '%$search%'
                                        OR namaakun LIKE '%$search%'
                                    )")->row();
        $total = isset($data->total) ? $data->total:0;
        return $total;
    }

    public function opt_kas()
    {
        $result = [];

        $data = $this->db->query("SELECT * FROM acc_master_akun
        WHERE `status`=1
        AND flag='kas'")->result();
        if($data) {
            foreach ($data as $row) {
                $result[$row->kodeakun] = $row->namaakun;
            }
        }

        return $result;
    }

    public function opt_bank()
    {
        $result = [];

        $data = $this->db->query("SELECT * FROM acc_master_akun
        WHERE `status`=1
        AND flag='bank'")->result();
        if($data) {
            foreach ($data as $row) {
                $result[$row->kodeakun] = $row->namaakun;
            }
        }

        return $result;
    }

    public function opt_all_coa()
    {
        $respon = [];

        $data = $this->db->query("SELECT * FROM acc_master_akun
        WHERE `status`=1
        AND type_data='detail'")->result();
        
        $respon[''] = ' - COA - ';
        if($data) {
            foreach ($data as $row) {
                $respon[$row->kodeakun] = $row->namaakun;
            }
        }

        return $respon;
    }
    // ===== END COA ===== //

    public function data_buku_besar($params=[])
    {
        $kodeakun   = $params['kodeakun'];
        $date_start = $params['date_start'];
        $date_end   = $params['date_end'];
        $keterangan = $params['keterangan'];

        return $this->db->query("SELECT 
        @grup := (SELECT grup FROM acc_master_akun 
            WHERE kodeakun='$kodeakun') AS grup,
        @sa_debit  := (SELECT SUM(debit) AS debit 
                FROM acc_saldo_awal_akun 
                WHERE periode='201912'
                AND kodeakun='$kodeakun') AS sa_debit,
        @sa_kredit := (SELECT SUM(kredit) AS kredit 
                FROM acc_saldo_awal_akun 
                WHERE periode='201912'
                AND kodeakun='$kodeakun') AS sa_kredit,
        '' AS id, 'SA' AS nobukti, '' AS tanggal, '' AS nip, 'Saldo Awal' AS keterangan, 
            '' AS kodeakun, '' AS  namaakun, 0 AS debit, 0 AS kredit,
            CASE 
            WHEN @grup = 2 OR @grup = 3 OR @grup = 4
            THEN 
                CASE
                WHEN MONTH(DATE_SUB('$date_start', INTERVAL 1 MONTH)) = 12 AND YEAR(DATE_SUB('$date_start', INTERVAL 1 MONTH)) = 2019
                THEN 
                    (IFNULL(@sa_kredit,0) - IFNULL(@sa_debit,0))
                ELSE 
                    (IFNULL((SUM(j.kredit) + IFNULL(@sa_kredit,0)),0) - IFNULL((SUM(j.debit) + IFNULL(@sa_debit,0)),0))
                END
            ELSE 
            CASE 
                WHEN    MONTH(DATE_SUB('$date_start', INTERVAL 1 MONTH)) = 12 AND YEAR(DATE_SUB('$date_start', INTERVAL 1 MONTH)) = 2019
                THEN 
                (IFNULL(@sa_debit,0) - IFNULL(@sa_kredit,0))
                ELSE
                (IFNULL((SUM(j.debit) + IFNULL(@sa_debit,0)),0) - IFNULL((SUM(j.kredit) + IFNULL(@sa_kredit,0)),0))
                END
            END AS saldo,
            '-' `status`, '-' AS usertgl
            FROM acc_jurnalumum j
            JOIN acc_master_akun a ON j.kodeakun=a.kodeakun
            WHERE j.kodeakun='$kodeakun'
            AND j.tanggal BETWEEN CONCAT(YEAR('$date_start'),'-01-01') AND DATE_SUB('$date_start', INTERVAL 1 DAY)
        UNION ALL
        SELECT a.grup, '' AS sa_debit,'' AS sa_kredit, j.id, j.nobukti, DATE_FORMAT(j.tanggal,'%d/%m/%Y') AS tanggal, j.nip, 
            j.keterangan, j.kodeakun, a.namaakun, j.debit, j.kredit, 0 AS saldo, 
            j.status, j.usertgl 
            FROM acc_jurnalumum j
            JOIN acc_master_akun a ON j.kodeakun=a.kodeakun
            WHERE j.kodeakun='$kodeakun'
            AND j.tanggal BETWEEN '$date_start' AND '$date_end'
            AND (
            j.keterangan LIKE '%$keterangan%'
            )
        ORDER BY tanggal ASC")->result();
    }

    public function akun_header($in='')
    {
        return $this->db->query("SELECT * FROM acc_master_akun
        WHERE `status`=1
        AND type_data='header'
        AND grup IN($in)
        ORDER BY grup ASC")->result();
    }

    // ===== start report neraca ===== //
    public function neraca($params=[])
    {
        $bulan  = $params['bulan'];
        $tahun  = $params['tahun'];
        $parent = $params['parent'];
        $mekanisme = $params['mekanisme'];

        // mekanisme //
        // 0 Debet (+), Kredit (-)
        // 1 Debet (-), Kredit (+)

        $condition_lastday = " LAST_DAY(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH)) ";
        if($bulan == '01') {
            $condition_lastday = " LAST_DAY('$tahun-$bulan-01') ";
        }

        $total_saldo_awal = " ((IFNULL(total_kredit,0) + IFNULL(sa.saldoawal,0)) - IFNULL(total_debit,0)) AS total ";
        if($mekanisme == '0') {
            $total_saldo_awal = " ((IFNULL(total_debit,0) + IFNULL(sa.saldoawal,0)) - IFNULL(total_kredit,0)) AS total ";
        }

        return $this->db->query("SELECT a.kodeakun, a.namaakun, 
        $total_saldo_awal
        FROM acc_master_akun a
        LEFT JOIN (
            SELECT kodeakun, SUM(debit) AS total_debit, SUM(kredit) AS total_kredit 
                FROM acc_jurnalumum j
                WHERE YEAR(tanggal)='$tahun'
                AND MONTH(tanggal)='$bulan'
                GROUP BY kodeakun
        ) j ON a.kodeakun=j.kodeakun
        LEFT JOIN (
            SELECT j.kodeakun,
                CASE 
                    WHEN a.grup='2' OR a.grup='3' OR a.grup='4'
                        THEN 
                            CASE 
                                WHEN DATE_FORMAT(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH),'%m') = '12'
                                AND DATE_FORMAT(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH),'%Y') = '2019'
                                THEN (IFNULL(sa_kredit.kredit,0) - IFNULL(sa_debit.debit,0))
                                ELSE 
                                ((SUM(j.kredit) + IFNULL(sa_kredit.kredit,0)) - (SUM(j.debit) + IFNULL(sa_debit.debit,0)))
                            END
                    ELSE
                            CASE 
                                WHEN DATE_FORMAT(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH),'%m') = '12'
                                AND DATE_FORMAT(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH),'%Y') = '2019'
                                THEN (IFNULL(sa_debit.debit,0) - IFNULL(sa_kredit.kredit,0))
                                ELSE 
                                ((SUM(j.debit) + IFNULL(sa_debit.debit,0)) - (SUM(j.kredit) + IFNULL(sa_kredit.kredit,0)))
                            END
                END AS saldoawal
                FROM acc_jurnalumum j
                JOIN acc_master_akun a ON j.kodeakun=a.kodeakun
                LEFT JOIN (
                    SELECT kodeakun, SUM(debit) AS debit 
                                                FROM acc_saldo_awal_akun 
                                                WHERE periode='201912'
                                                GROUP BY kodeakun
                ) sa_debit ON j.kodeakun=sa_debit.kodeakun
                LEFT JOIN (
                    SELECT kodeakun, SUM(kredit) AS kredit 
                                                FROM acc_saldo_awal_akun 
                                                WHERE periode='201912'
                                                GROUP BY kodeakun
                ) sa_kredit ON j.kodeakun=sa_kredit.kodeakun
                WHERE a.parent='$parent'
                AND j.tanggal BETWEEN '$tahun-01-01' 
                AND $condition_lastday
                GROUP BY j.kodeakun
        ) sa ON a.kodeakun=sa.kodeakun
        WHERE a.parent='$parent'
        ORDER BY a.kodeakun ASC")->result();
    }

    public function report_neraca($params=[])
    {
        $bulan = $params['bulan'];
        $tahun = $params['tahun'];

        $total_aktiva = 0;
        $total_pasiva = 0;

        // ===== Aktiva ===== //
        $aktiva = $this->akun_header('1');
        $element_aktiva = '<table class="table table-striped table-bordered"><tbody>';
        foreach ($aktiva as $row_1) {
            $element_aktiva .= '<tr class="text-black">
                                    <td style="padding-left: 40px;">'.$row_1->kodeakun.' - '.$row_1->namaakun.'</td>
                                    <td align="right"></td>
                                </tr>';

            $dt_aktiva = $this->neraca([
                'bulan'    => $bulan,
                'tahun'    => $tahun,
                'parent'   => $row_1->kodeakun,
                'mekanisme'=> $row_1->mekanisme
            ]);
            foreach ($dt_aktiva as $val_1) {
                $total_aktiva  += $val_1->total;
                $element_aktiva.= '<tr>
                                    <td style="padding-left: 80px;"><a target="_blank" href="'.base_url().'Buku_besar?q='.$val_1->kodeakun.'&t='.$tahun.'&b='.$bulan.'">'.$val_1->kodeakun.' - '.$val_1->namaakun.'</a></td>
                                    <td align="right">'.currency($val_1->total).'</td>
                                </tr>';
            }
        }
        $element_aktiva.= '</tbody></table>';

        // ===== Pasiva ===== //
        $pasiva   = $this->akun_header('2, 3');
        $element_pasiva = '<table class="table table-striped table-bordered"><tbody>';
        foreach ($pasiva as $row_2) {
            $element_pasiva .= '<tr class="text-black">
                                    <td style="padding-left: 40px;">'.$row_2->kodeakun.' - '.$row_2->namaakun.'</td>
                                    <td align="right"></td>
                                </tr>';

            $dt_pasiva = $this->neraca([
                'bulan'    => $bulan,
                'tahun'    => $tahun,
                'parent'   => $row_2->kodeakun,
                'mekanisme'=> $row_2->mekanisme
            ]);
            foreach ($dt_pasiva as $val_2) {
                $total_pasiva  += $val_2->total;
                $element_pasiva.= '<tr>
                                    <td style="padding-left: 80px;"><a target="_blank" href="'.base_url().'Buku_besar?q='.$val_2->kodeakun.'&t='.$tahun.'&b='.$bulan.'">'.$val_2->kodeakun.' - '.$val_2->namaakun.'</a></td>
                                    <td align="right">'.currency($val_2->total).'</td>
                                </tr>';
            }
        }
        $element_pasiva.= '</tbody></table>';

        $result = [
            'table_aktiva' => $element_aktiva,
            'total_aktiva' => currency($total_aktiva),
            'table_pasiva' => $element_pasiva,
            'total_pasiva' => currency($total_pasiva)
        ];

        return $result;
    }
    // ===== end report neraca ===== //

    // ===== start report rugi laba ===== //
    public function rugi_laba($params=[])
    {
        $bulan = $params['bulan'];
        $tahun = $params['tahun'];
        $parent= $params['parent'];
        $mekanisme = $params['mekanisme'];

        // mekanisme //
        // 0 Debet (+), Kredit (-)
        // 1 Debet (-), Kredit (+)

        $total_saldo_awal = " ((IFNULL(total_kredit,0) + IFNULL(sa.saldoawal,0)) - IFNULL(total_debit,0)) AS total ";
        if($mekanisme == '0') {
            $total_saldo_awal = " ((IFNULL(total_debit,0) + IFNULL(sa.saldoawal,0)) - IFNULL(total_kredit,0)) AS total ";
        }

        return $this->db->query("SELECT a.kodeakun, a.namaakun, 
        $total_saldo_awal
        FROM acc_master_akun a
        LEFT JOIN (
            SELECT kodeakun, SUM(debit) AS total_debit, SUM(kredit) AS total_kredit 
                FROM acc_jurnalumum j
                WHERE YEAR(tanggal)='$tahun'
                AND MONTH(tanggal)='$bulan'
                GROUP BY kodeakun
        ) j ON a.kodeakun=j.kodeakun
        LEFT JOIN (
            SELECT j.kodeakun,
                CASE 
                    WHEN a.grup='2' OR a.grup='3'
                        THEN 
                            CASE 
                                WHEN DATE_FORMAT(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH),'%m') = '12'
                                AND DATE_FORMAT(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH),'%Y') = '2019'
                                THEN (IFNULL(sa_kredit.kredit,0) - IFNULL(sa_debit.debit,0))
                                ELSE 
                                ((SUM(j.kredit) + IFNULL(sa_kredit.kredit,0)) - (SUM(j.debit) + IFNULL(sa_debit.debit,0)))
                            END
                    ELSE
                            CASE 
                                WHEN DATE_FORMAT(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH),'%m') = '12'
                                AND DATE_FORMAT(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH),'%Y') = '2019'
                                THEN (IFNULL(sa_debit.debit,0) - IFNULL(sa_kredit.kredit,0))
                                ELSE 
                                ((SUM(j.debit) + IFNULL(sa_debit.debit,0)) - (SUM(j.kredit) + IFNULL(sa_kredit.kredit,0)))
                            END
                END AS saldoawal
                FROM acc_jurnalumum j
                JOIN acc_master_akun a ON j.kodeakun=a.kodeakun
                LEFT JOIN (
                    SELECT kodeakun, SUM(debit) AS debit 
                                                FROM acc_saldo_awal_akun 
                                                WHERE periode='201912'
                                                GROUP BY kodeakun
                ) sa_debit ON j.kodeakun=sa_debit.kodeakun
                LEFT JOIN (
                    SELECT kodeakun, SUM(kredit) AS kredit 
                                                FROM acc_saldo_awal_akun 
                                                WHERE periode='201912'
                                                GROUP BY kodeakun
                ) sa_kredit ON j.kodeakun=sa_kredit.kodeakun
                WHERE a.parent='$parent'
                AND j.tanggal BETWEEN '$tahun-01-01' 
                AND LAST_DAY(DATE_SUB('$tahun-$bulan-01', INTERVAL 1 MONTH))
                GROUP BY j.kodeakun
        ) sa ON a.kodeakun=sa.kodeakun
        WHERE a.parent='$parent'
        ORDER BY a.kodeakun ASC")->result();
    }

    public function report_rugi_laba($params=[])
    {
        $bulan = $params['bulan'];
        $tahun = $params['tahun'];

        $total_pendapatan = 0;
        $laba_rugi = 0;
        $header = $this->akun_header('4, 5, 6, 7');

        $res = '<table class="table table-striped table-bordered" style="padding-left: 50px;padding-right: 50px;">
                    <tr class="text-black">
                        <td>ACCOUNT</td>
                        <td>SUMMARY</td>
                    </tr>';
        
        foreach ($header as $rows) {
            $total_header = 0;
            $detail = $this->rugi_laba([
                'bulan'     => $bulan,
                'tahun'     => $tahun,
                'parent'    => $rows->kodeakun,
                'mekanisme' => $rows->mekanisme
            ]);
            $res .='<tr class="text-black">
                        <td>'.$rows->kodeakun.' - '.$rows->namaakun.'</td>
                    </tr>';

            foreach ($detail as $val) {
                $total_header += $val->total;
                if($rows->mekanisme == '1') {
                    $total_pendapatan += $val->total;
                }
                $res .= '<tr>
                            <td style="padding-left: 50px;">'.$val->kodeakun.' - '.$val->namaakun.'</td>
                            <td class="text-right" width="20%">'.currency($val->total).'</td>
                        </tr>';
            }
            $res .= '<tr class="text-black">
                        <td class="text-right">TOTAL '.$rows->namaakun.'</td>
                        <td class="text-right">'.currency($total_header).'</td>
                    </tr>';
            if($rows->mekanisme == '0') {
                $total_pendapatan = $total_pendapatan - abs($total_header);
                $laba_rugi = $total_pendapatan;
                $res .= '<tr class="text-black">
                            <td class="text-right" style="color:red;">LABA/RUGI</td>
                            <td class="text-right" style="color:red;">'.currency($laba_rugi).'</td>
                        </tr>';
            }
        }
        $res .= '<tr>
                    <td class="text-black">TOTAL LABA / RUGI BERSIH</td>
                    <td class="text-right text-black" width="20%">'.currency($laba_rugi).'</td>
                </tr>
            </table>';
        return $res;          
    }
    // ===== end report rugi laba ===== //
}

/* End of file Model_accounting.php */

?>