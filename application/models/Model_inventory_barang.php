<?php

class Model_inventory_barang extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }


    function get_data_table()
    {
        $kondisi = null;
        $column_order = array(null, 'd.nama', 'a.kode', 'a.nama', 'satuana', 'satuanb', 'a.tipe_pajak', 'a.tipe_barang', null, null, null);
        $order_name = (isset($_POST['order'])) ? $column_order[$_POST['order']['0']['column']] : 'a.nama';
        $order_dir = (isset($_POST['order'])) ? $_POST['order']['0']['dir'] : 'asc';

        if ($_POST['length'] != -1) $limit = ' LIMIT ' . $_POST['start'] . ',' . $_POST['length'];
        // $this->db->order_by($order_name, $order_dir);
        if (!empty($order_name) && !empty($order_dir)) {
            $order = "ORDER BY $order_name $order_dir";
        } else {
            $order = "ORDER BY a.nomor ASC";
        }
        if (!empty($this->input->post('searchkat'))) {
            $kondisi = " AND a.`kategori` = '" . $this->input->post('searchkat') . "'";
        }
        if (!empty($this->input->post('searchtipe'))) {
            $kondisi .= " AND a.`tipe_barang` = '" . $this->input->post('searchtipe') . "'";
        }

        $q = $this->db->query("SELECT SQL_CALC_FOUND_ROWS a.`id`,a.`kode`,a.`nama`,a.`tipe_pajak`,a.`tipe_barang`,b.`nama` AS kategori,
        CONCAT(c.`kode`,' - ',c.`nama`) AS satuana,CONCAT(d.`kode`,' - ',d.`nama`) AS satuanb
      FROM
        master_barang a
        LEFT JOIN master_kategori_barang b
          ON a.`kategori` = b.`id`
        LEFT JOIN master_satuan c
          ON a.`satuan1` = c.`id`
        LEFT JOIN master_satuan d
          ON a.`satuan2` = d.`id`
      WHERE a.`status` = 1 AND (
		`a`.`kode` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
            OR `a`.`nama` LIKE '%" . $this->input->post('search_keyword') . "%' ESCAPE '!'
		) $kondisi GROUP BY a.`id` $order $limit");
        $qn = $this->db->query('SELECT FOUND_ROWS() AS ttl');
        $n = $qn->row()->ttl;
        $data = array();
        $no = $_POST['start'];
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $r) {
                $tipe_pajak = $tipe_barang = $detail_proses = $detail_komponen = $query = $query2 = null;
                $opsi = '<a class="text-success" href="#" style="margin-right:20px" onClick="update_data(\'' . $r['id'] . '\')" title="Edit"><i class="icon-pencil"></i></a> <a class="text-warning" href="#" onClick="delete_data(\'' . $r['id'] . '\')" title="Hapus"><i class="icon-bin"></i></a>';

                if ($r['tipe_pajak'] == 1) {
                    $tipe_pajak = 'PPN';
                } else if ($r['tipe_pajak'] == 2) {
                    $tipe_pajak = 'PPnBM';
                } else if ($r['tipe_pajak'] == 3) {
                    $tipe_pajak = 'PPh23';
                } else if ($r['tipe_pajak'] == 4) {
                    $tipe_pajak = 'Bebas Pajak';
                } else if ($r['tipe_pajak'] == 5) {
                    $tipe_pajak = 'PPh22-0,3%';
                } else if ($r['tipe_pajak'] == 6) {
                    $tipe_pajak = 'PPN + PPh22(0,3%)';
                }
                if ($r['tipe_barang'] == 1) {
                    $tipe_barang = 'Hasil Produksi';
                } else if ($r['tipe_barang'] == 2) {
                    $tipe_barang = 'Dibeli';
                } else if ($r['tipe_barang'] == 3) {
                    $tipe_barang = 'Servis/Jasa/Barang di Biayakan';
                } else if ($r['tipe_barang'] == 4) {
                    $tipe_barang = 'Scrap';
                }
                $query = $this->db->query("SELECT * FROM master_barang a JOIN inventory_barang_komponen b ON b.`id_barang` = a.`id` WHERE a.`id`='" . $r['id'] . "'")->num_rows();
                if ($query > 0) {
                    $detail_komponen = '<a class="text-center" href="#" style="font-weight:700" onClick="detail_komponen(\'' . $r['id'] . '\')" title="Lihat Proses">Detail Komponen</a>';
                }
                $query2 = $this->db->query("SELECT * FROM master_barang a JOIN inventory_barang_proses b ON b.`id_barang` = a.`id` WHERE a.`id`='" . $r['id'] . "'")->num_rows();
                if ($query2 > 0) {
                    $detail_proses = '<a class="text-center" href="#" style="font-weight:700" onClick="detail_proses(\'' . $r['id'] . '\')" title="Lihat Proses">Detail Proses</a>';
                }
                $no++;
                $row  = array(
                    $no . '.',
                    $r['kategori'],
                    $r['kode'],
                    $r['nama'],
                    $r['satuana'],
                    $r['satuanb'],
                    $tipe_pajak,
                    $tipe_barang,
                    $detail_proses,
                    $detail_komponen,
                    $opsi
                );

                $data[] = $row;
            }
        }
        $q->free_result();

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $n,
            "recordsFiltered" => $n,
            "data" => $data,
        );
        echo json_encode($output);
    }

    function insert_data()
    {
        $data = $satuan2 = null;
        $flag = 1;
        if (isset($_POST['satuan2'])) {
            $satuan2 = $_POST['satuan2'];
            $flag = 2;
        }
        $data = array(
            'nama' => $this->input->post('nama_barang'),
            'tipe_barang' => $this->input->post('tipe_barang'),
            'tipe_pajak' => $this->input->post('tipe_pajak'),
            'kategori' => $this->input->post('kat_barang'),
            'satuan1' => $this->input->post('satuan'),
            'satuan2' => $satuan2,
            'flag' => $flag,
            'status' => 1,
            'insert_at' => date('Y-m-d'),
            'insert_by' => $this->session->userdata('userid')
        );
        $result = $this->db->insert('master_barang', $data);
        if ($result == true) {
            $id = $this->db->insert_id();
            if (isset($_POST['tahap_proses'])) {
                foreach ($_POST['tahap_proses'] as $k => $v) {
                    $data = array(
                        'id_barang' => $id,
                        'id_proses' => $_POST['tahap_proses'][$k],
                    );
                    $this->db->insert('inventory_barang_proses', $data);
                }
            }
            if (isset($_POST['benang'])) {
                foreach ($_POST['benang'] as $k => $v) {
                    $data = array(
                        'id_barang' => $id,
                        'id_benang' => $_POST['benang'][$k],
                        'persen_benang' => $_POST['persen_benang'][$k]
                    );
                    $this->db->insert('inventory_barang_komponen', $data);
                }
            }
            return 1;
        } else {
            return 0;
        }
    }

    function update_data()
    {
        $id = $this->input->post('id');
        $data = $satuan2 = null;
        $flag = 1;
        if (isset($_POST['satuan2'])) {
            $satuan2 = $_POST['satuan2'];
            $flag = 2;
        }
        $data = array(
            'nama' => $this->input->post('nama_barang'),
            'tipe_pajak' => $this->input->post('tipe_barang'),
            'kategori' => $this->input->post('kat_barang'),
            'satuan1' => $this->input->post('satuan'),
            'satuan2' => $satuan2,
            'flag' => $flag,
            'status' => 1,
            'update_at' => date('Y-m-d'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $this->db->update('master_barang', $data);

        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id_barang', $id);
        $this->db->update('inventory_barang_proses', $data);

        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id_barang', $id);
        $this->db->update('inventory_barang_komponen', $data);

        if (isset($_POST['tahap_proses'])) {
            foreach ($_POST['tahap_proses'] as $k => $v) {
                $data = array(
                    'id_barang' => $id,
                    'id_proses' => $_POST['tahap_proses'][$k],
                );
                $this->db->insert('inventory_barang_proses', $data);
            }
        }
        if (isset($_POST['benang'])) {
            foreach ($_POST['benang'] as $k => $v) {
                $data = array(
                    'id_barang' => $id,
                    'id_benang' => $_POST['benang'][$k],
                    'persen' => $_POST['persen_benang'][$k]
                );
                $this->db->insert('inventory_barang_komponen', $data);
            }
        }
        return 1;
    }

    function delete_data($id = '')
    {
        $data = array(
            'status' => 9,
            'update_at' => date('Y-m-d'),
            'update_by' => $this->session->userdata('userid')
        );
        $this->db->where('id', $id);
        $return = $this->db->update('master_barang', $data);
        if ($return == true) {
            return 1;
        } else {
            return 0;
        }
    }

    function get_kategoribarang()
    {
        return $this->db->query("SELECT * FROM master_kategori_barang WHERE status=1");
    }

    function get_satuanbarang()
    {
        return $this->db->query("SELECT * FROM master_satuan WHERE status=1");
    }

    function get_bahan($param = '')
    {
        $q = $this->db->query("SELECT * FROM master_barang WHERE status=1 AND kategori=16 AND (nama LIKE '%$param%' OR kode LIKE '%$param%') ORDER BY nama ASC LIMIT 25")->result();
        $data = array();
        foreach ($q as $row) {
            $data[] = array(
                "id" => $row->id,
                "text" => $row->kode . ' - ' . $row->nama . ' ' . $row->color
            );
        }
        return json_encode($data);
    }

    function get_proses()
    {
        return $this->db->query("SELECT * FROM master_tahap_proses WHERE status=1");
    }

    function select_data()
    {
        $satuan2 = $komponen = $response = $q2 = null;
        $a = 0;
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT * FROM master_barang WHERE id=$id AND status=1")->result();
        if (!empty($query)) {
            foreach ($query as $low) {
                if (!empty($low->satuan2)) {
                    $satuan2 .= '<label>Satuan 2<a class="text-danger" onclick="del_satuan2(this)"><i class="icon-cross3"></i></a></label><select class="form-control satuan2" name="satuan2">';
                    $q = $this->db->query("SELECT * FROM master_satuan WHERE status=1");
                    if ($q->num_rows() > 0) {
                        foreach ($q->result_array() as $row) {
                            if ($row['id'] ==  $low->satuan2) {
                                $satuan2 .= "<option value='" . $row['id'] . "' selected>" . $row['kode'] . " - " . $row['nama'] . "</option>";
                            } else {
                                $satuan2 .= "<option value='" . $row['id'] . "'>" . $row['kode'] . " - " . $row['nama'] . "</option>";
                            }
                        }
                    }
                    $satuan2 .= '</select>';
                }
                $query2 = $this->db->query("SELECT * FROM master_tahap_proses WHERE status=1");
                if ($low->flag == 2) {
                    $q = $this->db->query("SELECT * FROM master_barang a JOIN inventory_barang_proses b ON a.`id`=b.`id_barang` JOIN master_tahap_proses c ON b.`id_proses`=c.`id` WHERE a.`id` = $id");
                    if ($q->num_rows() > 0) {
                        foreach ($q->result_array() as $sow) {
                            if ($query2->num_rows() > 0) {
                                foreach ($query2->result_array() as $row) {
                                    if ($row['id'] ==  $sow['id_proses']) {
                                        $response .= "<option value='" . $row['id'] . "' selected>" . $row['kode'] . ' - ' . $row['nama'] . "</option>";
                                    } else {
                                        $response .= "<option value='" . $row['id'] . "'>" . $row['kode'] . ' - ' . $row['nama'] . "</option>";
                                    }
                                }
                            }
                            if ($a == 0) {
                                $komponen .= '<label><b>Tahapan Proses</b></label>
                            <div class="tahapan"><div class="row"><div class="col-lg-1 text-center"><label>No</label><input class="form-control no_urut" type="text" disabled></div><div class="col-lg-9 text-center"><label>Tahapan</label><select class="form-control" name="tahap_proses[]">' . $response . '</select></div><div class="col-lg-2 text-center"><label>Aksi</label><a class="btn btn-primary text-light form-control" onclick="tambah_proses(this)"><i class="icon-plus3"> Tambah</i></a></div>
                            </div>';
                                $a++;
                            } else {
                                $komponen .= '<div class="row"><br><div class="col-lg-1 text-center"><input class="form-control no_urut" type="text" disabled></div><div class="col-lg-9 text-center"><select class="form-control" name="tahap_proses[]">' . $response . '</select></div><div class="col-lg-2 text-center"><a class="btn btn-warning text-light form-control" onclick="del_proses(this)"><i class="icon-cross2"> Hapus</i></a></div></div>';
                                $a++;
                            }
                        }
                    }
                    $komponen .= '</div>';
                }

                $a = 0;
                if ($low->flag == 2) {
                    $q2 = $this->db->query("SELECT * FROM master_barang a JOIN inventory_barang_komponen b ON a.`id`=b.`id_barang` JOIN master_barang c ON b.`id_benang`=c.`id` WHERE a.`id` = $id");
                    if ($q2->num_rows() > 0) {

                        foreach ($q2->result_array() as $row) {
                            if ($a == 0) {
                                $komponen .= '<br><label><b>Komponen Benang</b></label><div class="komponen"><div class="row">
                                <div class="col-lg-1 text-center">
                                    <label>No</label>
                                    <input class="form-control no_urut2" type="text" disabled>
                                </div>
                                <div class="col-lg-6 text-center">
                                    <label>Nama Benang</label>
                                    <select class="form-control get_bahan" id="get_bahan_' . $a . '" name="benang[]"></select>
                                </div>
                                <div class="col-lg-2 text-center">
                                    <label>Jumlah</label>
                                    <input class="form-control persen_benang" type="text" name="persen_benang[]" value="' . $row['persen_benang'] . '"/>
                                </div>
                                <div class="col-lg-1 text-center">
                                    <br><br>
                                    <label><b>%</b></label>
                                </div>
                                <div class="col-lg-2 text-center">
                                    <label>Aksi</label><a class="btn btn-primary text-light form-control" onclick="tambah_bahan(this)"><i class="icon-plus3"> Tambah</i></a>
                                </div>
                            </div';
                            } else {
                                $komponen .= '<div class="row"><br><div class="col-lg-1 text-center"><input class="form-control no_urut2" type="text" disabled></div><div class="col-lg-6 text-center"><select class="form-control get_bahan" id="get_bahan_' . $a . '" name="benang[]"></select></div><div class="col-lg-2 text-center"><input class="form-control persen_benang" type="text" name="persen_benang[]" value="' . $row['persen_benang'] . '"/></div><div class="col-lg-1 text-center"><br><label><b>%</b></label></div><div class="col-lg-2 text-center"><a class="btn btn-warning text-light form-control" onclick="del_bahan(this)"><i class="icon-cross2"> Hapus</i></a></div></div>';
                            }
                        }
                        $komponen .= '</div>';
                    }
                }
            }
            if (!empty($q2)) {
                $data = array('data' => $query, 'det' => $komponen, 'sat2' => $satuan2, 'bahan' => $q2->result());
                return $data;
            } else {
                $data = array('data' => $query, 'det' => $komponen, 'sat2' => $satuan2);
                return $data;
            }
        } else {
            return false;
        }
    }

    function detail_komponen()
    {
        $a = $i = 0;
        $komponen = null;
        $id = $this->input->post('id');
        $q2 = $this->db->query("SELECT *,a.`nama` AS nama_kom FROM master_barang a JOIN inventory_barang_komponen b ON a.`id`=b.`id_barang` JOIN master_barang c ON b.`id_benang`=c.`id` WHERE a.`id` = $id");
        if ($q2->num_rows() > 0) {
            foreach ($q2->result_array() as $row) {
                $i++;
                if ($a == 0) {
                    $komponen .= '<label><b>Komponen Benang : ' . $row['nama_kom'] . '</b></label>
                    <div class="row">
                        <div class="col-lg-2 text-center">
                            <label>No</label>
                            <input class="form-control " type="text" value="' . $i . '" readonly/>
                        </div>
                        <div class="col-lg-7 text-center">
                            <label>Nama Benang</label>
                            <input class="form-control" type="text" value="' . $row['nama'] . '" readonly/>
                        </div>
                        <div class="col-lg-2 text-center">
                            <label>Jumlah</label>
                            <input class="form-control" type="text" value="' . $row['persen_benang'] . '" readonly/>
                        </div>
                        <div class="col-lg-1 text-center">
                            <br><br>
                            <label><b>%</b></label>
                        </div>
                    </div';
                    $a++;
                } else {
                    $komponen .= '<br><div class="row">
                    <div class="col-lg-2 text-center">
                    <label>No</label>
                    <input class="form-control " type="text" value="' . $i . '" readonly/>
                </div>
                <div class="col-lg-7 text-center">
                    <label>Nama Benang</label>
                    <input class="form-control" type="text" value="' . $row['nama'] . '" readonly/>
                </div>
                <div class="col-lg-2 text-center">
                    <label>Jumlah</label>
                    <input class="form-control" type="text" value="' . $row['persen_benang'] . '" readonly/>
                </div>
                <div class="col-lg-1 text-center">
                    <br><br>
                    <label><b>%</b></label>
                </div>
                    </div>';
                }
            }
            $data = array('det' => $komponen);
            return $data;
        } else {
            return 0;
        }
    }

    function detail_proses()
    {
        $a = $i = 0;
        $proses = null;
        $id = $this->input->post('id');
        $query = $this->db->query("SELECT *,a.`nama` AS nama_barang,c.`nama` AS nama_proses FROM master_barang a JOIN inventory_barang_proses b ON a.`id`=b.`id_barang` JOIN master_tahap_proses c ON b.`id_proses`=c.`id` WHERE a.`id` = $id");
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $i++;
                if ($a == 0) {
                    $proses .= '<label><b>Tahapan Proses : ' . $row['nama_barang'] . '</b></label>
                            <div class="row">
                                <div class="col-lg-2 text-center">
                                <label>No</label>
                                <input class="form-control" type="text" value="' . $i . '" readonly/>
                                </div>
                                <div class="col-lg-10 text-center">
                                <label>Tahapan</label>
                                <input class="form-control" type="text" value="' . $row['nama_proses'] . '"/>
                                </div>
                            </div>';
                    $a++;
                } else {
                    $proses .= '<br><div class="row">
                    <div class="col-lg-2 text-center">
                    <input class="form-control" type="text" value="' . $i . '" readonly/>
                    </div>
                    <div class="col-lg-10 text-center">
                    <input class="form-control" type="text" value="' . $row['nama_proses'] . '"/>
                    </div></div>';
                }
            }

            $data = array('det' => $proses);
            return $data;
        } else {
            return 0;
        }
    }

    function cek_kode($id = '')
    {
        $q = $this->db->query("SELECT * FROM master_barang WHERE kode='" . $id . "' AND status=1")->result();
        if (empty($q)) {
            return false;
        } else {
            return 1;
        }
    }

    function cek_kode_edit()
    {
        $id = $this->input->post('id');
        $kode = $this->input->post('kode');
        $q = $this->db->query("SELECT * FROM master_barang WHERE kode='" . $kode . "' AND status=1 AND id!=$id")->result();
        if (empty($q)) {
            return false;
        } else {
            return 1;
        }
    }
}
