<?php
defined('BASEPATH') or exit('No direct script access allowed');

$lang['profile_modal_title'] = 'Update Profile';
$lang['changepass_modal_title'] = 'Change Password';
$lang['my_name'] = 'Name';
$lang['my_email'] = 'Email';
$lang['my_username'] = 'Username';
$lang['my_password'] = 'Password';

$lang['my_name_required'] = 'Name is required';
$lang['my_email_required'] = 'Email is required';
$lang['my_email_email'] = 'Format email invalid';

$lang['profile_update_seccess'] = 'Your profile has been updated';
$lang['profile_update_failed'] = "Wrong password, Profile update failed";
$lang['password_update_seccess'] = 'Your password has been updated';
$lang['password_update_failed'] = 'Failed, Your password not updated';
