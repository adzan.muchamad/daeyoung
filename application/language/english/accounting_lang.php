<?php
defined('BASEPATH') or exit('No direct script access allowed');

$prefix = 'IT INVENTORY - ';

$lang['accounting_alltitle']   = 'ACCOUNTING';
$lang['browser_title']         = 'ACCOUNTING';

$lang['coa_category_alltitle'] = 'COA CATEGORY';
$lang['coa_lain_lain_alltitle']= 'COA LAIN LAIN';
$lang['coa_kas_bank_alltitle'] = 'COA KAS & BANK';

$lang['jurnal_umum']     = 'JURNAL UMUM';
$lang['jurnal_kas_bank'] = 'JURNAL KAS & BANK';
$lang['jurnal_otomatis'] = 'JURNAL OTOMATIS';

$lang['jurnal_umum_form']= 'FORM JURNAL UMUM';
$lang['jurnal_kas_bank_form']= 'FORM JURNAL KAS & BANK';

$lang['report_neraca']= 'REPORT NERACA';
$lang['report_rugi_laba']= 'REPORT RUGI LABA';
$lang['buku_besar']= 'BUKU BESAR';

$lang['piutang']= 'INVOICE CUSTOMER';
$lang['pelunasan_piutang']= 'DATA PELUNASAN PIUTANG';
$lang['pelunasan_piutang_form']= 'FORM PELUNASAN PIUTANG';

$lang['hutang']= 'KARTU HUTANG';
$lang['penerimaan_invoice_ap']= 'PENERIMAAN INVOICE';