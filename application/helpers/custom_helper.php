<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function dt_searching($kolom, $keyword)
{
	$condition = '';
	if ($keyword <> '') {
		$condition .= " AND ( ";
		for ($i = 0; $i < count($kolom); $i++) {
			$condition .= " $kolom[$i] LIKE '%$keyword%' ";

			if (end($kolom) <> $kolom[$i]) {
				$condition .= " OR ";
			}
		}
		$condition .= " ) ";
	}

	return $condition;
}

function dt_order($kolom, $order_column, $order_mode)
{
	$order = " ORDER BY $kolom[$order_column] $order_mode ";

	return $order;
}

function custom_date_format($date, $from, $to)
{
	if ($date <> '') {
		$date = date_create_from_format($from, $date);
		$date = date_format($date, $to);
	}

	return $date;
}

function daeyoung() {
	return 'daeyoung';
}

function daesystem()
{
	return 'daesystem';
}

function interval($date, $durasi)
{
    $date = date_create($date);
    date_add($date, date_interval_create_from_date_string("$durasi"));
    return date_format($date, 'Y-m-d');
}

function datediff_month($datestart, $dateend)
{
    $ts1 = strtotime($datestart);
    $ts2 = strtotime($dateend);

    $year1 = date('Y', $ts1);
    $year2 = date('Y', $ts2);

    $month1 = date('m', $ts1);
    $month2 = date('m', $ts2);

    $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

    return $diff + 1;
}