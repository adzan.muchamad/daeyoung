<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function simple_arr($source, $val)
{
	$arr = array();
	if(!empty($source)):
		foreach($source as $row):
			$arr[] = $row[$val];
		endforeach;
	endif;
	return $arr;
}

function arr_to_query($arr)
{
	$CI =& get_instance();
	switch ($arr[1]) {
		case '=':
			$CI->db->where($arr[0], $arr[2]);
		break;

		case '!=':
			$CI->db->where($arr[0].' !=', $arr[2]);
		break;

		default:
		break;
	};
}

function arr($source, $key, $val)
{
	$arr = array();
	if(!empty($source)):
		foreach($source as $row):
			$arr[$row[$key]] = $row[$val];
		endforeach;
	endif;
	return $arr;
}

function arr_to_comma($arr=array())
{
	$comma = '';
	if(!empty($arr)):
		foreach($arr as $row):
			$comma .= "'".$row."', ";
		endforeach;
	endif;
	$comma = substr($comma, 0, strlen($comma)-2);
	return $comma;
}

function arr_string_to_int($data,$int_arr=array())
{
	$i = 0;
	$arr = array();
	foreach ($data as $row) {
		foreach ($row as $key => $value) {
			if (in_array($key, $int_arr)) {
				$arr[$i][$key] = intval($value);
			} else {
				$arr[$i][$key] = $value;
			}
		}
		$i++;
	}
	return $arr;
}
