<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Buku_besar extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('buku_besar') == FALSE) {
            gak_boleh('buku_besar');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_accounting');
        $this->load->model('Model_main');
        $this->load->model('Model_jurnal');
        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('buku_besar');
        $this->browser_title    = $this->lang->line('buku_besar');
        $this->modul_name       = $this->lang->line('buku_besar');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }
    
    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('buku_besar') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/report/data_buku_besar_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('buku_besar');
        $data['opt_akun']   = $this->Model_accounting->opt_all_coa();

        $kodeakun = $this->input->get('q');
        $gt_tahun = $this->input->get('t');
        $gt_bulan = $this->input->get('b');

        $date_start = date('Y-m-01');
        $date_end   = date('Y-m-d');

        if($gt_tahun != '' && $gt_bulan != '' && $kodeakun != '') {
            $date_start = date($gt_tahun.'-'.$gt_bulan.'-01');
            $date_end   = date('t',strtotime($date_start));
            $date_end   = date('Y-'.$gt_bulan.'-'.$date_end);
        }

        $data['kodeakun']   = $kodeakun;
        $data['date_start'] = $date_start;
        $data['date_end']   = $date_end;

        $konten = $this->load->view('accounting/report/data_buku_besar', $data, TRUE);
        $this->admin_view($konten);
    }

    public function ajax_data_buku_besar()
    {
        $this->Model_main->ajax_function();
        $kodeakun = $this->input->get('kodeakun',TRUE);
        $keterangan = $this->input->get('keterangan',TRUE);
        
        $date_start = $this->input->get('date_start',TRUE);
        $date_end = $this->input->get('date_end',TRUE);

        $data = $this->Model_accounting->data_buku_besar([
            'date_start' => $date_start,
            'date_end' => $date_end,
            'kodeakun' => $kodeakun,
            'keterangan' => $keterangan
        ]);

        $arr = array();
        if(!empty($data)) {
            $no = 1;
            $saldo = 0;
            $total_debit  = 0;
            $total_kredit = 0;
            foreach ($data as $row => $val) {
                $nobukti = $val->nobukti;
                if($val->nobukti == 'SA') {
                    $saldo += $val->saldo;
                }
                if($val->grup == 2 || $val->grup == 3 || $val->grup == 4) { // grup pasiva & modal
                    $saldo -= $val->debit;
                    $saldo += $val->kredit;
                }
                if($val->grup != 2 && $val->grup != 3 && $val->grup != 4) {
                    $saldo += $val->debit;
                    $saldo -= $val->kredit;
                }
                $total_debit  += $val->debit;
                $total_kredit += $val->kredit;
                $arr[$row] ='<tr>
                                <td>'.$no++.'</td>
                                <td>'.$val->tanggal.'</td>
                                <td>'.$nobukti.'</td>
                                <td>'.$val->keterangan.'</td>
                                <td class="text-right">'.currency($val->debit).'</td>
                                <td class="text-right">'.currency($val->kredit).'</td>
                                <td class="text-right bold">'.currency($saldo).'</td>
                            </tr>';
            }
        }
            $respon = [
                'data' => $arr,
                'total_debit' => currency($total_debit),
                'total_kredit' => currency($total_kredit),
                'saldo' => currency($saldo)
            ];
        echo json_encode($respon);        
    }

    public function excel()
    {
        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

        $kodeakun   = $this->input->get('kodeakun',TRUE);
        $keterangan = $this->input->get('keterangan',TRUE);
        $date_start = $this->input->get('date_start',TRUE);
        $date_end   = $this->input->get('date_end',TRUE);

        $data = $this->Model_accounting->data_buku_besar([
            'date_start' => $date_start,
            'date_end' => $date_end,
            'kodeakun' => $kodeakun,
            'keterangan' => $keterangan
        ]);
        
        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);

        $merge_center = [
        	'alignment' => [
        		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        		'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        	]
        ];

        $object->getActiveSheet()->mergeCells('A1:G1')
        ->setCellValue('A1', 'REPORT BUKU BESAR '.$date_start.' - '.$date_end)
        ->getStyle('A1')
        ->applyFromArray($merge_center);

        $object->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $object->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $object->getActiveSheet()->getColumnDimension('C')->setWidth(25);
        $object->getActiveSheet()->getColumnDimension('D')->setWidth(45);
        $object->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $object->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $object->getActiveSheet()->getColumnDimension('G')->setWidth(20);

        $i = 1;
        $start_row = 2;
        $saldo = 0;
        if($data) {
            foreach($data as $row) {
                if($row->nobukti == 'SA') {
                    $saldo += $row->saldo;
                }
                if($row->grup == 2 || $row->grup == 3 || $row->grup == 4) { // grup pasiva & modal
                    $saldo -= $row->debit;
                    $saldo += $row->kredit;
                }
                if($row->grup != 2 || $row->grup != 3 || $row->grup != 4) {
                    $saldo += $row->debit;
                    $saldo -= $row->kredit;
                }

                $object->getActiveSheet()
                ->getStyle('E'.$start_row)
                ->getNumberFormat()
                ->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

                $object->getActiveSheet()
                ->getStyle('F'.$start_row)
                ->getNumberFormat()
                ->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

                $object->getActiveSheet()
                ->getStyle('G'.$start_row)
                ->getNumberFormat()
                ->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

                $object->getActiveSheet()->setCellValue('A'.$start_row, $i);
                $object->getActiveSheet()->setCellValue('B'.$start_row, $row->tanggal);
                $object->getActiveSheet()->setCellValue('C'.$start_row, $row->nobukti);
                $object->getActiveSheet()->setCellValue('D'.$start_row, $row->keterangan);
                $object->getActiveSheet()->setCellValue('E'.$start_row, $row->debit);
                $object->getActiveSheet()->setCellValue('F'.$start_row, $row->kredit);
                $object->getActiveSheet()->setCellValue('G'.$start_row, $saldo);

                $start_row++;
                $i++;
            } 
        }

        $date = date('YmdHis').rand();
		$object_writer = IOFactory::createWriter($object, 'Excel5');
		$filename = urlencode("buku-besar-".$date.".xls");

		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"'); 
		header('Cache-Control: max-age=0'); 

		$object_writer->save('php://output');
    }

}

/* End of file Buku_besar.php */

?>