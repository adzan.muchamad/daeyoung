<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coa_category extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('coa_category') == FALSE) {
            gak_boleh('coa_category');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_accounting');
        $this->load->model('Model_main');

        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('coa_category_alltitle');
        $this->browser_title    = $this->lang->line('coa_category_alltitle');
        $this->modul_name       = $this->lang->line('coa_category_alltitle');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }
    
    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('coa_category_alltitle') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/coa/coa_category_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('coa_category_alltitle');

        $konten = $this->load->view('accounting/coa/coa_category', $data, TRUE);
        $this->admin_view($konten);
    }

    function ajax_data_coa_category()
    {
        $data = $this->Model_accounting->data_coa_category();
            $arr = array();
            $no = 1;
            if(!empty($data)) {
                foreach ($data as $row => $val) {
                    $arr[$row] = array(
                                    'no' => $no++,
                                    'grup' => $val->grup,
                                    'kodeakun' => $val->kodeakun,
                                    'namaakun' => $val->namaakun,
                                    'mekanisme' => $val->mekanisme_name,
                                    'aksi' => '<button title="Edit" type="button" class="btn btn-info btn-sm" onclick="edit('.$val->id.')"><i class="icon-pencil"></i></button>&nbsp;<button title="Hapus" type="button" class="btn btn-danger btn-sm" onclick="hapus('.$val->id.')"><i class="icon-bin"></i></button>'
                                );
                }
            }

            $json = array(
                        'data' => $arr
                    );
        echo json_encode($json);
    }

    function ajax_save()
    {
        $username = $this->session->userdata('userid');
        $id = $this->input->post('id');

        $grup = $this->input->post('grup');
        $kode = $this->input->post('kode');
        $nama = $this->input->post('nama');
        $mekanisme = $this->input->post('mekanisme');

        if($grup == '' || $grup == '0' || $kode == '' || $nama == '' || $mekanisme == '') {
            $message = '';
            if($grup == '' || $grup == '0') $message.='Form grup harus di isi ! <br>';
            if($kode == '') $message.='Form kode account harus di isi ! <br>';
            if($nama == '') $message.='Form nama account harus di isi ! <br>';
            if($mekanisme == '') $message.='Form mekanisme harus di isi ! <br>';
            $respon = array('status'=>FALSE,'message'=>$message);
            echo json_encode($respon);exit;
        }
        $message = '';
        $data = array(
                    'kodeakun' => $kode,
                    'namaakun' => $nama,
                    'mekanisme' => $mekanisme,
                    'grup' => $grup,
                    'flag' => 'lain-lain',
                    'type_data' => 'header',
                    'parent' => '0'
                );

        // check grup
        if($id == '') {
            $check_grup = $this->Model_main->view_by_id('acc_master_akun', array('grup'=>$grup, 'type_data'=>'header'));
            if(!empty($check_grup)) {
                $respon = array('status'=>FALSE,'message'=>'Grup sudah di gunakan !');
                echo json_encode($respon);exit;
            }
        }

        // check kode
        if($id == '') {
            $check_kode = $this->Model_main->view_by_id('acc_master_akun', array('kodeakun'=>$kode));
            if(!empty($check_kode)) {
                $respon = array('status'=>FALSE,'message'=>'Kode akun sudah di gunakan !');
                echo json_encode($respon);exit;
            }
        }

        if($id == '') {
            // push value
            $data['user_insert'] = $username;
            $data['tanggal_insert'] = $this->Model_main->time_server();
            $this->Model_main->process_data('acc_master_akun', $data);
            $message = 'Simpan data sukses !';
        } else {
            // push value
            $data['user_update'] = $username;

            // check eksis grup id
            $check_grup_id = $this->Model_main->view_by_id('acc_master_akun', array('id'=>$id,'grup'=>$grup));
            if(empty($check_grup_id)) {
                $check_grup = $this->Model_main->view_by_id('acc_master_akun', array('grup'=>$grup, 'type_data'=>'header'));
                if(!empty($check_grup)) {
                    $respon = array('status'=>FALSE,'message'=>'Grup sudah di gunakan !');
                    echo json_encode($respon);exit;
                }
            }

            // check eksis kode id
            $check_kode_id = $this->Model_main->view_by_id('acc_master_akun', array('id'=>$id,'kodeakun'=>$kode));
            if(empty($check_kode_id)) {
                $check_kode = $this->Model_main->view_by_id('acc_master_akun', array('kodeakun'=>$kode));
                if(!empty($check_kode)) {
                    $respon = array('status'=>FALSE,'message'=>'Kode akun sudah di gunakan !');
                    echo json_encode($respon);exit;
                }
            }

            $this->Model_main->process_data('acc_master_akun', $data, array('id'=>$id));
            $message = 'Update data sukses !';
        }
        // update detail // 
        $this->Model_main->process_data('acc_master_akun',['mekanisme'=>$mekanisme],['parent'=>$kode]);

        $respon = array('status'=>TRUE,'message'=>$message);
        echo json_encode($respon);
    }

    function ajax_edit($id='')
    {
        $data = $this->Model_accounting->data_coa_category_id($id);
        echo json_encode($data);
    }

    function ajax_hapus($id='')
    {
        $username = $this->session->userdata('userid');
        // check
        $check = $this->Model_main->view_by_id('acc_master_akun', array('id'=>$id, 'status'=>'1'));
        if(empty($check)) {
            $respon = array('status'=>FALSE,'message'=>'Data tidak ditemukan !');
            echo json_encode($respon);
        }

        $data = array(
                    'status' => '0',
                    'user_update' => $username
                );

        $this->Model_main->process_data('acc_master_akun', $data, array('id'=>$id));
        echo json_encode(array('status'=>TRUE,'message'=>'Hapus data sukses !'));
    }

}

/* End of file Coa_category.php */

?>