<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal_umum_form extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('jurnal_umum_form') == FALSE) {
            gak_boleh('jurnal_umum_form');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_accounting');
        $this->load->model('Model_jurnal');
        $this->load->model('Model_main');

        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('jurnal_umum_form');
        $this->browser_title    = $this->lang->line('jurnal_umum_form');
        $this->modul_name       = $this->lang->line('jurnal_umum_form');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }

    public function index()
    {
        $id = $this->input->get('q',TRUE);
        
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('jurnal_umum_form') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/jurnal/jurnal_umum_form_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('jurnal_umum_form');
        $data['jobs_id'] = session_id();
        $data['detail']  = $this->Model_main->view_by_id('acc_jurnalumum',['id' => $id]);

        $konten = $this->load->view('accounting/jurnal/jurnal_umum_form', $data, TRUE);
        $this->admin_view($konten);
    }

    public function ajax_detail_jurnal()
    {
        $this->Model_main->ajax_function();

        $nobukti = $this->input->get('nobukti',true);
        $jobs_id = $this->input->get('jobs_id',true);
        
        $data = $this->Model_jurnal->detail_jurnal($nobukti);
        if($nobukti == '') {
            $data = $this->Model_jurnal->detail_jurnal_temp($jobs_id);
        }

        $arr = array();
            $tot_debit  = 0;
            $tot_kredit = 0;
            if(!empty($data)) {
                foreach ($data as $row => $val) {
                    $btn_aksi  = '<a class="btn btn-danger btn-xs btn-hapus" data-id="'.$val->id.'"><i class="icon-bin"></i></a>';
                    if($val->auto == '1') {
                        $btn_aksi = '#';
                    } 
                    $arr[$row] = array(
                                    'id' => $val->id,
                                    'kodeakun' => $val->kodeakun,
                                    'namaakun' => $val->namaakun,
                                    'keterangan' => $val->keterangan,
                                    'debit' => currency($val->debit),
                                    'kredit' => currency($val->kredit),
                                    'aksi' => $btn_aksi
                                );

                    $tot_debit += $val->debit;
                    $tot_kredit += $val->kredit;
                }
            }
            $out_of_balance = $tot_debit - $tot_kredit;
            $json = array(
                        'data'=> $arr,
                        'tot_debit'=> currency($tot_debit),
                        'tot_kredit'=> currency($tot_kredit),
                        'out_of_balance'=> currency($out_of_balance)
                    );
        echo json_encode($json);
    }

    public function ajax_data_coa()
    {
        $this->Model_main->ajax_function();

        $search= $this->input->get('q');
        $start = $this->input->get('page');
        $limit = 10;

        $start = ceil($start - 1) * $limit;
        
        $data = $this->Model_accounting->ajax_data_all_coa($search, $start, $limit);
        $count_data = $this->Model_accounting->ajax_data_all_coa_count($search);

        $results = array(
            "results" => $data,
            "pagination" => array(
                "more" => ($start * $limit) < $count_data
            )
        );

        echo json_encode($results);
    }

    public function ajax_simpan_detail()
    {
        $this->Model_main->ajax_function();

        $username = $this->session->userdata('username');

        $jobs_id    = $this->input->post('jobs_id');

        $tanggal    = $this->input->post('tanggal',true);
        $nobukti    = $this->input->post('nobukti',true);
        $kodejurnal = $this->input->post('kodejurnal',true);
        $kodeakun   = $this->input->post('kodeakun',true);
        $keterangan = $this->input->post('keterangan',true);

        $debit      = $this->Model_main->replace_money($this->input->post('debit'));
        $kredit     = $this->Model_main->replace_money($this->input->post('kredit'));

        if($kodejurnal == '') {
            $respon = array('status'=>FALSE,'message'=>'Silahkan pilih Kode jurnal');
            echo json_encode($respon);exit;
        }

        // get rule //
        $get_rule = $this->Model_main->view_by_id('acc_rule_jurnal', ['kode' => $kodejurnal]);
        $tipe = isset($get_rule->tipe) ? $get_rule->tipe:'';
        $oto_debit  = isset($get_rule->debit) ? $get_rule->debit:'';
        $oto_kredit = isset($get_rule->kredit) ? $get_rule->kredit:'';

        if($kodeakun == '') {
            $respon = array('status'=>FALSE,'message'=>'Silahkan pilih Coa pemotong / penambah terlebih dahulu');
            echo json_encode($respon);exit;
        }

        if(($debit == '0' && $kredit == '0') || ($debit == '' && $kredit == '')) {
            $respon = array('status'=>FALSE,'message'=>'Debit atau kredit mohon di isi !');
            echo json_encode($respon);exit;
        }

        $data_join = [];
        $id_auto_data = '';
        
        $data = array(
                    'tanggal' => $tanggal,
                    'keterangan' => $keterangan,
                    'kodeakun' => $kodeakun,
                    'debit' => $debit,
                    'kredit' => $kredit,
                    'userid' => $username,
                    'usertgl' => $this->Model_main->time_server(),
                    'auto'      => '0'
                );

        if($oto_debit == '1' || $oto_kredit == '1') {
            // check auto data //
            $check_auto_data = $this->Model_main->view_by_id('acc_jurnalumum_temp', ['jobs_id'=>$jobs_id,'auto'=>'1']);

            $check_eksis = $this->Model_main->view_by_id('acc_jurnalumum', ['nobukti'=>$nobukti]);
            if(!empty($check_eksis)) {
                $check_auto_data = $this->Model_main->view_by_id('acc_jurnalumum', ['nobukti'=>$nobukti, 'auto'=>'1']);
            }

            $id_auto_data = isset($check_auto_data->id) ? $check_auto_data->id:'';

            // jika auto data masih kosong maka push array ini //
            if(empty($check_auto_data)) {
                // push data //
                $data_join[0] = array(
                                'tanggal'   => $tanggal,
                                'keterangan'=> $keterangan,
                                'userid'    => $username,
                                'usertgl'   => $this->Main_model->time_server(),
                                'auto'      => '1'
                            );

                if($oto_debit == '1') {
                    $data_join[0]['debit']  = $kredit; // -> nilai kebalikan dari debit
                    $data_join[0]['kredit'] = 0; 
                }

                if($oto_kredit == '1') {
                    $data_join[0]['kredit'] = $debit; // -> nilai kebalikan dari kredit
                    $data_join[0]['debit']  = 0; 
                }
            }

            // push data //
            $data_join[1] = $data;
        }

        $respon = $this->Model_jurnal->simpan_detail([
            'jobs_id' => $jobs_id,
            'nobukti' => $nobukti,
            'data' => $data,
            'get_rule' => $get_rule,
            'data_join' => $data_join,
            'id_auto_data' => $id_auto_data
        ]);
        echo json_encode($respon);
    }

    public function ajax_hapus_detail()
    {
        $this->Model_main->ajax_function();

        $id = $this->input->get('id',TRUE);
        $nobukti = $this->input->get('nobukti',TRUE);
        $kodejurnal = $this->input->get('kodejurnal',TRUE);
        
        $respon = $this->Model_jurnal->hapus_detail([
            'kodejurnal'=>$kodejurnal,
            'nobukti'=>$nobukti,
            'id'=>$id
        ]);
        echo json_encode($respon);
    }

    public function ajax_simpan_jurnal()
    {
        $this->Model_main->ajax_function();

        $jobs_id    = $this->input->post('jobs_id');

        $tanggal    = $this->input->post('tanggal');
        $nobukti    = $this->input->post('nobukti');
        $kodejurnal = $this->input->post('kodejurnal');
        $keterangan = $this->input->post('keterangan');

        if($tanggal == '' || $kodejurnal == '' || $keterangan == '') {
            $message = '';
            if($tanggal == '') $message .='Form tanggal harus di isi ! <br>';
            if($kodejurnal == '') $message .='Form kode jurnal harus di isi ! <br>';
            if($keterangan == '') $message .='Form keterangan harus di isi ! <br>';
            $respon = array('status'=>FALSE,'message'=>$message);
            echo json_encode($respon);exit;
        }

        // check detail
        if($nobukti == '') {
            $check = $this->Model_main->view_by_id('acc_jurnalumum_temp', ['jobs_id'=>$jobs_id]);
            if(empty($check)) {
                $respon = array('status'=>FALSE,'message'=>'Detail jurnal harap di isi !');
                echo json_encode($respon);exit;
            }
        } else {
            $check = $this->Model_main->view_by_id('acc_jurnalumum', ['nobukti'=>$nobukti]);
            if(empty($check)) {
                $respon = array('status'=>FALSE,'message'=>'Detail jurnal harap di isi !');
                echo json_encode($respon);exit;
            }
        }

            $respon = $this->Model_jurnal->simpan_jurnal_umum([
                'jobs_id' => $jobs_id,
                'kodejurnal' => $kodejurnal,
                'nobukti' => $nobukti,
                'keterangan' => $keterangan
            ]);
        echo json_encode($respon);
    }

}

/* End of file Jurnal_umum_form.php */

?>