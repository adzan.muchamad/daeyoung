<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coa_lain_lain extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('coa_lain_lain') == FALSE) {
            gak_boleh('coa_lain_lain');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_accounting');
        $this->load->model('Model_main');

        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('coa_lain_lain_alltitle');
        $this->browser_title    = $this->lang->line('coa_lain_lain_alltitle');
        $this->modul_name       = $this->lang->line('coa_lain_lain_alltitle');

        $this->css_include      = '<link href="'.base_url().'assets/js/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css" />';
        $this->js_include       = '<script src="'.base_url().'assets/js/jstree/dist/jstree.min.js" type="text/javascript"></script>';
        $this->js_inject        = '';
    }
    
    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('coa_lain_lain_alltitle') => '#');
        $data = array();
        $data['tree_coa']   = json_encode($this->Model_accounting->json_coa_lain_lain());

        $this->js_inject  .= $this->load->view('accounting/coa/coa_lain_lain_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('coa_lain_lain_alltitle');

        $konten = $this->load->view('accounting/coa/coa_lain_lain', $data, TRUE);
        $this->admin_view($konten);
    }

}

/* End of file Coa_lain_lain.php */

?>