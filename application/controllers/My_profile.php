<?php
defined('BASEPATH') or exit('No direct script access allowed');

class My_profile extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		check_login();
	}

	function update()
	{
		if ($this->form_validation->run('update_profile')) :
			$arr_msg = array();
			$data = array(
				'keterangan'	=> htmlspecialchars($this->input->post('name')),
				'email'  		=> htmlspecialchars($this->input->post('email'))
			);
			$query = $this->db->query("SELECT * FROM ms_user WHERE username='" . $this->input->post('username') . "' AND password ='" . md5($this->input->post('password')) . "'")->num_rows();
			if ($query > 0) {
				$this->db->where('id', my_id());
				$this->db->update('ms_user', $data);
				$arr_msg['status'] = 'sukses';
				$arr_msg['msg'] = 'Your profile has been updated';
				$arr_msg['new_name'] = htmlspecialchars($this->input->post('name'));
				$response = json_encode($arr_msg);
				echo $response;
			} else {
				$arr_msg['status'] = 'gagal';
				$arr_msg['msg'] = 'Wrong password, Profile update failed';
				$response = json_encode($arr_msg);
				echo $response;
			} else :
			$arr_msg = array();
			$arr['my_name'] = my_name();
			$arr['my_email'] = my_email();
			$arr['my_username'] = my_username();
			echo json_encode($arr);
		endif;
	}

	function changepass()
	{
		if ($this->input->post('new_password') == $this->input->post('confirm_pass')) {
			$data = array(
				'password' => md5($this->input->post('new_password'))
			);
			$query = $this->db->query("SELECT * FROM ms_user WHERE id='" . $this->session->userdata('userid') . "' AND password ='" . md5($this->input->post('old_password')) . "'")->num_rows();
			if ($query > 0) {
				$this->db->where('id', my_id());
				$this->db->update('ms_user', $data);
				$arr_msg['status'] = 'sukses';
				$arr_msg['msg'] = 'Your password has been updated';
				$response = json_encode($arr_msg);
				echo $response;
			} else {
				$arr_msg['status'] = 'gagal';
				$arr_msg['msg'] = 'Failed, Wrong your old password';
				$response = json_encode($arr_msg);
				echo $response;
			}
		} else {
			$arr_msg['status'] = 'gagal';
			$arr_msg['msg'] = 'Failed, New Password Not Same With Confirm Password';
			$response = json_encode($arr_msg);
			echo $response;
		}
	}
}
