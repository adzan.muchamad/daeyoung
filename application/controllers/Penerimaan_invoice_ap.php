<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Penerimaan_invoice_ap extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('penerimaan_invoice_ap') == FALSE) {
            gak_boleh('penerimaan_invoice_ap');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_finance');
        $this->load->model('Model_main');
        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('penerimaan_invoice_ap');
        $this->browser_title    = $this->lang->line('browser_title');
        $this->modul_name       = $this->lang->line('penerimaan_invoice_ap');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }

    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('penerimaan_invoice_ap') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/finance/data_penerimaan_invoice_ap_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('penerimaan_invoice_ap');

        $konten = $this->load->view('accounting/finance/data_penerimaan_invoice_ap', $data, TRUE);
        $this->admin_view($konten);
    }

}

/* End of file Penerimaan_invoice.php */

?>