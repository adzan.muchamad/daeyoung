<?php
defined('BASEPATH') or exit('No direct script access allowed');

class lap_mutasi_barang_pemasukan extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('lap_mutasi_barang_pemasukan') == FALSE) {
            gak_boleh('lap_mutasi_barang_pemasukan');
        }
        $this->load->helper('my_func_helper');
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_lap_mutasi_barang_pemasukan', 'lap_mutasi_barang_pemasukan');
        $this->lang->load('lap_mutasi_barang_pemasukan');
        $this->load->library('ImportFile2', 'importfile2');
        $this->active_root_menu = $this->lang->line('lap_mutasi_barang_pemasukan_alltitle');
        $this->browser_title = $this->lang->line('lap_mutasi_barang_pemasukan_alltitle');
        $this->modul_name = $this->lang->line('lap_mutasi_barang_pemasukan_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
        ini_set('memory_limit', '-1');
        set_time_limit(0);
    }

    public function index()
    {
        // $options = array();
        // $options['modul_code'] = 'bts';
        // $this->frame->main_crud($options);
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('lap_mutasi_barang_pemasukan_alltitle') => '#');
        $data = array();

        //$this->js_inject .= $this->load->view('finance_accounting_report_als/js_table', $data, TRUE);
        $this->js_inject .= $this->load->view('lap_mutasi_barang_pemasukan/js', $data, true);
        //$this->js_inject .= $this->load->view('finance_accounting_report_als/valid', $data, TRUE);
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('jszip');
        $this->js_include .= $this->ui->js_include('pdfmake');
        $this->js_include .= $this->ui->js_include('vfs_fonts');
        $this->js_include .= $this->ui->js_include('buttons');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');
        //$this->css_include .= $this->ui->load_css('jquery_ui');
        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('lap_mutasi_barang_pemasukan_alltitle');
        $data['insert_view'] = $this->load->view('lap_mutasi_barang_pemasukan/form', $data, true);
        $konten = $this->load->view('lap_mutasi_barang_pemasukan/index', $data, true);
        $this->admin_view($konten);
    }

    public function get_data_table()
    {
        $this->lap_mutasi_barang_pemasukan->get_data_table();
    }
}
