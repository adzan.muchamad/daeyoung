<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asset extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->lang->load('asset/jenis_asset');
        $this->active_root_menu = $this->lang->line('jenis_asset_alltitle');
        $this->browser_title = $this->lang->line('jenis_asset_alltitle');
        $this->modul_name = $this->lang->line('jenis_asset_alltitle');
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $this->load->model('model_asset', 'asset');
        $this->load->model('model_main');
        $this->load->model('kamus_model');
        $this->load->helper('custom');
	}

	public function jenis()
	{
		check_login();
        if (have_privileges('jenis_asset') == FALSE) {
            gak_boleh('jenis_asset');
        }

        $data = [];
        $this->js_inject .= $this->load->view('asset/js', $data, TRUE);

        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('jenis_asset_alltitle') => '#');

        $data['title_page_table'] = $this->lang->line('jenis_asset_alltitle');
        $data['insert_view'] = $this->load->view('asset/form', $data, TRUE);

        $konten = $this->load->view('asset/jenis', $data, TRUE);
        $this->admin_view($konten);
	}

	public function dt_jenis()
	{
		$draw = $this->input->get('draw', TRUE);
		$order = $this->input->get('order', TRUE);
		$order_columns = isset($order[0]['column']) ? $order[0]['column'] : '';
		$order_mode = isset($order[0]['dir']) ? $order[0]['dir'] : '';
		$start = $this->input->get('start', TRUE);
		$length = $this->input->get('length', TRUE);
		$search = $this->input->get('search', TRUE);
		$search_value = isset($search['value']) ? $search['value'] : '';

		$keyword = $this->input->get('search_keyword', TRUE);

		$data = $this->asset->view_jenis_asset([
			'draw' => $draw,
			'order' => [
				'column' => $order_columns,
				'mode' => $order_mode
			],
			'start' => $start,
			'length' => $length,
			'keyword' => $keyword
		]);

		echo json_encode($data);
	}

	public function add_jenis()
	{
		$id = $this->input->post('id', TRUE);
		$jenis = $this->input->post('jenis', TRUE);
		$kode = $this->input->post('kode', TRUE);
		$username = $this->session->userdata('username');
		$db = daeyoung();

		$status = 0;
		$message = 'Terjadi kesalahan saat menyimpan data';

		$validasi_kode = true;
		$exists = $this->model_main->view_by_id($db.'.master_jenis_asset', ['kode' => $kode, 'status' => 1], 'row');

		if ($id) {

			$exists = $this->model_main->view_by_id($db.'.master_jenis_asset', ['kode' => $kode, 'id <>' => $id, 'status' => 1], 'row');

			if ($exists) {
				$validasi_kode = false;
			}

		} else {
			if ($exists) {
				$validasi_kode = false;
			}
		}

		if ($jenis <> '' && $validasi_kode == true) {
			$user = ($id != '') ? 'user_update' : 'user_insert';
			$time = ($id != '') ? 'update_at' : 'insert_at';
			$condition = ($id != '') ? ['id' => $id] : [];

			$data = [
				'kode' => $kode,
				'jenis' => $jenis,
				$user => $username,
				$time => date('Y-m-d H:i:s')
			];

			$simpan = $this->model_main->process_data($db.'.master_jenis_asset', $data, $condition);

			if ($simpan > 0) {
				$status = 1;
				$message = 'Data berhasil disimpan';
			} else {
				$message = 'Gagal menyimpan data';
			}

		} else {
			if ($validasi_kode == false) {
				$message = 'Kode sudah terdaftar';
			} else {
				$message = 'Masukkan Jenis';
			}
		}

		$result = [
			'status' => $status,
			'message' => $message
		];

		echo json_encode($result);
	}

	public function delete_jenis_asset()
	{
		$id = $this->input->get('id', TRUE);
		$db = daeyoung();
		$now = date('Y-m-d H:i:s');
		$username = $this->session->userdata('username');

		$hapus = $this->model_main->process_data($db.'.master_jenis_asset', ['status' => 0, 'user_update' => $username, 'update_at' => $now], ['id' => $id]);

		$status = 0;
		$message = 'Gagal menghapus data';

		if ($hapus > 0) {
			$status = 1;
			$message = 'Data berhasil dihapus';
		}

		$result = [
			'status' => $status,
			'message' => $message
		];

		echo json_encode($result);
	}

	public function jenis_id()
	{
		$id = $this->input->get('id', TRUE);
		$db = daeyoung();

		$data = $this->model_main->view_by_id($db.'.master_jenis_asset', ['id' => $id], 'row');

		echo json_encode($data);
	}

	public function control_asset()
	{
		check_login();
        if (have_privileges('control_asset') == FALSE) {
            gak_boleh('control_asset');
        }

        $this->lang->load('asset/control_asset');
        $this->active_root_menu = $this->lang->line('control_asset_alltitle');
        $this->browser_title = $this->lang->line('control_asset_alltitle');
        $this->modul_name = $this->lang->line('control_asset_alltitle');

        $data = [];
        $this->js_inject .= $this->load->view('asset/control/js', $data, TRUE);

        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('control_asset_alltitle') => '#');

        $opt_merk = [];
		$merk = $this->model_main->view_by_id('master_merk_asset', ['status' => 1], 'result');
		if ($merk) {
			foreach ($merk as $row) {
				$opt_merk[$row->id] = $row->merk;
			}
		}

		$opt_jenis = [];
		$jenis = $this->model_main->view_by_id('master_jenis_asset', ['status' => 1], 'result');
		if ($jenis) {
			foreach ($jenis as $row) {
				$opt_jenis[$row->id] = $row->jenis;
			}
		}

		$data['merk'] = $opt_merk;
		$data['jenis'] = $opt_jenis;

        $data['title_page_table'] = $this->lang->line('control_asset_alltitle');
        $data['insert_view'] = $this->load->view('asset/control/form', $data, TRUE);

        $konten = $this->load->view('asset/control/asset', $data, TRUE);
        $this->admin_view($konten);
	}

	public function dt_control_asset()
	{
		$draw = $this->input->get('draw', TRUE);
		$order = $this->input->get('order', TRUE);
		$order_columns = isset($order[0]['column']) ? $order[0]['column'] : '';
		$order_mode = isset($order[0]['dir']) ? $order[0]['dir'] : '';
		$start = $this->input->get('start', TRUE);
		$length = $this->input->get('length', TRUE);
		$search = $this->input->get('search', TRUE);
		$search_value = isset($search['value']) ? $search['value'] : '';

		$keyword = $this->input->get('search_keyword', TRUE);

		$data = $this->asset->view_control_asset([
			'draw' => $draw,
			'order' => [
				'column' => $order_columns,
				'mode' => $order_mode
			],
			'start' => $start,
			'length' => $length,
			'keyword' => $keyword
		]);

		echo json_encode($data);
	}

	public function add_control()
	{
		$id = $this->input->post('id', TRUE);
		$kode = $this->input->post('kode', TRUE);
		$nama = $this->input->post('nama', TRUE);
		$merk = $this->input->post('merk', TRUE);
		$jenis = $this->input->post('jenis', TRUE);
		$keterangan = $this->input->post('keterangan-tambah', TRUE);
		$unit = $this->input->post('unit', TRUE);
		$harga = $this->input->post('harga', TRUE);
		$tglbeli = $this->input->post('tglbeli', TRUE);
		$time_server = date('Y-m-d H:i:s');
		$username = $this->session->userdata('username');
		$db = daeyoung();

		$this->form_validation->set_rules('nama', 'Nama', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('unit', 'Unit', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('harga', 'Harga', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('tglbeli', 'Tanggal Beli', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('merk', 'Merk', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('jenis', 'Jenis', 'required', ['required' => 'Masukkan %s']);

		$status = 0;
		$message = 'Gagal menyimpan data';

		if ($this->form_validation->run()) {
			$condition = ($id != '') ? ['id' => $id] : [];
			$time = ($id != '') ? 'update_at' : 'insert_at';
			$user = ($id != '') ? 'user_update' : 'user_insert';

			# if update jenis
			$is_update_jenis = false;
			if ($id != '') {
				$old = $this->model_main->view_by_id($db.'.tb_asset', ['id' => $id], 'row');
				$old_jenis = isset($old->jenis) ? $old->jenis : '';

				if ($old_jenis != $jenis) {
					$is_update_jenis = true;
				}
			}

			if ($kode == '' || $is_update_jenis) {
				$kode = $this->asset->create_counter($jenis);
			}

			$data = [
				'kode_asset' => $kode,
				'nama' => $nama,
				'merk' => $merk,
				'jenis' => $jenis,
				'keterangan' => $keterangan,
				'unit' => $unit,
				'harga_beli' => $harga,
				'tgl_beli' => $tglbeli,
				$time => $time_server,
				$user => $username
			];

			$simpan = $this->asset->add_asset($data, $id);

			$message = 'Gagal menyimpan data';

			if ($simpan) {
				$status = 1;
				$message = 'Data berhasil disimpan';
			}
		} else {
			$message = validation_errors();
		}

		$result = [
			'status' => $status,
			'message' => $message
		];

		echo json_encode($result);
	}

	public function pengurangan()
	{
		$id = $this->input->post('id-asset', TRUE);
		$kode = $this->input->post('kode-pengurangan', TRUE);
		$nama = $this->input->post('nama-pengurangan', TRUE);
		$pengurangan = $this->input->post('pengurangan', TRUE);
		$jenis = $this->input->post('jenis-pengurangan', TRUE);
		$keterangan = $this->input->post('keterangan', TRUE);
		$tgl = $this->input->post('tgl-pengurangan', TRUE);
		$time_server = date('Y-m-d H:i:s');
		$username = $this->session->userdata('username');

		$this->form_validation->set_rules('kode-pengurangan', 'Kode', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('pengurangan', 'Pengurangan', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('jenis-pengurangan', 'Jenis Pengurangan', 'required', ['required' => 'Masukkan %s']);

		$status = 0;
		$message = 'Gagal menyimpan data';

		if ($this->form_validation->run()) {
			$data = [
				'id_asset' => $id,
				'kode' => $kode,
				'nama' => $nama,
				'tgl' => $tgl,
				'jumlah' => '-'.$pengurangan,
				'status' => $jenis,
				'keterangan' => $keterangan,
				'insert_at' => $time_server,
				'user_insert' => $username
			];

			$simpan = $this->asset->pengurangan($data);

			$message = 'Gagal menyimpan data';

			if ($simpan) {
				$status = 1;
				$message = 'Data berhasil disimpan';
			}
		} else {
			$message = validation_errors();
		}

		$result = [
			'status' => $status,
			'message' => $message
		];

		echo json_encode($result);
	}

	public function dt_history()
	{
		$id = $this->input->get('id', TRUE);
		$dt_table = [];
		$response = [];

		$data = $this->asset->dt_history($id);

		if ($data) {
			$i = 1;
			foreach ($data as $row) {
				$status = '<a href="javascript:;" class="btn btn-danger btn-xs">'.$row->status.'</a>';
				if ($row->status == 'Baru') {
					$status = '<a href="javascript:;" class="btn btn-success btn-xs">'.$row->status.'</a>';
				} 

				$dt_table[] = [
					$i++,
					$row->kode,
					$row->nama,
					custom_date_format($row->tgl, 'Y-m-d', 'd/m/Y'),
					$row->jumlah,
					$status,
					$row->keterangan
				];
			}
		}

		$response['data'] = $dt_table;

		echo json_encode($response);
	}

	public function delete_asset()
	{
		$id = $this->input->get('id', TRUE);
		$db = daeyoung();

		$hapus = $this->model_main->delete_data($db.'.tb_asset', ['id' => $id]);

		$hapus_control = $this->model_main->delete_data($db.'.tb_control_asset', ['id_asset' => $id]);

		$status = 0;
		$message = 'Gagal menghapus data';

		if ($hapus > 0) {
			$status = 1;
			$message = 'Data berhasil dihapus';
		}

		$result = [
			'status' => $status,
			'message' => $message
		];

		echo json_encode($result);
	}

	public function control_id()
	{
		$id = $this->input->get('id', TRUE);
		$db = daeyoung();
		$result = [];

		$data = $this->model_main->view_by_id($db.'.tb_asset', ['id' => $id], 'row');
		if ($data) {
			$result = [
				'id' => $data->id,
				'kode' => $data->kode_asset,
				'nama' => $data->nama,
				'merk' => $data->merk,
				'jenis' => $data->jenis,
				'keterangan' => $data->keterangan,
				'tglbeli' => $data->tgl_beli,
				'unit' => $data->unit,
				'harga' => $data->harga_beli
			];	
		}

		echo json_encode($result);
	}

	public function penyusutan()
	{
		check_login();
        if (have_privileges('penyusutan') == FALSE) {
            gak_boleh('penyusutan');
        }

        $this->lang->load('asset/penyusutan_asset');
        $this->active_root_menu = $this->lang->line('penyusutan_asset_alltitle');
        $this->browser_title = $this->lang->line('penyusutan_asset_alltitle');
        $this->modul_name = $this->lang->line('penyusutan_asset_alltitle');

        $data = [];
        $this->js_inject .= $this->load->view('asset/penyusutan/js', $data, TRUE);

        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('penyusutan_asset_alltitle') => '#');

        $data['title_page_table'] = $this->lang->line('penyusutan_asset_alltitle');
        $data['insert_view'] = $this->load->view('asset/penyusutan/form', $data, TRUE);

        $konten = $this->load->view('asset/penyusutan/penyusutan', $data, TRUE);
        $this->admin_view($konten);
	}

	public function dt_penyusutan()
	{
		$draw = $this->input->get('draw', TRUE);
		$order = $this->input->get('order', TRUE);
		$order_columns = isset($order[0]['column']) ? $order[0]['column'] : '';
		$order_mode = isset($order[0]['dir']) ? $order[0]['dir'] : '';
		$start = $this->input->get('start', TRUE);
		$length = $this->input->get('length', TRUE);
		$search = $this->input->get('search', TRUE);
		$search_value = isset($search['value']) ? $search['value'] : '';

		$keyword = $this->input->get('search_keyword', TRUE);

		$data = $this->asset->view_penyusutan([
			'draw' => $draw,
			'order' => [
				'column' => $order_columns,
				'mode' => $order_mode
			],
			'start' => $start,
			'length' => $length,
			'keyword' => $keyword
		]);

		echo json_encode($data);
	}

	public function add_interval()
	{
		$datestart = $this->input->post('datestart', TRUE); 
		$durasi = $this->input->post('durasi', TRUE);

		// $datestart = custom_date_format($datestart, 'd/m/Y', 'Y-m-d');

		$date = interval($datestart, "$durasi month");
		echo $date;
		// echo custom_date_format($date, 'Y-m-d', 'd/m/Y');
	}

	public function add_penyusutan()
	{
		$id = $this->input->post('id', TRUE);
		$kode = $this->input->post('kode', TRUE);
		$nama = $this->input->post('nama', TRUE);
		$tarif = $this->input->post('tarif', TRUE);
		$mulai = $this->input->post('mulai', TRUE);
		$durasi = $this->input->post('durasi', TRUE);
		$selesai = $this->input->post('selesai', TRUE);
		$time_server = date('Y-m-d H:i:s');
		$username = $this->session->userdata('username');

		$this->form_validation->set_rules('kode', 'Kode', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('nama', 'Nama', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('tarif', 'Tarif', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('mulai', 'Mulai Penyusutan', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('durasi', 'Durasi', 'required', ['required' => 'Masukkan %s']);
		$this->form_validation->set_rules('selesai', 'Selesai Penyusutan', 'required', ['required' => 'Masukkan %s']);

		$status = 0;
		$message = 'Gagal menyimpan data';

		if ($this->form_validation->run()) {
			$condition = ($id != '') ? ['kode' => $kode] : [];
			$time = ($id != '') ? 'update_at' : 'insert_at';
			$user = ($id != '') ? 'user_update' : 'user_insert';

			$data = [
				'mulai_penyusutan' => $mulai,
				'selesai_penyusutan' => $selesai,
				'durasi' => $durasi,
				'tarif' => $tarif,
				$time => $time_server,
				$user => $username
			];

			$simpan = $this->asset->add_penyusutan($data, $id);

			$message = 'Gagal menyimpan data';

			if ($simpan) {
				$status = 1;
				$message = 'Data berhasil disimpan';
			}
		} else {
			$message = validation_errors();
		}

		$result = [
			'status' => $status,
			'message' => $message
		];

		echo json_encode($result);
	}

	public function report_penyusutan()
	{
		check_login();
        if (have_privileges('report_penyusutan') == FALSE) {
            gak_boleh('report_penyusutan');
        }

        $this->lang->load('asset/report_asset');
        $this->active_root_menu = $this->lang->line('report_asset_alltitle');
        $this->browser_title = $this->lang->line('report_asset_alltitle');
        $this->modul_name = $this->lang->line('report_asset_alltitle');

        $ms_jenis = $this->model_main->view_by_id('master_jenis_asset', ['status' => 1], 'result');
		$jenis = [];
		if ($ms_jenis) {
			foreach ($ms_jenis as $row) {
				$jenis[$row->id] = $row->jenis;
			}
		}

        $data = [];
        $this->js_inject .= $this->load->view('asset/report/js', $data, TRUE);

        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('report_asset_alltitle') => '#');

        $data['title_page_table'] = $this->lang->line('report_asset_alltitle');
        $data['insert_view'] = $this->load->view('asset/report/form', $data, TRUE);
        $data['jenis'] = $jenis;

        $konten = $this->load->view('asset/report/report', $data, TRUE);
        $this->admin_view($konten);
	}

	public function download()
	{
		$tgl = $this->input->get('tgl', TRUE);
		// $tgl = ($tgl == '') ? date('Y-m-d') : custom_date_format($tgl, 'd/m/Y', 'Y-m-d');
		$jenis = $this->input->get('jenis', TRUE);
		$jenis = ($jenis <> '') ? explode(',', $jenis) : [];

		require_once(APPPATH.'libraries/PHPExcel.php');
		require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

		$tahun = custom_date_format($tgl, 'Y-m-d', 'Y');
		$tahun_lalu = $tahun - 1;
		$tgl_akhir_tahun_lalu = $tahun_lalu.'-12-31';
		$tgl_awal_tahun_ini = $tahun.'-01-01';

		$tgl_indo = $this->kamus_model->tanggal_indo($tgl, 1);

		# jenis selected
		$data_jenis = $this->asset->ms_jenis($jenis);

        $object = new PHPExcel();
        $object->setActiveSheetIndex(0);

        $merge_center = [
        	'alignment' => [
        		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        		'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
        	]
        ];

        # header
        $object->getActiveSheet()->setCellValueByColumnAndRow(0, 1, 'JENIS ASET');
        $object->getActiveSheet()->setCellValueByColumnAndRow(5, 1, 'PEROLEHAN');
        $object->getActiveSheet()->setCellValueByColumnAndRow(13, 1, 'PENYUSUTAN');
        $object->getActiveSheet()->setCellValueByColumnAndRow(19, 1, 'NILAI BUKU');

        $object->getActiveSheet()->mergeCells('A1:E1');
        $object->getActiveSheet()->mergeCells('F1:M1');
        $object->getActiveSheet()->mergeCells('N1:S1');

        $object->getActiveSheet()->setCellValueByColumnAndRow(0, 2, 'No');
        $object->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'Kode');
        $object->getActiveSheet()->setCellValueByColumnAndRow(2, 2, 'Nama');
        $object->getActiveSheet()->setCellValueByColumnAndRow(3, 2, 'Merk');
        $object->getActiveSheet()->setCellValueByColumnAndRow(4, 2, 'Keterangan');
        $object->getActiveSheet()->setCellValueByColumnAndRow(5, 2, 'Unit');
        $object->getActiveSheet()->setCellValueByColumnAndRow(6, 2, 'Tarif');
        $object->getActiveSheet()->setCellValueByColumnAndRow(7, 2, 'Bulan/Tahun');
        $object->getActiveSheet()->setCellValueByColumnAndRow(8, 2, 'Per Unit');
        $object->getActiveSheet()->setCellValueByColumnAndRow(9, 2, 'Per 31 Des '.$tahun_lalu);
        $object->getActiveSheet()->setCellValueByColumnAndRow(10, 2, 'Mutasi Tahun '.$tahun);
        $object->getActiveSheet()->setCellValueByColumnAndRow(12, 2, 'Per '.$tgl_indo);
        $object->getActiveSheet()->setCellValueByColumnAndRow(13, 2, 's/d 31 Des '.$tahun_lalu);
        $object->getActiveSheet()->setCellValueByColumnAndRow(15, 2, 'Tahun '.$tahun);
        $object->getActiveSheet()->setCellValueByColumnAndRow(17, 2, 'Per '.$tgl_indo);
        $object->getActiveSheet()->setCellValueByColumnAndRow(19, 2, 'Per '.$tgl_indo);

        $object->getActiveSheet()->getStyle('A2:T2')->applyFromArray($merge_center);

        $object->getActiveSheet()->mergeCells('A2:A4');
        $object->getActiveSheet()->mergeCells('B2:B4');
        $object->getActiveSheet()->mergeCells('C2:C4');
        $object->getActiveSheet()->mergeCells('D2:D4');
        $object->getActiveSheet()->mergeCells('E2:E4');
        $object->getActiveSheet()->mergeCells('F2:F4');
        $object->getActiveSheet()->mergeCells('G2:G4');
        $object->getActiveSheet()->mergeCells('H2:H4');

        $object->getActiveSheet()->mergeCells('I2:I3');
        $object->getActiveSheet()->mergeCells('J2:J3');
        $object->getActiveSheet()->mergeCells('K2:L2');
        $object->getActiveSheet()->mergeCells('M2:M3');
        $object->getActiveSheet()->mergeCells('N2:O3');
        $object->getActiveSheet()->mergeCells('P2:Q3');
        $object->getActiveSheet()->mergeCells('R2:S3');
        $object->getActiveSheet()->mergeCells('T2:T3');

        $object->getActiveSheet()->setCellValueByColumnAndRow(10, 3, 'Penambahan');
        $object->getActiveSheet()->setCellValueByColumnAndRow(11, 3, 'Pengurangan');

        $object->getActiveSheet()->setCellValueByColumnAndRow(8, 4, 'Rp');
        $object->getActiveSheet()->setCellValueByColumnAndRow(9, 4, 'Rp');
        $object->getActiveSheet()->setCellValueByColumnAndRow(10, 4, 'Rp');
        $object->getActiveSheet()->setCellValueByColumnAndRow(11, 4, 'Rp');
        $object->getActiveSheet()->setCellValueByColumnAndRow(12, 4, 'Rp');
        $object->getActiveSheet()->setCellValueByColumnAndRow(13, 4, 'Bulan');
        $object->getActiveSheet()->setCellValueByColumnAndRow(14, 4, 'Rp');
        $object->getActiveSheet()->setCellValueByColumnAndRow(15, 4, 'Bulan');
        $object->getActiveSheet()->setCellValueByColumnAndRow(16, 4, 'Rp');
        $object->getActiveSheet()->setCellValueByColumnAndRow(17, 4, 'Bulan');
        $object->getActiveSheet()->setCellValueByColumnAndRow(18, 4, 'Rp');
        $object->getActiveSheet()->setCellValueByColumnAndRow(19, 4, 'Rp');

        $start_row = 5;

        if ($data_jenis) {
        	foreach ($data_jenis as $row) {
        		$object->getActiveSheet()->setCellValueByColumnAndRow(1, $start_row, $row->jenis);
        		$start_row++;

        		$i = 1;
        		$data_asset = $this->asset->asset_jenis($row->id, $tgl);

        		$total_unit = 0;
	        	$total_akhir_tahun_lalu = 0;
	        	$total_penambahan = 0;
	        	$total_pengurangan = 0;
	        	$total_bulan_ini = 0;
	        	$total_penyusutan_tahun_lalu = 0;
	        	$total_penyusutan_tahun_ini = 0;
	        	$total_penyusutan_sekarang = 0;
	        	$total_nilai_buku = 0;
        		if ($data_asset) {
        			foreach ($data_asset as $val) {
        				$object->getActiveSheet()->setCellValueByColumnAndRow(0, $start_row, $i++);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(1, $start_row, $val->kode_asset);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(2, $start_row, $val->nama);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(3, $start_row, $val->merk);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(4, $start_row, $val->keterangan);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(5, $start_row, $val->unit);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(6, $start_row, $val->tarif.'%');
        				$object->getActiveSheet()->setCellValueByColumnAndRow(7, $start_row, custom_date_format($val->tgl_beli, 'Y-m-d', 'M-y'));
        				$object->getActiveSheet()->setCellValueByColumnAndRow(8, $start_row, $val->harga_beli);
        				$object->getActiveSheet()->getStyle('I'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

        				$total_beli = $val->harga_beli * $val->unit;
        				$object->getActiveSheet()->setCellValueByColumnAndRow(9, $start_row, $total_beli);
        				$object->getActiveSheet()->getStyle('J'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

        				$penambahan = 0;
        				if (custom_date_format($val->tgl_beli, 'Y-m-d', 'Y') == $tahun) {
        					$penambahan = $val->harga_beli * $val->unit;
        				}

        				$pengurangan = 0;
        				$dt_pengurangan = $this->asset->dt_pengurangan($val->id, $tahun);
        				if ($dt_pengurangan) {
        					$pengurangan = $dt_pengurangan->jumlah;
        				}

        				$object->getActiveSheet()->setCellValueByColumnAndRow(10, $start_row, $penambahan);
        				$object->getActiveSheet()->getStyle('K'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        				$object->getActiveSheet()->setCellValueByColumnAndRow(11, $start_row, $pengurangan);
        				$object->getActiveSheet()->getStyle('L'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        				$object->getActiveSheet()->setCellValueByColumnAndRow(12, $start_row, $total_beli);
        				$object->getActiveSheet()->getStyle('M'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

        				$penyusutan_perbulan = 0;
        				if ($val->durasi > 0) {
        					$penyusutan_perbulan = $val->harga_beli / $val->durasi;
        				}

        				$masa_smp_akhir_tahun_lalu = ($val->tgl_beli <= $tgl_akhir_tahun_lalu) ? datediff_month($val->tgl_beli, $tgl_akhir_tahun_lalu) : 0;
        				$masa_tahun_ini = datediff_month($tgl_awal_tahun_ini, $tgl);

        				if ($masa_smp_akhir_tahun_lalu + $masa_tahun_ini > $val->durasi) {
        					$masa_tahun_ini = $val->durasi - $masa_smp_akhir_tahun_lalu;
        				}

        				if ($masa_smp_akhir_tahun_lalu > $val->durasi) {
        					$masa_smp_akhir_tahun_lalu = 0;
        				}

        				$penyusutan_smp_akhir_tahun_lalu = $masa_smp_akhir_tahun_lalu * $penyusutan_perbulan;
        				$object->getActiveSheet()->setCellValueByColumnAndRow(13, $start_row, $masa_smp_akhir_tahun_lalu);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(14, $start_row, $penyusutan_smp_akhir_tahun_lalu);
        				$object->getActiveSheet()->getStyle('O'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

        				
        				if ($masa_smp_akhir_tahun_lalu == 0) {
        					$masa_tahun_ini = 0;
        				}
        				$penyusutan_tahun_ini = $masa_tahun_ini * $penyusutan_perbulan;

        				$object->getActiveSheet()->setCellValueByColumnAndRow(15, $start_row, $masa_tahun_ini);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(16, $start_row, $penyusutan_tahun_ini);
        				$object->getActiveSheet()->getStyle('Q'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

        				$masa_smp_sekarang = datediff_month($val->tgl_beli, $tgl);
        				if ($masa_smp_sekarang > $val->durasi) {
        					$masa_smp_sekarang = $val->durasi;
        				}

        				$penyusutan_sekarang = $masa_smp_sekarang * $penyusutan_perbulan;
        				$object->getActiveSheet()->setCellValueByColumnAndRow(17, $start_row, $masa_smp_sekarang);
        				$object->getActiveSheet()->setCellValueByColumnAndRow(18, $start_row, $penyusutan_sekarang);
        				$object->getActiveSheet()->getStyle('S'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

        				$nilai_buku = $total_beli - $penyusutan_sekarang;
        				$object->getActiveSheet()->setCellValueByColumnAndRow(19, $start_row, $nilai_buku);
        				$object->getActiveSheet()->getStyle('T'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

        				$total_unit = $total_unit + $val->unit;
        				$total_akhir_tahun_lalu = $total_akhir_tahun_lalu + $total_beli;
        				$total_penambahan = $total_penambahan + $penambahan;
        				$total_pengurangan = $total_pengurangan + $pengurangan;
        				$total_bulan_ini = $total_bulan_ini + $total_beli;
        				$total_penyusutan_tahun_lalu = $total_penyusutan_tahun_lalu + $penyusutan_smp_akhir_tahun_lalu;
        				$total_penyusutan_tahun_ini = $total_penyusutan_tahun_ini + $penyusutan_tahun_ini;
						$total_penyusutan_sekarang = $total_penyusutan_sekarang + $penyusutan_sekarang;
						$total_nilai_buku = $total_nilai_buku + $nilai_buku;

        				$start_row++;
        			}

        			$object->getActiveSheet()->setCellValueByColumnAndRow(5, $start_row, $total_unit);
        			$object->getActiveSheet()->setCellValueByColumnAndRow(9, $start_row, $total_akhir_tahun_lalu);
        			$object->getActiveSheet()->setCellValueByColumnAndRow(10, $start_row, $total_penambahan);
        			$object->getActiveSheet()->setCellValueByColumnAndRow(11, $start_row, $total_pengurangan);
        			$object->getActiveSheet()->setCellValueByColumnAndRow(12, $start_row, $total_bulan_ini);
        			$object->getActiveSheet()->setCellValueByColumnAndRow(14, $start_row, $total_penyusutan_tahun_lalu);
        			$object->getActiveSheet()->setCellValueByColumnAndRow(16, $start_row, $total_penyusutan_tahun_ini);
        			$object->getActiveSheet()->setCellValueByColumnAndRow(18, $start_row, $total_penyusutan_sekarang);
        			$object->getActiveSheet()->setCellValueByColumnAndRow(19, $start_row, $total_nilai_buku);

        			$object->getActiveSheet()->getStyle('J'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        			$object->getActiveSheet()->getStyle('K'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        			$object->getActiveSheet()->getStyle('L'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        			$object->getActiveSheet()->getStyle('M'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        			$object->getActiveSheet()->getStyle('O'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        			$object->getActiveSheet()->getStyle('Q'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        			$object->getActiveSheet()->getStyle('S'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");
        			$object->getActiveSheet()->getStyle('T'.$start_row)->getNumberFormat()->setFormatCode("_(\"\"* #,##_);_(\"\"* \(#,##\);_(\"\"* \"-\"??_);_(@_)");

        			$start_row++;
        		}
        	}
        }

        $object->getActiveSheet()->getStyle('A1:T'.($start_row - 1))->applyFromArray([
        	'borders' => [
        		'allborders' => [
        			'style' => PHPExcel_Style_Border::BORDER_THIN
        		]
        	]
        ]);
        
        $date = date('YmdHis').rand();
		$object_writer = IOFactory::createWriter($object, 'Excel5');
		$filename = urlencode("laporan-asset".$date.".xls");

		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"'); 
		header('Cache-Control: max-age=0'); 

		$object_writer->save('php://output');
	}
}

/* End of file Asset.php */
/* Location: ./application/controllers/Asset.php */ ?>