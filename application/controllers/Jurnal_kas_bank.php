<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal_kas_bank extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('jurnal_kas_bank') == FALSE) {
            gak_boleh('jurnal_kas_bank');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_accounting');
        $this->load->model('Model_jurnal');
        $this->load->model('Model_main');

        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('jurnal_kas_bank');
        $this->browser_title    = $this->lang->line('jurnal_kas_bank');
        $this->modul_name       = $this->lang->line('jurnal_kas_bank');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }

    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('jurnal_kas_bank') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/jurnal/jurnal_kas_bank_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('jurnal_kas_bank');

        $konten = $this->load->view('accounting/jurnal/jurnal_kas_bank', $data, TRUE);
        $this->admin_view($konten);
    }

    function ajax_data_jurnal_kas_bank()
    {
        $this->Model_main->ajax_function();
        $tgl_mulai  = $this->input->get('tgl_mulai');
        $tgl_sampai = $this->input->get('tgl_sampai');
        $keterangan = $this->input->get('keterangan');

        $data = $this->Model_jurnal->data_jurnal_kas_bank($tgl_mulai, $tgl_sampai, $keterangan);
            $arr = array();
            $no=1;
            if (!empty($data)) {
                $nobukti_def   = '';
                foreach ($data as $row => $val) {                     
                    $btn_aksi  = '';
                    if($nobukti_def != $val->nobukti) {
                        $btn_aksi = '<a title="Detail Jurnal" href="'.base_url().'Jurnal_kas_bank_form?q='.$val->id.'" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></a>&nbsp;<a title="Hapus" class="btn btn-danger btn-xs btn-hapus" data-id="'.$val->id.'"><i class="icon-bin"></i></a>';
                        $nobukti_def = $val->nobukti;
                    }
                    $arr[$row] =array(
                                    'no' => $no++,
                                    'nobukti' => $val->nobukti,
                                    'tgl' => $val->tanggal_format,
                                    'kodeakun' => $val->kodeakun,
                                    'namaakun' => $val->namaakun,
                                    'keterangan' => $val->keterangan,
                                    'debit' => currency($val->debit),
                                    'kredit' => currency($val->kredit),
                                    'aksi' => $btn_aksi
                                );
                }
            }
            $json = array(
                        'data' => $arr
                    );
        echo json_encode($json);
    }

    public function ajax_hapus_jurnal()
    {
        $this->Model_main->ajax_function();

        $id = $this->input->get('id',TRUE);
        
        $check_eksis = $this->Model_main->view_by_id('acc_jurnalumum', ['id'=>$id]);
        if(empty($check_eksis)) {
            $respon = ['status'=>FALSE,'message'=>'data tidak ditemukan'];
            echo json_encode($respon);exit;
        }

            $nobukti  = isset($check_eksis->nobukti) ? $check_eksis->nobukti:'';
            $respon = $this->Model_jurnal->hapus_jurnal_umum([
                'nobukti' => $nobukti
            ]);
        echo json_encode($respon);
    }

}

/* End of file Jurnal_kas_bank.php */

?>