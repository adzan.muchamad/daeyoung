<?php
defined('BASEPATH') or exit('No direct script access allowed');

class customers_master extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('customers_master') == FALSE) {
            gak_boleh('customers_master');
        }
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_customers_master', 'customers_master');
        $this->lang->load('customers_master');
        $this->active_root_menu = $this->lang->line('customers_master_alltitle');
        $this->browser_title = $this->lang->line('customers_master_alltitle');
        $this->modul_name = $this->lang->line('customers_master_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
    }

    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('customers_master_alltitle') => '#');
        $data = array();

        $this->js_inject .= $this->load->view('customers_master/js', $data, TRUE);
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('customers_master_alltitle');
        $data['insert_view'] = $this->load->view('customers_master/form', $data, TRUE);

        $konten = $this->load->view('customers_master/index', $data, TRUE);
        $this->admin_view($konten);
    }

    public function get_data_table()
    {
        $this->customers_master->get_data_table();
    }

    public function insert_data()
    {
        echo $this->customers_master->insert_data();
    }

    public function select_data()
    {
        echo json_encode($this->customers_master->select_data());
    }

    public function view_data($id)
    {
        echo $this->customers_master->view_data($id);
    }

    public function update_data()
    {
        echo $this->customers_master->update_data();
    }

    public function delete_data($id = '')
    {
        echo $this->customers_master->delete_data($id);
    }

    function cek_kode()
    {
        echo $this->customers_master->cek_kode();
    }
}
