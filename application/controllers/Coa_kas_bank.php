<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coa_kas_bank extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('coa_kas_bank') == FALSE) {
            gak_boleh('coa_kas_bank');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_accounting');
        $this->load->model('Model_main');

        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('coa_kas_bank_alltitle');
        $this->browser_title    = $this->lang->line('coa_kas_bank_alltitle');
        $this->modul_name       = $this->lang->line('coa_kas_bank_alltitle');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }
    
    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('coa_kas_bank_alltitle') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/coa/coa_kas_bank_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('coa_kas_bank_alltitle');

        $konten = $this->load->view('accounting/coa/coa_kas_bank', $data, TRUE);
        $this->admin_view($konten);
    }

    function ajax_data_coa_kas_bank()
    {
        $this->Model_main->ajax_function();
        $data = $this->Model_accounting->data_coa_kas_bank();
            $arr = array();
            $no = 1;
            if(!empty($data)) {
                foreach ($data as $row => $val) {
                    $arr[$row] = array(
                                    'no' => $no++,
                                    'parent' => $val->parent,
                                    'flag' => strtoupper($val->flag),
                                    'kodeakun' => $val->kodeakun,
                                    'namaakun' => $val->namaakun,
                                    'aksi' => '<button title="Edit" type="button" class="btn btn-info btn-sm" onclick="edit('.$val->id.')"><i class="icon-pencil"></i></button>&nbsp;<button title="Hapus" type="button" class="btn btn-danger btn-sm" onclick="hapus('.$val->id.')"><i class="icon-bin"></i></button>'
                                );
                }
            }

            $json = array(
                        'data' => $arr
                    );
        echo json_encode($json);
    }

    function ajax_coa_parent()
    {
        $this->Model_main->ajax_function();
        $data = $this->Model_main->view_by_id('acc_master_akun', array('status'=>'1'), 'result');
        echo json_encode($data);
    }

    function ajax_coa_parent_id($kode='')
    {
        $this->Model_main->ajax_function();
        $condition = array(
                        'kodeakun' => $kode,
                        'status' => '1'
                    );
        $data = $this->Model_main->view_by_id('acc_master_akun', $condition);
        echo json_encode($data);
    }

    function ajax_save()
    {
        $this->Model_main->ajax_function();
        $username = $this->session->userdata('username');
        $id = $this->input->post('id');

        $parent = $this->input->post('parent');
        $kode = $this->input->post('kode');
        $nama = $this->input->post('nama');
        $flag = $this->input->post('flag');

        if($parent == '' || $kode == '' || $nama == '' || $flag == '') {
            $message = '';
            if($parent == '') $message.='Parent harus di pilih ! <br>';
            if($kode == '') $message.='Form Kode Akun harus di pilih ! <br>';
            if($nama == '') $message.='Form Nama Akun harus di pilih ! <br>';
            if($flag == '') $message.='Form Kategori harus di pilih ! <br>';
            $respon = array('status'=>FALSE,'message'=>$message);
            echo json_encode($respon);exit;
        }
        $message = '';
        // get grup
        $get = $this->Model_main->view_by_id('acc_master_akun', array('kodeakun'=>$parent));
        $grup= isset($get->grup) ? $get->grup:'';
        $mekanisme= isset($get->mekanisme) ? $get->mekanisme:'';

        $data = array(
                    'kodeakun' => $kode,
                    'namaakun' => $nama,
                    'parent' => $parent,
                    'mekanisme' => $mekanisme,
                    'grup' => $grup,
                    'type_data' => 'detail',
                    'flag' => $flag
                );

        // check kode akun
        if($id == '') {
            $check_kode = $this->Model_main->view_by_id('acc_master_akun', array('kodeakun'=>$kode));
            if(!empty($check_kode)) {
                $akun_eksis = isset($check_kode->namaakun) ? $check_kode->namaakun:'';
                $respon = array('status'=>FALSE,'message'=>'Kode akun sudah di gunakan - '.$akun_eksis);
                echo json_encode($respon);exit;
            }
        }

        if($id == '') {
            // push value
            $data['user_insert'] = $username;
            $data['tanggal_insert'] = $this->Model_main->time_server();
            $this->Model_main->process_data('acc_master_akun', $data);
            $message = 'Simpan data sukses ';
        } else {
            // push value
            $data['user_update'] = $username;

            // check kode akun by id
            $check_kode_id = $this->Model_main->view_by_id('acc_master_akun', array('id'=>$id,'kodeakun'=>$kode));
            if(empty($check_kode_id)) {
                $check_kode = $this->Model_main->view_by_id('acc_master_akun', array('kodeakun'=>$kode));
                if(!empty($check_kode)) {
                    $akun_eksis = isset($check_kode->namaakun) ? $check_kode->namaakun:'';
                    $respon = array('status'=>FALSE,'message'=>'Kode akun sudah di gunakan - '.$akun_eksis);
                    echo json_encode($respon);exit;
                }
            }

            $this->Model_main->process_data('acc_master_akun', $data, array('id'=>$id));
            $message = 'Update data sukses !';
        }

        $respon = array('status'=>TRUE,'message'=>$message);
        echo json_encode($respon);
    }

    function ajax_edit($id='')
    {
        $this->Model_main->ajax_function();
        $data = $this->Model_main->view_by_id('acc_master_akun', array('id'=>$id));
        echo json_encode($data);
    }

    function ajax_hapus($id='')
    {
        $this->Model_main->ajax_function();
        $username = $this->session->userdata('username');
        // check
        $check = $this->Model_main->view_by_id('acc_master_akun', array('id'=>$id,'status'=>'1'));
        if(empty($check)) {
            $respon = array('status'=>FALSE,'message'=>'Data tidak ditemukan !');
            echo json_encode($respon);exit;
        }

        $data = array(
                    'status'=>'0',
                    'user_update' => $username
                );

        $this->Model_main->process_data('acc_master_akun', $data, array('id'=>$id));
        $respon = array('status'=>TRUE,'message'=>'Hapus data sukses ');
        echo json_encode($respon);
    }

}

/* End of file Coa_kas_bank.php */

?>