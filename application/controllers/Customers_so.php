<?php
defined('BASEPATH') or exit('No direct script access allowed');

class customers_so extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_customers_so', 'customers_so');
        $this->lang->load('customers_so');
        $this->active_root_menu = $this->lang->line('customers_so_alltitle');
        $this->browser_title = $this->lang->line('customers_so_alltitle');
        $this->modul_name = $this->lang->line('customers_so_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
    }

    public function index()
    {
        // $options = array();
        // $options['modul_code'] = 'bts';
        // $this->frame->main_crud($options);

        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('customers_so_alltitle') => '#');
        $data = array();

        //$this->js_inject .= $this->load->view('customers_so/js_table', $data, TRUE);
        $this->js_inject .= $this->load->view('customers_so/js', $data, TRUE);
        //$this->js_inject .= $this->load->view('customers_so/valid', $data, TRUE);
        //$this->js_include .= $this->ui->js_include('flexigridMaster');
        //$this->js_include .= $this->ui->load_css('flexigridMaster');
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');
        //$this->css_include .= $this->ui->load_css('jquery_ui');
        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('customers_so_alltitle');
        //$data['update_view'] = $this->load->view('customers_so/update', $data, TRUE);
        $data['insert_view'] = $this->load->view('customers_so/form', $data, TRUE);
        //$data['delete_view'] = $this->load->view('customers_so/delete', $data, TRUE);

        $konten = $this->load->view('customers_so/index', $data, TRUE);
        $this->admin_view($konten);
    }

    public function get_data_table()
    {
        $this->customers_so->get_data_table();
    }

    function get_customers()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->customers_so->get_customers($param);
        } else {
            echo $this->customers_so->get_customers();
        }
    }

    function get_branch()
    {
        $id = $this->input->post('id');
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->customers_so->get_branch($param, $id);
        } else {
            echo $this->customers_so->get_branch(null, $id);
        }
    }

    function get_alamat()
    {
        echo $this->customers_so->get_alamat();
    }

    function get_mata_uang()
    {
        echo $this->customers_so->get_mata_uang();
    }

    function get_kurs()
    {
        echo $this->customers_so->get_kurs();
    }

    function get_satuanbarang()
    {
        echo $this->customers_so->get_satuanbarang();
    }

    function get_barang()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->customers_so->get_barang($param);
        } else {
            echo $this->customers_so->get_barang();
        }
    }

    public function insert_data()
    {
        echo $this->customers_so->insert_data();
    }

    public function select_data()
    {
        echo json_encode($this->customers_so->select_data());
    }

    public function view_data($id)
    {
        echo $this->customers_so->view_data($id);
    }

    public function update_data()
    {
        echo $this->customers_so->update_data();
    }

    public function delete_data($id = '')
    {
        echo $this->customers_so->delete_data($id);
    }

    public function buat_master()
    {
        echo $this->customers_so->buat_master();
    }
}
