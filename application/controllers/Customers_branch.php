<?php
defined('BASEPATH') or exit('No direct script access allowed');

class customers_branch extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('customers_branch') == FALSE) {
            gak_boleh('customers_branch');
        }
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_customers_branch', 'customers_branch');
        $this->lang->load('customers_branch');
        $this->active_root_menu = $this->lang->line('customers_branch_alltitle');
        $this->browser_title = $this->lang->line('customers_branch_alltitle');
        $this->modul_name = $this->lang->line('customers_branch_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
    }

    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('customers_branch_alltitle') => '#');
        $data = array();

        $this->js_inject .= $this->load->view('customers_branch/js', $data, TRUE);
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('customers_branch_alltitle');
        $data['insert_view'] = $this->load->view('customers_branch/form', $data, TRUE);

        $konten = $this->load->view('customers_branch/index', $data, TRUE);
        $this->admin_view($konten);
    }

    public function get_data_table()
    {
        $this->customers_branch->get_data_table();
    }

    public function insert_data()
    {
        echo $this->customers_branch->insert_data();
    }

    public function select_data()
    {
        echo json_encode($this->customers_branch->select_data());
    }

    public function view_data($id)
    {
        echo $this->customers_branch->view_data($id);
    }

    public function update_data()
    {
        echo $this->customers_branch->update_data();
    }

    public function delete_data($id = '')
    {
        echo $this->customers_branch->delete_data($id);
    }

    function cek_kode()
    {
        echo $this->customers_branch->cek_kode();
    }

    public function get_customers()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->customers_branch->get_customers($param);
        } else {
            echo $this->customers_branch->get_customers();
        }
    }
}
