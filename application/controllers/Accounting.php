<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('accounting') == FALSE) {
            gak_boleh('accounting');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('accounting_alltitle');
        $this->browser_title    = $this->lang->line('browser_title');
        $this->modul_name       = $this->lang->line('accounting_alltitle');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }
    
    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('accounting_alltitle') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('accounting_alltitle');

        $konten = $this->load->view('accounting/index', $data, TRUE);
        $this->admin_view($konten);
    }

}
/* End of file Accounting.php */

?>