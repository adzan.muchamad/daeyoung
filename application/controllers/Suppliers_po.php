<?php
defined('BASEPATH') or exit('No direct script access allowed');

class suppliers_po extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('suppliers_po') == FALSE) {
            gak_boleh('suppliers_po');
        }
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_suppliers_po', 'suppliers_po');
        $this->lang->load('suppliers_po');
        $this->active_root_menu = $this->lang->line('suppliers_po_alltitle');
        $this->browser_title = $this->lang->line('suppliers_po_alltitle');
        $this->modul_name = $this->lang->line('suppliers_po_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
        ini_set('memory_limit', '-1');
        set_time_limit(0);
    }

    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('suppliers_po_alltitle') => '#');
        $data = array();

        $this->js_inject .= $this->load->view('suppliers_po/js', $data, TRUE);
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('suppliers_po_alltitle');
        $data['insert_view'] = $this->load->view('suppliers_po/form', $data, TRUE);

        $konten = $this->load->view('suppliers_po/index', $data, TRUE);
        $this->admin_view($konten);
    }

    public function get_data_table()
    {
        $this->suppliers_po->get_data_table();
    }

    function get_suppliers()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->suppliers_po->get_suppliers($param);
        } else {
            echo $this->suppliers_po->get_suppliers();
        }
    }

    function get_alamat()
    {
        echo $this->suppliers_po->get_alamat();
    }

    function get_mata_uang()
    {
        echo $this->suppliers_po->get_mata_uang();
    }

    function get_kurs()
    {
        echo $this->suppliers_po->get_kurs();
    }

    function get_satuanbarang()
    {
        echo $this->suppliers_po->get_satuanbarang();
    }

    function get_barang()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->suppliers_po->get_barang($param);
        } else {
            echo $this->suppliers_po->get_barang();
        }
    }

    public function insert_data()
    {
        echo $this->suppliers_po->insert_data();
    }

    public function select_data()
    {
        echo json_encode($this->suppliers_po->select_data());
    }

    public function view_data($id)
    {
        echo $this->suppliers_po->view_data($id);
    }

    public function update_data()
    {
        echo $this->suppliers_po->update_data();
    }

    public function delete_data($id = '')
    {
        echo $this->suppliers_po->delete_data($id);
    }

    public function buat_master()
    {
        echo $this->suppliers_po->buat_master();
    }
}
