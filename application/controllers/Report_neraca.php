<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_neraca extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('report_neraca') == FALSE) {
            gak_boleh('report_neraca');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_main');
        $this->load->model('Model_jurnal');
        $this->load->model('Model_accounting');
        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('report_neraca');
        $this->browser_title    = $this->lang->line('report_neraca');
        $this->modul_name       = $this->lang->line('report_neraca');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }
    
    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('report_neraca') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/report/data_report_neraca_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('report_neraca');
        $data['opt_bulan'] = $this->Model_main->opt_bulan();
        $data['opt_tahun'] = $this->Model_jurnal->opt_tahun_jurnal();

        $konten = $this->load->view('accounting/report/data_report_neraca', $data, TRUE);
        $this->admin_view($konten);
    }

    public function print_data()
    {
        $bulan = $this->input->post('bulan',TRUE);
        $tahun = $this->input->post('tahun',TRUE);
        $periode = $tahun.$bulan;
        $data['title']  = 'Report Neraca';
        $data['periode']= $this->Model_main->get_bulan($bulan).' '.$tahun;
        $data['params'] = $tahun.'-'.$bulan;

        $get_data = $this->Model_accounting->report_neraca([
            'bulan' => $bulan,
            'tahun' => $tahun
        ]);
        
        $data['table_aktiva'] = $get_data['table_aktiva'];
        $data['total_aktiva'] = $get_data['total_aktiva'];
        $data['table_pasiva'] = $get_data['table_pasiva'];
        $data['total_pasiva'] = $get_data['total_pasiva'];

        $this->load->view('accounting/report/print_report_neraca', $data);
    }

}

/* End of file Report_neraca.php */

?>