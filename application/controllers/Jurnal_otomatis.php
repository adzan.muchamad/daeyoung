<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jurnal_otomatis extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('jurnal_otomatis') == FALSE) {
            gak_boleh('jurnal_otomatis');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_accounting');
        $this->load->model('Model_jurnal');
        $this->load->model('Model_main');

        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('jurnal_otomatis');
        $this->browser_title    = $this->lang->line('jurnal_otomatis');
        $this->modul_name       = $this->lang->line('jurnal_otomatis');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }

    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('jurnal_otomatis') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/jurnal/jurnal_otomatis_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('jurnal_otomatis');

        $konten = $this->load->view('accounting/jurnal/jurnal_otomatis', $data, TRUE);
        $this->admin_view($konten);
    }

    function ajax_data_jurnal_otomatis()
    {
        $this->Model_main->ajax_function();
        $tgl_mulai  = $this->input->get('tgl_mulai');
        $tgl_sampai = $this->input->get('tgl_sampai');
        $keterangan = $this->input->get('keterangan');

        $data = $this->Model_jurnal->data_jurnal_otomatis($tgl_mulai, $tgl_sampai, $keterangan);
            $arr = array();
            $no=1;
            if (!empty($data)) {
                $nobukti_def   = '';
                foreach ($data as $row => $val) {
                    $arr[$row] =array(
                                    'no' => $no++,
                                    'nobukti' => $val->nobukti,
                                    'tgl' => $val->tanggal_format,
                                    'kodeakun' => $val->kodeakun,
                                    'namaakun' => $val->namaakun,
                                    'keterangan' => $val->keterangan,
                                    'debit' => currency($val->debit),
                                    'kredit' => currency($val->kredit)
                                );
                }
            }
            $json = array(
                        'data' => $arr
                    );
        echo json_encode($json);
    }

}

/* End of file Jurnal_otomatis.php */

?>