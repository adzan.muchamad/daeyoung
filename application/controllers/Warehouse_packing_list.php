<?php
defined('BASEPATH') or exit('No direct script access allowed');

class warehouse_packing_list extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('warehouse_packing_list') == FALSE) {
            gak_boleh('warehouse_packing_list');
        }
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_warehouse_dyeing_system', 'warehouse_packing_list');
        $this->load->model('model_inventory_barang', 'inventory_barang');
        $this->lang->load('warehouse_packing_list');
        $this->active_root_menu = $this->lang->line('warehouse_dyeing_system_alltitle');
        $this->browser_title = $this->lang->line('warehouse_dyeing_system_alltitle');
        $this->modul_name = $this->lang->line('warehouse_dyeing_system_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
    }

    public function index()
    {
        // $options = array();
        // $options['modul_code'] = 'bts';
        // $this->frame->main_crud($options);

        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('warehouse_dyeing_system_alltitle') => '#');
        $data = array();

        //$this->js_inject .= $this->load->view('warehouse_packing_list/js_table', $data, TRUE);
        $this->js_inject .= $this->load->view('warehouse_packing_list/js', $data, TRUE);
        //$this->js_inject .= $this->load->view('warehouse_packing_list/valid', $data, TRUE);
        //$this->js_include .= $this->ui->js_include('flexigridMaster');
        //$this->js_include .= $this->ui->load_css('flexigridMaster');
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');
        //$this->css_include .= $this->ui->load_css('jquery_ui');
        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('warehouse_dyeing_system_alltitle');
        //$data['update_view'] = $this->load->view('warehouse_packing_list/update', $data, TRUE);
        $data['insert_view'] = $this->load->view('warehouse_packing_list/form', $data, TRUE);
        //$data['delete_view'] = $this->load->view('warehouse_packing_list/delete', $data, TRUE);

        $konten = $this->load->view('warehouse_packing_list/index', $data, TRUE);
        $this->admin_view($konten);
    }

    public function get_data_table()
    {
        $this->warehouse_packing_list->get_data_table();
    }
}
