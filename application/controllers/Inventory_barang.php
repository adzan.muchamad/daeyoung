<?php
defined('BASEPATH') or exit('No direct script access allowed');

class inventory_barang extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('inventory_barang') == FALSE) {
            gak_boleh('inventory_barang');
        }
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_inventory_barang', 'inventory_barang');
        $this->lang->load('inventory_barang');
        $this->active_root_menu = $this->lang->line('inventory_barang_alltitle');
        $this->browser_title = $this->lang->line('inventory_barang_alltitle');
        $this->modul_name = $this->lang->line('inventory_barang_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
    }

    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('inventory_barang_alltitle') => '#');
        $data = array();

        $this->js_inject .= $this->load->view('inventory_barang/js', $data, TRUE);
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('inventory_barang_alltitle');
        $data['insert_view'] = $this->load->view('inventory_barang/form', $data, TRUE);

        $konten = $this->load->view('inventory_barang/index', $data, TRUE);
        $this->admin_view($konten);
    }

    public function get_data_table()
    {
        $this->inventory_barang->get_data_table();
    }

    public function get_satuanbarang()
    {
        $response = null;
        $query = $this->inventory_barang->get_satuanbarang();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $response .= "<option value='" . $row['id'] . "'>" . $row['kode'] . " - " . $row['nama'] . "</option>";
            }
        }
        echo $response;
    }

    public function get_proses()
    {
        $response = null;
        $query = $this->inventory_barang->get_proses();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $response .= "<option value='" . $row['id'] . "'>" . $row['kode'] . ' - ' . $row['nama'] . "</option>";
            }
        }
        echo $response;
    }

    public function get_bahan()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->inventory_barang->get_bahan($param);
        } else {
            echo $this->inventory_barang->get_bahan();
        }
    }

    public function insert_data()
    {
        echo $this->inventory_barang->insert_data();
    }

    public function select_data()
    {
        echo json_encode($this->inventory_barang->select_data());
    }

    public function update_data()
    {
        echo $this->inventory_barang->update_data();
    }

    public function delete_data($id = '')
    {
        echo $this->inventory_barang->delete_data($id);
    }

    public function detail_komponen()
    {
        echo json_encode($this->inventory_barang->detail_komponen());
    }

    public function detail_proses()
    {
        echo json_encode($this->inventory_barang->detail_proses());
    }

    function cek_kode($id)
    {
        echo $this->inventory_barang->cek_kode($id);
    }

    function cek_kode_edit()
    {
        echo $this->inventory_barang->cek_kode_edit();
    }
}
