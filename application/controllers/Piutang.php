<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Piutang extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('piutang') == FALSE) {
            gak_boleh('piutang');
        }
        
        $this->load->model('model_global', 'm_global');
        $this->load->model('Model_finance');
        $this->load->model('Model_main');
        $this->lang->load('accounting');

        $this->active_root_menu = $this->lang->line('piutang');
        $this->browser_title    = $this->lang->line('browser_title');
        $this->modul_name       = $this->lang->line('piutang');

        $this->css_include      = '';
        $this->js_include       = '';
        $this->js_inject        = '';
    }
    
    public function index()
    {
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('piutang') => '#');
        $data = array();

        $this->js_inject  .= $this->load->view('accounting/finance/data_piutang_js', $data, TRUE);
        
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');

        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('piutang');

        $konten = $this->load->view('accounting/finance/data_piutang', $data, TRUE);
        $this->admin_view($konten);
    }

}

/* End of file Piutang.php */

?>