<?php
defined('BASEPATH') or exit('No direct script access allowed');

class warehouse_rak_system extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('warehouse_rak_system') == FALSE) {
            gak_boleh('warehouse_rak_system');
        }
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_warehouse_rak_system', 'warehouse_rak_system');
        $this->lang->load('warehouse_rak_system');
        $this->active_root_menu = $this->lang->line('warehouse_rak_system_alltitle');
        $this->browser_title = $this->lang->line('warehouse_rak_system_alltitle');
        $this->modul_name = $this->lang->line('warehouse_rak_system_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
    }

    public function index()
    {
        // $options = array();
        // $options['modul_code'] = 'bts';
        // $this->frame->main_crud($options);

        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('warehouse_rak_system_alltitle') => '#');
        $data = array();

        //$this->js_inject .= $this->load->view('warehouse_rak_system/js_table', $data, TRUE);
        $this->js_inject .= $this->load->view('warehouse_rak_system/js', $data, TRUE);
        //$this->js_inject .= $this->load->view('warehouse_rak_system/valid', $data, TRUE);
        //$this->js_include .= $this->ui->js_include('flexigridMaster');
        //$this->js_include .= $this->ui->load_css('flexigridMaster');
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');
        //$this->css_include .= $this->ui->load_css('jquery_ui');
        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('warehouse_rak_system_alltitle');
        //$data['update_view'] = $this->load->view('warehouse_rak_system/update', $data, TRUE);
        $data['insert_view'] = $this->load->view('warehouse_rak_system/form', $data, TRUE);
        //$data['delete_view'] = $this->load->view('warehouse_rak_system/delete', $data, TRUE);

        $konten = $this->load->view('warehouse_rak_system/index', $data, TRUE);
        $this->admin_view($konten);
    }

    public function get_barang_jadi()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->warehouse_rak_system->get_barang_jadi($param);
        } else {
            echo $this->warehouse_rak_system->get_barang_jadi();
        }
    }
    //barang jadi
    public function insert_data_jadi()
    {
        echo $this->warehouse_rak_system->insert_data_jadi();
    }

    public function insert_data_out_jadi()
    {
        echo $this->warehouse_rak_system->insert_data_out_jadi();
    }

    public function insert_data_bahan()
    {
        echo $this->warehouse_rak_system->insert_data_bahan();
    }

    public function get_detail_baku()
    {
        echo $this->warehouse_rak_system->get_detail_baku();
    }

    public function get_detail_gudang_bahan()
    {
        echo $this->warehouse_rak_system->get_detail_gudang_bahan();
    }

    public function get_detail_jadi()
    {
        echo $this->warehouse_rak_system->get_detail_jadi();
    }

    public function get_detail_gudang_jadi()
    {
        echo $this->warehouse_rak_system->get_detail_gudang_jadi();
    }

    public function cek_data_jadi()
    {
        echo json_encode($this->warehouse_rak_system->cek_data_jadi());
    }

    public function get_history_jadi()
    {
        echo $this->warehouse_rak_system->get_history_jadi();
    }

    public function get_barang_out_jadi()
    {
        echo json_encode($this->warehouse_rak_system->get_barang_out_jadi());
    }

    public function get_bahan()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->warehouse_rak_system->get_bahan($param);
        } else {
            echo $this->warehouse_rak_system->get_bahan();
        }
    }

    public function get_customer()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->warehouse_rak_system->get_customer($param);
        } else {
            echo $this->warehouse_rak_system->get_customer();
        }
    }
}
