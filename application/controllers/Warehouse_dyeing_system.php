<?php
defined('BASEPATH') or exit('No direct script access allowed');

class warehouse_dyeing_system extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('warehouse_dyeing_system') == FALSE) {
            gak_boleh('warehouse_dyeing_system');
        }
        $this->load->model('model_global', 'm_global');
        $this->load->model('model_warehouse_dyeing_system', 'warehouse_dyeing_system');
        $this->load->model('model_inventory_barang', 'inventory_barang');
        $this->lang->load('warehouse_dyeing_system');
        $this->active_root_menu = $this->lang->line('warehouse_dyeing_system_alltitle');
        $this->browser_title = $this->lang->line('warehouse_dyeing_system_alltitle');
        $this->modul_name = $this->lang->line('warehouse_dyeing_system_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
    }

    public function index()
    {
        // $options = array();
        // $options['modul_code'] = 'bts';
        // $this->frame->main_crud($options);

        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('warehouse_dyeing_system_alltitle') => '#');
        $data = array();

        //$this->js_inject .= $this->load->view('warehouse_dyeing_system/js_table', $data, TRUE);
        $this->js_inject .= $this->load->view('warehouse_dyeing_system/js', $data, TRUE);
        //$this->js_inject .= $this->load->view('warehouse_dyeing_system/valid', $data, TRUE);
        //$this->js_include .= $this->ui->js_include('flexigridMaster');
        //$this->js_include .= $this->ui->load_css('flexigridMaster');
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');
        //$this->css_include .= $this->ui->load_css('jquery_ui');
        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('warehouse_dyeing_system_alltitle');
        //$data['update_view'] = $this->load->view('warehouse_dyeing_system/update', $data, TRUE);
        $data['insert_view'] = $this->load->view('warehouse_dyeing_system/form', $data, TRUE);
        //$data['delete_view'] = $this->load->view('warehouse_dyeing_system/delete', $data, TRUE);

        $konten = $this->load->view('warehouse_dyeing_system/index', $data, TRUE);
        $this->admin_view($konten);
    }

    public function get_data_table()
    {
        $this->warehouse_dyeing_system->get_data_table();
    }

    public function get_data_barang()
    {
        $this->warehouse_dyeing_system->get_data_barang();
    }

    public function get_satuanbarang()
    {
        $response = null;
        $query = $this->warehouse_dyeing_system->get_satuanbarang();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $response .= "<option value='" . $row['id'] . "'>" . $row['kode'] . " - " . $row['nama'] . "</option>";
            }
        }
        echo $response;
    }

    public function get_proses()
    {
        $response = null;
        $query = $this->warehouse_dyeing_system->get_proses();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $response .= "<option value='" . $row['id'] . "'>" . $row['kode'] . ' - ' . $row['nama'] . "</option>";
            }
        }
        echo $response;
    }

    public function get_barang()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->warehouse_dyeing_system->get_barang($param);
        } else {
            echo $this->warehouse_dyeing_system->get_barang();
        }
    }

    public function get_barang_daftar()
    {
        $param = $this->input->post('searchTerm');
        if (!empty($param)) {
            echo $this->warehouse_dyeing_system->get_barang_daftar($param);
        } else {
            echo $this->warehouse_dyeing_system->get_barang_daftar();
        }
    }

    public function insert_data()
    {
        echo $this->warehouse_dyeing_system->insert_data();
    }

    public function insert_data_in()
    {
        echo $this->warehouse_dyeing_system->insert_data_in();
    }

    public function insert_data_out()
    {
        echo $this->warehouse_dyeing_system->insert_data_out();
    }

    public function view_data($id)
    {
        echo json_encode($this->warehouse_dyeing_system->view_data($id));
    }

    public function update_data()
    {
        echo $this->warehouse_dyeing_system->update_data();
    }

    public function delete_data($id = '')
    {
        echo $this->warehouse_dyeing_system->delete_data($id);
    }

    public function detail_komponen()
    {
        echo json_encode($this->warehouse_dyeing_system->detail_komponen());
    }

    public function detail_proses()
    {
        echo json_encode($this->warehouse_dyeing_system->detail_proses());
    }
}
