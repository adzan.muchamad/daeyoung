<?php
defined('BASEPATH') or exit('No direct script access allowed');

class laporan_beacukai extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        check_login();
        if (have_privileges('laporan_beacukai') == FALSE) {
            gak_boleh('laporan_beacukai');
        }
        $this->load->helper('my_func_helper');
        $this->load->model('model_global', 'm_global');
        $this->lang->load('laporan_beacukai');
        $this->active_root_menu = $this->lang->line('laporan_beacukai_alltitle');
        $this->browser_title = $this->lang->line('laporan_beacukai_alltitle');
        $this->modul_name = $this->lang->line('laporan_beacukai_alltitle');
        $this->css_include = '';
        $this->js_include = '';
        $this->js_inject = '';
        ini_set('memory_limit', '-1');
        set_time_limit(0);
    }

    public function index()
    {
        // $options = array();
        // $options['modul_code'] = 'bts';
        // $this->frame->main_crud($options);
        $this->breadcrumb = array('Home' => base_url(), $this->lang->line('laporan_beacukai_alltitle') => '#');
        $data = array();

        //$this->js_inject .= $this->load->view('finance_accounting_report_als/js_table', $data, TRUE);
        // $this->js_inject .= $this->load->view('laporan_beacukai/js', $data, true);
        //$this->js_inject .= $this->load->view('finance_accounting_report_als/valid', $data, TRUE);
        $this->js_include .= $this->ui->load_css('MaterialIcons');
        $this->js_include .= $this->ui->js_include('jquery_ui');
        $this->js_include .= $this->ui->js_include('mask_money');
        $this->js_include .= $this->ui->js_include('dt_fixed_columns');
        $this->js_include .= $this->ui->js_include('jszip');
        $this->js_include .= $this->ui->js_include('pdfmake');
        $this->js_include .= $this->ui->js_include('vfs_fonts');
        $this->js_include .= $this->ui->js_include('buttons');
        $this->js_include .= $this->ui->js_include('select2');
        $this->js_include .= $this->ui->js_include('custom_page');
        $this->js_include .= $this->ui->js_include('toastr');
        //$this->css_include .= $this->ui->load_css('jquery_ui');
        $this->css_include .= $this->ui->load_css('toastr');
        $this->css_include .= $this->ui->load_css('custom_page');

        $data['title_page_table'] = $this->lang->line('laporan_beacukai_alltitle');
        // $data['insert_view'] = $this->load->view('laporan_beacukai/form', $data, true);
        $konten = $this->load->view('laporan_beacukai/index', $data, true);
        $this->admin_view($konten);
    }
}
