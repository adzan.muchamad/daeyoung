<input type="hidden" id="id" name="id" />
<div class="row ">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Customer
        </div>
        <div class="col-lg-10">
            <select class="form-control searchcust" id="nama_cust" name="nama_cust" onchange="get_branch(this)"></select>
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Kredit Saat Ini
        </div>
        <div class="col-lg-4 kredit_val">
            <a class="form-control text-primary" style="border: 0px">sdasd</a>
        </div>
        <div class="col-lg-2">
            Dokumen Pajak
        </div>
        <div class="col-lg-4">
            <select class="form-control" id="dok_pajak" name="dok_pajak">
                <?php $res = $this->m_global->get_dok_pajak();
                echo '<option value=""> Tidak Ada </option>';
                foreach ($res as $row) {
                    echo '<option value="' . $row->id . '">' . $row->nama . '</option>';
                } ?>
            </select>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Cabang
        </div>
        <div class="col-lg-10">
            <select class="form-control searchbranch" id="branch" name="branch" onchange="cek_matauang()"></select>
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Tanggal
        </div>
        <div class="col-lg-4">
            <input type="text" class="form-control date_picker" id="tgl_order" name="tgl_order" value="<?= date('Y-m-d'); ?>" />
        </div>
        <div class="col-lg-2">
            Nomor Dokumen
        </div>
        <div class="col-lg-4">
            <input type="text" class="form-control" id="nomor_dok" name="nomor_dok" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Mata Uang
        </div>
        <div class="col-lg-4">
            <select class="form-control" id="mata_uang">
                <?php $res = $this->m_global->get_currency();
                foreach ($res as $row) {
                    echo '<option value="' . $row->id . '">' . $row->nama . '</option>';
                } ?>
            </select>
        </div>
        <div class="col-lg-2">
            Dikirim Dari
        </div>
        <div class="col-lg-4">
            <select class="form-control" name="lokasi" id="lokasi" onchange="get_alamat();">
                <option value=""></option>
                <?php $res = $this->m_global->get_loc();
                foreach ($res as $row) {
                    echo '<option value="' . $row->id . '">' . $row->loc_nama . '</option>';
                }
                ?>
            </select>
        </div>

    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Tanggal Tagihan
        </div>
        <div class="col-lg-4">
            <input type="text" class="form-control date_picker" id="tgl_tagih" name="tgl_tagih" value="<?= date("Y-m-d", strtotime("+ 2 month")); ?>" />
        </div>
        <div class="col-lg-2">
            Tanggal Dokumen
        </div>
        <div class="col-lg-4">
            <input type="text" class="form-control date_picker" id="tgl_dok" name="tgl_dok" />
        </div>
    </div>


</div>
<hr>
<div class="row" style="margin-top:5px">
    <div class="col-md-5">
        <input class="form-control" type="text" id="search_no" name="search_no" placeholder="Search Sales Order" />
    </div>
</div>
<div class="row">
    <table class="table">
        <thead>
            <tr>
                <td style="text-align: center;width:60px">*</td>
                <td style="text-align: center;width:210px">No SO</td>
                <td style="text-align: center;width:160px">Tanggal</td>
                <td style="text-align: center;width:120px">Mata Uang</td>
                <td style="text-align: center;width:120px">Total</td>
                <td style="text-align: center;width:310px">Note</td>
            </tr>
        </thead>
    </table>
</div>
<div class="row" style="overflow-y:scroll;height:90px;overflow-x:hidden">
    <table class="table" id="mytable" style="padding: 0;">
    </table>
</div>
<div class="row detail_barang" style="overflow-y:scroll;height:250px;overflow-x:hidden">

</div>
<hr>
<div class="row">
    <div class="col-lg-8">
        <div class="col-lg-6 display-flex-center">
            <div class="col-lg-4">
                Buyer
            </div>
            <div class="col-lg-4">
                <input type="text" class="form-control" id="buyer" name="buyer" />
            </div>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Sub Total
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="jumlah_harga" name="jumlah_harga" readonly />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="col-lg-6 display-flex-center">
            <div class="col-lg-4">
                PPN
            </div>
            <div class="col-lg-4">
                <select class="form-control" name="ppn" id="ppn" onchange="count_all()">
                    <option value="0">Tidak</option>
                    <option value="1">Iya</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Diskon
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="diskon" name="diskon" onkeyup="count_all()" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="col-lg-6 display-flex-center">
            <div class="col-lg-4">
                Note
            </div>
            <div class="col-lg-8">
                <textarea type="text" class="form-control" id="note_order" name="note_order"></textarea>
            </div>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Pajak
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="pajak" readonly />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            <b>Jumlah Total</b>
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="total" name="total" readonly />
        </div>
    </div>
</div>