<script type="text/javascript">
	$(function() {
		var pageUri = $('#pageUri').val();

		//SET DATATABLES
		$.extend($.fn.dataTable.defaults, {
			//autoWidth: true,
			"sLength": "form-control"
		});
		var height_windows = $(window).height();
		var height_navbar = $('#navbar-main').height();
		var height_page = $('#page-header').height();
		var height_table = height_windows - height_navbar - height_page - 370;
		var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

		table = $('#datamain_datatable').DataTable({
			dom: 'J<"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
			"pageLength": 50,
			aaSorting: [
				[2, 'asc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_data_table',
				"type": "POST",
				"data": function(data) {
					data.searchtaxgroup = $('#searchtaxgroup').val();
					data.search_keyword = $('#search_keyword').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},

			columnDefs: [{
				orderable: false,
				width: '150px',
				targets: [0, tabel_kolom]
			}, {
				className: "text-center",
				"targets": [tabel_kolom, 0]
			}],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
			scrollX: '100%',
			scrollY: height_table,
			scrollCollapse: false,
			fixedColumns: {
				leftColumns: 0,
				rightColumns: 1
			},
		});

		$('#buttonPencarian').click(function() {
			table.ajax.reload(null, false);
			$('#button_form_dropdown_search').next().toggle();
		});
		$('#search_keyword').keyup(function() {
			table.ajax.reload(null, false);
		});
		$('.dataTables_length select').select2({
			minimumResultsForSearch: Infinity,
			width: 'auto',
		});
		//END SET DATATABLES

		$("#formulir_modal").validate({
			rules: {
				nama_barang: {
					required: true
				},
			},
			submitHandler: function(form) {
				var cekTransaksi = $("#formulir_modal").attr('transaksi');
				if (cekTransaksi == 'tambah') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'insert_data',
						data: $('#formulir_modal').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								$('#formulir_modal').clearForm();
								$('#modal_supplier').modal('hide');
								toastr.success('Data berhasil disimpan');
								table.ajax.reload(null, false);

							} else if (response == '0') {
								toastr.warning('Data gagal disimpan');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				} else if (cekTransaksi == 'edit') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'update_data',
						data: $('#formulir_modal').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								toastr.success('Data berhasil diupdate');
								table.ajax.reload(null, false);
								$('#modal_supplier').modal('hide');
							} else if (response == '0') {
								toastr.warning('Data gagal diupdate');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				}
				return false;
			}
		});
		get_customers();

	});

	function get_customers() {
		var pageUri = $('#pageUri').val();
		$(".searchcust").select2({
			placeholder: "Pilih Customers",
			width: "100%",
			ajax: {
				url: pageUri + 'get_customers',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response

					};
				},
				cache: false
			}
		});

	};

	function get_branch(obj) {
		var pageUri = $('#pageUri').val();
		var id_cust = $(obj).val();
		$(".searchbranch").empty();
		$(".searchbranch").select2({
			placeholder: "Pilih Cabang",
			width: "100%",
			ajax: {
				url: pageUri + 'get_branch',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term,
						id: id_cust
					};
				},
				processResults: function(response) {
					return {
						results: response
					};
				},
				cache: false
			}
		});
	};


	function cek_matauang() {
		var pageUri = $('#pageUri').val();
		var mata_uang = $('#mata_uang').val();
		$.ajax({
			type: 'POST',
			url: pageUri + 'get_mata_uang',
			data: {
				id: mata_uang
			},
			success: function(response) {
				$('#mata_uang').val(response).change();
			}
		});
	}

	function get_matauang(obj) {
		var pageUri = $('#pageUri').val();
		var cust = $('#branch').val();
		$.ajax({
			type: 'POST',
			url: pageUri + 'get_kurs',
			data: {
				id: cust
			},
			dataType: 'json',
			success: function(response) {
				if (response != '') {
					if (response.mata_uang != 1) {
						$('#mata_uang').val(response.mata_uang);

					}
				}
			}
		});
	}

	function get_alamat() {
		var pageUri = $('#pageUri').val();
		var lok = $('#lokasi').val();
		$.ajax({
			type: 'POST',
			url: pageUri + 'get_alamat',
			data: {
				id: lok
			},
			success: function(response) {
				$('#alamat').val(response);
			}
		});
	}

	function get_barang() {
		var pageUri = $('#pageUri').val();
		$(".barang").select2({
			placeholder: "Masukan Nama Barang",
			width: "100%",
			ajax: {
				url: pageUri + 'get_barang',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};

				},
				cache: false
			}
		});
	};

	function get_satuan(obj) {
		var pageUri = $('#pageUri').val();
		var id = $(obj).val();
		$.ajax({
			type: 'POST',
			url: pageUri + 'get_satuanbarang/',
			data: {
				id: id
			},
			success: function(response) {
				$(obj).closest('.row').find('.satuan').html('').append(response);
			}
		});

	}

	function add_barang(obj) {
		$('.list_barang').append(' <div class="row"><div class="col-lg-1 text-center" style="width:5%;"><input class="form-control no_urut text-center" type="text" readonly /></div><div class="col-lg-4 text-center"><select class="form-control barang" name="barang[]" onchange="get_satuan(this);"></select></div><div class="col-lg-1 text-center"><input class="form-control qty" type="text" name="qty[]" onkeyup="count_jmlh(this);" /></div><div class="col-lg-1 text-center"><select name="satuan[]" class="form-control satuan"></select></div><div class="col-lg-2 text-center"><input class="form-control harga text-right" type="text" name="harga[]" onkeyup="count_jmlh2(this);" /></div><div class="col-lg-2 text-center"><input class="form-control text-right jumlah" type="text" name="jumlah[]" readonly /></div><div class="col-lg-1"><a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a></div></div>');
		get_barang();
		pemberian_no();
		$('.qty').autoNumeric('init', {
			mDec: '6'
		});
		$('.harga').autoNumeric({
			mDec: '2'
		});
		count_all();
	}

	function del_barang(obj) {
		$(obj).closest('.row').remove();
		pemberian_no();
		count_all();
	}

	function replaceAll(str, token, newToken, ignoreCase) {
		var _token;
		var i = -1;

		if (typeof token === "string") {

			if (ignoreCase) {

				_token = token.toLowerCase();

				while ((
						i = str.toLowerCase().indexOf(
							_token, i >= 0 ? i + newToken.length : 0
						)) !== -1) {
					str = str.substring(0, i) +
						newToken +
						str.substring(i + token.length);
				}

			} else {
				return str.split(token).join(newToken);
			}

		}
		return str;
	};

	function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
		try {
			decimalCount = Math.abs(decimalCount);
			decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

			const negativeSign = amount < 0 ? "-" : "";

			let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
			let j = (i.length > 3) ? i.length % 3 : 0;

			return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
		} catch (e) {
			console.log(e)
		}
	}

	function count_jmlh(obj) {
		var a = replaceAll($(obj).val(), '.', '');
		var b = replaceAll($(obj).closest('.row').find('.harga').val(), '.', '');
		a = a.replace(/[,]+/g, ".");
		b = b.replace(/[,]+/g, ".");
		if (a == null || a == 'undefined' || a == '') {
			a = 0;
		}
		if (b == null || b == 'undefined' || b == '') {
			b = 0;
		}
		var c = parseFloat(parseFloat(a) * parseFloat(b)).toFixed(2);
		$(obj).closest('.row').find('.jumlah').val(formatMoney(c));
		count_all();
	}

	function count_jmlh2(obj) {
		var a = replaceAll($(obj).closest('.row').find('.qty').val(), '.', '');
		var b = replaceAll($(obj).val(), '.', '');
		a = a.replace(/[,]+/g, ".");
		b = b.replace(/[,]+/g, ".");
		if (a == null || a == 'undefined' || a == '') {
			a = 0;
		}
		if (b == null || b == 'undefined' || b == '') {
			b = 0;
		}
		var c = parseFloat(parseFloat(a) * parseFloat(b)).toFixed(2);
		$(obj).closest('.row').find('.jumlah').val(formatMoney(c));
		count_all();
	}

	function count_all() {
		var a = 0;
		var tot = 0;
		var total = 0;
		var total2 = 0;
		$('.jumlah').each(function() {
			a = replaceAll($(this).val(), '.', '');
			a = a.replace(/[,]+/g, ".");
			if (a == null || a == 'undefined' || a == '') {
				tot = parseFloat(parseFloat(tot));
			} else {
				tot = parseFloat(parseFloat(tot) + parseFloat(a));
			}

		});

		var diskon = replaceAll($('#diskon').val(), '.', '');
		diskon = diskon.replace(/[,]+/g, ".");
		if (diskon == null || diskon == 'undefined') {
			diskon = 0;
		}

		total = parseFloat(tot).toFixed(2);
		var ppn = $('#ppn').val();
		if (ppn == 1) {
			ppn = (tot - diskon) * 10 / 100;
		} else {
			ppn = 0;
		}
		var pajak = parseFloat(ppn);
		$('#pajak').val(formatMoney(pajak));
		total2 = parseFloat(parseFloat(tot) - diskon + ppn).toFixed(2);
		total = formatMoney(total);
		$('#jumlah_harga').val(total);
		total2 = formatMoney(total2);
		$('#total').val(total2);
	}

	function pemberian_no() {
		var no_urut = parseInt(1);
		var tObj = document.getElementsByClassName('no_urut');
		for (var i = 0; i < tObj.length; i++) {
			tObj[i].value = no_urut;
			no_urut += parseInt(1);
		}
	};

	function input_data() {
		$("#formulir_modal").attr('transaksi', 'tambah');
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Buat Sales Order');
		$('#formSearch').dialog('close');
		$('#modal_supplier').modal('show');
		get_customers();
		pemberian_no();
		get_barang();
		$('.qty').autoNumeric('init', {
			mDec: '6'
		});
		$('.harga').autoNumeric({
			mDec: '2'
		});
		$('#diskon').autoNumeric({
			mDec: '2'
		});
		return false;
	}


	function delete_data(id) {
		var pageUri = $('#pageUri').val();

		swal({
			title: "Anda Yakin?",
			text: "Anda akan hapus data ini.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes!",
			closeOnConfirm: false
		}, function() {
			$.ajax({
				type: 'GET',
				url: pageUri + 'delete_data/' + id,
				beforeSend: function() {
					blockUI();
				},
				success: function(response) {
					if (response == '1') {
						table.ajax.reload(null, false);
						toastr.success('Data berhasil dihapus');
					} else if (response == '0') {
						toastr.error('Data gagal dihapus');
					} else {
						toastr.warning(response);
					}
					swal.close();
					unBlockUI();
				}
			});
		});
	};

	function update_data(id) {
		var pageUri = $('#pageUri').val();
		$('#id').val(id);
		$("#formulir_modal").attr('transaksi', 'edit');
		$.ajax({
			type: 'POST',
			data: $('#formulir_modal').serialize(),
			url: pageUri + 'select_data/' + id,
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#nama_cust').empty().append($("<option/>").val(html.data[0].id_cust).text(html.data[0].nama_cust)).trigger('change');
					$('#branch').empty().append($("<option/>").val(html.data[0].id_branch).text(html.data[0].nama_branch)).trigger('change');
					$('#tempo').val(html.data[0].tempo_bayar).change();
					$('#tgl_order').val(html.data[0].tanggal);
					$('#tgl_kirim').val(html.data[0].tanggal_batas);
					$('#mata_uang').val(html.data[0].mata_uang).change();
					$('#nilai').val(formatMoney(html.data[0].rate));
					$('#ref_cust').val(html.data[0].ref);
					$('#buyer').val(html.data[0].buyer);
					$('#note_order').val(html.data[0].note);
					$('#lokasi').val(html.data[0].id_loc).change();
					if (html.det != null) {
						// $('.form_detail').css('display', 'block');
						$('.list_barang').html('').append(html.det);
						get_barang();
						pemberian_no();
						var tObj = document.getElementsByClassName('barang');
						for (var i = 0; i < tObj.length; i++) {
							$('#get_barang_' + i).empty().append($("<option/>").val(html.barang[i].id_barang).text(html.barang[i].kode_barang + ' - ' + html.barang[i].nama_barang)).trigger('change');
						}
					}
					$('#diskon').autoNumeric({
						mDec: '2'
					});
					$('#diskon').val(html.data[0].diskon).change();
					$('#ppn').val(html.data[0].ppn).change();
					$('#modal_supplier').modal('show');
				} else if (html == '0') {
					toastr.error('Data gagal dibuka');
				} else {
					toastr.warning(html);
				}
				unBlockUI();
			},
		});
	}

	function view_data(id) {
		var pageUri = $('#pageUri').val();
		$.ajax({
			type: 'GET',
			url: pageUri + 'view_data/' + id,
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				$('.detail_supp').html('').append(html);
				$('#modal_supplier_detail').modal('show');
				unBlockUI();
			},
		});
	}

	function get_so() {
		var pageUri = $('#pageUri').val();
		var id_cust = $('#nama_cust').val();
		var id_branch = $('#branch').val();
		if ($('#branch').val() != null || $('#branch').val() != 'undefined') {
			$.ajax({
				type: "POST",
				url: pageUri + 'select_so',
				data: {
					id_cust: id_cust,
					id_branch: id_branch
				},
				dataType: "json",
				success: function(response) {
					$('#mytable').html(response.html);
				}
			});

		}
	}

	$("#search_no").on("keyup", function() {
		var value = $(this).val().toUpperCase();

		$("#mytable tr").each(function(index) {

			$row = $(this);

			var id = $row.find("td:eq(1)").text().toUpperCase();
			var id2 = $row.find("td:eq(2)").text().toUpperCase();
			if (id.indexOf(value) > -1 || id2.indexOf(value) > -1) {
				$row.show();

			} else {
				$row.hide();
			}
		});
	});

	$(document).ready(function() {
		$('#branch').change(function() {
			get_so();
		});

		function myFunction() {
			var input, filter, table, tr, td, i, txtValue;
			input = document.getElementById("search_no");
			filter = input.value.toUpperCase();
			table = document.getElementById("mytable");
			tr = table.getElementsByTagName("tr");
			for (i = 0; i < tr.length; i++) {
				td = tr[i].getElementsByTagName("td")[1];
				td2 = tr[i].getElementsByTagName("td")[2];
				if (td || td2) {
					txtValue = td.textContent || td.innerText;
					txtValue2 = td2.textContent || td2.innerText;
					if (txtValue.toUpperCase().indexOf(filter) > -1 || txtValue2.toUpperCase().indexOf(filter) > -1) {
						tr[i].style.display = "";
					} else {
						tr[i].style.display = "none";
					}
				}
			}
		}
	});
</script>