<?php
$id_kas = ($this->uri->segment(3) != '') ? $this->uri->segment(3) : '';
$id_tgl_awal = ($this->uri->segment(4) != '') ? $this->uri->segment(4) : '';
$id_tgl_akhir = ($this->uri->segment(5) != '') ? $this->uri->segment(5) : '';

?>
<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<input type="hidden" id="domainUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?> <em id="info_coa" style="font-weight:normal;"></em></h6>
        <div class="heading-elements">
            <form id="formsearch" onSubmit="return false;">
                <div class="input-group">
                    <input type="text" class="form-control" style="width:250px;" placeholder="Pencarian" id="search_keyword" name="search_keyword">
                    <div class="input-group-btn" style="text-align:right;">
                        <button id="button_dropdown_search" type="button" class="btn dropdown-toggle btn-icon btn-default btn-raised" data-toggle="dropdown">
                            <span class="caret"></span> <i class="icon-search4"></i>
                        </button>

                        <ul class="dropdown-menu dropdown-content dropdown-menu-right" style="width:600px;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Lokasi</label>
                                        <select class="form-control" id="idloc" name="idloc">
                                            <option value="">Semua Lokasi</option>
                                            <option value="BT">BT Color</option>
                                            <option value="DYG">DYEING</option>
                                            <option value="ENG">ENGINEERING</option>
                                            <option value="EXP">EXPANSE PT.DAE YOUNG</option>
                                            <option value="GFG">FINISH GOOD</option>
                                            <option value="FNS">FINISHING</option>
                                            <option value="GBB">GDG BAHAN BAKU</option>
                                            <option value="GDC">GDG CHEMICAL</option>
                                            <option value="GDI">GDG INGREY</option>
                                            <option value="RJK">GUDANG REJECT</option>
                                            <option value="SCR">GUDANG SCRAP</option>
                                            <option value="LAB">LABORATORY</option>
                                            <option value="PRD">LINE PRODUKSI</option>
                                            <option value="RND">RND </option>
                                            <option value="WRP">WARPING</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>No Dokumen Pabean</label>
                                        <input type="text" class="form-control" id="no_dok" name="no_dok" />
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label>Jenis Dokumen</label>
                                        <select class="form-control" id="tipe_bc" name="tipe_bc">
                                            <option value="">Semua Jenis</option>
                                            <option value="BC23">BC 2.3</option>
                                            <option value="BC27">BC 2.7</option>
                                            <option value="BC40">BC 4.0</option>
                                            <option value="BC262">BC 2.6.2</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-9">
                                        <label>Barang</label><br>
                                        <select class="form-control" id="idbarang" name="idbarang">
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>dari tanggal</label>
                                        <input type="text" class="form-control date_picker" id="searchDateFirst" name="searchDateFirst" readonly value="<?= date('Y-m-d', strtotime('- 1 month')); ?>" />
                                    </div>
                                    <div class="col-lg-6">
                                        <label>sampai tanggal</label>
                                        <input type="text" class="form-control date_picker" id="searchDateFinish" name="searchDateFinish" readonly value="<?= date('Y-m-d'); ?>" />
                                    </div>
                                </div>
                                <br />
                                <div class="row" style="text-align:right;">
                                    <button id="buttonPencarian" type="button" class="btn btn-default btn-raised"><b><i class="icon-search4"></i> Search</b></button>
                                </div>
                            </div>
                        </ul>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <table id="datamain_datatable" style="width:100%;border-collapse: collapse;border-spacing: 0px;" class="table table-hover datatable-ajax table-striped table-xxs table-bordered text-size-small">
        <colgroup>
            <col>
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-2">
            <col class="col-md-1">
            <col class="col-md-3">
            <col>
            <col>
            <col>
        </colgroup>
        <thead>
            <tr>
                <td>No.</td>
                <td>Jenis Dok. Pabean </td>
                <td>Nomor Dok. Pabean </td>
                <td>Tgl Dok. Pabean </td>
                <td>No. Bukti Penerimaan</td>
                <td>Tgl. Bukti Penerimaan</td>
                <td>Pengiriman Barang</td>
                <td>Kode Barang</td>
                <td>Nama Barang</td>
                <td>Pemasukan</td>
                <td>Satuan</td>
                <td>Nilai Satuan</td>
            </tr>
        </thead>
    </table>
</div>