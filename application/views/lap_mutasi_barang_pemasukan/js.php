<script type="text/javascript">
	$(function() {
		var pageUri = $('#pageUri').val();

		//SET DATATABLES
		$.extend($.fn.dataTable.defaults, {
			//autoWidth: true,
			"sLength": "form-control"
		});

		table = $('#datamain_datatable').DataTable({
			dom: '<"datatable-header"B><"datatable-scroll datatable-scroll-wrap"t>',
			buttons: {
				buttons: [{
						extend: 'pdfHtml5',
						orientation: 'landscape',
						pageSize: 'LEGAL',
						className: 'btn btn-default',
						text: '<i class="icon-file-pdf position-left"></i> Pdf'
					},
					{
						extend: 'excelHtml5',
						className: 'btn btn-default',
						text: '<i class="icon-file-spreadsheet position-left"></i> Excel',
						fieldSeparator: '\t',
					}
				]
			},
			aaSorting: [
				[1, 'desc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_data_table',
				"type": "POST",
				"data": function(data) {
					data.idloc = $('#idloc').val();
					data.no_dok = $('#no_dok').val();
					data.tipe_bc = $('#tipe_bc').val();
					data.idbarang = $('#idbarang').val();
					data.search_keyword = $('#search_keyword').val();
					data.searchDateFirst = $('#searchDateFirst').val();
					data.searchDateFinish = $('#searchDateFinish').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},
			"ordering": false,
			columnDefs: [{
				className: "text-center",
				"targets": [0, 1, 2, 3, 5, 10]
			}, {
				className: "text-right",
				"targets": [9, 11]
			}, {
				className: "text-wrap",
				"targets": [8]
			}],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
		});

		$('#buttonPencarian').click(function() {
			table.ajax.reload();
		});
		$('#search_keyword').keyup(function() {
			table.ajax.reload(null, false);
		});
		$('.dataTables_length select').select2({
			minimumResultsForSearch: Infinity,
			width: 'auto',
		});
		//END SET DATATABLES

		$('#button_dropdown_search').on('click', function(e) {
			$(this).next().toggle();

		});
		$('#button_dropdown_search.keep-open').on('click', function(e) {
			e.stopPropagation();
		});
		get_barang();
	});

	function get_barang() {
		var pageUri = $('#pageUri').val();
		$("#idbarang").select2({
			placeholder: "Masukan Nama Barang",
			width: "100%",
			ajax: {
				url: pageUri + 'get_barang',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response

					};
				},
				cache: false
			}
		});
	};
</script>