<?php
$arr = array();
$arr['modal_icon'] = 'icon-magazine';
$arr['modal_id'] = 'modal_profile';
$arr['form_id'] = 'form_profile';
$arr['form_action'] = base_url() . 'my_profile/update';
$arr['form_input_top'] = '<div id="alert_modal_myprofile"></div>';
$arr['modal_title'] = $this->lang->line('profile_modal_title');
$arr['form_input'] = array(
    $this->ui->load_template(
        'form_group_text',
        array(
            'label'         => $this->lang->line('my_name'),
            'id'            => 'my_name',
            'name'          => 'name',
            'class'         => 'form-control cos',
            'maxlength'     => '250',
        )
    ),
    $this->ui->load_template(
        'form_group_text_readonly',
        array(
            'label'         => $this->lang->line('my_email'),
            'id'            => 'my_email',
            'name'          => 'email',
            'class'         => 'form-control cos text-lowercase',
            'maxlength'     => '250',
        )
    ),
    $this->ui->load_template(
        'form_group_text_readonly',
        array(
            'label'         => $this->lang->line('my_username'),
            'id'            => 'my_username',
            'name'          => 'username',
            'class'         => 'form-control cos text-lowercase',
            'readonly'      => 'readonly',
            'maxlength'     => '250'
        )
    ),
    $this->ui->load_template(
        'form_group_password',
        array(
            'label'         => $this->lang->line('my_password'),
            'id'            => 'my_password',
            'name'          => 'password',
            'class'         => 'form-control cos',
            'maxlength'     => '250'
        )
    ),
    $this->ui->load_template(
        'hidden',
        array(
            'id'            => 'my_name_required',
            'name'          => 'my_name_required',
            'value'         => $this->lang->line('my_name_required'),
        )
    ),
    $this->ui->load_template(
        'hidden',
        array(
            'id'            => 'my_email_required',
            'name'          => 'my_email_required',
            'value'         => $this->lang->line('my_email_required'),
        )
    ),
    $this->ui->load_template(
        'hidden',
        array(
            'id'            => 'my_email_email',
            'name'          => 'my_email_email',
            'value'         => $this->lang->line('my_email_email'),
        )
    ),
    $this->ui->load_template(
        'hidden',
        array(
            'id'            => 'id_profile',
            'name'          => 'id',
        )
    ),
    $this->ui->load_template(
        'hidden',
        array(
            'id'            => 'profile_update_seccess',
            'name'          => 'profile_update_seccess',
            'value'         => $this->lang->line('profile_update_seccess'),
        )
    ),
    $this->ui->load_template(
        'hidden',
        array(
            'id'            => 'profile_update_failed',
            'name'          => 'profile_update_failed',
            'value'         => $this->lang->line('profile_update_failed'),
        )
    ),

);


// pre($arr);
echo $this->ui->load_template('modal_default_form_one_column', $arr, TRUE);
$arr = array();
$arr['modal_icon'] = 'icon-magazine';
$arr['modal_id'] = 'modal_changepass';
$arr['form_id'] = 'form_changepass';
$arr['form_action'] = base_url() . 'my_profile/changepass';
$arr['form_input_top'] = '<div id="alert_modal_changepass"></div>';
$arr['modal_title'] = $this->lang->line('changepass_modal_title');
$arr['form_input'] = array(
    $this->ui->load_template(
        'form_group_password',
        array(
            'label'         => 'Old Password',
            'id'            => 'old_password',
            'name'          => 'old_password',
            'class'         => 'form-control cos',
            'maxlength'     => '250',
            'value'         => '',
        )
    ),
    $this->ui->load_template(
        'form_group_password',
        array(
            'label'         => 'New Password',
            'id'            => 'new_password',
            'name'          => 'new_password',
            'class'         => 'form-control cos',
            'maxlength'     => '250',
            'pwindicator'   => 'pwindicator_my_password',
            'value'         => '',
        )
    ),
    $this->ui->load_template(
        'form_group_password',
        array(
            'label'         => 'Confirm Password',
            'id'            => 'confirm_pass',
            'name'          => 'confirm_pass',
            'class'         => 'form-control cos',
            'maxlength'     => '250',
            'value'         => '',
            'onkeyup'       => 'match_pass(this)'
        )
    ),
    $this->ui->load_template(
        'hidden',
        array(
            'id'            => 'password_update_seccess',
            'name'          => 'password_update_seccess',
            'value'         => $this->lang->line('password_update_seccess'),
        )
    ),

);
echo $this->ui->load_template('modal_default_form_one_column', $arr, TRUE);
echo $this->load->view('focus/default', '', TRUE);
echo $this->load->view('flat/bug_report', '', TRUE);
?>
<input type="hidden" name="base_url_js" id="base_url_js" value="<?php echo base_url(); ?>">
<div id="not_div"></div>

<div class="footer">
    <div class="navbar navbar-default" id="navbar-second-foot">
        <div class="text-center" style="color:yellow;padding-top:10px">
            Copyright PT DAE YOUNG TEXTILE &copy; 2020. All Right Reserved.
        </div>
    </div>
</div>