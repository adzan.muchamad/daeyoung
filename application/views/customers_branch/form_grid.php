<input type="hidden" id="id" name="id" />
<label><b>Detail Cabang</b></label>
<div class="row">
    <div class="col-lg-3 text-center">
        <label>Nama Customer</label>
        <select class="form-control" id="nama_cust" name="nama_cust"> </select>
    </div>
    <div class="col-lg-4 text-center">
        <label>Nama Cabang</label>
        <input type="text" class="form-control" id="nama_branch" name="nama_branch" />
    </div>


    <div class="col-lg-3 text-center">
        <label>NPWP</label>
        <input type="text" class="form-control" id="npwp" name="npwp" />
    </div>
    <div class="col-lg-2 text-center">
        <label>Mata Uang</label>
        <select class="form-control" id="mata_uang" name="mata_uang">
            <?php $currency = $this->m_global->get_currency();
            foreach ($currency as $row) {
                echo "<option value='" . $row->id . "'>" . $row->nama . "</option>";
            }
            ?>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 text-center">
        <label>Fax</label>
        <input type="text" class="form-control" id="fax" name="fax" />
    </div>
    <div class="col-lg-2 text-center">
        <label>Limit Kredit</label>
        <input type="text" class="form-control" id="limit" name="limit" placeholder="Rp 250.000.000" />
    </div>
    <div class="col-lg-3 text-center">
        <label>Termin Pembayaran</label>
        <select class="form-control" id="termin" name="termin">
            <option value="3">30 Hari</option>
            <option value="2">7 Hari</option>
            <option value="5">Bayar dimuka</option>
            <option value="4">Tgl 10 BB</option>
            <option value="1">Tunai</option>
        </select>
    </div>
    <div class="col-lg-2 text-center">
        <label>PPN</label>
        <select class="form-control" id="harga_pajak" name="harga_pajak">
            <option value="1">Tidak</option>
            <option value="2">Iya</option>
        </select>
    </div>
    <div class="col-lg-2 text-center">
        <label>Kawasan Berikat</label>
        <select class="form-control" id="kaber" name="kaber">
            <option value="0">Tidak</option>
            <option value="1">Iya</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 text-center">
        <label>Alamat</label>
        <textarea class="form-control" id="alamat" name="alamat" style="resize:vertical;"></textarea>
    </div>
    <div class="col-lg-4 text-center">
        <label>Alamat Invoice</label>
        <textarea class="form-control" id="alamat2" name="alamat_invoice" style="resize:vertical;"></textarea>
    </div>
    <div class="col-lg-4 text-center">
        <label>Keterangan</label>
        <textarea class="form-control" id="ket" name="keterangan" style="resize:vertical;"></textarea>
    </div>
</div>
<hr>
<label><b>Contact</b></label>
<div class="contact">
    <div class="row">
        <div class="col-lg-1 text-center">
            <label>No</label>
            <input type="text" class="form-control text-center no_urutan2" value="1" disabled />
        </div>
        <div class="col-lg-3 text-center">
            <label>Nama</label>
            <input type="text" class="form-control nama_contact" name="nama_contact[]" />
        </div>
        <div class="col-lg-2 text-center">
            <label>Jabatan</label>
            <input type="text" class="form-control jabatan" name="jabatan[]" />
        </div>
        <div class="col-lg-2 text-center">
            <label>Phone</label>
            <input type="text" class="form-control phone" name="phone[]" />
        </div>
        <div class="col-lg-2 text-center">
            <label>Email</label>
            <input type="text" class="form-control email" name="email[]" />
        </div>
        <div class="col-lg-2">
            <label>&nbsp;</label>
            <a class="btn btn-primary form-control" onclick="add_contact(this)"><i class="icon-plus3"></i>&nbsp;Tambah</a>
        </div>
    </div>
</div>
<br><br>