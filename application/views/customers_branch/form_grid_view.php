<div class="detail_supp">
    <div class="col-lg-12 detail_supp">
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Kode
            </div>
            <div class="col-lg-2 text-center">
                <input type="text" class="form-control" disabled />
            </div>
            <div class="col-lg-2">
                Nama Customer
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control" disabled />
            </div>
        </div>
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Alamat
            </div>
            <div class="col-lg-10">
                <textarea class="form-control" style="resize:vertical;"></textarea>
            </div>
        </div>
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Alamat Invoice
            </div>
            <div class="col-lg-10">
                <textarea class="form-control" style="resize:vertical;"></textarea>
            </div>
        </div>
        <div class="row display-flex-center">
            <div class="col-lg-2">
                NPWP
            </div>
            <div class="col-lg-4">
                <input type="text" class="form-control" disabled />
            </div>
            <div class="col-lg-2">
                FAX
            </div>
            <div class="col-lg-4">
                <input type="text" class="form-control" disabled />
            </div>
        </div>
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Limit Kredit
            </div>
            <div class="col-lg-2">
                <input type="text" class="form-control" disabled />
            </div>
            <div class="col-lg-2">
                Mata Uang
            </div>
            <div class="col-lg-2">
                <input type="text" class="form-control" disabled />
            </div>
            <div class="col-lg-2">
                Termin Pembayaran
            </div>
            <div class="col-lg-2">
                <input type="text" class="form-control" disabled />
            </div>
        </div>
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Kawasan Berikat
            </div>
            <div class="col-lg-2">
                <input type="text" class="form-control" disabled />
            </div>
            <div class="col-lg-3">
                PPN
            </div>
            <div class="col-lg-2">
                <input type="text" class="form-control" disabled />
            </div>
        </div>
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Keterangan
            </div>
            <div class="col-lg-10">
                <textarea class="form-control" id="ket" name="keterangan" style="resize:vertical;"></textarea>
            </div>
        </div>
    </div>

    <hr>
    <label><b>Contact</b></label>
    <div class="contact">
        <div class="row">
            <div class="col-lg-1 text-center">
                <label>No</label>
                <input type="text" class="form-control text-center" disabled />
            </div>
            <div+988 class="col-lg-3 text-center">
                <label>Nama</label>
                <input type="text" class="form-control nama_contact" disabled />
            </div+988>
            <div class="col-lg-3 text-center">
                <label>Jabatan</label>
                <input type="text" class="form-control jabatan" disabled />
            </div>
            <div class="col-lg-2 text-center">
                <label>Phone</label>
                <input type="text" class="form-control phone" disabled />
            </div>
            <div class="col-lg-3 text-center">
                <label>Email</label>
                <input type="text" class="form-control email" disabled />
            </div>
        </div>
    </div>
    <br><br>
</div>