<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <form id="formsearch" onSubmit="return false;">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button id="button_form_dropdown_search" type="button" class="btn dropdown-toggle btn-icon btn-default btn-raised" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu dropdown-content" style="width:475px;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>kategori</label>
                                        <select class="form-control" id="searchlokasi" name="searchlokasi">
                                            <option value="">Semua</option>
                                            <?php $query =  $this->m_global->get_loc_full();
                                            foreach ($query as $row) {
                                                echo '<option value ="' . $row->kode . '">' . $row->loc_nama . '</option>';
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Tipe PO</label>
                                        <select class="form-control" id="searchtipe" name="searchtipe">
                                            <option value="">Semua</option>
                                            <option value="1">Barang Produksi</option>
                                            <option value="2">Mesin Dan Peralatan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>suppliers</label><br>
                                        <select class="form-control searchsupp" name="searchsupp">
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>dari tanggal</label>
                                        <input type="text" class="form-control date_picker" id="searchDateFirst" name="searchDateFirst" readonly value="<?= date('Y-m-d', strtotime('- 1 month')); ?>" />
                                    </div>
                                    <div class="col-lg-6">
                                        <label>sampai tanggal</label>
                                        <input type="text" class="form-control date_picker" id="searchDateFinish" name="searchDateFinish" readonly value="<?= date('Y-m-d'); ?>" />
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                    <div class="col-lg-8">
                                    </div>
                                    <div class="col-lg-2">
                                        <button id="buttonPencarian" type="button" class="btn btn-default btn-raised"><b>&nbsp;&nbsp;&nbsp;<i class="icon-search4"></i> &nbsp;&nbsp;&nbsp;Search</b></button>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                    <input type="text" class="form-control" style="width:250px;" placeholder="Pencarian" id="search_keyword" name="search_keyword">

                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default btn-raised" onClick="input_data()"><b><i class="icon-plus-circle2"></i></b> Purchase Order</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <table id="datamain_datatable" style="width:100%" class="table datatable-ajax table-xxs text-size-small table-bordered">
        <thead>
            <tr>
                <th>no</th>
                <th>Tipe</th>
                <th>No Referensi</th>
                <th>Nama Supplier</th>
                <th>Lokasi</th>
                <th>Request</th>
                <th>Tanggal Pesan</th>
                <th>Total</th>
                <th><i class="icon-menu7 position-left"></i></th>
            </tr>
        </thead>
    </table>
</div>



<?php echo $insert_view; ?>