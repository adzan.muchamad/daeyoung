<input type="hidden" id="id" name="id" />
<div class="row ">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Supplier
        </div>
        <div class="col-lg-10">
            <select class="form-control searchsupp" id="nama_supp" name="nama_supp" onchange="cek_matauang()"></select>
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Kredit Saat Ini
        </div>
        <div class="col-lg-4 kredit_val">
            <a class="form-control text-primary" style="border: 0px">sdasd</a>
        </div>
        <div class="col-lg-2">
            Tipe PO
        </div>
        <div class="col-lg-4">
            <select class="form-control" name="tipe" id="tipe">
                <option value="1">Barang Produksi</option>
                <option value="2">Peralatan Kantor</option>
            </select>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Tanggal
        </div>
        <div class="col-lg-4">
            <input type="text" class="form-control date_picker" id="tgl_order" name="tgl_order" value="<?= date('Y-m-d'); ?>" />
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Requestor
        </div>
        <div class="col-lg-4">
            <input type="text" class="form-control" id="req" name="req" />
        </div>
        <div class="col-lg-2">
            PPN
        </div>
        <div class="col-lg-4">
            <select class="form-control" name="ppn" id="ppn" onchange="count_all()">
                <option value="0">Tidak</option>
                <option value="1">Iya</option>
            </select>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Mata Uang
        </div>
        <div class="col-lg-4">
            <select class="form-control" id="mata_uang" disabled onchange="get_matauang(this);">
                <?php $res = $this->m_global->get_currency();
                foreach ($res as $row) {
                    echo '<option value="' . $row->id . '">' . $row->nama . '</option>';
                } ?>
            </select>
        </div>
    </div>

    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Diterima
        </div>
        <div class="col-lg-4">
            <select class="form-control" name="lokasi" id="lokasi" onchange="get_alamat();">
                <option value=""></option>
                <?php $res = $this->m_global->get_loc();
                foreach ($res as $row) {
                    echo '<option value="' . $row->id . '">' . $row->loc_nama . '</option>';
                }
                ?>
            </select>
        </div>
        <div class="col-lg-2">
            Pajak
        </div>
        <div class="col-lg-4">
            <input type="hidden" name="pph" id="pph" />
            <select class="form-control" name="kd_pajak" id="kd_pajak" onchange="count_all()">
                <option value="0">Tidak</option>
                <option value="1">PPh 23</option>
                <option value="2">PPh 4 ayat 2</option>
            </select>
        </div>
    </div>

</div>
<div class="row ">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2  nilai">
            Nilai Tukar
        </div>
        <div class="col-lg-4  nilai">
            <?php $res = $this->m_global->get_kurs_bi();
            echo '<input type="text" class="form-control" id="nilai" name="nilai" value="' . number_format($res->rate, 2, ',', '.') . '" />';
            ?>

        </div>
        <div class="col-lg-2  nilai">
            = 1 USD
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Dikirim Ke
        </div>
        <div class="col-lg-10">
            <input type="text" class="form-control" id="alamat" readonly />
        </div>
    </div>
</div>
<hr style="margin: 0px;margin-bottom:10px">
<center style="margin-bottom:10px"><b>BARANG ORDER</b></center>
<div class="list_barang">
    <div class="row">
        <div class="col-lg-1 text-center" style="width:5%">
            <label>No</label>
            <input class="form-control no_urut text-center" type="text" readonly />
        </div>
        <div class="col-lg-4 text-center">
            <label>Barang</label>
            <select class="form-control barang" name="barang[]" onchange="get_satuan(this);">
            </select>
        </div>
        <div class="col-lg-1 text-center">
            <label>Qty</label>
            <input class="form-control qty" type="text" name="qty[]" onkeyup="count_jmlh(this);" />
        </div>
        <div class="col-lg-1 text-center">
            <label>Satuan</label>
            <select name="satuan[]" class="form-control satuan">
            </select>
        </div>
        <div class="col-lg-1 text-center">
            <label>Tanggal Pengiriman</label>
            <input type="text" class="form-control date_picker tgl_kirim" name="tgl_kirim[]" value="<?= date('Y-m-d'); ?>" />
        </div>
        <div class="col-lg-1 text-center" style="width: 11.5%;">
            <label>Harga</label>
            <input class="form-control harga text-right" type="text" name="harga[]" onkeyup="count_jmlh2(this);" />
        </div>
        <div class="col-lg-2 text-center">
            <label>Jumlah Harga</label>
            <input class="form-control text-right jumlah" type="text" name="jumlah[]" readonly />
        </div>
        <div class="col-lg-1 text-center">
            <label>Action</label>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-2 text-center" style="float:right">
        <a class="btn btn-primary form-control" onclick="add_barang(this)"><i class="icon-plus3"></i>&nbsp;Tambah</a>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Sub Total
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="jumlah_harga" name="jumlah_harga" readonly />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Diskon
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="diskon" name="diskon" onkeyup="count_all()" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Pajak
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="pajak" readonly />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            <b>Jumlah Total</b>
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="total" name="total" readonly />
        </div>
    </div>
</div>