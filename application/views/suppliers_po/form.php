<?php
$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_supplier';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i><span id="title_modal_flexible"></span>';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('suppliers_po/form_grid', $data, TRUE);
echo $this->ui->load_component($options);


$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_supplier_detail';
$options['modal_size'] = 'modal-lg';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Detail Supplier <span id="title_modal_flexible"></span>';
$options['modal_footer'] = 'yes';
$options['form_id'] = 'formulir_modal';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('suppliers_po/form_grid_view', $data, TRUE);
echo $this->ui->load_component($options);
