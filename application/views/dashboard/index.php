<div class="row" id="widget_container">
	<?php
	// pre($widget_lists);
	if (!empty($widget_lists)) :
		foreach ($widget_lists as $widget) :
			// pre($widget);
			$widget_code = $widget['code'];
			$widget_detail = $this->dashboard->widget_detail($widget_code);

			switch ($widget['component']) {
				case 'panel_pill':
					$options['component'] = 'component/panel/panel_pill';
					$options['panel_icon'] = $widget_detail['icon'];
					$options['panel_title'] = $widget_detail['name'];
					$options['panel_action'] = array();
					$options['panel_padding'] = $widget_detail['padding'];
					$options['max'] = $widget_detail['max_tab'];
					$options['selected_tab'] = $widget_detail['selected_tab'];
					$options['tabs'] = unserialthis($widget_detail['tabs']);
					$options['panel_content'] = '<div id=""></div>';
					break;

				case 'panel_custom_action':
					$options['component'] = 'component/panel/panel_custom_action';
					$options['panel_icon'] = $widget_detail['icon'];
					$options['panel_title'] = $widget_detail['name'];
					$options['panel_action'] = $this->load->view('dashboard/panel_custom_action/statistik_pekerjaan', '', TRUE);
					$options['panel_padding'] = $widget_detail['padding'];
					$options['panel_content'] = '<div id="' . $widget['code'] . '"></div>';
					break;

				default:
					$options['component'] = 'component/panel/panel_default';
					$options['panel_icon'] = $widget_detail['icon'];
					$options['panel_title'] = $widget_detail['name'];
					$options['panel_action'] = array();
					$options['panel_padding'] = $widget_detail['padding'];
					$options['panel_content'] = '<div id="' . $widget['code'] . '"></div>';
					break;
			}

	?>
			<div class="widget_item <?php echo $widget['column_width']; ?>">
				<?php echo $this->ui->load_component($options); ?>
			</div>
	<?php

			if (is_file(APPPATH . 'views/dashboard/ext/' . $widget_code . '.php')) :
				// $this->load->view($my_view);
				echo $this->load->view('dashboard/ext/' . $widget_code, '', TRUE);
			endif;

		endforeach;
	endif;
	?>
</div>

<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<div class="panel panel-white" style="padding-top: 50px;padding-bottom:150px;">
	<table border="0" width="100%">
		<tbody>
			<tr>
				<td width="50%" class="menu_group">
					<font size="25px" color="red">
						<center><b>Selamat Datang di Dae Young IT System<br><br></b></center>
						<div><img style="display: block; margin: 0 auto; text-align: center; width: 566px; height: 400px;" src="./assets/images/logo_h.png">
						</div>
					</font>
				</td>
			</tr>
		</tbody>
	</table>
</div>