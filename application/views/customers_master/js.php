<script type="text/javascript">
	$(function() {
		var pageUri = $('#pageUri').val();

		//SET DATATABLES
		$.extend($.fn.dataTable.defaults, {
			//autoWidth: true,
			"sLength": "form-control"
		});
		var height_windows = $(window).height();
		var height_navbar = $('#navbar-main').height();
		var height_page = $('#page-header').height();
		var height_table = height_windows - height_navbar - height_page - 370;
		var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

		table = $('#datamain_datatable').DataTable({
			dom: 'J<"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
			"pageLength": 50,
			aaSorting: [
				[1, 'asc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_data_table',
				"type": "POST",
				"data": function(data) {
					data.search_keyword = $('#search_keyword').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},

			columnDefs: [{
				orderable: false,
				width: '150px',
				targets: [0, tabel_kolom]
			}, {
				className: "text-center",
				"targets": [tabel_kolom, 0]
			}],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
			scrollX: '100%',
			scrollY: height_table,
			scrollCollapse: false,
			fixedColumns: {
				leftColumns: 0,
				rightColumns: 1
			},
		});

		$('#buttonPencarian').click(function() {
			table.ajax.reload(null, false);
			$('#button_form_dropdown_search').next().toggle();
		});
		$('#search_keyword').keyup(function() {
			table.ajax.reload(null, false);
		});
		$('.dataTables_length select').select2({
			minimumResultsForSearch: Infinity,
			width: 'auto',
		});
		//END SET DATATABLES

		$("#formulir_modal").validate({
			rules: {
				nama_barang: {
					required: true
				},
			},
			submitHandler: function(form) {
				if ($('.respon_kode').text() != 'Kode Tidak Tersedia') {
					var cekTransaksi = $("#formulir_modal").attr('transaksi');
					if (cekTransaksi == 'tambah') {
						$.ajax({
							type: 'POST',
							url: pageUri + 'insert_data',
							data: $('#formulir_modal').serialize(),
							beforeSend: function() {
								blockUI();
							},
							success: function(response) {
								if (response == '1') {
									$('#formulir_modal').clearForm();
									$('#modal_customer').modal('hide');
									toastr.success('Data berhasil disimpan');
									table.ajax.reload(null, false);

								} else if (response == '0') {
									toastr.warning('Data gagal disimpan');
								} else {
									toastr.error(response);
								}
								unBlockUI();
							}
						});
					} else if (cekTransaksi == 'edit') {
						$.ajax({
							type: 'POST',
							url: pageUri + 'update_data',
							data: $('#formulir_modal').serialize(),
							beforeSend: function() {
								blockUI();
							},
							success: function(response) {
								if (response == '1') {
									toastr.success('Data berhasil diupdate');
									table.ajax.reload(null, false);
									$('#modal_customer').modal('hide');
								} else if (response == '0') {
									toastr.warning('Data gagal diupdate');
								} else {
									toastr.error(response);
								}
								unBlockUI();
							}
						});
					}
				} else {
					toastr.warning('Kode Tidak Tersedia');
				}
				return false;
			}
		});

		$('#npwp').mask('00.000.000.0-000.000', {
			reverse: true
		});

		$('#kode').mask('000000');

	});

	function replaceAll(str, token, newToken, ignoreCase) {
		var _token;
		var i = -1;

		if (typeof token === "string") {

			if (ignoreCase) {

				_token = token.toLowerCase();

				while ((
						i = str.toLowerCase().indexOf(
							_token, i >= 0 ? i + newToken.length : 0
						)) !== -1) {
					str = str.substring(0, i) +
						newToken +
						str.substring(i + token.length);
				}

			} else {
				return str.split(token).join(newToken);
			}

		}
		return str;
	};

	function formatMoney(amount, decimalCount = 4, decimal = ",", thousands = ".") {
		try {
			decimalCount = Math.abs(decimalCount);
			decimalCount = isNaN(decimalCount) ? 4 : decimalCount;

			const negativeSign = amount < 0 ? "-" : "";

			let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
			let j = (i.length > 3) ? i.length % 3 : 0;

			return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
		} catch (e) {
			console.log(e)
		}
	}

	function set_input() {
		$("#limit").inputmask({
			'alias': 'decimal',
			'groupSeparator': ',',
			'autoGroup': true,
			'radixPoint': ",",
			'min': 0,
		});
	}

	function cek_kode() {
		$('#kode').keyup(function() {
			var pageUri = $('#pageUri').val();
			var id = $('#id').val();
			var kode = $('#kode').val();
			if (kode == null || kode == 'undefined' || kode == '') {
				$('.respon_kode').text('');
				$('.respon_kode').removeClass('text-success');
				$('.respon_kode').removeClass('text-warning');
			} else {
				$.ajax({
					type: 'POST',
					url: pageUri + 'cek_kode',
					data: {
						id: id,
						kode: kode
					},
					success: function(response) {
						if (response != '') {
							$('.respon_kode').text('Kode Tidak Tersedia');
							$('.respon_kode').removeClass('text-success');
							$('.respon_kode').addClass('text-warning');
						} else {
							$('.respon_kode').text('Kode Tersedia');
							$('.respon_kode').removeClass('text-warning');
							$('.respon_kode').addClass('text-success');

						}
					}
				});
			}
		});
	}

	function pemberian_no() {
		var no_urut = parseInt(1);
		var tObj = document.getElementsByClassName('no_urutan');
		for (var i = 0; i < tObj.length; i++) {
			tObj[i].value = no_urut;
			no_urut += parseInt(1);
		}
	};

	function pemberian_no2() {
		var no_urut = parseInt(1);
		var tObj = document.getElementsByClassName('no_urutan2');
		for (var i = 0; i < tObj.length; i++) {
			tObj[i].value = no_urut;
			no_urut += parseInt(1);
		}
	};


	function add_contact(obj) {
		$(obj).closest('.contact').append('<div class="row"><div class="col-lg-1 text-center"><input type="text" class="form-control text-center no_urutan2" disabled /></div><div class="col-lg-3 text-center"><input type="text" class="form-control nama_contact" name="nama_contact[]" /></div><div class="col-lg-2 text-center"><input type="text" class="form-control jabatan" name="jabatan[]" /></div><div class="col-lg-2 text-center"><input type="text" class="form-control phone" name="phone[]" /></div><div class="col-lg-2 text-center"><input type="text" class="form-control email" name="email[]" /></div><div class="col-lg-2"><a class="btn btn-warning form-control" onclick="delete_contact(this)">Hapus</a></div></div>');
		pemberian_no2();
	}

	function delete_contact(obj) {
		$(obj).closest('.row').remove();
		pemberian_no2();
	}

	function delete_bank(obj) {
		$(obj).closest('.row').remove();
		pemberian_no();
	}

	function input_data() {
		$('#id').val('');
		$('#formulir_modal').clearForm();
		$("#formulir_modal").attr('transaksi', 'tambah');
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Tambah');
		$('#formSearch').dialog('close');
		$('#modal_customer').modal('show');
		$('.contact').html('').append('<div class="row"><div class="col-lg-1 text-center"><label>No</label><input type="text" class="form-control no_urutan2" disabled /></div><div class="col-lg-3 text-center"><label>Nama</label><input type="text" class="form-control nama_contact" name="nama_contact[]" /></div><div class="col-lg-2 text-center"><label>Jabatan</label><input type="text" class="form-control jabatan" name="jabatan[]" /></div><div class="col-lg-2 text-center"><label>Phone</label><input type="text" class="form-control phone" name="phone[]" /></div><div class="col-lg-2 text-center"><label>Email</label><input type="text" class="form-control email" name="email[]" /></div><div class="col-lg-2"><label>&nbsp;</label><a class="btn btn-primary form-control" onclick="add_contact(this)">Tambah</a></div></div>');
		pemberian_no();
		pemberian_no2();
		set_input();
		cek_kode();
		return false;
	}


	function delete_data(id) {
		var pageUri = $('#pageUri').val();

		swal({
			title: "Anda Yakin?",
			text: "Anda akan hapus data ini.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes!",
			closeOnConfirm: false
		}, function() {
			$.ajax({
				type: 'GET',
				url: pageUri + 'delete_data/' + id,
				beforeSend: function() {
					blockUI();
				},
				success: function(response) {
					if (response == '1') {
						table.ajax.reload(null, false);
						toastr.success('Data berhasil dihapus');
					} else if (response == '0') {
						toastr.error('Data gagal dihapus');
					} else {
						toastr.warning(response);
					}
					swal.close();
					unBlockUI();
				}
			});
		});
	};

	function update_data(id) {
		var pageUri = $('#pageUri').val();
		$('#id').val(id);
		$("#formulir_modal").attr('transaksi', 'edit');
		$.ajax({
			type: 'POST',
			data: $('#formulir_modal').serialize(),
			url: pageUri + 'select_data/' + id,
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#nama_cust').val(html.data[0].nama);
					$('#kode').val(html.data[0].kode);
					// $('#mata_uang').val(html.data[0].mata_uang).change();
					$('#alamat').val(html.data[0].alamat);
					$('#ket').val(html.data[0].keterangan);
					cek_kode();
					$('#kode').keyup();
					$('#modal_customer').modal('show');
				} else if (html == '0') {
					toastr.error('Data gagal dibuka');
				} else {
					toastr.warning(html);
				}
				unBlockUI();
			},
		});
	}

	function view_data(id) {
		var pageUri = $('#pageUri').val();
		$.ajax({
			type: 'GET',
			url: pageUri + 'view_data/' + id,
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				$('.detail_supp').html('').append(html);
				$('#modal_customer_detail').modal('show');
				unBlockUI();
			},
		});
	}
</script>