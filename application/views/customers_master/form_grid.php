<input type="hidden" id="id" name="id" />
<label><b>Detail Customers</b></label>
<div class="row">
    <div class="col-lg-8 text-center">
        <label>Nama Customer</label>
        <input type="text" class="form-control" id="nama_cust" name="nama_cust" />
    </div>
    <div class="col-lg-4 text-center">
        <label>Kode</label>
        <input type="text" class="form-control" id="kode" name="kode" /><a class="respon_kode"></a>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <label>Alamat</label>
        <textarea class="form-control" id="alamat" name="alamat" style="resize:vertical;"></textarea>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        <label>Keterangan</label>
        <textarea class="form-control" id="ket" name="keterangan" style="resize:vertical;"></textarea>
    </div>
</div>