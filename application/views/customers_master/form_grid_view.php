<div class="detail_supp">
    <div class="col-lg-12 detail_supp">
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Kode
            </div>
            <div class="col-lg-2 text-center">
                <input type="text" class="form-control" disabled />
            </div>
            <div class="col-lg-2">
                Nama Customer
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control" disabled />
            </div>
        </div>
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Alamat
            </div>
            <div class="col-lg-10">
                <textarea class="form-control" style="resize:vertical;"></textarea>
            </div>
        </div>
        <div class="row display-flex-center">
            <div class="col-lg-2">
                Keterangan
            </div>
            <div class="col-lg-10">
                <textarea class="form-control" id="ket" name="keterangan" style="resize:vertical;"></textarea>
            </div>
        </div>

    </div>
</div>