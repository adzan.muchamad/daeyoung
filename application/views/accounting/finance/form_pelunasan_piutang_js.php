<script>
    base_url = '<?php echo base_url()?>';

    $(".format_uang").autoNumeric();

    dt_detail = $("#dt_detail").DataTable({
        responsive:true
    });

    dt_customer = $("#dt_customer").DataTable({
        responsive:true
    });
    
    $("#btn-customer").click(function() {
        $("#modal_customer").modal("show");
    });
    
    dt_invoice = $("#dt_invoice").DataTable({
        responsive:true
    });

    $("#btn-invoice").click(function() {
        $("#modal_invoice").modal("show");
    });
</script>