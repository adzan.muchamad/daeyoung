<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <!--  -->
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <div class="row">
            <div class="col-md-12 form-inline">
                <div class="form-group">
                    <a href="<?php echo isset($url_action) ? $url_action:'';?>" class="btn btn-success btn-sm" title="Tambah Data"><i class="fa fa-plus"></i> Tambah Data</a>                                            
                </div> 
                <div class="form-group">
                    <input type="text" class="form-control date_picker" name="date_start" id="date_start" value="<?php echo isset($tanggal) ? $tanggal:'';?>" data-date-format="dd/mm/yyyy" placeholder="Dari Tanggal"> 
                </div>
                <div class="form-group">
                    <input type="text" class="form-control date_picker" name="date_end" id="date_end" value="<?php echo isset($tanggal) ? $tanggal:'';?>" data-date-format="dd/mm/yyyy" placeholder="Sampai Tanggal">
                </div>
                <div class="form-group">
                    <button onclick="loadtable()" class="btn btn-primary btn-sm" title="Tampilkan Data"><i class="fa fa-search"></i> Tampilkan Data</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <table id="dt-table" class="table datatable-ajax table-xxs text-size-small table-bordered" style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No. Bukti</th>
                            <th>Tanggal</th>
                            <th>Customer</th>
                            <th>Total</th>
                            <th>Crbayar</th>
                            <th>Kas/Bank</th>
                            <th>Keterangan</th>
                            <th>#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>