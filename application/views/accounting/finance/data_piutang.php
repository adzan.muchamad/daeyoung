<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <!--  -->
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <div class="row">
            <div class="col-md-12 form-inline">
                <div class="form-group">
                    <label>Short Tanggal</label>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="date_start">Tanggal Mulai</label>
                    <input type="text" class="form-control date_picker" name="date_start" id="date_start" placeholder="Dari Tanggal" value="<?php echo date('Y-m-d');?>"> 
                </div>
                <div class="form-group">
                    <label class="sr-only" for="date_end">Tanggal Selesai</label>
                    <input type="text" class="form-control date_picker" name="date_end" id="date_end" placeholder="Sampai Tanggal" value="<?php echo date('Y-m-d');?>"> 
                </div>
                <div class="form-group">
                    <button type="button" id="btn-tampil" class="btn btn-primary btn-sm">Tampilkan</button>
                    <button type="button" id="btn-bayar" class="btn btn-success btn-sm">Bayar</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <table id="dt-table" class="table datatable-ajax table-xxs text-size-small table-bordered" style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No. Invoice</th>
                            <th>Tanggal</th>
                            <th>Customer</th>
                            <th>Tgl Tempo</th>
                            <th>Umur <sup>hr</sup></th>
                            <th>Piutang</th>
                            <th>Bayar</th>
                            <th>Kekurangan</th>
                            <th>#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>