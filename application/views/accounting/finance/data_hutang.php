<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <!--  -->
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <div class="row">
            <div class="col-md-12">
                <h6>Rekap By Supplier</h6>
                <table id="dt-grup" class="table datatable-ajax table-xxs text-size-small table-bordered" style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Kode</th>
                            <th>Supplier</th>
                            <th>Total Hutang</th>
                            <th>Total Terbayar</th>
                            <th>Kekurangan</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <h6>Detail Nota Supplier</h6>
                <table id="dt-detail" class="table datatable-ajax table-xxs text-size-small table-bordered" style="width:100%;">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No. Invoice</th>
                            <th>No. Invoice Supp</th>
                            <th>Tanggal</th>
                            <th>Tgl. Invoice Supp</th>
                            <th>Supplier</th>
                            <th>Tgl Tempo</th>
                            <th>Umur <sup>hr</sup></th>
                            <th>Hutang</th>
                            <th>Terbayar</th>
                            <th>Kekurangan</th>
                            <th>#</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>