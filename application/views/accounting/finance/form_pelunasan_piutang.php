<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <!--  -->
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <form id="form-data">
            <div class="row">
                <input type="hidden" name="id" id="id" value="<?php echo isset($edit->id) ? $edit->id:'';?>" readonly>
                <!-- left side -->
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-4 text-right">Tanggal*</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control date_picker" name="tanggal" id="tanggal" placeholder="..." readonly>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 text-right">No. Pelunasan*</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="nobukti" id="nobukti" placeholder="..." readonly>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 text-right">Customer*</label>
                        <div class="col-md-8">
                            <div class="input-group input-group">
                                <input type="hidden" name="kdcus" id="kdcus" readonly>
                                <input type="text" class="form-control" id="customer" name="customer" placeholder="..." readonly>
                                <span class="input-group-btn">
                                    <a class="btn btn-info btn-sm" type="button" id="btn-customer" title="Cari">Cari</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 text-right">Keterangan</label>
                        <div class="col-md-8">
                            <textarea rows="4" class="form-control" name="keterangan" id="keterangan" placeholder="..."></textarea>
                        </div>
                    </div>
                </div>
                <!-- right side -->
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-3 text-right"> Cara Bayar*</label>
                        <div class="col-md-2">
                            <div class="mt-radio-list">
                                <label class="mt-radio mt-radio-line">
                                    <input type="radio" name="optionsbayar" id="optionsbayar" class="crbayar" value="T">&nbsp;Tunai
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="mt-radio-list">
                                <label class="mt-radio mt-radio-line">
                                    <input type="radio" name="optionsbayar" id="optionsbayar" class="crbayar" value="TF">&nbsp;Transfer
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-3 text-right">Kode Akun*</label>
                        <div class="col-md-6">
                            <input type="hidden" name="edit_akun" id="edit_akun" readonly>
                            <?php echo form_dropdown('kodeakun', array(), '','id="kodeakun" class="form-control input-sm"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <!-- detail form -->
            <div class="row">
                <input type="hidden" name="id_detail" id="id_detail" readonly>
                <!-- left side -->
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-4 text-right">No. Invoice*</label>
                        <div class="col-md-6">
                            <div class="input-group input-group">
                                <input type="text" class="form-control" id="no_invoice" name="no_invoice" placeholder="..." readonly>
                                <span class="input-group-btn">
                                    <a class="btn btn-info btn-sm" type="button" id="btn-invoice" title="Cari">Cari</a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 text-right">Detail Invoice</label>
                        <div class="col-md-8">
                            <textarea rows="5" class="form-control" name="detail_invoice" id="detail_invoice" placeholder="..." readonly></textarea>
                        </div>
                    </div>
                </div>
                <!-- right side -->
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-3 text-right">Jumlah Bayar*</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control text-right format_uang" name="bayar" id="bayar" placeholder="...">
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6 col-md-offset-2">
                    <button type="button" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="<?php echo base_url()?>Pelunasan_piutang" class="btn btn-default btn-sm">Kembali</a>
                    <a href="<?php echo base_url()?>Pelunasan_piutang_form" class="btn btn-success btn-sm">Baru</a>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <h6>Detail No. Invoice Pelunasan</h6>
                    <table id="dt_detail" class="table datatable-ajax table-xxs text-size-small table-bordered" style="width: 100%;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>No. Invoice</th>
                                <th>Detail Invoice</th>
                                <th>Jumlah Bayar</th>
                                <th>Sisa Piutang</th>
                                <th>#</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- start modal -->
<div class="modal fade bs-modal-lg" id="modal_customer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Data Customer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="dt_customer" class="table datatable-ajax table-xxs text-size-small table-bordered" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Kode</th>
                                    <th>Customer</th>
                                    <th>Alamat</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-lg" id="modal_invoice" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Data Invoice Customer</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table id="dt_invoice" class="table datatable-ajax table-xxs text-size-small table-bordered" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No Invoice</th>
                                    <th>Tanggal</th>
                                    <th>Keterangan</th>
                                    <th>Total</th>
                                    <th>Terbayar</th>
                                    <th>Kekurangan</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                 <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
<!-- end modal -->