<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default btn-raised btn-sm" id="add_form"><b><i class="icon-plus-circle2"></i></b> Tambah</button>
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <div class="row">
            <div class="col-md-12">
                <table id="tb-data" class="table datatable-ajax table-xxs text-size-small table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Grup No.</th>
                            <th>Kode Akun</th>
                            <th>Nama Akun</th>
                            <th>Mekanisme</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="modal_form" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Form COA Category</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-data" method="POST">
                            <input type="text" class="hidden" name="id" id="id">
                            <div class="form-group">
                                <label>Group Number*</label>
                                <div>
                                    <input type="number" min="0" class="form-control" name="grup" id="grup" placeholder="Group Number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Kode Account*</label>
                                <div>
                                    <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode Account">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nama Account*</label>
                                <div>
                                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Account">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Mekanisme*</label>
                                <div>
                                    <?= form_dropdown('mekanisme', [
                                        '' => ' - - ',
                                        '0'=> 'Debet (+), Kredit (-)',
                                        '1'=> 'Debet (-), Kredit (+)'
                                    ], '','class="form-control" id="mekanisme"');?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" onclick="save()" id="btn-simpan" class="btn btn-success">Simpan</button>
            </div>
        </div>
    </div>
</div>