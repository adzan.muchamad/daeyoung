<script>
    base_url = '<?php echo base_url()?>';
    var menu_tree = $("#tree_coa").jstree({
        plugins: ["types"],
        core: {
            themes: {
                responsive: !1
            },
            data : <?php echo isset($tree_coa) ? $tree_coa:"";?>
        },
        types: {
            "default": {
                icon: "fa fa-folder icon-state-warning icon-lg"
            },
            file: {
                icon: "fa fa-file icon-state-warning icon-lg"
            }
        }
    });

    function clearForm() {
        $("#id").val("");
        $("#kode").val("");
        $("#nama").val("");
        $("#grup").val("");
        $("#parent").val("");
    }

    $("#add_form").click(function() {
        $("#modal_form").modal('show');
        clearForm();
        opt_parent();
    });

    $("#parent").append($("<option value=''> - Parent - </option>"));
    function opt_parent(edit) {
        $("#parent").val("").trigger("change");
        $("#parent").find('option').remove().end();
        $.ajax({
            url:base_url+"Coa_kas_bank/ajax_coa_parent",
            type:"GET",
            dataType:"JSON",
            beforeSend:function() {
                $("#parent").attr('readonly',true).append($("<option></option>").text("Loading").val("x"));
            },
            complete:function() {
                $("#parent").attr('readonly',false).find('option[value="x"]').remove();
            },
            success:function(jsonResult) {
                $("#parent").attr("enabled","true");
                if (jsonResult != null) {
                    $("#parent").append($("<option></option>").text("- Parent - ").val(""));
                    $.each(jsonResult, function(){
                        $("#parent").append(
                            $("<option></option>").text(this.namaakun).val(this.kodeakun)
                        );
                    });
                    if(edit != '') { $("#parent").val(edit).trigger('change'); }
                }
            },
            error:function(err) {
                console.log(err);
            }
        });
    }

    function get_parent_id(kode) {
        $.ajax({
            url:base_url+"Coa_kas_bank/ajax_coa_parent_id/"+kode,
            type:"GET",
            dataType:"JSON",
            success:function(r) {
                $("#grup").val(r.grup);
            },
            error:function(err) {
                console.log(err);
            }
        });
    }

    function save() {
        $.ajax({
            url:base_url+"Coa_kas_bank/ajax_save",
            type:"POST",
            data:$("#form-data").serialize(),
            dataType:"JSON",
            beforeSend:function() {
                $("#btn-simpan").attr('disabled',true).html('Menyimpan ...');
            },
            complete:function() {
                $("#btn-simpan").attr('disabled',false).html('Simpan');
            },
            success:function(r) {
                if(r.status == true) {
                    toastr.success(r.message);
                    $("#modal_form").modal('hide');
                    clearForm();
                    window.location.reload();
                } else {
                    toastr.error(r.message);
                }
            },
            error:function(err) {
                console.log(err);
            }
        });
    }

    function edit(id) {
        $.ajax({
            url:base_url+"Coa_kas_bank/ajax_edit/"+id,
            type:"GET",
            dataType:"JSON",
            beforeSend:function() {
                blockUI();
            },
            complete:function() {
                unBlockUI();
            },
            success:function(r) {
                $("#modal_form").modal('show');
                $("#id").val(r.id);
                $("#parent").val(r.parent);
                $("#kode").val(r.kodeakun);
                $("#nama").val(r.namaakun);
                $("#flag").val(r.flag);
                opt_parent(r.parent);
            },
            error:function(err) {
                console.log(err);
            }
        });
    }

    function hapus(id) {
        swal({
			title: "Anda Yakin?",
			text: "Anda akan hapus data ini.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes!",
			closeOnConfirm: false
		}, function() {
            $.ajax({
                url :base_url+"Coa_kas_bank/ajax_hapus/"+id,
                type : "GET",
                dataType : "JSON",
                beforeSend:function() {
                    blockUI();
                },
                complete:function() {
                    unBlockUI();
                },
                success:function(data) {
                    if(data.status == true) {
                        toastr.success(data.message);
                        window.location.reload();
                    } else {
                        toastr.error(data.message);
                    }
                    swal.close();
                },
                error:function(error) {
                    console.log(error);
                }
            });
		});
    }
</script>