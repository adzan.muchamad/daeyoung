<script>
    base_url = '<?php echo base_url()?>';
    var table = $("#tb-data").DataTable({
            ajax : {
                url : base_url+"Coa_category/ajax_data_coa_category",
                type : "GET"
            },
            responsive:true,
            processing:true,
            bDestroy:true,
            columns : [
                    {data : 'no'},
                    {data : 'grup', class:'text-center'},
                    {data : 'kodeakun', class:'text-center'},
                    {data : 'namaakun'},
                    {data : 'mekanisme'},
                    {data : 'aksi'}
                ],
            scrollY:"300px",
            iDisplayLength: 100
        });
    
    function clearForm() {
        $("#id").val("");
        $("#kode").val("");
        $("#nama").val("");
        $("#grup").val("");
        $("#mekanisme").val("");
    }

    $("#add_form").click(function() {
        $("#modal_form").modal('show');
        clearForm();
    });

    function save() {
        $.ajax({
            url:base_url+"Coa_category/ajax_save",
            type:"POST",
            data:$("#form-data").serialize(),
            dataType:"JSON",
            beforeSend:function() {
                $("#btn-simpan").attr('disabled',true).html('Menyimpan ...');
            },
            complete:function() {
                $("#btn-simpan").attr('disabled',false).html('Simpan');
            },
            success:function(r) {
                table.ajax.url(base_url+'Coa_category/ajax_data_coa_category').load();
                if(r.status == true) {
                    toastr.success(r.message);
                    $("#modal_form").modal('hide');
                    clearform();
                } else {
                    toastr.error(r.message);
                }
            },
            error:function(err) {
                console.log(err);
            }
        });
    }

    function edit(id) {
        $.ajax({
            url:base_url+"Coa_category/ajax_edit/"+id,
            type:"GET",
            dataType:"JSON",
            beforeSend:function() {
                blockUI();
            },
            complete:function() {
                unBlockUI();
            },
            success:function(r) {
                $("#modal_form").modal('show');
                $("#id").val(r.id);
                $("#grup").val(r.grup);
                $("#kode").val(r.kodeakun);
                $("#nama").val(r.namaakun);
                $("#mekanisme").val(r.mekanisme);
            },
            error:function(err) {
                console.log(err);
            }
        });
    }

    function hapus(id) {
        swal({
			title: "Anda Yakin?",
			text: "Anda akan hapus data ini.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes!",
			closeOnConfirm: false
		}, function() {
            $.ajax({
                url :base_url+"Coa_category/ajax_hapus/"+id,
                type : "GET",
                dataType : "JSON",
                beforeSend:function() {
                    blockUI();
                },
                complete:function() {
                    unBlockUI();
                },
                success:function(data) {
                    if(data.status == true) {
                        toastr.success(data.message);
                        table.ajax.url(base_url+'Coa_category/ajax_data_coa_category').load();
                    } else {
                        toastr.error(data.message);
                    }
                    swal.close();
                },
                error:function(error) {
                    console.log(error);
                }
            });
		});
    }
</script>