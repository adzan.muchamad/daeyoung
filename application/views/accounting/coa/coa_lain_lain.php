<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <button type="button" class="btn btn-default btn-raised btn-sm" id="add_form"><b><i class="icon-plus-circle2"></i></b> Tambah</button>
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <div class="row">
            <div class="col-md-12" style="height: 350px;overflow: auto;">
                <div id="tree_coa" class="tree-demo"> </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="modal_form" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Form COA LAIN-LAIN</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-data" method="POST">
                            <input type="text" class="hidden" name="id" id="id">
                            <div class="form-group">
                                <label>Parent <span class="text-danger">*</span></label>
                                <div>
                                    <?php echo form_dropdown('parent', array(), '', 'class="form-control input-sm" onchange="get_parent_id(this.value)" id="parent"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Account Number <span class="text-danger">*</span></label>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="grup" id="grup" readonly="" style="width: 80px;">
                                    </div>
                                    <div class="form-group">
                                        -
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nama <span class="text-danger">*</span></label>
                                <div>
                                    <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Account">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Kategori <span class="text-danger">*</span></label>
                                <div>
                                    <select class="form-control" name="flag" id="flag">
                                        <option value="lain-lain">Lain-lain</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" onclick="save()" id="btn-simpan" class="btn btn-success">Simpan</button>
            </div>
        </div>
    </div>
</div>