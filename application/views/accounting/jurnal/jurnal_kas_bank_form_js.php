<script>
    $(document).ready(function() {
        nobukti  = $("#nobukti").val();
        jobs_id  = $("#jobs_id").val();

        base_url = '<?php echo base_url()?>';
        
        $(".format_uang").autoNumeric();

        tb_data  = $("#tb-data").DataTable({
            responsive:true,
            processing:true,
            ordering:false,
            info:false,
            scrollY:"300px",
            iDisplayLength:100,
            ajax : {
                url : base_url+"Jurnal_kas_bank_form/ajax_detail_jurnal?nobukti="+nobukti+"&jobs_id="+jobs_id,
                type : "GET",
                dataSrc:function (json) {
                    $("#tot_debit").val(json.tot_debit);
                    $("#tot_kredit").val(json.tot_kredit);
                    $("#out_of_balance").val(json.out_of_balance);
                    return json.data;
                }
            },
            columns : [
                {data : 'kodeakun'},
                {data : 'namaakun'},
                {data : 'keterangan'},
                {data : 'debit', class:'text-right'},
                {data : 'kredit', class:'text-right'},
                {data : 'aksi'}
            ]
        });

        $("#kodeakun").select2({
            placeholder: 'Search Coa',
            ajax: {
                url: '<?php echo base_url()?>Jurnal_umum_form/ajax_data_coa',
                dataType: 'json',
                delay: 250,
                pagination: {
                    more: true
                },
                data:function(params) {
                    let key_def = $("#key_default").val();
                    $("#key_default").val(params.term);
                    var query = {
                        q: params.term || key_def,
                        page: params.page || 1
                    }
                    return query;
                },
                // processResults: function (data) {
                //     return {
                //         results: data
                //     };
                // },
                cache: true
            }
        });

        function clearForm() {
            $("#id_detail").val("");
            $("#kodeakun").val("").trigger('change');
            $("#debit").val("").attr('readonly',false);
            $("#kredit").val("").attr('readonly',false);
        }

        $("#btn-add").click(function() {
            $.ajax({
                url:base_url+"Jurnal_kas_bank_form/ajax_simpan_detail",
                type:"POST",
                data:$("#form-data").serialize(),
                dataType:"JSON",
                beforeSend:function() {
                    blockUI();
                },
                complete:function() {
                    unBlockUI();
                },
                success:function(r) {
                    (r.status == true) ? toastr.success(r.message) : toastr.error(r.message);

                    if(r.status == true) {
                        clearForm();
                    }

                    tb_data.ajax.url(base_url+"Jurnal_kas_bank_form/ajax_detail_jurnal?nobukti="+$("#nobukti").val()+"&jobs_id="+$("#jobs_id").val()).load();
                },
                error:function(err) {
                    toastr.error("Terjadi kesalahan menyimpan data");
                }
            });
        });

        $("#debit").keydown(function(e) {
            if(e.keyCode == 13) {
                $("#btn-add").click();
            }
        });

        $("#kredit").keydown(function(e) {
            if(e.keyCode == 13) {
                $("#btn-add").click();
            }
        });

        $(document).on('click','.btn-hapus',function() {
            id = $(this).data('id');

            swal({
                title: "Anda Yakin?",
                text: "Anda akan hapus data ini.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes!",
                closeOnConfirm: false
            }, function() {
                $.ajax({
                    url :base_url+"Jurnal_kas_bank_form/ajax_hapus_detail?id="+id+'&nobukti='+$("#nobukti").val()+'&kodejurnal='+$("#kodejurnal").val(),
                    type : "GET",
                    dataType : "JSON",
                    beforeSend:function() {
                        blockUI();
                    },
                    complete:function() {
                        unBlockUI();
                    },
                    success:function(data) {
                        (data.status == true) ? toastr.success(data.message) : toastr.error(data.message);

                        if(data.status == true) {
                            tb_data.ajax.url(base_url+"Jurnal_kas_bank_form/ajax_detail_jurnal?nobukti="+$("#nobukti").val()+"&jobs_id="+$("#jobs_id").val()).load();
                        } 

                        swal.close();
                    },
                    error:function(error) {
                        console.log(error);
                    }
                });
            });
        });

        $("#btn-submit").click(function() {
            $.ajax({
                url:base_url+"Jurnal_kas_bank_form/ajax_simpan_jurnal",
                type:"POST",
                data:$("#form-data").serialize(),
                dataType:"JSON",
                beforeSend:function() {
                    blockUI();
                },
                complete:function() {
                    unBlockUI();
                },
                success:function(r) {
                    (r.status == true) ? toastr.success(r.message) : toastr.error(r.message);

                    if(r.status == true) {
                        clearForm();
                        window.history.pushState("", "", base_url+"Jurnal_kas_bank_form?q="+r.id_auto);
                        $("#nobukti").val(r.nobukti);
                    }

                    tb_data.ajax.url(base_url+"Jurnal_kas_bank_form/ajax_detail_jurnal?nobukti="+r.nobukti+"&jobs_id="+$("#jobs_id").val()).load();
                },
                error:function(err) {
                    toastr.error("Terjadi kesalahan menyimpan data");
                }
            });
        });
    });

    function checkRule() {
        kodejurnal = $("#kodejurnal").val();
        $.ajax({
            url:base_url+"Jurnal_kas_bank_form/ajax_check_rule_jurnal?kode="+kodejurnal,
            type:"GET",
            dataType : "JSON",
            beforeSend:function() {
                blockUI();
            },
            complete:function() {
                unBlockUI();
            },
            success:function(r) {
                if(r.status == true) {
                    if(r.tipe == 'kas') {
                        $(".form-kas").show('normal');
                        $(".form-bank").hide('normal');
                    }

                    if(r.tipe == 'bank') {
                        $(".form-bank").show('normal');
                        $(".form-kas").hide('normal');
                    }

                    if(r.tipe == 'free') {
                        $(".form-bank").hide('normal');
                        $(".form-kas").hide('normal');
                        $("#kredit").attr('readonly',false);
                        $("#debit").attr('readonly',false);
                    }

                    if(r.debit == '1') {
                        $("#kredit").attr('readonly',false);
                        $("#debit").attr('readonly',true);
                    }

                    if(r.kredit == '1') {
                        $("#debit").attr('readonly',false);
                        $("#kredit").attr('readonly',true);
                    }
                } else {

                }
            },
            error:function(err) {
                console.log(err);
            }
        });
    } checkRule();
</script>