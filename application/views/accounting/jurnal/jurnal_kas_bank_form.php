<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <!--  -->
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <form id="form-data" method="POST">
            <div class="row">
                <!-- Left side -->
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-4 text-right">Tanggal*</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control date_picker" name="tanggal" id="tanggal" value="<?= isset($detail->tanggal) ? $detail->tanggal:date('Y-m-d')?>" readonly required>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 text-right">No. Bukti*</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control" name="nobukti" id="nobukti" value="<?= isset($detail->nobukti) ? $detail->nobukti:'';?>" readonly required>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-md-4 text-right">Kode Jurnal*</label>
                        <div class="col-md-5">
                            <?php echo form_dropdown('kodejurnal', [
                                'JBM' => 'JBM  - Bank Masuk',
                                'JBK' => 'JBK  - Bank Keluar',
                                'JKM' => 'JKM  - Kas Masuk',
                                'JKK' => 'JKK  - Kas Keluar'
                            ], isset($detail->kodejurnal) ? $detail->kodejurnal:'', 'class="form-control" id="kodejurnal" onchange="checkRule()" required'); ?>
                        </div>
                    </div>
                    <div class="row on-top-5 form-bank" style="display:none;">
                        <label class="col-md-4 text-right small">Bank*</label>
                        <div class="col-md-7">
                            <?php echo form_dropdown('bank', $opt_bank, isset($detail->kodeakun) ? $detail->kodeakun:'','class="form-control input-sm" id="bank"'); ?>
                        </div>
                    </div>
                    <div class="row on-top-5 form-kas" style="display:none;">
                        <label class="col-md-4 text-right small">Kas*</label>
                        <div class="col-md-7">
                            <?php echo form_dropdown('kas', $opt_kas, isset($detail->kodeakun) ? $detail->kodeakun:'','class="form-control input-sm" id="kas"'); ?>
                        </div>
                    </div>

                    <div class="row">
                        <label class="col-md-4 text-right">Keterangan*</label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="4" name="keterangan" id="keterangan" required><?= isset($detail->keterangan) ? $detail->keterangan:'';?></textarea>
                        </div>
                    </div>
                </div>
                <!-- Right side -->
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-3 text-right">Debit</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control text-right" readonly name="tot_debit" id="tot_debit">
                        </div>
                    </div>
                    <div class="row on-top-5">
                        <label class="col-md-3 text-right">Kredit</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control text-right" readonly name="tot_kredit" id="tot_kredit">
                        </div>
                    </div>
                    <div class="row on-top-5">
                        <label class="col-md-3 text-right">Out of Balance</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control text-right" readonly name="out_of_balance" id="out_of_balance">
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <input type="text" class="hidden" name="id_detail" id="id_detail">
                <input type="text" class="hidden" value="<?php echo isset($jobs_id) ? $jobs_id:'';?>" name="jobs_id" id="jobs_id">
                <!-- Left side -->
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-4 text-right">
                            Akun Pemotong / Penambah*
                        </label>
                        <div class="col-md-8">
                            <select class="form-control" id="kodeakun" name="kodeakun">
                                <option value=""></option>
                            </select>
                            <input type="hidden" id="key_default" readonly>
                        </div>
                    </div>
                </div>
                <!-- Right side -->
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-4">
                            <input type="text" class="form-control text-right format_uang" name="debit" id="debit" placeholder="Debit" min="0" required>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control text-right format_uang" name="kredit" id="kredit" placeholder="Kredit" min="0" required>
                        </div>
                        <div class="col-md-1">
                            <a href="javascript:;" class="btn btn-info" id="btn-add"><i class="icon-plus-circle2"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 col-md-offset-2">
                    <button type="button" id="btn-submit" class="btn btn-primary btn-sm">Submit</button>
                    <a href="<?= base_url()?>Jurnal_kas_bank" class="btn btn-default btn-sm">Kembali</a>
                    <a href="<?= base_url()?>Jurnal_kas_bank_form" class="btn btn-success btn-sm">Baru</a>
                </div>
            </div>
            <hr>
            <h6>Detail Data</h6>
            <table id="tb-data" class="table table-hover table-striped" style="width: 100%;">
                <thead>
                    <tr>
                        <th>Kode akun</th>
                        <th>Nama akun</th>
                        <th>Keterangan</th>
                        <th>Debit</th>
                        <th>Kredit</th>
                        <th>#</th>
                    </tr>
                </thead>
            </table>
        </form>
    </div>
</div>