<script>
    base_url = '<?php echo base_url()?>';

    var date_start = $("#date_start").val();
    var date_end   = $("#date_end").val();
    var keterangan = $("#keterangan").val();

    var table = $("#tb-data").DataTable({
        ajax : {
            url : base_url+"Jurnal_kas_bank/ajax_data_jurnal_kas_bank?tgl_mulai="+date_start+"&tgl_sampai="+date_end+"&keterangan="+keterangan,
            type : "GET"
        },
        responsive:true,
        processing:true,
        columns : [
            {data : 'no'},
            {data : 'nobukti'},
            {data : 'tgl'},
            {data : 'kodeakun'},
            {data : 'namaakun'},
            {data : 'keterangan'},
            {class: 'text-right',data : 'debit'},
            {class: 'text-right',data : 'kredit'},
            {data : 'aksi'}
        ],
        scrollY:"350px",
        iDisplayLength: 100
    });

    $("#btn-tampil").click(function() {
        date_start = $("#date_start").val();
        date_end   = $("#date_end").val();
        keterangan = $("#keterangan").val();

        table.ajax.url(base_url+"Jurnal_kas_bank/ajax_data_jurnal_kas_bank?tgl_mulai="+date_start+"&tgl_sampai="+date_end+"&keterangan="+keterangan).load();
    });

    $("#btn-tambah").click(function() {
        window.location.href = base_url+"Jurnal_kas_bank_form";
    });

    $(document).on('click','.btn-hapus',function() {
        id = $(this).data('id');

        swal({
            title: "Anda Yakin?",
            text: "Anda akan hapus data ini.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false
        }, function() {
            $.ajax({
                url :base_url+"Jurnal_kas_bank/ajax_hapus_jurnal?id="+id,
                type : "GET",
                dataType : "JSON",
                beforeSend:function() {
                    blockUI();
                },
                complete:function() {
                    unBlockUI();
                },
                success:function(data) {
                    (data.status == true) ? toastr.success(data.message) : toastr.error(data.message);

                    if(data.status == true) {
                        table.ajax.url(base_url+"Jurnal_kas_bank/ajax_data_jurnal_kas_bank?tgl_mulai="+$("#date_start").val()+"&tgl_sampai="+$("#date_end").val()+"&keterangan="+$("#keterangan").val()).load();
                    } 

                    swal.close();
                },
                error:function(error) {
                    console.log(error);
                }
            });
        });
    });
</script>