<script>
    base_url = '<?php echo base_url()?>';

    var date_start = $("#date_start").val();
    var date_end   = $("#date_end").val();
    var keterangan = $("#keterangan").val();

    var table = $("#tb-data").DataTable({
        ajax : {
            url : base_url+"Jurnal_otomatis/ajax_data_jurnal_otomatis?tgl_mulai="+date_start+"&tgl_sampai="+date_end+"&keterangan="+keterangan,
            type : "GET"
        },
        responsive:true,
        processing:true,
        columns : [
            {data : 'no'},
            {data : 'nobukti'},
            {data : 'tgl'},
            {data : 'kodeakun'},
            {data : 'namaakun'},
            {data : 'keterangan'},
            {class: 'text-right',data : 'debit'},
            {class: 'text-right',data : 'kredit'}
        ],
        scrollY:"350px",
        iDisplayLength: 100
    });

    $("#btn-tampil").click(function() {
        date_start = $("#date_start").val();
        date_end   = $("#date_end").val();
        keterangan = $("#keterangan").val();

        table.ajax.url(base_url+"Jurnal_otomatis/ajax_data_jurnal_otomatis?tgl_mulai="+date_start+"&tgl_sampai="+date_end+"&keterangan="+keterangan).load();
    });
</script>