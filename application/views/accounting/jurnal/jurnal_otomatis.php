<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <!--  -->
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <div class="row">
            <div class="col-md-12 form-inline">
                <div class="form-group">
                    <label>Short Tanggal</label>
                </div>
                <div class="form-group">
                    <label class="sr-only" for="date_start">Tanggal Mulai</label>
                    <input type="text" class="form-control date_picker" name="date_start" id="date_start" placeholder="Tanggal Mulai" value="<?php echo date('Y-m-d');?>"> 
                </div>
                <div class="form-group">
                    <label class="sr-only" for="date_end">Tanggal Selesai</label>
                    <input type="text" class="form-control date_picker" name="date_end" id="date_end" placeholder="Tanggal Selesai" value="<?php echo date('Y-m-d');?>"> 
                </div>
                <div class="form-group" style="width: 325px;">
                    <input class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan" style="width: 100%;">
                </div>
                <div class="form-group">
                    <button type="button" id="btn-tampil" class="btn btn-primary">Tampilkan</button>
                </div>
            </div>
            <div class="col-md-12"><br>
                <table class="table datatable-ajax table-xxs text-size-small table-bordered" id="tb-data" style="width:100%">
                    <thead>
                        <tr>            
                            <th width="3%">#</th>
                            <th width="115px">No. Bukti</th>
                            <th>Tanggal</th>
                            <th>No. Akun</th>
                            <th>Nama Akun</th>
                            <th>Keterangan</th>
                            <th>Debit</th>
                            <th>Kredit</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>