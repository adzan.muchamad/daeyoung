<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <!--  -->
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <form id="form-data" action="#">
            <div class="row">
                <div class="col-md-12 form-inline">
                    <div class="form-group">
                        Short
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control date_picker" name="date_start" id="date_start" placeholder="Tanggal Mulai" data-date-format="dd/mm/yyyy" value="<?= isset($date_start) ? $date_start:'';?>">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control date_picker" name="date_end" id="date_end" placeholder="Tanggal Sampai" data-date-format="dd/mm/yyyy" value="<?= isset($date_end) ? $date_end:'';?>">
                    </div>
                    <div class="form-group" style="width: 300px;">
                        <?= form_dropdown('kodeakun', $opt_akun, isset($kodeakun) ? $kodeakun:'','class="form-control select2" id="kodeakun" style="width: 100%;"');?>
                    </div>
                    <div class="form-group" style="width: 350px;">
                        <input class="form-control " name="keterangan" id="keterangan" placeholder="Keterangan" style="width: 100%;">
                    </div>
                    <div class="form-group">
                        <button title="Tampilkan" type="button" id="btn-tampilkan" class="btn btn-primary">Tampilkan</button>
                        <button title="Export Excel" type="button" id="btn-excel" class="btn btn-success">Download</button>
                    </div>
                </div>
            </div>
            <hr>
            <div id="result" class="table-responsive"></div>
        </form>
    </div>
</div>