<!DOCTYPE html>
<html>
<head>
    <noscript>
        <meta http-equiv="refresh" content="0; url=<?php echo base_url(); ?>nojs" />
    </noscript>
    <title><?php echo $title; ?></title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>/assets/images/favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
    <link href="<?php echo base_url(); ?>assets/css/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/components.css?x=3" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/password_indicator.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/chosen.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/pace.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/css/break.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/js/datatables/extensions/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/invoice/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
    <style>
        body {
            font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }

        .wrap-title-top {
            background: #EEEDED;
            text-align: center;
            padding: 10px;
            font-weight: 600;
            font-size: 16px;
            border-bottom: 2px solid #BBBBBB;
        }

        .wrap-body {
            width: 100%;
            font-size: 14px;
        }

        .text-black {
            font-weight: bold;
        }
    </style>
</head>
<body id="body_loader">
    <div class="wrap-title-top">
        <span>NERACA</span>
        <div>
            PERIODE : <?php echo isset($periode) ? strtoupper($periode):'';?>
            <input type="text" class="hidden" value="<?php echo isset($params) ? $params:'';?>" name="periode" id="periode">
        </div>
    </div>
    <div class="wrap-body">
        <table class="table table-striped table-bordered">
            <tr>
                <td valign="top" style="padding:0; border:0;">
                    <?php echo isset($table_aktiva) ? $table_aktiva:'';?>
                </td>
                <td valign="top" style="padding:0; border:0; vertical-align:top;">
                    <?php echo isset($table_pasiva) ? $table_pasiva:'';?>
                </td>
            </tr>
            <tr class="text-black" style="font-size: 16px;">
                <td>
                    Total Aktiva
                    <span style="float: right;"><?php echo isset($total_aktiva) ? $total_aktiva:'';?></span>
                </td>
                <td>
                    Total Pasiva
                    <span style="float: right;"><?php echo isset($total_pasiva) ? $total_pasiva:'';?></span>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/blockui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/datatables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.pwstrength.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autoNumeric.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/navbar.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/switch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pushjs/push.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pushjs/serviceWorker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/func.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/pace.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.mask.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/select2/js/select2.full.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/invoice/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.inputmask.bundle.js"></script>
</body>
</html>