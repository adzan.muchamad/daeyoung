<input type="hidden" id="pageUri" value="<?php echo base_url(); ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <div class="input-group-btn">
                <!--  -->
            </div>
        </div>
    </div>
    <div class="panel panel-body">
        <form method="POST" target="_blank" action="<?php echo base_url()?>Report_neraca/print_data">
            <div class="row">
                <div class="col-md-4 form-group">
                    <label>Bulan</label>
                    <div>
                        <?php echo form_dropdown('bulan', $opt_bulan, date('m'), 'class="form-control input-sm" required'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 form-group">
                    <label>Tahun</label>
                    <div>
                        <?php echo form_dropdown('tahun', $opt_tahun, date('Y'), 'class="form-control input-sm" required'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> View Report</button>
                </div>
            </div>
        </form>
    </div>
</div>