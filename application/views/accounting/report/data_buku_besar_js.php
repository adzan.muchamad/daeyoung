<script>
    $(document).ready(function() {
        base_url = '<?php echo base_url()?>';

        $("#kodeakun").select2();

        $("#btn-tampilkan").click(function() {
            $.ajax({
                url:base_url+"Buku_besar/ajax_data_buku_besar",
                type:"GET",
                data:$("#form-data").serialize(),
                dataType:"JSON",
                beforeSend:function() {
                    blockUI();
                },
                complete:function() {
                    unBlockUI();
                },
                success:function(res) {
                    data = res.data;
                    html_data ='<table class="table table-striped table-hover table-bordered" id="table-data" width="1600px"><thead><tr><th width="20px">No</th><th width="120px">Tanggal</th><th width="200px">Nomor</th><th width="500px">Keterangan</th><th width="180px">Debit</th><th width="180px">Kredit</th><th width="180px">Saldo</th></tr></thead><tbody>';
                    for (let i = 0; i < data.length; i++) {
                        html_data += data[i];
                    }
                    html_data +='<tr><td colspan="4" class="bold">TOTAL</td><td class="text-right bold">'+res.total_debit+'</td><td class="text-right bold">'+res.total_kredit+'</td><td class="text-right bold">'+res.saldo+'</td></tr></tbody></table>';
                    $("#result").html(html_data);
                },
                error:function(err) {
                    console.log(err);
                }
            });
        }); $("#btn-tampilkan").click();

        $("#btn-excel").click(function() {
            window.location.href = base_url+'Buku_besar/excel?'+$("#form-data").serialize();
        });
    });
</script>