<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <!-- Right Side -->
        </div>
    </div>
    <div class="panel panel-body">
        <div class="row">
            <div class="col-md-6">
                <h5>Chart of Account</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Coa_category">COA Category</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Coa_kas_bank">COA Cash and Bank</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Coa_lain_lain">COA Other</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Closing Transaction Accounts</a>
                        </div>
                    </div>
                </div>

                <h5>General Ledger</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Jurnal_umum_form">General Entry</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Jurnal_kas_bank_form">Cash and Bank Entry</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Jurnal_umum">General List</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Jurnal_kas_bank">Cash and Bank List</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Jurnal_otomatis">Automatic List</a>
                        </div>
                    </div>
                </div>

                <h5>Report</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Report_neraca">Balance Sheet</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Report_rugi_laba">Profit and Loss</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Buku_besar">Cashbook</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Cash Flow</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-6">
                <h5>Accounts Receivable</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Piutang">Customer Invoice</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Pelunasan_piutang_form">Payment Entry</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Pelunasan_piutang">Payment List</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Customer Invoice Report</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Payment Report</a>
                        </div>
                    </div>
                </div>

                <h5>Accounts Payable</h5>
                <div class="row">
                    <div class="col-md-12">
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Invoice Entry</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Payment Entry</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Invoice List</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>Hutang">Debt Card</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Payment List</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Invoice Report</a>
                        </div>
                        <div>
                            <i>~</i> <a href="<?= base_url();?>">Payment Report</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>