<input type="hidden" id="id" name="id" />
<div class="row ">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Customer
        </div>
        <div class="col-lg-10">
            <select class="form-control searchcust" id="nama_cust" name="nama_cust" onchange="get_branch(this)"></select>
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Tempo
        </div>
        <div class="col-lg-4">
            <select class="form-control" id="tempo" name="tempo">
                <option value="3">30 Hari</option>
                <option value="2">7 Hari</option>
                <option value="5">Bayar dimuka</option>
                <option value="4">Tgl 10 BB</option>
                <option value="1">Tunai</option>
            </select>
        </div>
        <div class="col-lg-2">
            Kredit Saat Ini
        </div>
        <div class="col-lg-4 kredit_val">
            <a class="form-control text-primary" style="border: 0px">sdasd</a>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Cabang
        </div>
        <div class="col-lg-10">
            <select class="form-control searchbranch" id="branch" name="branch" onchange="cek_matauang()"></select>
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Tanggal Order
        </div>
        <div class="col-lg-4">
            <input type="text" class="form-control date_picker" id="tgl_order" name="tgl_order" value="<?= date('Y-m-d'); ?>" />
        </div>
        <div class="col-lg-2">
            PPN
        </div>
        <div class="col-lg-4">
            <select class="form-control" name="ppn" id="ppn" onchange="count_all()">
                <option value="0">Tidak</option>
                <option value="1">Iya</option>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Mata Uang
        </div>
        <div class="col-lg-4">
            <select class="form-control" id="mata_uang" onchange="get_matauang(this);">
                <?php $res = $this->m_global->get_currency();
                foreach ($res as $row) {
                    echo '<option value="' . $row->id . '">' . $row->nama . '</option>';
                } ?>
            </select>
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-2">
            Batas Tanggal Kirim
        </div>
        <div class="col-lg-4">
            <input type="text" class="form-control date_picker" id="tgl_kirim" name="tgl_kirim" value="<?= date('Y-m-d'); ?>" />
        </div>
    </div>


</div>
<hr style="margin: 0px;margin-bottom:10px">
<center style="margin-bottom:10px"><b>BARANG ORDER</b></center>
<div class="list_barang">
    <div class="row">
        <div class="col-lg-1 text-center" style="width:5%">
            <label>No</label>
            <input class="form-control no_urut text-center" type="text" readonly />
        </div>
        <div class="col-lg-4 text-center">
            <label>Barang</label>
            <select class="form-control barang" name="barang[]" onchange="get_satuan(this);">
            </select>
        </div>
        <div class="col-lg-1 text-center">
            <label>Qty</label>
            <input class="form-control qty" type="text" name="qty[]" onkeyup="count_jmlh(this);" />
        </div>
        <div class="col-lg-1 text-center">
            <label>Satuan</label>
            <select name="satuan[]" class="form-control satuan">
            </select>
        </div>
        <div class="col-lg-2 text-center">
            <label>Harga</label>
            <input class="form-control harga text-right" type="text" name="harga[]" onkeyup="count_jmlh2(this);" />
        </div>
        <div class="col-lg-2 text-center">
            <label>Jumlah Harga</label>
            <input class="form-control text-right jumlah" type="text" name="jumlah[]" readonly />
        </div>
        <div class="col-lg-1 text-center">
            <label>Action</label>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-2 text-center" style="float:right">
        <a class="btn btn-primary form-control" onclick="add_barang(this)"><i class="icon-plus3"></i>&nbsp;Tambah</a>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-8">
        <div class="col-lg-6 display-flex-center">
            <div class="col-lg-4">
                Buyer
            </div>
            <div class="col-lg-4">
                <input type="text" class="form-control" id="buyer" name="buyer" />
            </div>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Sub Total
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="jumlah_harga" name="jumlah_harga" readonly />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="col-lg-6 display-flex-center">
            <div class="col-lg-4">
                Referensi Pelanggan
            </div>
            <div class="col-lg-4">
                <input type="text" class="form-control" id="ref_cust" name="ref_cust" />
            </div>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Diskon
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="diskon" name="diskon" onkeyup="count_all()" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
        <div class="col-lg-6 display-flex-center">
            <div class="col-lg-4">
                Note
            </div>
            <div class="col-lg-8">
                <textarea type="text" class="form-control" id="note_order" name="note_order"></textarea>
            </div>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            Pajak
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="pajak" readonly />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3 text-right">
            <b>Jumlah Total</b>
        </div>
        <div class="col-lg-6">
            <input class="form-control text-right" id="total" name="total" readonly />
        </div>
    </div>
</div>