<div class="form_barang_in">
    <div class="row">
        <div class="col-lg-12">
            <label>Barang</label>
            <select class="form-control" id="barang_in" name="barang">
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <label>Qty</label>
            <input type="text" class="form-control" id="qty1" name="qty1" />
        </div>
        <div class="col-lg-2">
            <label>Satuan</label>
            <select class="form-control satuan1" name="satuan1" readonly>
                <option value="1">KGM</option>
            </select>
        </div>
        <div class="col-lg-4">
            <label>Qty</label>
            <input type="text" class="form-control" id="qty2" name="qty2" />
        </div>
        <div class="col-lg-2">
            <label>Satuan</label>
            <select class="form-control satuan2" name="satuan2" onchange="konversi()">
                <option value="1">Meter</option>
                <option value="2">Yard</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <label>LOT</label>
            <input type="text" class="form-control" id="lot_barang" name="lot_barang" />
        </div>
        <div class="col-lg-6">
            <label>Keterangan</label>
            <textarea type="text" class="form-control" id="ket" name="ket" style="resize: vertical;text-transform:none"></textarea>
        </div>
    </div>
</div>