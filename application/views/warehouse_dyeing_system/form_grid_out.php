<div class="form_barang_out">
    <div class="row">
        <div class="col-lg-12">
            <label>Barang</label>
            <select class="form-control" id="barang_out" name="barang">
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <label>Qty</label>
            <input type="text" class="form-control" id="oqty" name="qty" />
        </div>
        <div class="col-lg-2">
            <label>Satuan</label>
            <select class="form-control" name="satuan" readonly>
                <option value="1">KGM</option>
            </select>
        </div>
        <div class="col-lg-4">
            <label>Qty</label>
            <input type="text" class="form-control" id="oqty2" name="qty2" />
        </div>
        <div class="col-lg-2">
            <label>Satuan</label>
            <select class="form-control satuan3" name="satuan2" onchange="konversi2()">
                <option value="1">Meter</option>
                <option value="2">Yard</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <label>LOT</label>
            <input type="text" class="form-control" name="lot_barang" />
        </div>
        <div class="col-lg-6">
            <label>Keterangan</label>
            <textarea type="text" class="form-control" id="oket" name="ket" style="resize: vertical;text-transform:none"></textarea>
        </div>
    </div>
</div>