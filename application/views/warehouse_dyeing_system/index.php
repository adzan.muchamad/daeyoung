<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <form id="formsearch" onSubmit="return false;">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button id="button_form_dropdown_search" type="button" class="btn dropdown-toggle btn-icon btn-default btn-raised" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu dropdown-content" style="width:395px;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>kategori</label>
                                        <select class="form-control" id="searchlokasi" name="searchlokasi">
                                            <option value="">Semua</option>
                                            <option value="1">Dyeing</option>
                                            <option value="2">Finishing</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <br>
                                    <div class="col-lg-8">
                                    </div>
                                    <div class="col-lg-3">
                                        <button id="buttonPencarian" type="button" class="btn btn-default btn-raised"><b><i class="icon-search4"></i> Search</b></button>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                    <input type="text" class="form-control" style="width:250px;" placeholder="Pencarian" id="search_keyword" name="search_keyword">

                    <div class="input-group-btn">
                        <button type="button" class="btn btn-default btn-raised" onClick="input_data()"><b><i class="icon-plus-circle2"></i></b> Tambah Barang</button>
                        <button type="button" class="btn btn-default btn-raised" onClick="brgin_data()"><b><i class="icon-plus-circle2"></i></b> IN</button>
                        <button type="button" class="btn btn-default btn-raised" onClick="brgout_data()"><b><i class="icon-minus-circle2"></i></b> OUT</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <table id="datamain_datatable" style="width:100%" class="table datatable-ajax table-xxs text-size-small table-bordered">
        <thead>
            <tr>
                <th>no</th>
                <th>kode barang</th>
                <th>nama barang</th>
                <th>lokasi</th>
                <th>stok Kilogram</th>
                <th>stok Yard</th>
                <th>stok Meter</th>
                <th><i class="icon-menu7 position-left"></i></th>
            </tr>
        </thead>
    </table>
</div>



<?php echo $insert_view; ?>