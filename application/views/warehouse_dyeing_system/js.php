<script type="text/javascript">
	$(function() {
		var pageUri = $('#pageUri').val();

		//SET DATATABLES
		$.extend($.fn.dataTable.defaults, {
			//autoWidth: true,
			"sLength": "form-control"
		});
		var height_windows = $(window).height();
		var height_navbar = $('#navbar-main').height();
		var height_page = $('#page-header').height();
		var height_table = height_windows - height_navbar - height_page - 370;
		var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

		table = $('#datamain_datatable').DataTable({
			dom: 'J<"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
			"pageLength": 50,
			aaSorting: [
				[1, 'desc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_data_table',
				"type": "POST",
				"data": function(data) {
					data.searchlokasi = $('#searchlokasi').val();
					data.search_keyword = $('#search_keyword').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},

			columnDefs: [{
				orderable: false,
				width: '150px',
				targets: [0, tabel_kolom]
			}, {
				className: "text-center",
				"targets": [tabel_kolom, 0]
			}],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
			scrollX: '100%',
			scrollY: height_table,
			scrollCollapse: false,
			fixedColumns: {
				leftColumns: 0,
				rightColumns: 1
			},
		});


		table2 = $('#datamain_datatable2').DataTable({
			dom: '<"datatable-header"B><"datatable-scroll datatable-scroll-wrap"t>',
			"ordering": false,
			aaSorting: [
				[1, 'desc']
			],
			"searching": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_data_barang',
				"type": "POST",
				"data": function(data) {
					data.idbarang = $('#idbarang').val();
					data.searchDateFirst = $('#searchDateFirst').val();
					data.searchDateFinish = $('#searchDateFinish').val();
					data.searchLot = $('#searchLot').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},
			columnDefs: [{
				orderable: false,
				className: "text-center",
				width: '150px',
				targets: [0]
			}, {
				className: "text-center",
				"targets": [1, 2, 3, 4, 5, 6, 7, 8, 10]
			}, ],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			}
		});


		$('#buttonPencarian').click(function() {
			table.ajax.reload(null, false);
			$('#button_form_dropdown_search').next().toggle();
		});
		$('#search_keyword').keyup(function() {
			table.ajax.reload(null, false);
		});
		$('#searchDateFirst').change(function() {
			table2.ajax.reload(null, false);
		});
		$('#searchDateFinish').change(function() {
			table2.ajax.reload(null, false);
		});
		$('#searchLot').keyup(function() {
			table2.ajax.reload(null, false);
		});
		$('.dataTables_length select').select2({
			minimumResultsForSearch: Infinity,
			width: 'auto',
		});
		//END SET DATATABLES

		$("#formulir_modal").validate({
			rules: {
				barang: {
					required: true
				},
			},
			submitHandler: function(form) {
				var cekTransaksi = $("#formulir_modal").attr('transaksi');
				if (cekTransaksi == 'tambah') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'insert_data',
						data: $('#formulir_modal').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								$('#formulir_modal').clearForm();
								toastr.success('Data berhasil disimpan');
								table.ajax.reload(null, false);

							} else if (response == '0') {
								toastr.warning('Data gagal disimpan');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				}
				return false;
			}
		});

		//in
		$("#formulir_modal_in").validate({
			rules: {
				barang: {
					required: true
				},
			},
			submitHandler: function(form) {
				var cekTransaksi = $("#formulir_modal_in").attr('transaksi');
				if (cekTransaksi == 'tambah') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'insert_data_in',
						data: $('#formulir_modal_in').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								$('#barang_in').val('').change();
								$('#formulir_modal_in').clearForm();
								toastr.success('Data berhasil disimpan');
								table.ajax.reload(null, false);
							} else if (response == '0') {
								toastr.warning('Data gagal disimpan');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				}
				return false;
			}
		});

		//out

		$("#formulir_modal_out").validate({
			rules: {
				barang: {
					required: true
				},
			},
			submitHandler: function(form) {
				var cekTransaksi = $("#formulir_modal_out").attr('transaksi');
				if (cekTransaksi == 'tambah') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'insert_data_out',
						data: $('#formulir_modal_out').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								$('#barang_out').val('').change();
								$('#formulir_modal_out').clearForm();
								toastr.success('Data berhasil disimpan');
								table.ajax.reload(null, false);

							} else if (response == '0') {
								toastr.warning('Data gagal disimpan');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				}
				return false;
			}
		});
	});

	function replaceAll(str, token, newToken, ignoreCase) {
		var _token;
		var i = -1;

		if (typeof token === "string") {

			if (ignoreCase) {

				_token = token.toLowerCase();

				while ((
						i = str.toLowerCase().indexOf(
							_token, i >= 0 ? i + newToken.length : 0
						)) !== -1) {
					str = str.substring(0, i) +
						newToken +
						str.substring(i + token.length);
				}

			} else {
				return str.split(token).join(newToken);
			}

		}
		return str;
	};

	function formatMoney(amount, decimalCount = 4, decimal = ",", thousands = ".") {
		try {
			decimalCount = Math.abs(decimalCount);
			decimalCount = isNaN(decimalCount) ? 4 : decimalCount;

			const negativeSign = amount < 0 ? "-" : "";

			let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
			let j = (i.length > 3) ? i.length % 3 : 0;

			return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
		} catch (e) {
			console.log(e)
		}
	}

	function get_barang() {
		var pageUri = $('#pageUri').val();
		$("#barang").select2({
			placeholder: "Masukan Nama Barang",
			width: "100%",
			ajax: {
				url: pageUri + 'get_barang',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response

					};
				},
				cache: false
			}
		});
	};

	function get_barang_in() {
		var pageUri = $('#pageUri').val();
		$("#barang_in").select2({
			placeholder: "Masukan Nama Barang",
			width: "100%",
			ajax: {
				url: pageUri + 'get_barang_daftar',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response

					};
				},
				cache: false
			}
		});
	};

	function get_barang_out() {
		var pageUri = $('#pageUri').val();
		$("#barang_out").select2({
			placeholder: "Masukan Nama Barang",
			width: "100%",
			ajax: {
				url: pageUri + 'get_barang_daftar',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response

					};
				},
				cache: false
			}
		});
	};

	function set_input_in() {
		$("#qty1").inputmask({
			'alias': 'decimal',
			'groupSeparator': ',',
			'autoGroup': true,
			'radixPoint': ",",
			'min': 0,
		});
		$("#qty2").inputmask({
			'alias': 'decimal',
			'groupSeparator': ',',
			'autoGroup': true,
			'radixPoint': ",",
			'min': 0,
		});
	}

	function set_input_out() {
		$("#oqty").inputmask({
			'alias': 'decimal',
			'groupSeparator': ',',
			'autoGroup': true,
			'radixPoint': ",",
			'min': 0,
		});
		$("#oqty2").inputmask({
			'alias': 'decimal',
			'groupSeparator': ',',
			'autoGroup': true,
			'radixPoint': ",",
			'min': 0,
		});
	}

	function input_data() {
		get_barang();
		$('#formulir_modal').clearForm();
		$("#formulir_modal").attr('transaksi', 'tambah');
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Tambah');
		$('#formSearch').dialog('close');
		$('#modal_barang').modal('show');
		return false;
	}

	function brgin_data() {
		$('.form_barang_in').html('').append('<div class="row"><div class="col-lg-12"><label>Barang</label><select class="form-control" id="barang_in" name="barang"></select></div></div><div class="row"><div class="col-lg-4"><label>Qty</label><input type="text" class="form-control" id="qty1" name="qty1" /></div><div class="col-lg-2"><label>Satuan</label><select class="form-control satuan1" name="satuan1" disabled><option value="1">KGM</option></select></div><div class="col-lg-4"><label>Qty</label><input type="text" class="form-control" id="qty2" name="qty2" /></div><div class="col-lg-2"><label>Satuan</label><select class="form-control satuan2" name="satuan2" onchange="konversi()"><option value="1">Meter</option><option value="2">Yard</option></select></div></div><div class="row"><div class="col-lg-6"><label>LOT</label><input type="text" class="form-control" id="lot_barang" name="lot_barang" /></div><div class="col-lg-6"><label>Keterangan</label><textarea type="text" class="form-control" id="ket" name="ket" style="resize:vertical;text-transform:none"/></textarea></div></div>');
		set_input_in();
		get_barang_in();
		$('#formulir_modal_in').clearForm();
		$("#formulir_modal_in").attr('transaksi', 'tambah');
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Tambah');
		$('#formSearch').dialog('close');
		$('#modal_barang_in').modal('show');
		return false;

	}

	function brgout_data() {
		$('.form_barang_out').html('').append('<div class="row"><div class="col-lg-12"><label>Barang</label><select class="form-control" id="barang_out" name="barang"></select></div></div><div class="row"><div class="col-lg-4"><label>Qty</label><input type="text" class="form-control" id="oqty" name="qty" /></div><div class="col-lg-2"><label>Satuan</label><select class="form-control" name="satuan" readonly><option value="1">KGM</option></select></div><div class="col-lg-4"><label>Qty</label><input type="text" class="form-control" id="oqty2" name="qty2" /></div><div class="col-lg-2"><label>Satuan</label><select class="form-control satuan3" name="satuan2" onchange="konversi2()"><option value="1">Meter</option><option value="2">Yard</option></select></div></div><div class="row"><div class="col-lg-6"><label>LOT</label><input type="text" class="form-control" name="lot_barang" /></div><div class="col-lg-6"><label>Keterangan</label><textarea type="text" class="form-control" id="oket" name="ket" style="resize: vertical;text-transform:none"></textarea></div></div>');
		set_input_out();
		get_barang_out();
		$('#formulir_modal_out').clearForm();
		$("#formulir_modal_out").attr('transaksi', 'tambah');
		$("#tabelDetailInvoice tbody").html('');
		$('#formSearch').dialog('close');
		$('#modal_barang_out').modal('show');
		return false;
	}



	function konversi() {
		var qty2 = $('#qty2').val();
		var mtr = 1.0936133;
		var yard = 0.9144;
		var satuan = $('.satuan2').val();
		if (qty2 == null || qty2 == 'undefined' || qty2 == '') {
			return false;
		} else {
			if (satuan == 1) {
				qty2 = replaceAll(qty2, '.', '');
				qty2 = qty2.replace(/[,]+/g, ".");
				qty2 = parseFloat(parseFloat(qty2) * yard).toFixed(4);
			}
			if (satuan == 2) {
				qty2 = replaceAll(qty2, '.', '');
				qty2 = qty2.replace(/[,]+/g, ".");
				qty2 = parseFloat(parseFloat(qty2) * mtr).toFixed(4);
			}
			$('#qty2').val(formatMoney(qty2));
		}
	}

	function konversi2() {
		var qty2 = $('#oqty2').val();
		var mtr = 1.0936133;
		var yard = 0.9144;
		var satuan = $('.satuan3').val();
		if (qty2 == null || qty2 == 'undefined' || qty2 == '') {
			return false;
		} else {
			if (satuan == 1) {
				qty2 = replaceAll(qty2, '.', '');
				qty2 = qty2.replace(/[,]+/g, ".");
				qty2 = parseFloat(parseFloat(qty2) * yard).toFixed(4);
			}
			if (satuan == 2) {
				qty2 = replaceAll(qty2, '.', '');
				qty2 = qty2.replace(/[,]+/g, ".");
				qty2 = parseFloat(parseFloat(qty2) * mtr).toFixed(4);
			}
			$('#oqty2').val(formatMoney(qty2));
		}
	}

	function delete_data(id) {
		var pageUri = $('#pageUri').val();

		swal({
			title: "Anda Yakin?",
			text: "Anda akan hapus data ini.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes!",
			closeOnConfirm: false
		}, function() {
			$.ajax({
				type: 'GET',
				url: pageUri + 'delete_data/' + id,
				beforeSend: function() {
					blockUI();
				},
				success: function(response) {
					if (response == '1') {
						table.ajax.reload(null, false);
						toastr.success('Data berhasil dihapus');
					} else if (response == '0') {
						toastr.error('Data gagal dihapus');
					} else {
						toastr.warning(response);
					}
					swal.close();
					unBlockUI();
				}
			});
		});
	};

	function view_data(id) {
		var pageUri = $('#pageUri').val();
		$('#idbarang').val(id);
		$.ajax({
			type: 'GET',
			data: $('#formulir_modal_riwayat').serialize(),
			url: pageUri + 'view_data/' + id,
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#kode_brg').val(html.kode_barang);
					$('#lokasi_brg').val(html.lokasi);
					$('#nama_brg').val(html.nama_barang);
					$('#modal_barang_riwayat').modal('show');
					table2.ajax.reload(null, false);
					table2.on('draw', function() {
						$('tr').each(function() {
							if ($(this).find('td .stat').val() == 'out') {
								$(this).addClass('table-danger');
							}
						})
					});
				} else if (html == '0') {
					toastr.error('Data gagal dibuka');
				} else {
					toastr.warning(html);
				}
				unBlockUI();
			},
		});
	}
</script>