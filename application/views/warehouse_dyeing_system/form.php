<?php
$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_barang';
$options['modal_size'] = 'modal-lg';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Form <span id="title_modal_flexible"></span>';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_dyeing_system/form_grid', $data, TRUE);
echo $this->ui->load_component($options);


$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_barang_in';
$options['modal_size'] = 'modal-lg';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Barang Masuk';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal_in';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_dyeing_system/form_grid_in', $data, TRUE);
echo $this->ui->load_component($options);

$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_barang_out';
$options['modal_size'] = 'modal-lg';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Barang Keluar';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal_out';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_dyeing_system/form_grid_out', $data, TRUE);
echo $this->ui->load_component($options);

$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_default';
$options['modal_id'] = 'modal_barang_riwayat';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Riwayat Barang';
$options['modal_footer'] = 'yes';
$options['form_id'] = 'formulir_modal_riwayat';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_dyeing_system/riwayat', $data, TRUE);
echo $this->ui->load_component($options);
