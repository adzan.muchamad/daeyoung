<?php
$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_barang';
$options['modal_size'] = 'modal-lg';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Form <span id="title_modal_flexible"></span>';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('inventory_barang/form_grid', $data, TRUE);
echo $this->ui->load_component($options);


$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_barang_view';
$options['modal_size'] = 'modal-md';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> View';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal_view';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('inventory_barang/form_grid_view', $data, TRUE);
echo $this->ui->load_component($options);
