<script type="text/javascript">
	$(function() {
		var pageUri = $('#pageUri').val();

		//SET DATATABLES
		$.extend($.fn.dataTable.defaults, {
			//autoWidth: true,
			"sLength": "form-control"
		});
		var height_windows = $(window).height();
		var height_navbar = $('#navbar-main').height();
		var height_page = $('#page-header').height();
		var height_table = height_windows - height_navbar - height_page - 370;
		var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

		table = $('#datamain_datatable').DataTable({
			dom: 'J<"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
			"pageLength": 50,
			aaSorting: [
				[1, 'desc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_data_table',
				"type": "POST",
				"data": function(data) {
					data.search_keyword = $('#search_keyword').val();
					data.searchkat = $('#searchkat').val();
					data.searchtipe = $('#searchtipe').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},

			columnDefs: [{
				orderable: false,
				width: '150px',
				targets: [0, tabel_kolom]
			}, {
				className: "text-center",
				"targets": [tabel_kolom, 0]
			}, {
				orderable: false,
				targets: [8, 9]
			}],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
			scrollX: '100%',
			scrollY: height_table,
			scrollCollapse: false,
			fixedColumns: {
				leftColumns: 0,
				rightColumns: 1
			},
		});

		$('#buttonPencarian').click(function() {
			table.ajax.reload(null, false);
			$('#button_form_dropdown_search').next().toggle();
		});
		$('#search_keyword').keyup(function() {
			table.ajax.reload(null, false);
		});
		$('.dataTables_length select').select2({
			minimumResultsForSearch: Infinity,
			width: 'auto',
		});
		//END SET DATATABLES

		$("#formulir_modal").validate({
			rules: {
				nama_barang: {
					required: true
				},
				kode_barang: {
					required: true
				}
			},
			submitHandler: function(form) {
				if ($('.respon_kode').text() != 'Kode Tidak Tersedia') {
					var jmlah = 0;
					if ($('#jumlah_benang').val() == null || $('#jumlah_benang').val() == 'undefined' || $('#jumlah_benang').val() == '') {
						jmlah = 0;
					} else {
						jmlah = parseFloat($('#jumlah_benang').val()).toFixed(0);
					}
					if (jmlah <= 100) {
						var cekTransaksi = $("#formulir_modal").attr('transaksi');
						if (cekTransaksi == 'tambah') {
							$.ajax({
								type: 'POST',
								url: pageUri + 'insert_data',
								data: $('#formulir_modal').serialize(),
								beforeSend: function() {
									blockUI();
								},
								success: function(response) {
									if (response == '1') {
										$('#formulir_modal').clearForm();
										$('.satuan2').html('<label>&nbsp;</label><a class="btn btn-primary form-control" onclick="satuan2(this)"><i class="icon-plus3"></i>&nbsp;Tambah Satuan</a>');
										$('.form_detail').css('display', 'none');
										$('.form_detail').html('<div class="row"><div class="col-lg-3"><a class="btn btn-success form-control" onclick="open_form_detail(this)"><i class="icon-plus3"></i>&nbsp;Detail Produksi</a></div></div>');
										toastr.success('Data berhasil disimpan');
										table.ajax.reload(null, false);

									} else if (response == '0') {
										toastr.warning('Data gagal disimpan');
									} else {
										toastr.error(response);
									}
									unBlockUI();
								}
							});
						} else if (cekTransaksi == 'edit') {
							$.ajax({
								type: 'POST',
								url: pageUri + 'update_data',
								data: $('#formulir_modal').serialize(),
								beforeSend: function() {
									blockUI();
								},
								success: function(response) {
									if (response == '1') {
										toastr.success('Data berhasil diupdate');
										table.ajax.reload(null, false);
										$('#modal_barang').modal('hide');
									} else if (response == '0') {
										toastr.warning('Data gagal diupdate');
									} else {
										toastr.error(response);
									}
									unBlockUI();
								}
							});
						}

					} else {
						toastr.error('Bahan Melebihi Batasan 100%');
					}
				} else {
					toastr.warning('Kode Tidak Tersedia');
				}
				return false;
			}
		});
		persen();
	});

	function satuan2(obj) {
		var pageUri = $('#pageUri').val();
		$.ajax({
			type: 'GET',
			url: pageUri + 'get_satuanbarang',
			success: function(response) {
				$(obj).closest('.satuan2').html('').append('<label>Satuan 2<a class="text-danger" onclick="del_satuan2(this)"><i class="icon-cross3"></i></a></label><select class="form-control satuan2" name="satuan2">' + response + '</select>');
			}
		});
	}

	function open_form_detail(obj) {
		var pageUri = $('#pageUri').val();
		$.ajax({
			type: 'GET',
			url: pageUri + 'get_proses',
			success: function(response) {
				$(obj).closest('.form_detail').html('').append('<label><b>Tahapan Proses</b></label><div class="tahapan"><div class="row"><div class="col-lg-1 text-center"><label>No</label><input class="form-control no_urut" type="text" disabled></div><div class="col-lg-9 text-center"><label>Tahapan</label><select class="form-control" name="tahap_proses[]">' + response + '</select></div><div class="col-lg-2 text-center"><label>Aksi</label><a class="btn btn-primary text-light form-control" onclick="tambah_proses(this)"><i class="icon-plus3"> Tambah</i></a></div></div></div><br><label><b>Komponen Benang</b></label><div class="komponen"><div class="row"><div class="col-lg-1 text-center"><label>No</label><input class="form-control no_urut2" type="text" disabled></div><div class="col-lg-6 text-center"><label>Nama Benang</label><select class="form-control get_bahan" name="benang[]"></select></div><div class="col-lg-2 text-center"><label>Jumlah</label><input class="form-control text-center persen_benang" name="persen_benang[]" onkeyup="count_benang()" onchange="count_benang()"/></div><div class="col-lg-1 text-center"><br><br><label><b>%</b></label></div><div class="col-lg-2 text-center"><label>Aksi</label><a class="btn btn-primary text-light form-control" onclick="tambah_bahan(this)"><i class="icon-plus3"> Tambah</i></a></div></div></div><div class="row"><div class="col-lg-6"></div><div class="col-lg-1 text-center"><label>Total</label></div><div class="col-lg-2 text-center"><input class="form-control text-right" type="text" id="jumlah_benang" readonly /></div></div>');
				pemberian_no();
				pemberian_no2();
				get_bahan();
				persen();
			}
		});
	}

	function tambah_proses(obj) {
		var pageUri = $('#pageUri').val();
		$.ajax({
			type: 'GET',
			url: pageUri + 'get_proses',
			success: function(response) {
				$(obj).closest('.tahapan').append('<div class="row"><br><div class="col-lg-1 text-center"><input class="form-control no_urut" type="text" disabled></div><div class="col-lg-9 text-center"><select class="form-control" name="tahap_proses[]">' + response + '</select></div><div class="col-lg-2 text-center"><a class="btn btn-warning text-light form-control" onclick="del_proses(this)"><i class="icon-cross2"> Hapus</i></a></div></div>');
				pemberian_no();
			}
		});
	}

	function tambah_bahan(obj) {
		$(obj).closest('.komponen').append('<div class="row"><br><div class="col-lg-1 text-center"><input class="form-control no_urut2" type="text" disabled></div><div class="col-lg-6 text-center"><select class="form-control get_bahan" name="benang[]"></select></div><div class="col-lg-2 text-center"><input class="form-control persen_benang text-right" type="text" name="persen_benang[]" onkeyup="count_benang()" onchange="count_benang()"/></div><div class="col-lg-1 text-center"><br><label><b>%</b></label></div><div class="col-lg-2 text-center"><a class="btn btn-warning text-light form-control" onclick="del_bahan(this)"><i class="icon-cross2"> Hapus</i></a></div></div>');
		get_bahan();
		pemberian_no2();
		persen();
	}

	function del_satuan2(obj) {
		$(obj).closest('.satuan2').html('').append('<label>&nbsp;</label><a class="btn btn-primary form-control" onclick="satuan2(this)"><i class="icon-plus3"></i>&nbsp;Tambah Satuan</a>');
	}

	function del_proses(obj) {
		$(obj).closest('.row').remove();
		pemberian_no();
	}

	function del_bahan(obj) {
		$(obj).closest('.row').remove();
		pemberian_no2();
		count_benang();
	}

	function replaceAll(str, token, newToken, ignoreCase) {
		var _token;
		var i = -1;

		if (typeof token === "string") {

			if (ignoreCase) {

				_token = token.toLowerCase();

				while ((
						i = str.toLowerCase().indexOf(
							_token, i >= 0 ? i + newToken.length : 0
						)) !== -1) {
					str = str.substring(0, i) +
						newToken +
						str.substring(i + token.length);
				}

			} else {
				return str.split(token).join(newToken);
			}

		}
		return str;
	};

	function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
		try {
			decimalCount = Math.abs(decimalCount);
			decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

			const negativeSign = amount < 0 ? "-" : "";

			let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
			let j = (i.length > 3) ? i.length % 3 : 0;

			return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
		} catch (e) {
			console.log(e)
		}
	}

	function count_benang() {
		var persen = 0;
		var a = 0;
		var tObj = document.getElementsByClassName('persen_benang');
		for (var i = 0; i < tObj.length; i++) {
			if (tObj[i].value == null || tObj[i].value == 'undefined' || tObj[i].value == '') {
				a = 0;
				tObj[i].value = 0;
			} else {
				a = replaceAll(tObj[i].value, '.', '');
				a = a.replace(/[,]+/g, ".");
			}
			persen += parseFloat(a);
		}
		$('#jumlah_benang').val(formatMoney(persen));
	}

	function persen() {
		$(".persen_benang").inputmask({
			'alias': 'decimal',
			'autoGroup': true,
			'radixPoint': ",",
			'min': 0,
			'max': 100
		});
	}

	function check_kat(obj) {
		var kat = $(obj).val();
		if (kat === '3') {
			$('.form_detail').css('display', 'block');
		} else {
			$('.form_detail').css('display', 'none');
		}
	}

	function pemberian_no() {
		var no_urut = parseInt(1);
		var tObj = document.getElementsByClassName('no_urut');
		for (var i = 0; i < tObj.length; i++) {
			tObj[i].value = no_urut;
			no_urut += parseInt(1);
		}
	};

	function pemberian_no2() {
		var no_urut = parseInt(1);
		var tObj = document.getElementsByClassName('no_urut2');
		for (var i = 0; i < tObj.length; i++) {
			tObj[i].value = no_urut;
			no_urut += parseInt(1);
		}
	};

	function get_bahan() {
		var pageUri = $('#pageUri').val();
		$(".get_bahan").select2({
			placeholder: "Masukan Nama Bahan",
			width: "100%",
			ajax: {
				url: pageUri + 'get_bahan',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response

					};
				},
				cache: false
			}
		});
	};

	function input_data() {
		$('#formulir_modal').clearForm();
		$('.satuan2').html('<label>&nbsp;</label><a class="btn btn-primary form-control" onclick="satuan2(this)"><i class="icon-plus3"></i>&nbsp;Tambah Satuan</a>');
		$('.form_detail').css('display', 'none');
		$('.form_detail').html('<div class="row"><div class="col-lg-3"><a class="btn btn-success form-control" onclick="open_form_detail(this)"><i class="icon-plus3"></i>&nbsp;Detail Produksi</a></div></div>');
		$("#formulir_modal").attr('transaksi', 'tambah');
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Tambah');
		$('#formSearch').dialog('close');
		$('#modal_barang').modal('show');
		cek_kode();
		return false;
	}

	function delete_data(id) {
		var pageUri = $('#pageUri').val();

		swal({
			title: "Anda Yakin?",
			text: "Anda akan hapus data ini.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes!",
			closeOnConfirm: false
		}, function() {
			$.ajax({
				type: 'GET',
				url: pageUri + 'delete_data/' + id,
				beforeSend: function() {
					blockUI();
				},
				success: function(response) {
					if (response == '1') {
						table.ajax.reload(null, false);
						toastr.success('Data berhasil dihapus');
					} else if (response == '0') {
						toastr.error('Data gagal dihapus');
					} else {
						toastr.warning(response);
					}
					swal.close();
					unBlockUI();
				}
			});
		});
	};

	function update_data(id) {
		var pageUri = $('#pageUri').val();
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Edit');
		$('#id').val(id);
		var supp = 0;
		$.ajax({
			type: 'POST',
			data: $('#formulir_modal').serialize(),
			url: pageUri + 'select_data',
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#kat_barang').val(html.data[0].kategori).change();
					if (html.det != null) {
						// $('.form_detail').css('display', 'block');
						$('.form_detail').html('').append(html.det);
						get_bahan();
						pemberian_no();
						pemberian_no2();
						var tObj = document.getElementsByClassName('get_bahan');
						for (var i = 0; i < tObj.length; i++) {
							$('#get_bahan_' + i).empty().append($("<option/>").val(html.bahan[i].id_benang).text(html.bahan[i].kode + ' - ' + html.bahan[i].nama)).trigger('change');
						}
					}
					$('#tipe_barang').val(html.data[0].tipe_barang);
					$('#nama_barang').val(html.data[0].nama);
					$('#tipe_pajak').val(html.data[0].tipe_pajak);
					if (html.sat2 != null) {
						$('.satuan2').html('').append(html.sat2);
						var tObj = document.getElementsByClassName('satuan');
						tObj[0].value = html.data[0].satuan1;
					} else {
						var tObj = document.getElementsByClassName('satuan');
						tObj[0].value = html.data[0].satuan1;
					}

					$('#modal_barang').modal('show');
					unBlockUI();
				} else {
					toastr.warning('Data tidak ditemukan');
					unBlockUI();
				}
			}
		});
	};

	function detail_komponen(id) {
		var pageUri = $('#pageUri').val();
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Komponen Barang');
		$("#id_view").val(id);
		$.ajax({
			type: 'POST',
			data: $('#formulir_modal_view').serialize(),
			url: pageUri + 'detail_komponen',
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#formulir_modal_view').clearForm();
					$('.form_det').html('').append(html.det);
					$('#modal_barang_view').modal('show');
					unBlockUI();
				} else {
					toastr.warning('Data tidak ditemukan');
					unBlockUI();
				}
			}
		});
	}

	function detail_proses(id) {
		var pageUri = $('#pageUri').val();
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Proses Barang');
		$("#id_view").val(id);
		$.ajax({
			type: 'POST',
			data: $('#formulir_modal_view').serialize(),
			url: pageUri + 'detail_proses',
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#formulir_modal_view').clearForm();
					$('.form_det').html('').append(html.det);
					$('#modal_barang_view').modal('show');
					unBlockUI();
				} else {
					toastr.warning('Data tidak ditemukan');
					unBlockUI();
				}
			}
		});
	}

	function cek_kode() {
		var cekTransaksi = $("#formulir_modal").attr('transaksi');
		if (cekTransaksi == 'tambah') {
			$('#kode_barang').keyup(function() {
				var pageUri = $('#pageUri').val();
				var kode = $('#kode_barang').val();
				if (kode == null || kode == 'undefined' || kode == '') {
					$('.respon_kode').text('');
					$('.respon_kode').removeClass('text-success');
					$('.respon_kode').removeClass('text-warning');
				} else {
					$.ajax({
						type: 'GET',
						url: pageUri + 'cek_kode/' + kode,
						success: function(response) {
							if (response != '') {
								$('.respon_kode').text('Kode Tidak Tersedia');
								$('.respon_kode').removeClass('text-success');
								$('.respon_kode').addClass('text-warning');
							} else {
								$('.respon_kode').text('Kode Tersedia');
								$('.respon_kode').removeClass('text-warning');
								$('.respon_kode').addClass('text-success');

							}
						}
					});
				}

			});
		} else {
			$('#kode_barang').keyup(function() {
				var pageUri = $('#pageUri').val();
				var kode = $('#kode_barang').val();
				if (kode == null || kode == 'undefined' || kode == '') {
					$('.respon_kode').text('');
					$('.respon_kode').removeClass('text-success');
					$('.respon_kode').removeClass('text-warning');
				} else {
					$.ajax({
						type: 'POST',
						url: pageUri + 'cek_kode_edit',
						data: $('#formulir_modal').serialize(),
						success: function(response) {
							if (response != '') {
								$('.respon_kode').text('Kode Tidak Tersedia');
								$('.respon_kode').removeClass('text-success');
								$('.respon_kode').addClass('text-warning');
							} else {
								$('.respon_kode').text('Kode Tersedia');
								$('.respon_kode').removeClass('text-warning');
								$('.respon_kode').addClass('text-success');

							}
						}
					});
				}
			});
		}
	}
</script>