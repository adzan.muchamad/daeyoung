<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <form id="formsearch" onSubmit="return false;">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button id="button_form_dropdown_search" type="button" class="btn dropdown-toggle btn-icon btn-default btn-raised" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu dropdown-content" style="width:395px;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>kategori</label>
                                        <select class="form-control" id="searchkat" name="searchkat">
                                            <option value="">Semua</option>
                                            <?php
                                            $q = $this->inventory_barang->get_kategoribarang();
                                            if ($q->num_rows() > 0) {
                                                foreach ($q->result_array() as $r) {
                                                    echo '<option value="' . $r['id'] . '">' . $r['nama'] . '</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>kategori</label>
                                        <select class="form-control" id="searchtipe" name="searchtipe">
                                            <option value="">Semua</option>
                                            <option value="1">Hasil Produksi</option>
                                            <option value="2">Dibeli</option>
                                            <option value="3">Servis/Jasa/Barang di Biayakan</option>
                                            <option value="4">Scrap</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                    <input type="text" class="form-control" style="width:250px;" placeholder="Pencarian" id="search_keyword" name="search_keyword">

                    <div class="input-group-btn">
                        <button id="buttonPencarian" type="button" class="btn btn-default btn-raised"><b><i class="icon-search4"></i></b></button>
                        <button type="button" class="btn btn-default btn-raised" onClick="input_data()"><b><i class="icon-plus-circle2"></i></b> Tambah Barang</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <table id="datamain_datatable" style="width:100%" class="table datatable-ajax table-xxs text-size-small table-bordered">
        <thead>
            <tr>
                <th>no</th>
                <th>kategori</th>
                <th>kode</th>
                <th>nama</th>
                <th>satuan 1</th>
                <th>satuan 2</th>
                <th>pajak</th>
                <th>tipe barang</th>
                <th>detail proses</th>
                <th>detail komponen</th>
                <th><i class="icon-menu7 position-left"></i></th>
            </tr>
        </thead>
    </table>
</div>



<?php echo $insert_view; ?>