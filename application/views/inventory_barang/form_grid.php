<input type="hidden" id="id" name="id" />


<div class="row">
    <div class="col-lg-4">
        <label>Kode Barang</label>
        <input type="text" class="form-control" id="kode_barang" name="kode_barang" /><a class="respon_kode"></a>
    </div>
    <div class="col-lg-4">
        <label>Kategori</label>
        <select class="form-control" id="kat_barang" name="kat_barang" onchange="check_kat(this)">
            <?php
            $q = $this->inventory_barang->get_kategoribarang();
            if ($q->num_rows() > 0) {
                foreach ($q->result_array() as $r) {
                    echo '<option value="' . $r['id'] . '">' . $r['nama'] . '</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="col-lg-4">
        <label>Tipe Barang</label>
        <select class="form-control" id="tipe_barang" name="tipe_barang">
            <option value="1">Hasil Produksi</option>
            <option value="2">Dibeli</option>
            <option value="3">Servis/Jasa/Barang di Biayakan</option>
            <option value="4">Scrap</option>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <label>Nama Barang</label>
        <input class="form-control" type="text" id="nama_barang" name="nama_barang" />
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <label>Tipe Pajak</label>
        <select class="form-control" id="tipe_pajak" name="tipe_pajak">
            <option value="1">PPN</option>
            <option value="2">PPnBM</option>
            <option value="3">PPh23</option>
            <option value="4">Bebas Pajak</option>
            <option value="5">PPh22-0,3%</option>
            <option value="6">PPN + PPh22(0,3%)</option>
        </select>
    </div>
    <div class="col-lg-4 satuan1">
        <label>Satuan</label>
        <select class="form-control satuan" name="satuan">
            <?php
            $q = $this->inventory_barang->get_satuanbarang();
            if ($q->num_rows() > 0) {
                foreach ($q->result_array() as $r) {
                    echo '<option value="' . $r['id'] . '">' . $r['kode'] . ' - ' . $r['nama'] . '</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="col-lg-4 satuan2">
        <label>&nbsp;</label>
        <a class="btn btn-primary form-control" onclick="satuan2(this)"><i class="icon-plus3"></i>&nbsp;Tambah Satuan</a>
    </div>
</div>
<hr>
<input type="hidden" id="persen">
<div class="form_detail" style="display: none">
    <div class="row">
        <div class="col-lg-3">
            <a class="btn btn-success form-control" onclick="open_form_detail(this)"><i class="icon-plus3"></i>&nbsp;Detail Produksi</a>
        </div>
    </div>
</div>
<br>
<br>
<br>