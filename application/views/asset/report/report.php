<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <form action="javascript" class="row">
            <div class="col-md-6 form-group">
                <label for="">Tanggal <span class="text-danger">*</span></label>
                <input type="text" name="tgl" id="tgl" class="form-control date_picker" readonly="readonly" required="required">
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 form-group">
                <label for="">Jenis <span class="text-danger">*</span></label>
                <?= form_dropdown('jenis', $jenis, '', 'id="jenis" class="form-control" multiple'); ?>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <button class="btn btn-primary"><i class="fa fa-download"></i> Download</button>
                </div>
            </div>
        </form>
    </div>
</div>



<?php echo $insert_view; ?>