<script>
	let baseUrl = "<?= base_url(); ?>";
	$("#jenis").select2({
        placeholder: 'Pilih Jenis'
    });

    $("form").submit(function(e) {
        e.preventDefault();

        let tgl = $("#tgl").val();
        let jenis = $("#jenis").val();

        window.open(baseUrl + 'Asset/download?tgl=' + tgl + '&jenis=' + jenis, '_blank');

        return false;
    });
</script>