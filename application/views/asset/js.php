<script>
	let baseUrl = "<?= base_url(); ?>";
    $.extend($.fn.dataTable.defaults, {
        autoWidth: true,
        sLength: "form-control"
    });

    var height_windows = $(window).height();
    var height_navbar = $('#navbar-main').height();
    var height_page = $('#page-header').height();
    var height_table = height_windows - height_navbar - height_page - 370;
    var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

	const dtTable = $("#datamain_datatable").DataTable({
		processing: true, 
		serverSide: true,
		ajax: {
			url: baseUrl + 'Asset/dt_jenis',
            type: 'get',
            data: function(data) {
                data.searchtaxgroup = $('#searchtaxgroup').val();
                data.search_keyword = $('#search_keyword').val();
            },
            beforeSend: function() {
                blockElement('datamain_datatable_wrapper');
            }
		},
        drawCallback: function(settings) {
            unBlockElement('datamain_datatable_wrapper');
        },
        dom: 'J<"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
        pageLength: 50,
        aaSorting: [
            [2, 'asc']
        ],
		columnDefs: [
			{targets: 0, data: 'no', orderable: false},
			{targets: 1, data: 'kode'},
			{targets: 2, data: 'jenis'},
			{targets: 3, data: 'tools', orderable: false, width: '180px'}
		],
        language: {
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            }
        }
	});

	$("#buttonPencarian").click(function() {
		dtTable.ajax.reload(null, false);
	});

    $('#search_keyword').keyup(function() {
        dtTable.ajax.reload(null, false);
    });

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto',
    });

	function save() {
        blockElement('form-data');

        let config = {
            url: base_url + 'Asset/add_jenis',
            data: $("#form-data").serialize(),
            dataType: 'json',
            type: 'post'
        }

        $.ajax(config)
            .done(function(data) {

                unBlockElement('form-data');

                if (data.status == 1) {
                    $("#modal_form").modal('hide');
                    dtTable.ajax.reload(null, false);
                    toastr.success(data.message);
                } else {
                    toastr.error(data.message);
                }
            })
            .fail(function(e) {
            	unBlockElement('form-data');
                console.error('Terjadi kesalahan saat menyimpan data');
            });
    }

	function input_data() {
		$("#modal_form").modal('show');
        $("#form-data").trigger('reset');
        $("#kode").attr('readonly', false);
	}

    $(document).on('click', '.btn-hapus', function() {
        let id = $(this).data('id');

        swal({
            title: "Anda Yakin?",
            text: "Anda akan hapus data ini.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false
        }, function() {
            $.ajax({
                type: 'GET',
                url: baseUrl + 'Asset/delete_jenis_asset?id=' + id,
                dataType: 'json',
                beforeSend: function() {
                    $('.sweet-alert').block({ 
                        message: '<i class="icon-spinner4 spinner"></i>',
                        showOverlay: true,
                        css: {
                            width: 16,
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(data) {
                    if (data.status == 1) {
                        dtTable.ajax.reload(null, false);
                        toastr.success(data.message);
                    } else {
                        toastr.error(data.message);
                    }

                    swal.close();
                    $('.sweet-alert').unblock();
                },
                error: function(e) {
                    toastr.error('Terjadi kesalahan saat menghapus data');
                    $('.sweet-alert').unblock();
                }
            });
        });
    });

    $(document).on('click', '.btn-edit', function() {
        let id = $(this).data('id');
        $("#modal_form").modal('show');

        let config = {
            url: base_url + 'Asset/jenis_id?id=' + id,
            dataType: 'json'
        }

        blockUI();

        $.ajax(config)
            .done(function(data) {
                $("#id").val(data.id);
                $("#kode").val(data.kode);
                $("#jenis").val(data.jenis);
                $("#kode").attr('readonly', true);

                unBlockUI();
            })
            .fail(function(e) {
                console.error('Terjadi kesalahan saat memuat data');

                unBlockUI();
            });
    });
</script>
