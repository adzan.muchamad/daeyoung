<div class="modal fade " id="modal_form" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Form Control Asset</h4>
            </div>
            <form id="form-data" method="POST">
                <div class="modal-body">
                    <div class="row">
                        <input type="text" class="hidden" name="id" id="id">
                        <div class="form-group col-md-6">
                            <label for="">Kode Asset <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="kode" name="kode" required="required" readonly="readonly">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="">Jenis <span class="text-danger">*</span></label>
                            <?= form_dropdown('jenis', $jenis, '', 'id="jenis" class="form-control"'); ?>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Nama Asset <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" required="required">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="">Merk <span class="text-danger">*</span></label>
                            <?= form_dropdown('merk', $merk, '', 'id="merk" class="form-control"'); ?>
                        </div>

                        <div class="clearfix"></div>

                        <div class="form-group col-md-6">
                            <label for="">Keterangan</label>
                            <textarea name="keterangan-tambah" id="keterangan-tambah" cols="5" rows="4" class="form-control"></textarea>
                        </div>

                        <div class="col-md-12">
                            <hr />
                        </div>

                        <div class="form-group col-md-4">
                            <label for="">Tanggal beli <span class="text-danger">*</span></label>
                            <input type="text" class="form-control date_picker" id="tglbeli" name="tglbeli" readonly="readonly" required="required">   
                        </div>
                        
                        <div class="form-group col-md-4">
                            <label for="">Jumlah / Unit <span class="text-danger">*</span></label>
                            <input type="number" name="unit" id="unit" class="form-control" value="0" required="required">
                        </div>

                        <div class="form-group col-md-4">
                            <label for="">Harga <sup>per unit</sup> <span class="text-danger">*</span></label>
                            <input type="number" name="harga" id="harga" class="form-control" value="0" required="required">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" id="btn-simpan" class="btn btn-success ">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade " id="modal-pengurangan" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Pengurangan Asset</h4>
            </div>
            <form action="#" id="form-pengurangan">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Kode <span class="text-danger">*</span></label>
                        <input type="hidden" name="id-asset" id="id-asset">
                        <input type="text" name="kode-pengurangan" id="kode-pengurangan" class="form-control" placeholder="Kode" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label>Nama <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="nama-pengurangan" id="nama-pengurangan" placeholder="Nama" required="required" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal <span class="text-danger">*</span></label>
                        <input type="text" class="date_picker form-control" id="tgl-pengurangan" name="tgl-pengurangan" readonly="readonly" required="required">  
                    </div>
                    <div class="form-group">
                        <label for="">Jumlah Pengurangan <span class="text-danger">*</span></label>
                        <input type="number" name="pengurangan" id="pengurangan" class="form-control" required="required">
                    </div> 
                    <div class="form-group">
                        <label for="">Jenis Pengurangan <span class="text-danger">*</span></label>
                        <select name="jenis-pengurangan" id="jenis-pengurangan" class="form-control">
                            <option value="Dijual">Dijual</option>
                            <option value="Rusak">Rusak</option>
                            <option value="Hilang">Hilang</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Keterangan</label>
                        <textarea name="keterangan" id="keterangan" cols="5" rows="5" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success ">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-history" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">History Asset</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table datatable-basic" id="tb-history" style="width: 100%;">
                        <thead>
                            <tr>
                                <th width="10%">#</th>
                                <th>Kode</th>
                                <th>Nama Asset</th>
                                <th>Tanggal</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>