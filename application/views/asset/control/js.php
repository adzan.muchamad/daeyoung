<script>
	let baseUrl = "<?= base_url(); ?>";
    $.extend($.fn.dataTable.defaults, {
        autoWidth: true,
        sLength: "form-control"
    });

    var height_windows = $(window).height();
    var height_navbar = $('#navbar-main').height();
    var height_page = $('#page-header').height();
    var height_table = height_windows - height_navbar - height_page - 370;
    var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

	const dtTable = $("#datamain_datatable").DataTable({
		processing: true, 
		serverSide: true,
		ajax: {
			url: baseUrl + 'Asset/dt_control_asset',
            type: 'get',
            data: function(data) {
                data.searchtaxgroup = $('#searchtaxgroup').val();
                data.search_keyword = $('#search_keyword').val();
            },
            beforeSend: function() {
                blockElement('datamain_datatable_wrapper');
            }
		},
        drawCallback: function(settings) {
            unBlockElement('datamain_datatable_wrapper');
        },
        dom: 'J<"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
        pageLength: 50,
        aaSorting: [
            [2, 'asc']
        ],
		columnDefs: [
			{targets: 0, data: 'no', orderable: false},
			{targets: 1, data: 'kode_asset'},
			{targets: 2, data: 'nama'},
			{targets: 3, data: 'merk'},
			{targets: 4, data: 'jenis'},
			{targets: 5, data: 'keterangan'},
			{targets: 6, data: 'tgl_beli'},
			{targets: 7, data: 'unit'},
			{targets: 8, data: 'harga_beli'},
			{targets: 9, data: 'total'},
			{targets: 10, data: 'tools', orderable: false, width: '180px'}
		],
        language: {
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            }
        },
        scrollX: '100%',
		scrollY: height_table,
		scrollCollapse: false,
		fixedColumns: {
			leftColumns: 0,
			rightColumns: 1
		}
	});

	$("#buttonPencarian").click(function() {
		dtTable.ajax.reload(null, false);
	});

    $('#search_keyword').keyup(function() {
        dtTable.ajax.reload(null, false);
    });

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto',
    });

    const dtHistory = $("#tb-history").DataTable({
        processing: true,
        serverSide: false,
        ajax: {
            url: baseUrl + 'Asset/dt_history/'
        }
    });

    function input_data() {
        $("#modal_form").modal('show');
        $("#form-data").trigger('reset');
    }

    $("#form-data").submit(function(e) {
        e.preventDefault();

        let formData = new FormData($(this)[0]);
        let config = {
            url: baseUrl + 'Asset/add_control',
            type: 'post',
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false
        }

        blockElement('form-data');

        $.ajax(config)
            .done(function(data) {
                unBlockElement('form-data');

                if (data.status == 1) {
                    toastr.success(data.message);
                    $("#modal_form").modal('hide');
                } else {
                    toastr.error(data.message);
                }

                dtTable.ajax.reload(null, false);
            })
            .fail(function(e) {
                unBlockElement('form-data');
                toastr.error('Terjadi kesalahan saat menyimpan data');
            });

        return false;
    });

    $(document).on('click', '.btn-kurang', function() {
        let kode = $(this).data('kode');
        let nama = $(this).data('nama');
        let id = $(this).data('id');

        $("#modal-pengurangan").modal('show');

        $("#form-pengurangan").trigger('reset');

        $("#kode-pengurangan").val(kode);
        $("#nama-pengurangan").val(nama);
        $("#id-asset").val(id);
    });

    $("#form-pengurangan").submit(function(e) {
        e.preventDefault();

        let formData = new FormData($(this)[0]);
        let config = {
            url: baseUrl + 'Asset/pengurangan',
            type: 'post',
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false
        }

        blockElement('form-pengurangan');

        $.ajax(config)
            .done(function(data) {
                unBlockElement('form-pengurangan');

                if (data.status == 1) {
                    toastr.success(data.message);
                    $("#modal-pengurangan").modal('hide');
                } else {
                    toastr.error(data.message);
                }

                dtTable.ajax.reload(null, false);
            })
            .fail(function(e) {
                unBlockElement('form-pengurangan');
                toastr.error('Terjadi kesalahan saat menyimpan data');
            });

        return false;
    });

    $(document).on('click', '.btn-history', function() {
        let id = $(this).data('id');

        $("#modal-history").modal('show');
        dtHistory.ajax.url(baseUrl + 'Asset/dt_history?id=' + id).load();
    });

    $(document).on('click', '.btn-delete', function() {
        let id = $(this).data('id');

        swal({
            title: "Anda Yakin?",
            text: "Anda akan hapus data ini.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            closeOnConfirm: false
        }, function() {
            $.ajax({
                type: 'GET',
                url: baseUrl + 'Asset/delete_asset?id=' + id,
                dataType: 'json',
                beforeSend: function() {
                    $('.sweet-alert').block({ 
                        message: '<i class="icon-spinner4 spinner"></i>',
                        showOverlay: true,
                        css: {
                            width: 16,
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function(data) {
                    if (data.status == 1) {
                        dtTable.ajax.reload(null, false);
                        toastr.success(data.message);
                    } else {
                        toastr.error(data.message);
                    }

                    swal.close();
                    $('.sweet-alert').unblock();
                },
                error: function(e) {
                    toastr.error('Terjadi kesalahan saat menghapus data');
                    $('.sweet-alert').unblock();
                }
            });
        });
    });

    $(document).on('click', '.btn-edit', function() {
        let id = $(this).data('id');

        blockUI();

        let config = {
            url: baseUrl + 'Asset/control_id?id=' + id,
            dataType: 'json' 
        }

        $.ajax(config)
            .done(function(data) {
                $("#modal_form").modal('show');
                $("#id").val(data.id);
                $("#kode").val(data.kode);
                $("#nama").val(data.nama);
                $("#merk").val(data.merk);
                $("#jenis").val(data.jenis);
                $("#keterangan-tambah").val(data.keterangan);
                $("#tglbeli").val(data.tglbeli);
                $("#unit").val(data.unit);
                $("#harga").val(data.harga);

                unBlockUI();
            })
            .fail(function(e) {
                unBlockUI();
                toastr.error('Terjadi kesalahan saat memuat data');
            });
    });
</script>