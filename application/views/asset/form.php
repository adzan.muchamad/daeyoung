<div class="modal fade " id="modal_form" tabindex="-1" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Form Jenis</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form id="form-data" method="POST">
                            <input type="text" class="hidden" name="id" id="id">
                            
                            <div class="form-group">
                                <label>Kode <span class="text-danger">*</span></label>
                                <div>
                                    <input type="text" class="form-control" name="kode" id="kode" placeholder="Kode" required="required">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Jenis <span class="text-danger">*</span></label>
                                <div>
                                    <input type="text" class="form-control" name="jenis" id="jenis" placeholder="Jenis" required="required">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" onclick="save()" id="btn-simpan" class="btn btn-success ">Simpan</button>
            </div>
        </div>
    </div>
</div>