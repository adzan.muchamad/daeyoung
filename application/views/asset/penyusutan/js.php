<script>
	let baseUrl = "<?= base_url(); ?>";
    $.extend($.fn.dataTable.defaults, {
        autoWidth: true,
        sLength: "form-control"
    });

    var height_windows = $(window).height();
    var height_navbar = $('#navbar-main').height();
    var height_page = $('#page-header').height();
    var height_table = height_windows - height_navbar - height_page - 370;
    var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

	const dtTable = $("#datamain_datatable").DataTable({
		processing: true, 
		serverSide: true,
		ajax: {
			url: baseUrl + 'Asset/dt_penyusutan',
            type: 'get',
            data: function(data) {
                data.searchtaxgroup = $('#searchtaxgroup').val();
                data.search_keyword = $('#search_keyword').val();
            },
            beforeSend: function() {
                blockElement('datamain_datatable_wrapper');
            }
		},
        drawCallback: function(settings) {
            unBlockElement('datamain_datatable_wrapper');
        },
        dom: 'J<"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
        pageLength: 50,
        aaSorting: [
            [2, 'asc']
        ],
		columnDefs: [
			{targets: 0, data: 'no', orderable: false},
			{targets: 1, data: 'kode_asset'},
			{targets: 2, data: 'nama'},
			{targets: 3, data: 'tgl_beli'},
			{targets: 4, data: 'unit'},
			{targets: 5, data: 'tarif'},
			{targets: 6, data: 'durasi'},
			{targets: 7, data: 'tools', orderable: false, width: '180px'}
		],
        language: {
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: {
                'first': 'First',
                'last': 'Last',
                'next': '&rarr;',
                'previous': '&larr;'
            }
        },
        scrollX: '100%',
		scrollY: height_table,
		scrollCollapse: false,
		fixedColumns: {
			leftColumns: 0,
			rightColumns: 1
		}
	});

	$("#buttonPencarian").click(function() {
		dtTable.ajax.reload(null, false);
	});

    $('#search_keyword').keyup(function() {
        dtTable.ajax.reload(null, false);
    });

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto',
    });

    $(document).on('click', '.btn-penyusutan', function() {
        let kode = $(this).data('kode'); 
        let nama = $(this).data('nama');
        let id = $(this).data('id');
        let tglbeli = $(this).data('tglbeli');

        $("#modal-penyusutan").modal('show');

        $("#id").val(id);
        $("#kode").val(kode);
        $("#nama").val(nama);
        $("#mulai").val(tglbeli);

        $("#tarif").val('');
        $("#durasi").val('');
    });

    $("#durasi").on('change', function() {
        let datestart = $("#mulai").val();
        let durasi = $("#durasi").val();

        blockElement('form-penyusutan');

        $.post(baseUrl + 'Asset/add_interval', {
            'datestart': datestart, 
            'durasi': durasi
        }, function(result) {
            $("#selesai").val(result);

            unBlockElement('form-penyusutan');
        });
    });

    $("#form-penyusutan").submit(function(e) {
        e.preventDefault();

        let formData = new FormData($(this)[0]);
        let config = {
            url: baseUrl + 'Asset/add_penyusutan',
            type: 'post',
            dataType: 'json',
            data: formData,
            processData: false,
            contentType: false
        }

        blockElement('form-penyusutan');

        $.ajax(config)
            .done(function(data) {
                unBlockElement('form-penyusutan');

                if (data.status == 1) {
                    toastr.success(data.message);
                    $("#modal-penyusutan").modal('hide');
                } else {
                    toastr.error(data.message);
                }

                dtTable.ajax.reload(null, false);
            })
            .fail(function(e) {
                unBlockElement('form-penyusutan');
                toastr.error('Terjadi kesalahan saat menyimpan data');
            });

        return false;
    });

</script>