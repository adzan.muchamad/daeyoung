<div class="modal fade " id="modal-penyusutan" role="dialog" data-backdrop="static" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Penyusutan Asset</h4>
            </div>
            <form action="#" id="form-penyusutan">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Kode <span class="text-danger">*</span></label>
                        <input type="hidden" name="id" id="id">
                        <input type="text" name="kode" id="kode" class="form-control" placeholder="Kode" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label>Nama <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" required="required" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label for="">Tarif <sup>%</sup><span class="text-danger">*</span></label>
                        <input type="number" name="tarif" id="tarif" class="form-control" required="required" step="any">
                    </div> 

                    <div class="form-group">
                        <label for="">Mulai Penyusutan <span class="text-danger">*</span></label>
                        <input type="text" class="date_picker form-control" id="mulai" name="mulai" readonly="readonly" required="required">  
                    </div>
                    
                    <div class="form-group">
                        <label for="">Durasi <sup>bln</sup><span class="text-danger">*</span></label>
                        <input type="number" name="durasi" id="durasi" class="form-control" required="required">
                    </div> 

                    <div class="form-group">
                        <label for="">Selesai Penyusutan <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="selesai" name="selesai" readonly="readonly" required="required">  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success ">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>