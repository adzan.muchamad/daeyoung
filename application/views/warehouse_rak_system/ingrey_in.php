<input type="hidden" id="id" name="id" />
<div class="row">
    <div class="col-lg-8 display-flex-center">
        <div class="col-lg-2">
            Scan Barcode LOT
        </div>
        <div class="col-lg-4">
            <input class="form-control" type="text" id="scan_lot_ing" name="scan_lot" onchange="cek_barcode();" />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            RAK
        </div>
        <div class="col-lg-3">
            <select class="form-control text-center" id="abjad_ing">
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
                <option value="E">E</option>
                <option value="F">F</option>
            </select>
        </div>
        <div class="col-lg-3">
            <select class="form-control text-center" id="urut_ing">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
            </select>
        </div>
        <div class="col-lg-3">
            <select class="form-control text-center" id="tingkat_ing">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-7 display-flex-center">
        <div class="col-lg-2">
            LOT
        </div>
        <div class="col-lg-5">
            <input class="form-control" type="text" id="lot" readonly />
        </div>
    </div>
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            ROL
        </div>
        <div class="col-lg-6">
            <input class="form-control" type="text" id="rol" readonly />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-7 display-flex-center">
        <div class="col-lg-2">
            Barang
        </div>
        <div class="col-lg-10">
            <select class="form-control" id="get_barang">
            </select>
        </div>
    </div>
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-2">
            Jumlah
        </div>
        <div class="col-lg-3">
            <input class="form-control" type="text" id="jmlh" />
        </div>
        <div class="col-lg-3">
            <select class="form-control" id="sat">
                <option value="1">MTR</option>
                <option value="2">YRD</option>
            </select>
        </div>
        <div class="col-lg-1">
            KG
        </div>
        <div class="col-lg-3">
            <input class="form-control" type="text" id="kg" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            Tanggal Produksi
        </div>
        <div class="col-lg-9" style="padding-left:32px">
            <input class="form-control date_picker" type="text" id="tgl" />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-4">
            Customer
        </div>
        <div class="col-lg-8">
            <input class="form-control" type="text" id="cust" />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-4">
            Ingrey
        </div>
        <div class="col-lg-8">
            <input class="form-control" type="text" id="tipeingrey" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-2 text-center" style="float:right">
        <a class="btn btn-primary form-control" onclick="add_barang(this)"><i class="icon-plus3"></i>&nbsp;Tambah</a>
    </div>
</div>
<hr>
<center style="margin-bottom:10px"><b>LIST BARANG</b></center>
<div class="list_barang">
    <table id="table_list_barang" style="width:100%" class="table datatable-ajax table-striped table-xxs table-bordered text-size-small">
        <colgroup>
            <col>
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-3">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-2">
            <col class="col-md-1">
            <col>
        </colgroup>
        <thead>
            <tr>
                <th class="text-center">NO</th>
                <th class="text-center">RAK</th>
                <th class="text-center">LOT</th>
                <th class="text-center">Barang</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Jumlah</th>
                <th class="text-center">Satuan</th>
                <th class="text-center">Berat</th>
                <th class="text-center">Customer</th>
                <th class="text-center">Ingrey</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<br>
<br>