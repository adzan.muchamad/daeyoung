<input type="hidden" id="id" name="id" />
<div class="row">
    <div class="col-lg-7 display-flex-center">
        <div class="col-lg-2">
            Bahan
        </div>
        <div class="col-lg-10">
            <select class="form-control" id="get_bahan" name="get_bahan">
            </select>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            RAK
        </div>
        <div class="col-lg-3">
            <select class="form-control text-center" id="abjad2">
                <option value="A">A</option>
                <option value="B">B</option>
                <option value="C">C</option>
                <option value="D">D</option>
                <option value="E">E</option>
            </select>
        </div>
        <div class="col-lg-3">
            <select class="form-control text-center" id="urut2">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
        </div>
    </div>
</div>
<hr>
<label><b>Informasi Barang</b></label>
<div class="row">
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            Isi
        </div>
        <div class="col-lg-6" style="padding-left:32px">
            <input class="form-control" type="text" id="isi" />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            Satuan
        </div>
        <div class="col-lg-6" style="padding-left:32px">
            <select class="form-control" id="sat_bahan" readonly>
                <option value="9">KGM</option>
            </select>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            Jumlah
        </div>
        <div class="col-lg-6" style="padding-left:32px">
            <input class="form-control" type="text" id="jmlh_bahan" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            Tanggal Masuk
        </div>
        <div class="col-lg-9" style="padding-left:32px">
            <input class="form-control date_picker" type="text" id="tgl_masuk" />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            Supplier
        </div>
        <div class="col-lg-8" style="padding-left:32px">
            <input class="form-control" type="text" id="supp" />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            Referensi
        </div>
        <div class="col-lg-8" style="padding-left:32px">
            <input class="form-control" type="text" id="refbahan" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-2 text-center" style="float:right">
        <a class="btn btn-primary form-control" onclick="add_bahan(this)"><i class="icon-plus3"></i>&nbsp;Tambah</a>
    </div>
</div>
<hr>
<center style="margin-bottom:10px"><b>LIST BARANG</b></center>
<div class="list_barang">
    <table id="table_list_bahan" style="width:100%" class="table datatable-ajax table-striped table-xxs table-bordered text-size-small">
        <colgroup>
            <col>
            <col class="col-md-1">
            <col class="col-md-3">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-2">
            <col class="col-md-1">
            <col>
        </colgroup>
        <thead>
            <tr>
                <th class="text-center">NO</th>
                <th class="text-center">RAK</th>
                <th class="text-center">Barang</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Jumlah (KG)</th>
                <th class="text-center">Jumlah (Dus)</th>
                <th class="text-center">Supplier</th>
                <th class="text-center">Ref</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<br>
<br>