<input type="hidden" id="id_out" name="id_out" />
<input type="hidden" id="id_barang_out" />
<input type="hidden" id="tgl_out" />
<div class="row">
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-4">
            <b>Scan Barcode LOT</b>
        </div>
        <div class="col-lg-6">
            <input class="form-control" type="text" id="scan_lot_out" name="scan_lot_out" onchange="cek_barcode_out();" />
        </div>
    </div>
    <div class="col-lg-6 display-flex-center">
        <div class="col-lg-3">
            <b>Customer</b>
        </div>
        <div class="col-lg-9">
            <select class="form-control" id="customer" name="customer"></select>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-4">
            <b>LOT</b>
        </div>
        <div class="col-lg-7" style="padding-left: 2px;">
            <input class="form-control" type="text" id="lot_out" readonly />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-5">
            <b>ROL</b>
        </div>
        <div class="col-lg-7">
            <input class="form-control" type="text" id="rol_out" readonly />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-5">
            <b>RAK</b>
        </div>
        <div class="col-lg-7">
            <input class="form-control" type="text" id="rak_out" readonly />
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-4">
            <b>Material Code Cust</b>
        </div>
        <div class="col-lg-8">
            <input class="form-control" type="text" id="material_code" />

        </div>
    </div>
    <div class="col-lg-8 display-flex-center">
        <div class="col-lg-2">
            <b>Barang</b>
        </div>
        <div class="col-lg-10">
            <input class="form-control" type="text" id="barang_out" readonly />
        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-5 display-flex-center">
        <div class="col-lg-3">
            <b>Qty</b>
        </div>
        <div class="col-lg-9" style="padding-left: 9px;">
            <input class="form-control" type="text" id="qty_out" />
            <input type="hidden" id="qty_out_limit" />
        </div>
    </div>
    <div class="col-lg-2 display-flex-center">
        <div class="col-lg-12">
            <select class="form-control" id="sat_out" onchange="konversi()">
                <option value="1">MTR</option>
                <option value="2">YRD</option>
            </select>
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-4">
            <b>Width</b>
        </div>
        <div class="col-lg-8" style="padding-left: 30px;">
            <input class="form-control" type="text" id="width_brg" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-3">
            <b>Berat Bersih</b>
        </div>
        <div class="col-lg-9" style="padding-left: 35px;">
            <input class="form-control" type="text" id="kg_out" />
            <input type="hidden" id="kg_out_limit" />
        </div>
    </div>
    <div class="col-lg-4 display-flex-center">
        <div class="col-lg-4">
            <b>Berat Kotor</b>
        </div>
        <div class="col-lg-8">
            <input class="form-control" type="text" id="bruto" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 display-flex-center">
        <div class="col-lg-1">
            <b>Note</b>
        </div>
        <div class="col-lg-11" style="padding-left:30px">
            <textarea class="form-control" type="text" id="note_out" name="note_out" style="resize:vertical;"></textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-2 text-center" style="float:right">
        <a class="btn btn-primary form-control" onclick="add_barang_out(this)"><i class="icon-plus3"></i>&nbsp;Tambah</a>
    </div>
</div>
<hr>
<center style="margin-bottom:10px"><b>LIST BARANG</b></center>
<div class="list_barang" id='list_barang3'>
    <table id="table_list_barang_out" style="width:100%" class="table datatable-ajax table-striped table-xxs table-bordered text-size-small">
        <colgroup>
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-3">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
        </colgroup>
        <thead>
            <tr>
                <th class="text-center">NO</th>
                <th class="text-center">RAK</th>
                <th class="text-center">LOT</th>
                <th class="text-center">Code Cust</th>
                <th class="text-center">Barang</th>
                <th class="text-center">Tanggal</th>
                <th class="text-center">Jumlah</th>
                <th class="text-center">Satuan</th>
                <th class="text-center">Berat Bersih</th>
                <th class="text-center">Berat Kotor</th>
                <th class="text-center">Width</th>
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<br>
<br>