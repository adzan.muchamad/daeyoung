<script type="text/javascript">
	$(function() {
		var pageUri = $('#pageUri').val();

		//SET DATATABLES
		$.extend($.fn.dataTable.defaults, {
			//autoWidth: true,
			"sLength": "form-control"
		});
		var height_windows = $(window).height();
		var height_navbar = $('#navbar-main').height();
		var height_page = $('#page-header').height();
		var height_table = height_windows - height_navbar - height_page - 370;
		var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

		//bahan
		table3 = $('#datamain_datatable3').DataTable({
			dom: '<"datatable-header"B><"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
			"pageLength": 10,
			aaSorting: [
				[0, 'asc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_detail_gudang_bahan',
				"type": "POST",
				"data": function(data) {
					data.rak = $('#id_rak_bahan').val();
					data.search_keyword = $('#search_keyword_det_bahan').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable3_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable3_wrapper');
			},
			columnDefs: [{
				orderable: false,
				className: "text-center",
				width: '150px',
				targets: [0]
			}, {
				className: "text-center",
				"targets": [1, 3, 4, 5, 6, 7, 8, 9]
			}, ],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
			"footerCallback": function(row, data, start, end, display) {
				var api = this.api(),
					data;

				// Remove the formatting to get integer data for summation
				var intVal = function(i) {
					return typeof i === 'string' ? i.split(',').join('') * 1 :
						typeof i === 'number' ?
						i : 0;
				};

				// Total over all pages
				// total = api
				// 	.column(15)
				// 	.data()
				// 	.reduce(function(a, b) {
				// 		return intVal(a) + intVal(b);
				// 	}, 0);

				// Total over this page
				berat = api
					.column(4, {
						page: 'current'
					})
					.data()
					.reduce(function(a, b) {
						return parseFloat(parseFloat(a) + parseFloat(b));
					}, 0);
				// Update footer

				$(api.column(4).footer()).html(

					berat + ' KG'
				);
				// Total over this page
				dus = api
					.column(5, {
						page: 'current'
					})
					.data()
					.reduce(function(a, b) {
						return parseFloat(parseFloat(a) + parseFloat(b));
					}, 0);
				// Update footer

				$(api.column(5).footer()).html(

					dus + ' Dus'
				);
			},
		});

		table4 = $('#datamain_datatable4').DataTable({
			dom: '<"datatable-header"B><"datatable-scroll datatable-scroll-wrap"t>',
			"ordering": false,
			aaSorting: [
				[0, 'asc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_history_jadi',
				"type": "POST",
				"data": function(data) {
					data.lot = $('#id_lot').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable4_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable4_wrapper');
			},
			columnDefs: [{
				orderable: false,
				className: "text-center",
				width: '150px',
				targets: [0]
			}, {
				className: "text-center",
				"targets": [1, 2, 4, 5, 6, 7]
			}, ],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			}
		});
		//barang jadi
		table = $('#datamain_datatable').DataTable({
			dom: '<"datatable-header"B><"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
			"pageLength": 10,
			aaSorting: [
				[0, 'asc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_detail_gudang_jadi',
				"type": "POST",
				"data": function(data) {
					data.rak = $('#id_rak').val();
					data.search_keyword = $('#search_keyword_det').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},
			columnDefs: [{
				orderable: false,
				className: "text-center",
				width: '150px',
				targets: [0]
			}, {
				className: "text-center",
				"targets": [1, 2, 4, 5, 6, 7, 8, 9, 10]
			}, ],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
			"footerCallback": function(row, data, start, end, display) {
				var api = this.api(),
					data;

				// Remove the formatting to get integer data for summation
				var intVal = function(i) {
					return typeof i === 'string' ? i.split(',').join('') * 1 :
						typeof i === 'number' ?
						i : 0;
				};

				// Total over all pages
				// total = api
				// 	.column(15)
				// 	.data()
				// 	.reduce(function(a, b) {
				// 		return intVal(a) + intVal(b);
				// 	}, 0);

				// Total over this page
				mtr = api
					.column(4, {
						page: 'current'
					})
					.data()
					.reduce(function(a, b) {
						return parseFloat(parseFloat(a) + parseFloat(b));
					}, 0);
				// Update footer

				$(api.column(4).footer()).html(

					mtr + ' MTR'
				);
				// Total over this page
				yard = api
					.column(5, {
						page: 'current'
					})
					.data()
					.reduce(function(a, b) {
						return parseFloat(parseFloat(a) + parseFloat(b));
					}, 0);
				// Update footer

				$(api.column(5).footer()).html(

					yard + ' YRD'
				);
				// Total over this page
				kg = api
					.column(6, {
						page: 'current'
					})
					.data()
					.reduce(function(a, b) {
						return parseFloat(parseFloat(a) + parseFloat(b));
					}, 0);
				// Update footer

				$(api.column(6).footer()).html(

					kg + ' KG'
				);
			},
		});

		table2 = $('#datamain_datatable2').DataTable({
			dom: '<"datatable-header"B><"datatable-scroll datatable-scroll-wrap"t>',
			"ordering": false,
			aaSorting: [
				[0, 'asc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_history_jadi',
				"type": "POST",
				"data": function(data) {
					data.lot = $('#id_lot').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable2_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable2_wrapper');
			},
			columnDefs: [{
				orderable: false,
				className: "text-center",
				width: '150px',
				targets: [0]
			}, {
				className: "text-center",
				"targets": [1, 2, 4, 5, 6, 7]
			}, ],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			}
		});

		$('#search_keyword').keyup(function() {
			cek_data();
		})
		$('#search_keyword_det').keyup(function() {
			table.ajax.reload(null, false);
		});
		$('.dataTables_length select').select2({
			minimumResultsForSearch: Infinity,
			width: 'auto',
		});
		//END SET DATATABLES
		$('.submit').on("click", function() {
			var a = $(this).closest('form.valid').attr('id');
			var b = $('#' + a).valid();
			if (b) {
				$('#' + a).submit();
			}
		});

		$('.bahanbaku').on("click", function() {
			$('#search_tipe').val('1');
			cek_data();
		});

		$('.ingrey_2').on("click", function() {
			$('#search_tipe').val('2');
			cek_data();
		});

		$('.barang_jadi').on("click", function() {
			$('#search_tipe').val('3');
			cek_data();
		});

		$('.tombol_in').on("click", function() {
			if ($('#search_tipe').val() == 1) {
				$('#form_bahan_in').clearForm();
				$("#form_bahan_in").attr('transaksi', 'tambah');
				$('#formSearch').dialog('close');
				$('#modal_bahan_in').modal('show');
				get_bahan();
				$('#isi').autoNumeric({
					mDec: '4'
				});
				$('#jmlh_bahan').autoNumeric({
					mDec: '4'
				});
				return false;
			} else if ($('#search_tipe').val() == 2) {

			} else if ($('#search_tipe').val() == 3) {
				$('#form_barang_in').clearForm();
				$("#form_barang_in").attr('transaksi', 'tambah');
				$('#formSearch').dialog('close');
				$('#modal_barang_in').modal('show');
				get_barang();
				$('#jmlh').autoNumeric({
					mDec: '4'
				});
				$('#kg').autoNumeric({
					mDec: '4'
				});
				return false;
			}
		});
		$('.tombol_out').on("click", function() {
			if ($('#search_tipe').val() == 3) {
				$('#form_barang_out').clearForm();
				$("#form_barang_out").attr('transaksi', 'tambah');
				$('#formSearch').dialog('close');
				$('#modal_barang_out').modal('show');
				$('#qty_out').autoNumeric({
					mDec: '4'
				});
				$('#kg_out').autoNumeric({
					mDec: '2'
				});
				$('#bruto').autoNumeric({
					mDec: '2'
				});
				get_cust();
				$('#customer').html('');
				$('#list_barang3').html('<table id="table_list_barang_out" style="width:100%" class="table datatable-ajax table-striped table-xxs table-bordered text-size-small"><colgroup><col class="col-md-1"><col class="col-md-1"><col class="col-md-1"><col class="col-md-1"><col class="col-md-3"><col class="col-md-1"><col class="col-md-1"><col class="col-md-1"><col class="col-md-1"><col class="col-md-1"><col class="col-md-1"><col class="col-md-1"></colgroup><thead><tr><th class="text-center">NO</th><th class="text-center">RAK</th><th class="text-center">LOT</th><th class="text-center">Code Cust</th><th class="text-center">Barang</th><th class="text-center">Tanggal</th><th class="text-center">Jumlah</th><th class="text-center">Satuan</th><th class="text-center">Berat Bersih</th><th class="text-center">Berat Kotor</th><th class="text-center">Width</th><th class="text-center">Action</th></tr></thead><tbody></tbody></table>');
				return false;
			}
		});



		$('#modal_barang_in').on('shown.bs.modal', function() {
			$('#scan_lot').focus();
		})

		$('#modal_barang_out').on('shown.bs.modal', function() {
			$('#scan_lot_out').focus();
		})
		//bahan
		$("#form_bahan_in").validate({
			rules: {
				get_bahan: {
					required: true
				},
			},
			submitHandler: function(form) {
				var cekTransaksi = $("#form_bahan_in").attr('transaksi');
				if (cekTransaksi == 'tambah') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'insert_data_bahan',
						data: $('#form_bahan_in').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								$('#form_bahan_in').clearForm();
								toastr.success('Data berhasil disimpan');
								table.ajax.reload(null, false);
								table2.ajax.reload(null, false);
							} else if (response == '0') {
								toastr.warning('Data gagal disimpan');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				}
				return false;
			}
		});
		//barang jadi
		$("#form_barang_in").validate({
			rules: {
				scan_lot: {
					required: true
				},
			},
			submitHandler: function(form) {
				var cekTransaksi = $("#form_barang_in").attr('transaksi');
				if (cekTransaksi == 'tambah') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'insert_data_jadi',
						data: $('#form_barang_in').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								$('#form_barang_in').clearForm();
								toastr.success('Data berhasil disimpan');
								table.ajax.reload(null, false);
								table2.ajax.reload(null, false);
							} else if (response == '0') {
								toastr.warning('Data gagal disimpan');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				}
				return false;
			}
		});

		$("#form_barang_out").validate({
			rules: {
				scan_lot_out: {
					required: true
				},
			},
			submitHandler: function(form) {
				var cekTransaksi = $("#form_barang_out").attr('transaksi');
				if (cekTransaksi == 'tambah') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'insert_data_out_jadi',
						data: $('#form_barang_out').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								$('#form_barang_out').clearForm();
								toastr.success('Data berhasil disimpan');
								table.ajax.reload(null, false);
								table2.ajax.reload(null, false);
							} else if (response == '0') {
								toastr.warning('Data gagal disimpan');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				}
				return false;
			}
		});
	});


	function replaceAll(str, token, newToken, ignoreCase) {
		var _token;
		var i = -1;

		if (typeof token === "string") {

			if (ignoreCase) {

				_token = token.toLowerCase();

				while ((
						i = str.toLowerCase().indexOf(
							_token, i >= 0 ? i + newToken.length : 0
						)) !== -1) {
					str = str.substring(0, i) +
						newToken +
						str.substring(i + token.length);
				}

			} else {
				return str.split(token).join(newToken);
			}

		}
		return str;
	};

	function formatMoney(amount, decimalCount = 4, decimal = ",", thousands = ".") {
		try {
			decimalCount = Math.abs(decimalCount);
			decimalCount = isNaN(decimalCount) ? 4 : decimalCount;

			const negativeSign = amount < 0 ? "-" : "";

			let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
			let j = (i.length > 3) ? i.length % 3 : 0;

			return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
		} catch (e) {
			console.log(e)
		}
	}

	function get_bahan() {
		var pageUri = $('#pageUri').val();
		$("#get_bahan").select2({
			placeholder: "Masukan Nama Bahan",
			width: "100%",
			ajax: {
				url: pageUri + 'get_bahan',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response

					};
				},
				cache: false
			}
		});
	};

	function get_barang() {
		var pageUri = $('#pageUri').val();
		$("#get_barang").select2({
			placeholder: "Masukan Nama Barang",
			width: "100%",
			ajax: {
				url: pageUri + 'get_barang_jadi',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};

				},
				cache: false
			}
		});
	};

	function get_cust() {
		var pageUri = $('#pageUri').val();
		$("#customer").select2({
			placeholder: "Masukan Nama Customer",
			width: "100%",
			ajax: {
				url: pageUri + 'get_customer',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};

				},
				cache: false
			}
		});
	};
	//bahan 
	function open_baku(obj) {
		var pageUri = $('#pageUri').val();
		var rak = $(obj).attr('id');
		$.ajax({
			type: 'POST',
			data: {
				id: rak
			},
			url: pageUri + 'get_detail_baku',
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#search_keyword_det_baku').val($('#search_keyword').val());
					$('#title_modal_flexible4').html($(obj).text());
					$('#id_rak_bahan').val(rak);
					$('#modal_rak_detail_bahan').modal('show');
					table3.ajax.reload(null, false);
				} else if (html == '0') {
					toastr.error('Rak Kosong');
				} else {
					toastr.warning(html);
				}
				unBlockUI();
			},
		});
	}
	//barang jadi
	function open_data(obj) {
		var pageUri = $('#pageUri').val();
		var rak = $(obj).attr('id');
		$.ajax({
			type: 'POST',
			data: {
				id: rak
			},
			url: pageUri + 'get_detail_jadi',
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#search_keyword_det').val($('#search_keyword').val());
					$('#title_modal_flexible2').html($(obj).text());
					$('#id_rak').val(rak);
					$('#modal_rak_detail').modal('show');
					table.ajax.reload(null, false);
				} else if (html == '0') {
					toastr.error('Rak Kosong');
				} else {
					toastr.warning(html);
				}
				unBlockUI();
			},
		});
	}

	function cek_data() {
		var pageUri = $('#pageUri').val();
		var key = $('#search_keyword').val();
		var tipe = $('#search_tipe').val();
		$.ajax({
			type: 'POST',
			data: {
				key: key,
				tipe: tipe
			},
			url: pageUri + 'cek_data_jadi',
			dataType: "json",
			success: function(html) {
				$('.barangjadi').css("background-color", "green");
				$('.barangjadi_2').css("background-color", "purple");
				$('.ingrey').css("background-color", "green");
				$('.bahan_baku').css("background-color", "grey");
				if (html != 0) {
					if (tipe == 3) {
						$.each(html.data, function(i, n) {
							$('.barangjadi#' + n['rak']).css("background-color", "red");
							$('.barangjadi_2#' + n['rak']).css("background-color", "red");
						});
					} else if (tipe == 2) {
						$.each(html.data, function(i, n) {
							$('.ingrey#' + n['rak']).css("background-color", "red");
						});
					} else {
						$.each(html.data, function(i, n) {
							$('.bahan_baku#' + n['rak']).css("background-color", "red");
						});
					}
				}
				return false;
			},
		});
	}

	function view_history(id) {
		$('#id_lot').val(id);
		$('#modal_rak_history').modal('show');
		table2.ajax.reload(null, false);
	}

	function cek_barcode() {
		var scan = $('#scan_lot').val();
		var rol = scan.substring(8, scan.length);
		var lot = scan.substring(0, scan.length - rol.length - 1);
		$('#lot').val(lot);
		$('#rol').val(rol);
	}
	//bahan baku
	function add_bahan() {
		var a = $('#abjad2').val();
		var urut = $('#urut2').val();
		var rak = a + ' ' + urut;
		var rak2 = a + urut;
		var namabarang = $('#get_bahan').text();
		var idbrg = $('#get_bahan').val();
		var tgl = $('#tgl_masuk').val();
		var isi = $('#isi').val();
		var isi2 = replaceAll(isi, '.', '');
		isi2 = isi2.replace(/[,]+/g, ".");
		var jmlh = $('#jmlh_bahan').val();
		var sat = $('#sat_bahan').val();
		var dus = replaceAll(jmlh, '.', '');
		dus = jmlh.replace(/[,]+/g, ".");
		dus = Math.ceil(parseFloat(dus / isi2));
		var satuan = $('#sat_bahan option:selected').text();
		var supp = $('#supp').val();
		var ref = $('#refbahan').val();
		if (namabarang == null || namabarang == 'undefined' || namabarang == '') {
			toastr.error('Lengkapi Datanya');
		} else {
			var baris = '<tr><td class="text-center numer3"></td><td class="text-center">' + rak + '</td><input class="rak" type="hidden" name="rak[]" value="' + rak2 + '" /><td>' + namabarang + '</td><input class="idbrg" type="hidden" name="idbrg[]" value="' + idbrg + '" /><td class="text-center">' + tgl + '</td><input class="tglprd" type="hidden" name="tglprd[]" value="' + tgl + '" /><td class="text-center">' + jmlh + ' ' + satuan + '</td><input class="qty" type="hidden" name="qty[]" value="' + jmlh + '" /><input class="qty" type="hidden" name="isi[]" value="' + isi + '" /><input class="satuan" type="hidden" name="satuan[]" value="' + sat + '" /><td class="text-center">' + dus + '</td><td class="text-center">' + supp + '</td><input class="cust" type="hidden" name="supp[]" value="' + supp + '" /><td class="text-center">' + ref + '</td><input class="refbahan" type="hidden" name="refbahan[]" value="' + ref + '" /><td class="text-center"><a class="btn btn-warning form-control" onclick="del_bahan(this)"><i class="icon-cross2"></i></a></td></tr>';
			$("table#table_list_bahan tbody").append(baris);
			nomoring3();
		}
	}

	function del_bahan(obj) {
		$(obj).closest('tr').remove();
		nomoring3();
	}
	//barang jadi
	function add_barang() {
		var a = $('#abjad').val();
		var urut = $('#urut').val();
		var tingkat = $('#tingkat').val();
		var rak = a + ' ' + urut + '.' + tingkat;
		var rak2 = a + urut + tingkat;
		var lot = $('#lot').val();
		var rol = $('#rol').val();
		var nolot = lot + '-' + rol + 'R';
		var namabarang = $('#get_barang').text();
		var idbrg = $('#get_barang').val();
		var tgl = $('#tgl').val();
		var jmlh = $('#jmlh').val();
		var sat = $('#sat').val();
		var satuan = $('#sat option:selected').text();
		var berat = $('#kg').val();
		var cust = $('#cust').val();
		var ingrey = $('#tipeingrey').val();
		if (lot == null || lot == 'undefined' || lot == '') {
			toastr.error('Lengkapi Datanya');
		} else {
			var baris = '<tr><td class="text-center numer"></td><td class="text-center">' + rak + '</td><input class="rak" type="hidden" name="rak[]" value="' + rak2 + '" /><td class="text-center">' + nolot + '</td><input class="lot" type="hidden" name="lot[]" value="' + lot + '" /><input class="rol" type="hidden" name="rol[]" value="' + rol + '" /><td>' + namabarang + '</td><input class="idbrg" type="hidden" name="idbrg[]" value="' + idbrg + '" /><td class="text-center">' + tgl + '</td><input class="tglprd" type="hidden" name="tglprd[]" value="' + tgl + '" /><td class="text-center">' + jmlh + '</td><input class="qty" type="hidden" name="qty[]" value="' + jmlh + '" /><td class="text-center">' + satuan + '</td><input class="satuan" type="hidden" name="satuan[]" value="' + sat + '" /><td class="text-center">' + berat + ' KGM</td><input class="berat" type="hidden" name="berat[]" value="' + berat + '" /><td class="text-center">' + cust + '</td><input class="cust" type="hidden" name="cust[]" value="' + cust + '" /><td class="text-center">' + ingrey + '</td><input class="tipeingrey" type="hidden" name="ingrey[]" value="' + ingrey + '" /><td class="text-center"><a class="btn btn-warning form-control" onclick="del_barang(this)"><i class="icon-cross2"></i></a></td></tr>';
			$("table#table_list_barang tbody").append(baris);
			nomoring();
		}
	}

	function add_barang_out() {
		var id = $('#id_out').val();
		var rak = $('#rak_out').val();
		var lot = $('#lot_out').val();
		var rol = $('#rol_out').val();
		var nolot = lot + '-' + rol + 'R';
		var idbrg = $('#id_barang_out').val();
		var namabarang = $('#barang_out').val();
		var material_code = $('#material_code').val();
		var tgl = $('#tgl_out').val();
		var jmlh = $('#qty_out').val();
		var limit_jum = $('#qty_out_limit').val();
		var limit_berat = $('#kg_out_limit').val();
		var sat = $('#sat_out').val();
		var satuan = $('#sat_out option:selected').text();
		var berat = $('#kg_out').val();
		var bruto = $('#bruto').val();
		var width = $('#width_brg').val();
		if (namabarang == null || namabarang == 'undefined' || namabarang == '') {
			toastr.error('Lengkapi Datanya');
		} else {
			if (limit_jum < jmlh || limit_berat < limit_berat) {
				toastr.error('Barang Melebihi Stok');
			} else {
				if ($('#customer').data('select2')) {
					$('#customer').select2('destroy');
					$('#customer').attr('readonly', true);
				}
				var baris = '<tr><td class="text-center numer2"></td><td class="text-center">' + rak + '</td><input type="hidden" name="id[]" value="' + id + '" /><input class="rak" type="hidden" name="rak[]" value="' + rak + '" /><td class="text-center">' + nolot + '</td><input class="lot" type="hidden" name="lot[]" value="' + lot + '" /><input class="rol" type="hidden" name="rol[]" value="' + rol + '" /><td class="text-center">' + material_code + '</td><input class="lot" type="hidden" name="material_code[]" value="' + material_code + '" /><td>' + namabarang + '</td><input type="hidden" name="idbrg[]" value="' + idbrg + '" /><td class="text-center">' + tgl + '</td><input class="tglprd" type="hidden" name="tglprd[]" value="' + tgl + '" /><td class="text-center">' + jmlh + '</td><input class="qty" type="hidden" name="qty[]" value="' + jmlh + '" /><td class="text-center">' + satuan + '</td><input class="satuan" type="hidden" name="satuan[]" value="' + sat + '" /><td class="text-center">' + berat + ' KGM</td><input class="berat" type="hidden" name="berat[]" value="' + berat + '" /><td class="text-center">' + bruto + ' KGM</td><input class="bruto" type="hidden" name="bruto[]" value="' + bruto + '" /><td class="text-center">' + width + '"</td><input class="width_brg" type="hidden" name="width[]" value="' + width + '" /><td class="text-center"><a class="btn btn-warning form-control" onclick="del_barang(this)"><i class="icon-cross2"></i></a></td></tr>';
				$("table#table_list_barang_out tbody").append(baris);
				nomoring2();
			}
		}
	}

	function del_barang(obj) {
		$(obj).closest('tr').remove();
		nomoring();
	}

	function nomoring() {
		var no_urut = parseInt(1);
		$('.numer').each(function() {
			$(this).html(no_urut);
			no_urut += parseInt(1);
		});
	}

	function nomoring2() {
		var no_urut = parseInt(1);
		$('.numer2').each(function() {
			$(this).html(no_urut);
			no_urut += parseInt(1);
		});
	}

	function nomoring3() {
		var no_urut = parseInt(1);
		$('.numer3').each(function() {
			$(this).html(no_urut);
			no_urut += parseInt(1);
		});
	}

	function cek_barcode_out() {
		var pageUri = $('#pageUri').val();
		var scan = $('#scan_lot_out').val();
		var rol = scan.substring(8, scan.length);
		var lot = scan.substring(0, scan.length - rol.length - 1);
		$('#lot_out').val(lot);
		$('#rol_out').val(rol);
		$.ajax({
			type: 'POST',
			data: {
				lot: lot,
				rol: rol
			},
			url: pageUri + 'get_barang_out_jadi',
			dataType: "json",
			success: function(html) {
				$('#id_out').val(null);
				$('#tgl_out').val(null);
				$('#barang_out').val(null);
				$('#rak_out').val(null);
				$('#qty_out').val(null);
				$('#kg_out').val(null);
				$('#sat_out').val(null);
				if (html != null) {
					$('#id_out').val(html['id_header']);
					$('#tgl_out').val(html['tgl']);
					$('#barang_out').val(html['nama']);
					$('#rak_out').val(html['rak']);
					$('#qty_out').val(html['jumlah']);
					$('#qty_out_limit').val(html['jumlah']);
					$('#kg_out_limit').val(html['berat']);
					$('#kg_out').val(html['berat']);
					$('#sat_out').val(html['satuan']);
				} else {
					toastr.error('Data Tidak Ditemukan');
				}
			},
		});
	}

	function konversi() {
		var qty2 = $('#qty_out').val();
		var mtr = 1.0936133;
		var yard = 0.9144;
		var satuan = $('#sat_out').val();
		if (qty2 == null || qty2 == 'undefined' || qty2 == '') {
			return false;
		} else {
			if (satuan == 1) {
				qty2 = replaceAll(qty2, '.', '');
				qty2 = qty2.replace(/[,]+/g, ".");
				qty2 = parseFloat(parseFloat(qty2) * yard).toFixed(4);
			}
			if (satuan == 2) {
				qty2 = replaceAll(qty2, '.', '');
				qty2 = qty2.replace(/[,]+/g, ".");
				qty2 = parseFloat(parseFloat(qty2) * mtr).toFixed(4);
			}
			$('#qty_out').val(formatMoney(qty2));
		}
	}
</script>