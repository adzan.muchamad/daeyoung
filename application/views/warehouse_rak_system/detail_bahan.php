<input type="hidden" id="id_rak_bahan">
<div class="panel panel-white">
    <div class="panel-heading">
        <div class="heading-elements">
            <input type="text" class="form-control" style="width:250px;" placeholder="Pencarian" id="search_keyword_det_bhn" name="search_keyword_det_bhn">
        </div>
    </div>
    <table id="datamain_datatable3" style="width:100%" class="table datatable-ajax table-striped table-xxs table-bordered text-size-small">
        <colgroup>
            <col>
            <col class="col-md-1">
            <col class="col-md-3">
            <col>
            <col>
            <col>
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
            <col class="col-md-1">
        </colgroup>
        <thead>
            <tr>
                <th>NO</th>
                <th>Kode</th>
                <th class="text-center">Nama</th>
                <th>Isi</th>
                <th>Qty</th>
                <th>Box</th>
                <th>Tgl Masuk</th>
                <th>Supplier</th>
                <th>Ref</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th class="text-right"><b>TOTAL</b></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>