<?php
$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form2';
$options['modal_id'] = 'modal_barang_in';
$options['modal_size'] = 'modal-xl';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Form Tambah';
$options['modal_footer'] = 'no';
$options['form_id'] = 'form_barang_in';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/barang_jadi_in', $data, TRUE);
echo $this->ui->load_component($options);


$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form2';
$options['modal_id'] = 'modal_barang_out';
$options['modal_size'] = 'modal-xl';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Barang Keluar';
$options['modal_footer'] = 'no';
$options['form_id'] = 'form_barang_out';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/barang_jadi_out', $data, TRUE);
echo $this->ui->load_component($options);

$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form2';
$options['modal_id'] = 'modal_ingrey_in';
$options['modal_size'] = 'modal-xl';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Form Tambah';
$options['modal_footer'] = 'no';
$options['form_id'] = 'form_ingrey_in';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/ingrey_in', $data, TRUE);
echo $this->ui->load_component($options);


$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form2';
$options['modal_id'] = 'modal_ingrey_out';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Ingrey Keluar';
$options['modal_footer'] = 'no';
$options['form_id'] = 'form_ingrey_out';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/ingrey_out', $data, TRUE);
echo $this->ui->load_component($options);

$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form2';
$options['modal_id'] = 'modal_bahan_in';
$options['modal_size'] = 'modal-xl';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Form <span id="title_modal_flexible"></span>';
$options['modal_footer'] = 'no';
$options['form_id'] = 'form_bahan_in';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/bahan_in', $data, TRUE);
echo $this->ui->load_component($options);


$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form2';
$options['modal_id'] = 'modal_bahan_out';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Bahan Baku Keluar';
$options['modal_footer'] = 'no';
$options['form_id'] = 'form_bahan_out';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/bahan_out', $data, TRUE);
echo $this->ui->load_component($options);


$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_default';
$options['modal_id'] = 'modal_rak_detail_bahan';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> RAK <span id="title_modal_flexible4">';
$options['modal_footer'] = 'yes';
$options['form_id'] = 'formulir_modal_detail_bahan';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/detail_bahan', $data, TRUE);
echo $this->ui->load_component($options);

$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_center';
$options['modal_style'] = 'style="background-color:rgba(0, 0, 0, 0.71)"';
$options['modal_id'] = 'modal_rak_history_bahan';
$options['modal_size'] = 'modal-xl';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> History <span id="title_modal_flexible5">';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal_history_bahan';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/riwayat_bahan', $data, TRUE);
echo $this->ui->load_component($options);

$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_default';
$options['modal_id'] = 'modal_rak_detail';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> RAK <span id="title_modal_flexible2">';
$options['modal_footer'] = 'yes';
$options['form_id'] = 'formulir_modal_detail';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/detail', $data, TRUE);
echo $this->ui->load_component($options);

$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_center';
$options['modal_style'] = 'style="background-color:rgba(0, 0, 0, 0.71)"';
$options['modal_id'] = 'modal_rak_history';
$options['modal_size'] = 'modal-xl';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> History <span id="title_modal_flexible3">';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal_history';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('warehouse_rak_system/riwayat', $data, TRUE);
echo $this->ui->load_component($options);
