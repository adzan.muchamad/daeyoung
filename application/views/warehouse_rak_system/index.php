<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<div class="panel panel-white" style="margin-bottom:0px">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <form id="formsearch" onSubmit="return false;">
                <div class="input-group">
                    <!-- <div class="input-group-btn">
                        <button id="button_form_dropdown_search" type="button" class="btn dropdown-toggle btn-icon btn-default btn-raised" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu dropdown-content" style="width:395px;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>kategori</label>
                                        <select class="form-control" id="searchkat" name="searchkat">
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>kategori</label>
                                        <select class="form-control" id="searchtipe" name="searchtipe">
                                            <option value="">Semua</option>
                                            <option value="1">Hasil Produksi</option>
                                            <option value="2">Dibeli</option>
                                            <option value="3">Servis/Jasa/Barang di Biayakan</option>
                                            <option value="4">Scrap</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div> -->
                    <input type="hidden" name="search_tipe" id="search_tipe" value="1">
                    <input type="text" class="form-control" style="width:250px;" placeholder="Pencarian" id="search_keyword" name="search_keyword">

                    <div class="input-group-btn">
                        <!-- <button id="buttonPencarian" type="button" class="btn btn-default btn-raised"><b><i class="icon-search4"></i></b></button> -->
                        <button type="button" class="btn btn-default btn-raised tombol_in"><b><i class="icon-plus-circle2"></i></b> Barang Masuk</button>
                        <button type="button" class="btn btn-default btn-raised tombol_out"><b><i class="icon-minus-circle2"></i></b> Barang Keluar</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div role="tabpanel">
    <ul class="nav nav-tabs nav-tabs-head nav-tabs-highlight" role="tablist" style="background-color: white;">
        <li class="nav-item bahanbaku active"><a class="nav-link" href="components.html#bahan_baku" aria-controls="header" role="tab" data-toggle="tab">Bahan Baku</a></li>
        <li class="nav-item ingrey_2"><a class="nav-link" href="components.html#ingrey" aria-controls="barang" role="tab" data-toggle="tab">Ingrey</a></li>
        <li class="nav-item barang_jadi"><a class="nav-link" href="components.html#barang_jadi" aria-controls="barangtarif" role="tab" data-toggle="tab">Barang Jadi</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="bahan_baku">
            <!-- <img src="./assets/images/road2_1.png" style="position: absolute;z-index: 2;left: 40%;-ms-transform: rotate(90deg);transform: rotate(90deg);bottom:-80px;width:15%"> -->
            <div class="col-md-12 tk1" style="margin-top:20px">
                <div class="row" style="margin-bottom:1px;">
                    <!--1-->
                    <div class="col-md-3"></div>
                    <div class="col-md-1 text-center bahan_baku" id="A1" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>A 1</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="B1" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>B 1</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="C1" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>C 1</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="D1" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>D 1</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="E1" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>E 1</b></div>
                </div>
                <div class="row" style="margin-bottom:1px;">
                    <!--1-->
                    <div class="col-md-3"></div>
                    <div class="col-md-1 text-center bahan_baku" id="A2" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>A 2</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="B2" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>B 2</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="C2" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>C 2</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="D2" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>D 2</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="E2" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>E 2</b></div>
                </div>
                <div class="row" style="margin-bottom:1px;">
                    <!--1-->
                    <div class="col-md-3"></div>
                    <div class="col-md-1 text-center bahan_baku" id="A3" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>A 3</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="B3" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>B 3</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="C3" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>C 3</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="D3" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>D 3</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="E3" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>E 3</b></div>
                </div>
                <div class="row" style="margin-bottom:1px;">
                    <!--1-->
                    <div class="col-md-3"></div>
                    <div class="col-md-1 text-center bahan_baku" id="A4" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>A 4</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="B4" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>B 4</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="C4" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>C 4</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="D4" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>D 4</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="E4" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>E 4</b></div>
                </div>
                <div class="row" style="margin-bottom:1px;">
                    <!--1-->
                    <div class="col-md-3"></div>
                    <div class="col-md-1 text-center bahan_baku" id="A5" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>A 5</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="B5" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>B 5</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="C5" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>C 5</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="D5" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>D 5</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="E5" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>E 5</b></div>
                </div>
                <div class="row" style="margin-bottom:1px;">
                    <!--1-->
                    <div class="col-md-3"></div>
                    <div class="col-md-1 text-center bahan_baku" id="A6" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>A 6</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="B6" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>B 6</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="C6" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>C 6</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="D6" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>D 6</b></div>
                    <div class="col-md-1 text-center bahan_baku" id="E6" style="background-color: grey;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:20px;border-radius: 10px;" onclick="open_baku(this)"><b>E 6</b></div>
                </div>

            </div>
        </div>
        <div class="tab-pane" id="ingrey">
            <!-- <img src="./assets/images/road.png" style="position: absolute;z-index: 2;left: 39%;height: 73%;padding-top:75px"> -->
            <div role="tabpanel">
                <ul class="nav nav-tabs nav-tabs-child" role="tablist" style="background-color: white;">
                    <li class="nav-item active"><a class="nav-link" href="components.html#lt1" aria-controls="header" role="tab" data-toggle="tab">Tingkat 1</a></li>
                    <li class="nav-item"><a class="nav-link" href="components.html#lt2" aria-controls="barang" role="tab" data-toggle="tab">Tingkat 2</a></li>
                    <li class="nav-item"><a class="nav-link" href="components.html#lt3" aria-controls="barangtarif" role="tab" data-toggle="tab">Tingkat 3</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="lt1">
                        <div class="col-md-12 lt1">
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I11" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 1.1</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I21" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 2.1</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I31" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 3.1</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I41" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 4.1</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I51" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 5.1</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I61" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 6.1</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey" id="F11" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 1.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="F21" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 2.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="F31" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 3.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="F41" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 4.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="F51" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 5.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="F61" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 6.1</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey" id="H11" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 1.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="H21" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 2.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="H31" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 3.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="H41" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 4.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="H51" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 5.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="H61" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 6.1</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E11" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 1.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E21" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 2.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E31" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 3.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E41" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 4.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E51" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 5.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E61" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 6.1</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey" id="G11" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 1.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="G21" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 2.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="G31" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 3.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="G41" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 4.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="G51" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 5.1</b></div>
                                <div class="col-md-1 text-center ingrey" id="G61" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 6.1</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D11" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 1.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D21" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 2.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D31" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 3.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D41" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 4.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D51" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 5.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D61" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 6.1</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;margin-right:6px"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C11" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 1.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C21" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 2.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C31" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 3.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C41" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 4.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C51" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 5.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C61" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 6.1</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;margin-right:6px"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B11" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 1.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B21" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 2.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B31" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 3.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B41" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 4.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B51" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 5.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B61" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 6.1</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:50px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A11" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 1.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A21" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 2.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A31" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 3.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A41" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 4.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A51" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 5.1</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A61" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 6.1</b></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="lt2">
                        <div class="col-md-12 lt2">
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I12" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 1.2</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I22" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 2.2</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I32" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 3.2</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I42" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 4.2</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I52" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 5.2</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I62" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 6.2</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey" id="F12" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 1.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="F22" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 2.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="F32" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 3.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="F42" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 4.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="F52" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 5.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="F62" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 6.2</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey" id="H12" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 1.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="H22" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 2.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="H32" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 3.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="H42" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 4.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="H52" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 5.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="H62" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 6.2</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E12" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 1.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E22" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 2.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E32" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 3.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E42" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 4.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E52" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 5.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E62" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 6.2</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey" id="G12" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 1.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="G22" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 2.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="G32" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 3.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="G42" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 4.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="G52" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 5.2</b></div>
                                <div class="col-md-1 text-center ingrey" id="G62" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 6.2</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D12" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 1.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D22" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 2.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D32" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 3.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D42" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 4.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D52" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 5.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D62" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 6.2</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;margin-right:6px"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C12" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 1.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C22" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 2.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C32" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 3.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C42" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 4.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C52" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 5.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C62" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 6.2</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;margin-right:6px"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B12" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 1.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B22" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 2.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B32" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 3.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B42" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 4.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B52" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 5.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B62" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 6.2</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:50px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A12" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 1.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A22" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 2.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A32" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 3.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A42" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 4.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A52" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 5.2</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A62" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 6.2</b></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="lt3">
                        <div class="col-md-12 lt3">
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I13" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 1.3</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I23" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 2.3</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I33" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 3.3</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I43" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 4.3</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I53" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 5.3</b></div>
                                <div class="col-md-1 text-center ingrey_purp" id="I63" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>I 6.3</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey" id="F13" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 1.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="F23" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 2.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="F33" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 3.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="F43" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 4.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="F53" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 5.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="F63" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>F 6.3</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey" id="H13" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 1.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="H23" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 2.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="H33" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 3.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="H43" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 4.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="H53" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 5.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="H63" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>H 6.3</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E13" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 1.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E23" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 2.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E33" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 3.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E43" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 4.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E53" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 5.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="E63" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>E 6.3</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1 text-center ingrey" id="G13" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 1.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="G23" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 2.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="G33" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 3.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="G43" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 4.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="G53" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 5.3</b></div>
                                <div class="col-md-1 text-center ingrey" id="G63" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>G 6.3</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D13" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 1.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D23" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 2.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D33" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 3.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D43" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 4.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D53" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 5.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="D63" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>D 6.3</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;margin-right:6px"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C13" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 1.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C23" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 2.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C33" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 3.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C43" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 4.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C53" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 5.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="C63" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>C 6.3</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;margin-right:6px"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B13" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 1.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B23" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 2.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B33" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 3.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B43" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 4.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B53" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 5.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="B63" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>B 6.3</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 7%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:50px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A13" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 1.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A23" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 2.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A33" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 3.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A43" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 4.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A53" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 5.3</b></div>
                                <div class="col-md-1 text-center ingrey_blue" id="A63" style="background-color: blue;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_ingrey(this)"><b>A 6.3</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="barang_jadi">
            <!-- <img src="./assets/images/road.png" style="position: absolute;z-index: 2;left: 42%;height: 73%;padding-top:75px"> -->
            <div role="tabpanel">
                <ul class="nav nav-tabs nav-tabs-child" role="tablist" style="background-color: white;">
                    <li class="nav-item active"><a class="nav-link" href="components.html#tk1" aria-controls="header" role="tab" data-toggle="tab">Tingkat 1</a></li>
                    <li class="nav-item"><a class="nav-link" href="components.html#tk2" aria-controls="barang" role="tab" data-toggle="tab">Tingkat 2</a></li>
                    <li class="nav-item"><a class="nav-link" href="components.html#tk3" aria-controls="barangtarif" role="tab" data-toggle="tab">Tingkat 3</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tk1">
                        <div class="col-md-12 tk1">
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="A11" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 1.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A21" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 2.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A31" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 3.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A41" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 4.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A51" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 5.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A61" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 6.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A71" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 7.1</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="B11" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 1.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B21" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 2.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B31" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 3.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B41" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 4.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B51" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 5.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B61" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 6.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B71" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 7.1</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E11" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 1.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E21" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 2.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E31" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 3.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E41" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 4.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E51" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 5.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E61" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 6.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E71" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 7.1</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="C11" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 1.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C21" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 2.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C31" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 3.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C41" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 4.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C51" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 5.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C61" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 6.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C71" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 7.1</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F11" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 1.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F21" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 2.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F31" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 3.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F41" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 4.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F51" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 5.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F61" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 6.1</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F71" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 7.1</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="D11" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 1.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D21" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 2.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D31" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 3.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D41" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 4.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D51" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 5.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D61" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 6.1</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D71" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 7.1</b></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tk2">
                        <div class="col-md-12 tk2">
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="A12" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 1.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A22" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 2.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A32" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 3.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A42" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 4.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A52" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 5.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A62" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 6.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A72" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 7.2</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="B12" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 1.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B22" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 2.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B32" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 3.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B42" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 4.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B52" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 5.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B62" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 6.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B72" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 7.2</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E12" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 1.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E22" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 2.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E32" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 3.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E42" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 4.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E52" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 5.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E62" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 6.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E72" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 7.2</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="C12" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 1.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C22" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 2.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C32" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 3.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C42" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 4.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C52" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 5.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C62" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 6.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C72" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 7.2</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F12" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 1.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F22" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 2.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F32" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 3.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F42" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 4.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F52" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 5.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F62" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 6.2</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F72" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 7.2</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="D12" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 1.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D22" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 2.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D32" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 3.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D42" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 4.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D52" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 5.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D62" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 6.2</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D72" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 7.2</b></div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tk3">
                        <div class="col-md-12 tk3">
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="A13" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 1.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A23" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 2.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A33" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 3.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A43" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 4.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A53" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 5.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A63" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 6.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="A73" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>A 7.3</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--1-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="B13" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 1.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B23" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 2.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B33" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 3.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B43" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 4.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B53" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 5.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B63" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 6.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="B73" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>B 7.3</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--3-->
                                <div class="col-md-12" style="padding:40px"></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E13" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 1.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E23" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 2.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E33" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 3.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E43" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 4.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E53" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 5.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E63" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 6.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="E73" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>E 7.3</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="C13" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 1.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C23" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 2.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C33" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 3.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C43" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 4.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C53" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 5.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C63" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 6.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="C73" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>C 7.3</b></div>
                            </div>
                            <div class="row" style="margin-bottom:1px;">
                                <!--4-->
                                <div class="col-md-1" style="padding:20px;width: 5%;"></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F13" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 1.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F23" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 2.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F33" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 3.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F43" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 4.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F53" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 5.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F63" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 6.3</b></div>
                                <div class="col-md-1 text-center barangjadi_2" id="F73" style="background-color: purple;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>F 7.3</b></div>
                                <div class="col-md-2" style="padding:20px;width: 20%;"></div>
                                <div class="col-md-1 text-center barangjadi" id="D13" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 1.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D23" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 2.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D33" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 3.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D43" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 4.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D53" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 5.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D63" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 6.3</b></div>
                                <div class="col-md-1 text-center barangjadi" id="D73" style="background-color: green;color:white;margin:auto;padding:20px;cursor: pointer;border:2px solid black;margin-right:1px;border-radius: 10px;width:5%" onclick="open_data(this)"><b>D 7.3</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- =LOWER(LEFT(C6,1))&MID(C6,3,1)&RIGHT(C6,1) -->

<?php echo $insert_view; ?>