<input type="hidden" id="pageUri" value="<?php echo base_url() . $this->uri->segment(1) . '/'; ?>" />
<div class="panel panel-white">
    <div class="panel-heading">
        <h6 class="panel-title"><?php echo $title_page_table ?></h6>
        <div class="heading-elements">
            <!-- <form id="formsearch" onSubmit="return false;">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button id="button_form_dropdown_search" type="button" class="btn dropdown-toggle btn-icon btn-default btn-raised" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>

                        <ul class="dropdown-menu dropdown-content" style="width:395px;">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>kategori</label>
                                        <select class="form-control" id="searchkat" name="searchkat">
                                        </select>
                                    </div>
                                    <div class="col-lg-6">
                                        <label>kategori</label>
                                        <select class="form-control" id="searchtipe" name="searchtipe">
                                            <option value="">Semua</option>
                                            <option value="1">Hasil Produksi</option>
                                            <option value="2">Dibeli</option>
                                            <option value="3">Servis/Jasa/Barang di Biayakan</option>
                                            <option value="4">Scrap</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                    <input type="text" class="form-control" style="width:250px;" placeholder="Pencarian" id="search_keyword" name="search_keyword">

                    <div class="input-group-btn">
                        <button id="buttonPencarian" type="button" class="btn btn-default btn-raised"><b><i class="icon-search4"></i></b></button>
                        <button type="button" class="btn btn-default btn-raised" onClick="input_data()"><b><i class="icon-plus-circle2"></i></b> Tambah Barang</button>
                    </div>
                </div>
            </form> -->
        </div>
    </div>
</div>
<div class="panel">
    <div class="row" style="font-size:16px;padding:10px;margin: 15px 10px;padding-top: 20px;">
        <div class="col-lg-8">
            <img src="<?= base_url(); ?>assets/images/cukai/shopping_cart.png" width="20" height="20" border="0">&nbsp;&nbsp;
            <a href="#" onclick="popDialog('https://apps1.insw.go.id/tracking/index.php')">1.Tracking PIB/PEB</a>
        </div>
    </div>
    <div class="row" style="font-size:16px;padding:10px;margin: 15px 10px;">
        <div class="col-lg-8">
            <img src="<?= base_url(); ?>assets/images/cukai/inventory.png" width="20" height="20" border="0">&nbsp;&nbsp;
            <a href="#" onclick="popDialogIjin('https://apps1.insw.go.id/tracking-ijin/index.php')">2.Tracking Perijinan</a>
        </div>
    </div>
    <div class="row" style="font-size:16px;padding:10px;margin: 15px 10px;">
        <div class="col-lg-8">
            <img src="<?= base_url(); ?>assets/images/cukai/folder.gif" width="20" height="20" border="0">&nbsp;&nbsp;
            <a href="#" onclick="popDialogAtiga('https://apps1.insw.go.id/tracking-atiga/index.php')">3.Tracking e-COO</a>
        </div>
    </div>
    <div class="row" style="font-size:16px;padding:10px;margin: 15px 10px;">
        <div class="col-lg-8">
            <img src="<?= base_url(); ?>assets/images/cukai/shortcut.png" width="20" height="20" border="0">&nbsp;&nbsp;
            <a href="#" onclick="popDialogRekom('https://apps1.insw.go.id/tracking-rekom/index.php')">4.Tracking Rekomendasi</a>
        </div>
    </div>
    <div class="row" style="font-size:16px;padding:10px;margin: 15px 10px;">
        <div class="col-lg-8">
            <img src="<?= base_url(); ?>assets/images/cukai/report.png" width="20" height="20" border="0">&nbsp;&nbsp;
            <a href="#" onclick="popDialogNib('https://apps1.insw.go.id/tracking-nib/index.php')">5.Tracking NIB</a>
        </div>
    </div>
</div>