<input type="hidden" id="idbarang" name="id" />
<div class="row">
    <div class="col-lg-3">
        <label>Kode Barang</label>
        <input class="form-control" type="text" id="kode_brg" readonly />
    </div>
    <div class="col-lg-3">
        <label>Lokasi Barang</label>
        <input class="form-control" type="text" id="lokasi_brg" readonly />
    </div>
    <div class="col-lg-6">
        <label>Nama Barang</label>
        <input class="form-control" type="text" id="nama_brg" readonly />
    </div>
</div>
<br>
<hr>
<div class="row">
    <div class="col-lg-4 text-center">
        <label>LOT / Keterangan</label>
        <input type="text" class="form-control" id="searchLot" name="searchLot" />
    </div>
    <div class="col-lg-4 text-center">
        <label>Tanggal Awal</label>
        <input type="text" class="form-control date_picker" id="searchDateFirst" name="searchDateFirst" value="<?php echo date('Y-m-d'); ?>" />
    </div>
    <div class="col-lg-4 text-center">
        <label>Tanggal Akhir</label>
        <input type="text" class="form-control date_picker" id="searchDateFinish" name="searchDateFinish" value="<?php echo date('Y-m-t') ?>" />
    </div>
</div>
<table id="datamain_datatable2" style="width:100%" class="table datatable-ajax table-striped table-xxs table-bordered text-size-small">
    <colgroup>
        <col>
        <col class="col-md-1">
        <col class="col-md-1">
        <col class="col-md-1">
        <col class="col-md-1">
        <col class="col-md-1">
        <col class="col-md-1">
        <col class="col-md-1">
        <col class="col-md-1">
        <col>
        <col class="col-md-1">
    </colgroup>
    <thead>
        <tr>
            <th>no</th>
            <th>LOT</th>
            <th>jumlah (KGM)</th>
            <th>jumlah (YRD)</th>
            <th>jumlah (MTR)</th>
            <th>sisa (KGM)</th>
            <th>sisa (YRD)</th>
            <th>sisa (MTR)</th>
            <th>waktu</th>
            <th>keterangan</th>
            <th>status</th>
        </tr>
    </thead>
</table>