<script type="text/javascript">
	$(function() {
		var pageUri = $('#pageUri').val();

		//SET DATATABLES
		$.extend($.fn.dataTable.defaults, {
			//autoWidth: true,
			"sLength": "form-control"
		});
		var height_windows = $(window).height();
		var height_navbar = $('#navbar-main').height();
		var height_page = $('#page-header').height();
		var height_table = height_windows - height_navbar - height_page - 370;
		var tabel_kolom = ($(".datatable-ajax").find('thead tr')[0].cells.length - 1);

		table = $('#datamain_datatable').DataTable({
			dom: 'J<"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
			"pageLength": 50,
			aaSorting: [
				[1, 'desc']
			],
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'get_data_table',
				"type": "POST",
				"data": function(data) {
					data.search_keyword = $('#search_keyword').val();
					data.searchkat = $('#searchkat').val();
					data.searchtipe = $('#searchtipe').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},

			columnDefs: [{
				orderable: false,
				width: '150px',
				targets: [0, tabel_kolom]
			}, {
				className: "text-center",
				"targets": [tabel_kolom, 0]
			}, ],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
			scrollX: '100%',
			scrollY: height_table,
			scrollCollapse: false,
			fixedColumns: {
				leftColumns: 0,
				rightColumns: 1
			},
		});

		table2 = $('#datamain_datatable2').DataTable({
			dom: '<"datatable-header"B><"datatable-scroll datatable-scroll-wrap"t><"datatable-footer"pli>',
			"ordering": false,
			aaSorting: [
				[1, 'desc']
			],
			"searching": false,
			"processing": true,
			"serverSide": true,
			"ajax": {
				"url": pageUri + 'preview_data',
				"type": "POST",
				"data": function(data) {
					data.searchKeyword = $('#searchKeyword').val();
				},
				beforeSend: function() {
					blockElement('datamain_datatable_wrapper');
				},
			},
			"drawCallback": function(settings) {
				unBlockElement('datamain_datatable_wrapper');
			},
			columnDefs: [{
				orderable: false,
				className: "text-center",
				width: '150px',
				targets: [0]
			}, {
				className: "text-center",
				targets: [4]
			}],
			language: {
				lengthMenu: '<span>Show:</span> _MENU_',
				paginate: {
					'first': 'First',
					'last': 'Last',
					'next': '&rarr;',
					'previous': '&larr;'
				}
			},
			"lengthMenu": [
				[25, 50, 100, 250, -1],
				[25, 50, 100, 250, "All"]
			]
		});

		$('#buttonPencarian').click(function() {
			table.ajax.reload(null, false);
			$('#button_form_dropdown_search').next().toggle();
		});
		$('#search_keyword').keyup(function() {
			table.ajax.reload(null, false);
		});
		$('#searchKeyword').keyup(function() {
			table2.ajax.reload(null, false);
		});
		$('.dataTables_length select').select2({
			minimumResultsForSearch: Infinity,
			width: 'auto',
		});
		//END SET DATATABLES
		$("#formulir_modal").validate({
			rules: {
				barang: {
					required: true
				},
			},
			submitHandler: function(form) {

				var cekTransaksi = $("#formulir_modal").attr('transaksi');
				if (cekTransaksi == 'tambah') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'insert_data',
						data: $('#formulir_modal').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								$('#formulir_modal').clearForm();
								toastr.success('Data berhasil disimpan');
								table.ajax.reload(null, false);
								$('.list_barang').html('').append('<div class="row"><div class="col-lg-1 text-center"><label>No</label><input class="form-control no_urut text-center" type="text" readonly /></div><div class="col-lg-7 text-center"><label>Barang</label><select class="form-control barang" name="barang[]" onchange="get_satuan(this);"></select></div><div class="col-lg-2 text-center"><label>Qty</label><input class="form-control qty" type="text" name="qty[]" /></div><div class="col-lg-1 text-center"><label>Satuan</label><input class="form-control satuan text-center" type="text" readonly /></div><div class="col-lg-1"><label>Action</label><a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a></div></div>');
								pemberian_no();
							} else if (response == '0') {
								toastr.warning('Data gagal disimpan');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				} else if (cekTransaksi == 'edit') {
					$.ajax({
						type: 'POST',
						url: pageUri + 'update_data',
						data: $('#formulir_modal').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								toastr.success('Data berhasil diupdate');
								table.ajax.reload(null, false);
								$('#modal_so').modal('hide');
							} else if (response == '0') {
								toastr.warning('Data gagal diupdate');
							} else {
								toastr.error(response);
							}
							unBlockUI();
						}
					});
				}
				return false;
			}
		});

		$("#formulir_modal_proses").validate({
			submitHandler: function(form) {
				swal({
					title: "Anda Yakin?",
					text: "Anda akan proses stok opname.",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes!",
					closeOnConfirm: false
				}, function() {
					$.ajax({
						type: 'POST',
						url: pageUri + 'proses_data',
						data: $('#formulir_modal_proses').serialize(),
						beforeSend: function() {
							blockUI();
						},
						success: function(response) {
							if (response == '1') {
								toastr.success('Data berhasil diproses');
								$('#modal_so_proses').modal('hide');
								table.ajax.reload(null, false);
							} else if (response == '0') {
								toastr.warning('Data gagal diproses');
							} else {
								toastr.error(response);
							}
							swal.close();
							unBlockUI();
						}
					});
				})
			}
		});
	});

	function replaceAll(str, token, newToken, ignoreCase) {
		var _token;
		var i = -1;

		if (typeof token === "string") {

			if (ignoreCase) {

				_token = token.toLowerCase();

				while ((
						i = str.toLowerCase().indexOf(
							_token, i >= 0 ? i + newToken.length : 0
						)) !== -1) {
					str = str.substring(0, i) +
						newToken +
						str.substring(i + token.length);
				}

			} else {
				return str.split(token).join(newToken);
			}

		}
		return str;
	};

	function formatMoney(amount, decimalCount = 2, decimal = ",", thousands = ".") {
		try {
			decimalCount = Math.abs(decimalCount);
			decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

			const negativeSign = amount < 0 ? "-" : "";

			let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
			let j = (i.length > 3) ? i.length % 3 : 0;

			return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
		} catch (e) {
			console.log(e)
		}
	}


	function pemberian_no() {
		var no_urut = parseInt(1);
		var tObj = document.getElementsByClassName('no_urut');
		for (var i = 0; i < tObj.length; i++) {
			tObj[i].value = no_urut;
			no_urut += parseInt(1);
		}
	};

	function pemberian_no2() {
		var no_urut = parseInt(1);
		var tObj = document.getElementsByClassName('no_urut2');
		for (var i = 0; i < tObj.length; i++) {
			tObj[i].value = no_urut;
			no_urut += parseInt(1);
		}
	};

	function get_barang() {
		var pageUri = $('#pageUri').val();
		$(".barang").select2({
			placeholder: "Masukan Nama Barang",
			width: "100%",
			ajax: {
				url: pageUri + 'get_barang',
				type: "post",
				dataType: 'json',
				data: function(params) {
					return {
						searchTerm: params.term
					};
				},
				processResults: function(response) {
					return {
						results: response
					};

				},
				cache: false
			}
		});
	};

	function get_satuan(obj) {
		var pageUri = $('#pageUri').val();
		var id = $(obj).val();
		$.ajax({
			type: 'POST',
			url: pageUri + 'get_satuanbarang/',
			data: {
				id: id
			},
			success: function(response) {
				$(obj).closest('.row').find('.satuan').val(response);
			}
		});

	}

	function add_barang(obj) {
		$('.list_barang').append('<div class="row"><div class="col-lg-1 text-center"><input class="form-control no_urut text-center" type="text" readonly /></div><div class="col-lg-7 text-center"><select class="form-control barang" name="barang[]" onchange="get_satuan(this);"></select></div><div class="col-lg-2 text-center"><input class="form-control qty" type="text" name="qty[]" /></div><div class="col-lg-1 text-center"><input class="form-control satuan text-center" type="text" readonly /></div><div class="col-lg-1 text-center"><a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a></div></div>');
		get_barang();
		pemberian_no();
		$('.qty').autoNumeric('init', {
			mDec: '6'
		});
	}

	function del_barang(obj) {
		$(obj).closest('.row').remove();
		pemberian_no();
	}

	function input_data() {
		$('#formulir_modal').clearForm();
		$("#formulir_modal").attr('transaksi', 'tambah');
		$("#tabelDetailInvoice tbody").html('');
		$("#title_modal_flexible").html('Tambah');
		$('#formSearch').dialog('close');
		$('#modal_so').modal('show');
		$('#tanggal').val(new Date().toISOString().slice(0, 10));
		$('.list_barang').html('').append('<div class="row"><div class="col-lg-1 text-center"><label>No</label><input class="form-control no_urut text-center" type="text" readonly /></div><div class="col-lg-7 text-center"><label>Barang</label><select class="form-control barang" name="barang[]" onchange="get_satuan(this);"></select></div><div class="col-lg-2 text-center"><label>Qty</label><input class="form-control qty" type="text" name="qty[]" /></div><div class="col-lg-1 text-center"><label>Satuan</label><input class="form-control satuan text-center" type="text" readonly /></div><div class="col-lg-1"><label>Action</label><a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a></div></div>');
		get_barang();
		pemberian_no();
		$('.qty').autoNumeric('init', {
			mDec: '6'
		});
		return false;
	}

	function delete_data(id) {
		var pageUri = $('#pageUri').val();
		swal({
			title: "Anda Yakin?",
			text: "Anda akan hapus data ini.",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes!",
			closeOnConfirm: false
		}, function() {
			$.ajax({
				type: 'GET',
				url: pageUri + 'delete_data/' + id,
				beforeSend: function() {
					blockUI();
				},
				success: function(response) {
					if (response == '1') {
						table.ajax.reload(null, false);
						toastr.success('Data berhasil dihapus');
					} else if (response == '0') {
						toastr.error('Data gagal dihapus');
					} else {
						toastr.warning(response);
					}
					swal.close();
					unBlockUI();
				}
			});
		});
	};

	function update_data(id) {
		var pageUri = $('#pageUri').val();
		$("#formulir_modal").attr('transaksi', 'edit');
		$('#id').val(id);
		var supp = 0;
		$.ajax({
			type: 'POST',
			data: $('#formulir_modal').serialize(),
			url: pageUri + 'select_data',
			dataType: "json",
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				if (html != '') {
					$('#lokasi').val(html.data[0].loc_code).change();
					$('#tanggal').val(html.data[0].tran_date);
					$('#ref').val(html.data[0].reference);
					$('#ket').val(html.data[0].memo);
					$('.list_barang').html('').append(html.det);
					$('.qty').autoNumeric('init', {
						mDec: '6'
					});
					pemberian_no();
					get_barang();
					var tObj = document.getElementsByClassName('barang');
					for (var i = 0; i < tObj.length; i++) {
						$('#get_barang_' + i).empty().append($("<option/>").val(html.barang[i].id).text(html.barang[i].id + ' - ' + html.barang[i].namabarang)).trigger('change');
					}
					$('#modal_so').modal('show');
					unBlockUI();
				} else {
					toastr.warning('Data tidak ditemukan');
					unBlockUI();
				}
			}
		});
	};

	function view_data(id) {
		var pageUri = $('#pageUri').val();
		$('#id_view').val(id);
		$.ajax({
			type: 'GET',
			url: pageUri + 'view_data/' + id,
			beforeSend: function() {
				blockUI();
			},
			success: function(html) {
				$('.form_det').html('').append(html);
				pemberian_no2();
				$('#modal_so_view').modal('show');
				unBlockUI();
			},
		});

	}

	function proses_data() {
		var pageUri = $('#pageUri').val();
		$('#modal_so_proses').modal('show');
		table2.ajax.reload(null, false);
	}
</script>