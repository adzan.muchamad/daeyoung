<?php
$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_so';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Form <span id="title_modal_flexible"></span>';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('inventory_so/form_grid', $data, TRUE);
echo $this->ui->load_component($options);


$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_default';
$options['modal_id'] = 'modal_so_view';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> View';
$options['modal_footer'] = 'yes';
$options['form_id'] = 'formulir_modal_view';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('inventory_so/form_grid_view', $data, TRUE);
echo $this->ui->load_component($options);

$data = array();
$data['prefix'] = 'insert';
$options = array();
$options['component'] = 'component/modal/modal_form';
$options['modal_id'] = 'modal_so_proses';
$options['modal_size'] = 'modal-full';
//$options['modal_icon'] = $this->theme->icon('inventory_barang');
$options['modal_title'] = '<i class="material-icons">&#xE865;</i> Preview <span id="title_modal_flexible"></span>';
$options['modal_footer'] = 'no';
$options['form_id'] = 'formulir_modal_proses';
$options['form_action'] = '';
$options['main_content'] = $this->load->view('inventory_so/form_grid_proses', $data, TRUE);
echo $this->ui->load_component($options);
