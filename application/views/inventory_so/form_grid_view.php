<input type="hidden" id="id_view" name="id" />
<div class="form_det">
    <div class="row display-flex-center">
        <div class="col-lg-1">
            Lokasi
        </div>
        <div class="col-lg-2">
            <input class="form-control" type="text" value="" disabled />
        </div>
        <div class="col-lg-1">
            Tanggal
        </div>
        <div class="col-lg-2">
            <input class="form-control" type="text" value="" disabled />
        </div>
        <div class="col-lg-1">
            Memo
        </div>
        <div class="col-lg-5">
            <input class="form-control" type="text" value="" disabled />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-1 text-center">
            <label><b>No</b></label>
            <input class="form-control" type="text" disabled />
        </div>
        <div class="col-lg-2 text-center">
            <label><b>ID</b></label>
            <input class="form-control" type="text" disabled />
        </div>
        <div class="col-lg-3 text-center">
            <label><b>Nama</b></label>
            <input class="form-control" type="text" disabled />
        </div>
        <div class="col-lg-2 text-center">
            <label><b>Qty</b></label>
            <input class="form-control" type="text" disabled />
        </div>
        <div class="col-lg-1 text-center">
            <label><b>Satuan</b></label>
            <input class="form-control" type="text" disabled />
        </div>
        <div class="col-lg-2 text-center">
            <label><b>Stok IT Inventory</b></label>
            <input class="form-control" type="text" disabled />
        </div>
        <div class="col-lg-1 text-center">
            <label><b>Status</b></label>
            <a class="text-success text-center form-control " style="border: 0px"><i class="icon-checkmark"></i></a>
        </div>
    </div>
</div>