<input type="hidden" id="id" name="id" />
<input type="hidden" id="ref" name="ref" />
<div class="row display-flex-center">
    <div class="col-lg-1">
        <label>Lokasi</label>
    </div>
    <div class="col-lg-2">
        <select class="form-control" id="lokasi" name="lokasi">
            <?php
            $q = $this->inventory_so->get_lokasibarang();
            if ($q->num_rows() > 0) {
                foreach ($q->result_array() as $r) {
                    echo '<option value="' . $r['kode'] . '">' . $r['loc_nama'] . '</option>';
                }
            }
            ?>
        </select>
    </div>
    <div class="col-lg-1">
        Tanggal
    </div>
    <div class="col-lg-2">
        <input class="form-control date_picker" id="tanggal" name="tanggal" />
    </div>
    <div class="col-lg-1">
        <label>Memo</label>
    </div>
    <div class="col-lg-5">
        <input class="form-control" type="text" id="ket" name="ket" />
    </div>
</div>
<hr>
<center><b>STOK OPNAME BARANG</b></center>
<hr>
<div class="list_barang">
    <div class="row">
        <div class="col-lg-1 text-center">
            <label>No</label>
            <input class="form-control no_urut text-center" type="text" readonly />
        </div>
        <div class="col-lg-7 text-center">
            <label>Barang</label>
            <select class="form-control barang" name="barang[]" onchange="get_satuan(this);">
            </select>
        </div>
        <div class="col-lg-2 text-center">
            <label>Qty</label>
            <input class="form-control qty" type="text" name="qty[]" />
        </div>
        <div class="col-lg-1 text-center">
            <label>Satuan</label>
            <input class="form-control satuan text-center" type="text" readonly />
        </div>
        <div class="col-lg-1">
            <label>Action</label>
            <a class="btn btn-warning form-control" onclick="del_barang(this)">Hapus</a>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-2 text-center" style="float:right">
        <a class="btn btn-primary form-control" onclick="add_barang(this)"><i class="icon-plus3"></i>&nbsp;Tambah</a>
    </div>
</div>
<br>
<br>